import 'package:MyBFD/models/OnboardingQuestionModel.dart';
import 'package:MyBFD/models/PrefModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:MyBFD/services/PrefService.dart';

import 'extensions/HexColor.dart';

//Set at runtime.
PackageInfo packageInfo;
SharedPreferences prefs;
PrefModel serverPrefs;
double screenWidth;
double screenHeight;
double browserPadding;
String name, fName;

class WebVersionInfo {
  static const String name = '1.0.0';
  static const String build = '1';
  static const String version = '1';
}

double scrHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double scrWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

Future<PrefModel> setPrefs() async {
  serverPrefs = await locator<PrefService>().getPrefs();
  return serverPrefs;
}

//PATHS
const String ASSET_ICON_TRANSPARENT = 'assets/images/icon_transparent.png';
const String ASSET_ICON_BADGE = 'assets/images/icon_badge.png';
const String ASSET_ICON_CHECK = 'assets/images/icon_check.png';
const String ASSET_FOOTER_WAVE_SMALL = 'assets/images/footer_wave_small.png';
const String ASSET_GOALS_ORANGE = 'assets/images/icon_goals_orange.png';
const String ASSET_GOALS_WHITE = 'assets/images/icon_goals_white.png';
const String ASSET_COURSES_ORANGE = 'assets/images/icon_courses_orange.png';
const String ASSET_COURSES_WHITE = 'assets/images/icon_courses_white.png';
const String ASSET_REWARDS_WHITE = 'assets/images/icon_rewards_white.png';
const String ASSET_ACCOUNTS_WHITE = 'assets/images/icon_account_white.png';
const String ASSET_BACK_ARROW = 'assets/images/back_arrow.png';
const String ASSET_BACK_DARK = 'assets/images/back_arrow_dark.png';
const String ASSET_FORWARD_DARK = 'assets/images/forward_arrow_dark.png';
const String ASSET_TOOLS_ORANGE = 'assets/images/icon_calculator.png';
const String ASSET_HELP_ORANGE = 'assets/images/icon_message.png';
const String ASSET_ICON_FULL = 'assets/images/full_logo.png';
const String ASSET_REWARD_LAYOUT = 'assets/images/reward_layout.png';

//COLORS
final HexColor colorDark = HexColor('#222222');
final HexColor colorOrange = HexColor('#ff6500');

//KEYS
const String CLOUD_MESSAGING_SERVER_KEY =
    'AAAAG2NlcEo:APA91bEO8cVD-A2cxZrR159FUmDVy3WthBPyu0VHz43bYX7pyc_4CwNHIFXWJZWo-oMYYqwms5Iu4EobMCFPWgHTuQFrVpshLpqrZAATrC2Ynms5DJ5Yr8F49UQNEnGc9_v6OxRobQdE';

//URLS
const String DUMMY_PROFILE_PHOTO_URL =
    'https://firebasestorage.googleapis.com/v0/b/hidden-gems-e481d.appspot.com/o/Images%2FUsers%2FDummy%2FProfile.jpg?alt=media&token=99cd4cbd-7df9-4005-adef-b27b3996a6cc';

//MESSAGES
const String FORGOT_PASSWORD_MESSAGE =
    'If you can\'t remember your password put your details in below and press \'Reset Password\' to send you an email to change your password.';
const String EMAIL_SENT_MESSAGE =
    'Please check your email inbox to find instructions on how to change your pasword for this app.';
const String NO_EMAIL_MESSAGE =
    'Please note, your password reset email may take up to 3 hours to arrive so please be patient. You should so check your spam nbox in case the email as been sent there.\nIf you still have not received the email after 3 hours, please click \'Resend Email\', or contact us for support using the \'Need Help?\' link below.';
const String HELP_SENT_MESSAGE =
    'Your message has been sent succesfully and a member of our support team will respond as soon as possible';
const String ACCOUNT_UPDATE_SUCCESS_MESSAGE =
    'Thank you for updating your account. Your details will be verified by our support team and you should see the changes appear in your account within a few days.';
const String PASSWORD_UPDATE_SUCCESS_MESSAGE =
    'Thank you for updating your account. Your password has been changed and you will now need the new password to log into the app. Please make a note of your password as it will not be kept on our records.';
const String EMAIL_UPDATE_SUCCESS_MESSAGE =
    'Thank you for updating your account. Your email has been changed and you will now need the new email to log into the app. Please make a note of your email as it will not be kept on our records.';
const String NO_REWARDS_TEXT =
    "You currently do not have any rewards. To gain achievements and rewards, complete lessons in your assigned courses and regularly log into the app!";
const String CLAIM_REWARDS_TEXT =
    "All voucher and coupon codes are limited to one single use per user and expire within 6 months of being offered. Please read T&C for more information.";

const String TERMS_AND_CONDITIONS_P1 =
    'These Terms and Conditions constitue a legally binding a agreement made between you, whether personally or on behalf of an entity ("you") and [your business name] ("we,""us" or "our"), concerning your access to and use of our mobile application (the "Application"). You agree that by accessing the Application, you have read, understood, and agree to be bound by all of these Terms and Conditions Use. IF YOU DO NOT AGREE WITH ALL OF THESE TERMS AND CONDITIONS, THEN YOU ARE EXPRESSLY PROHIBITED FROM USING THE APPLICATION AND YOU MUST DISCONTINUE USE IMMEDIATELY.';
const String TERMS_AND_CONDITIONS_P2 =
    'Supplemental terms and conditions or documents that may be posted on the Application from time to time are hereby expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or modifications to these Terms and Conditions at any time and for any reason. We will alert you about any changes by updating the "Last Updated" data of these Terms and Conditions and you waive any right to receive specific notice of each such change. It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Application after the date such revised Terms are posted.';
const String TERMS_AND_CONDITIONS_P3 =
    'The information provided on the Application is not intended for distribution to or use by any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation or which would subject us to any registration requirement within such jurisdiction or country. Accordingly, those persons who choose to access the Application from other locations do so on their own initiative and are solely responsible for compliance with local laws, if and to the extent local laws are applicable.';
const String TERMS_AND_CONDITIONS_P4 =
    'These Terms and Conditions were generated by Termly\'s Terms and Conditions Generator.';
const String TERMS_AND_CONDITIONS_P5 = '[Choose one of the options below]';
const String TERMS_AND_CONDITIONS_P6 =
    'Option 1: The Application is intended for users who are at least 18 years old. Persons under the age of 13 are not permitted to register for the Application.';
const String TERMS_AND_CONDITIONS_P7 =
    'Option 2: The application is intended for users who are at least 13 years of age. All users who are minors in the jurisdiction in which they reside (generally under the age of 18) must have the permission of, and be directly supervised by, their parent or guardian to use the Application. If you are a minor, you must have your parent or guardian read and agree to these Terms of Use prior to you using the Application.';
const String TERMS_AND_CONDITIONS_P8 = 'INTELLECTUAL PROPERTY RIGHTS';
const String TERMS_AND_CONDITIONS_P9 =
    'Unless otherwise indicated, the Application is our proprietary property and all source code, databases, functionality, software, website designs, audio, video, text, photographs, and graphics on the Application (collectively, the "Content") and the trademarks, service marks, and logos contained therein (the "Marks") are owned or controlled by us or licensed to use, and are protected by copyright and trademark laws and various other intellectual property rights and unfair competition laws of the United States, foreign jurisdictions, and international conventions. The Content and the Marks are provided on the Application "AS IS" for your information and personal use only. Except as expressly provided in these Terms of Use, no part of the Application and no Content or Marks may be copied, reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded.';

const String noEmailText =
    "Please note, your password reset email may take up to 3 hours to arrive so pleasee patient. You should so check your spam nbox in case the email as been sent there.\nIf you still have not received the email after 3 hours, please click \"Resend Email\", or contact us for support using the \"Need Help?\" link below.";

const String helpSentText =
    "Your message has been sent succesfully and a member of our support team will respond as soon as possible";

const List<String> ASSETS_LIST = [
  'Select an Asset',
  'Cash in Hand',
  'Current Account Balance',
  'Saving Account Balance',
  'Individual Savings Account',
  'Owned Property',
  'Personal Pension',
  'Employer Pension',
  'Business',
  'Owned Vehicle',
  'Furniture and appliances',
  'Computers',
  'Jewellery',
  'Stocks and Shares',
  'Other Assets'
];

const List<String> LIABILITIES_LIST = [
  'Select a Liability',
  'Loan',
  'Overdraft',
  'Credit Card',
  'Mortgage',
  'Council Tax',
  'Unpaid Income Tax',
  'Student Loan',
  'Other Liabilities'
];

const List<String> BUDGET_FREQUENCY_LIST = [
  // 'frequency',
  'per week',
  'per month',
  'per year',
];

const String BEST_BUY =
    "Best buy deals are offers to purchase usually a larger quantity for what looks like a good deal. This is not always the case and being able to check this before buying can save money.";

const double LESSON_PASS_RATE_PERCENTAGE = 0.8;

final List<OnboardingQuestionModel> ONBOARDING_QUESTIONS = [
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Do you feel in control of your personal finances?',
    answers: ['Yes', 'No'],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Do you have financial goals that you are working towards?',
    answers: ['Yes', 'No'],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Do you have an opinion about your money habits?',
    answers: [
      'Yes I have good money habits',
      'Yes I have bad money habits',
      'No'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Have you ever spent time learning about money?',
    answers: [
      'Yes my parent/guardian taught me',
      'Yes I learned at school',
      'Yes I learned from those around me',
      'No'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Do you currently use a personal budget to manage your money?',
    answers: ['Yes', 'No'],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How much do you currently earn per month?',
    answers: [
      '£0',
      'Up to £1000',
      'Between £1001 and £2000',
      'Between £2001 and £3000',
      'Between £3001 and £4000',
      'Above £5000'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How much would you like to earn per month?',
    answers: [
      'Between £0 and £1,000',
      'Between £1000 and £2000',
      'Between £2001 and £3000',
      'Between £3001 and £4000',
      'Above £5000'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How much debt do you have?',
    answers: [
      '£0',
      'Between £1 and £1000',
      'Between £1001 and £5000',
      'Between £5001 and £10000',
      'Above £10001'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How do you feel about your debt levels?',
    answers: [
      'Not very good/worried',
      'Ok and not worried',
      'Good and in control',
      'No debt'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How much savings do you have?',
    answers: [
      'Between £0 and £1,000',
      'Between £1000 and £2000',
      'Between £2001 and £3000',
      'Between £3001 and £4000',
      'Between £5000 and £10000',
      'Above £10001'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How do you feel about your level of savings?',
    answers: [
      'Not very good/worried',
      'Ok and not worried',
      'Good and in control'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Are you aware of your credit score?',
    answers: ['Yes', 'No'],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
    isSingleSelect: false,
    question: 'Do you have investments? tick all that apply.',
    answers: [
      'Stocks and shares',
      'Bonds',
      'Pension',
      'Cryptocurrency',
      'Business',
      'Other'
    ],
    selectedAnswer: '',
    checkValues: [false, false, false, false, false, false],
  ),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'How much do you have in investments?',
    answers: [
      '£0',
      'Between £1 and £1000',
      'Between £1001 and £5000',
      'Between £5001 and £10000',
      'Above £10001'
    ],
    selectedAnswer: '',
  ),
  OnboardingQuestionModel(
      isSingleSelect: false,
      question: 'Do you have personal insurances? tick all that apply.',
      answers: ['Life', 'Critical Illness', 'Income protection', 'Other'],
      selectedAnswer: '',
      checkValues: [false, false, false, false]),
  OnboardingQuestionModel(
    isSingleSelect: true,
    question: 'Do you own a property?',
    answers: ['Yes', 'No'],
    selectedAnswer: '',
  )
];

// final List<CourseModel> COURSES = [
//   //Basic Personal Finance
//   CourseModel(
//     title: 'Basic Personal Finance',
//     summary: 'Summary goes here...',
//     modules: [
//       //Money & The Mind
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Introduction',
//             youtubeUrl: 'https://youtu.be/QnJobysROfQ',
//             summary: 'An introduction to money and the mind.',
//             activities: [],
//             questions: [],
//           ),
//           //Goal Setting
//           LessonModel(
//             title: 'Goal Setting',
//             youtubeUrl: null,
//             summary:
//                 'Learn about goal setting and how to use it to achieve your financial targets.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'Set aside time to work on creating some personal goals. See the steps below to create goals that you are more likely to achieve:',
//                   'Step 1: Write down at least five life goals. These are goals that you hope to achieve within the next 10 years. Consider the various areas of your life, including career, money, family, health, children, fun. Be as detailed as possible, consider when you want to achieve them, how much they will cost, and write why it is important for you to achieve it.',
//                   'Step 2: Select up to three goals from step one. Identify what you can do within the next year to get you closer to the big goals. These are mini goals.',
//                   'Step 3: For each of those mini goals, write out the steps you will need to take to complete the goal. Think about the obstacles you may face and what you need to do to overcome them.',
//                   'Step 4: Plan for the rewards you will give yourself upon completion of those steps and set up a method of recording your successes.'
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Enter your goals into the goals feature within the app.',
//                 ],
//               )
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'Why is goal setting so important?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'It provides motivation to work towards achieving the things we want in life.',
//                   '1': 'It guarantees achievement of the life you want.',
//                   '2': 'It makes you more successful.',
//                   '3': 'All of the above.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'What are the important components of goal setting?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Being specific and detailed about what you want to achieve.',
//                   '1': 'Creating a goal that is easy to measure and track.',
//                   '2': 'Ensuring the goal is realistic and meaningful.',
//                   '3': 'All of the above.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'Which of the following is a good goal description?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'I want to be financially free.',
//                   '1': 'I want to earn enough money to live without worry.',
//                   '2': 'I want to clear all my debts.',
//                   '3':
//                       'I want to clear my £6,000 debt within the next twelve months, so I have more disposable income to spend on my family.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following is a likely impact of goal setting?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Improved focus.',
//                   '1': 'Better clarity.',
//                   '2': 'More likely to make better decisions.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'How often should you review and update your goals?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'At least every month.',
//                   '1': 'Every year.',
//                   '2': 'Only once you have achieved your old goals.',
//                   '3': 'All the above.'
//                 },
//               ),
//             ],
//           ),
//           //Building Good Habits
//           LessonModel(
//             title: 'Building Good Habits',
//             youtubeUrl: 'https://youtu.be/BF0Ts5nPpvw',
//             summary: 'Learn how your beliefs and habits impact your finances.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'This activity is for you to reflect on your money habits and behaviours.',
//                   'Step 1. Take time to think through the past week and identify any money related decisions you made.',
//                   'Step 2. Identify some of the negative money related habits you have. Are there some reoccurring things that you do? Examples are avoiding looking at your bank balance, buying extra items from the supermarket that were not on your list, or going shopping after you’ve had a bad day in hopes of feeling better.',
//                   'Step 3. Write down the negative money habits you have and allow yourself to be forgiven for those poor choices in the past.',
//                   'Step 4. Identify things you could do instead of the negative behaviours you wrote down in step 3. For example, instead of buying extra items in the supermarket, write up a shopping list before you leave for the store.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Create a plan to change your negative habits into good habits. Set about committing to making the changes you described in activity 1. Detail in the plan what habits are to be changed and how you will monitor the changes. Check in with your progress regularly and keep a record in a notepad or in your diary.',
//                   'Find an accountability partner, someone who will check in on your progress, and possibly do the same task with you.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'Which of the following have an impact on your money decisions?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Marketing and advertising by companies trying to entice you to buy their products and services.',
//                   '1': 'Peer pressure to stay up to date and on trend.',
//                   '2': 'Personal habits and beliefs.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'Where do our habits tend to come from?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Through learned behaviour.',
//                   '1': 'From past experiences.',
//                   '2': 'Through reinforced learning.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'Can you work on changing your habits?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'Yes, with commitment, dedication, and self-reflection.',
//                   '1': 'Only if you do not have serious habits.',
//                   '2': 'No.',
//                   '3': 'Only if you have an accountability partner.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'How do money habits and beliefs affect how you handle money?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'They do not. If you have a lot of money your habits and beliefs do not affect how you handle money.',
//                   '1':
//                       'Only your negative money habits influence how you treat money.',
//                   '2':
//                       'Only your positive money habits influence how you treat money.',
//                   '3':
//                       'The habits and beliefs you hold inform how you treat money.'
//                 },
//               ),
//               QuestionModel(
//                 question: 'Which of the following are good money habits?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Regularly reviewing your income and expenses and spending less than you earn.',
//                   '1': 'Building up a savings pot of money.',
//                   '2': 'Committing to pay down on your debt.',
//                   '3': 'All the above.'
//                 },
//               )
//             ],
//           ),
//           //Dealing With Changes & Difficulty
//           LessonModel(
//             title: 'Dealing With Changes & Difficulty',
//             youtubeUrl: null,
//             summary: 'Summary can go here...',
//             activities: [],
//             questions: [
//               QuestionModel(
//                 question: 'What is the color of water?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'Blue',
//                   '1': 'Green',
//                   '2': 'Red',
//                 },
//               )
//             ],
//           ),
//           //Major Purchases:
//           LessonModel(
//             title: 'Major Purchases',
//             youtubeUrl: 'https://youtu.be/52FsEw2CEUQ',
//             summary:
//                 'Learn about the considerations to make before you make a major purchase.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'If you have a plan to make a major purchase soon, carry out the following tasks:',
//                   '1. Check you are paying the right price. Search for the item you plan to buy online and see if you can find a checker place to buy it from. If you must buy the item instore, visit several stores or call to locate the retailer offering the best deal.',
//                   '2. Search for cash back websites such as Top Cash Back or Quidco. Either use a search engine to see if you can earn some extra money by purchasing through their website. Or check if the bank account you hold allows you to earn cashback rewards by using the card to pay.',
//                   '3. Think about the timing of the purchase. Can you wait to buy the item in a sale? For example, it is a good idea to buy winter coats when it is not winter!',
//                   '4. Check to see if it would be beneficial to take out insurance to cover loss, damage, or theft. This is likely to be beneficial for expensive items such as TVs, fridges and freezers.',
//                   '5. If you do not have the funds to pay for the items in full now, take time to review your finance options. Find a friend who can help you review the loan, credit card and other borrowing options available. You want to choose the cheapest or ideally find free alternatives.'
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'If you do not have a major purchase to make soon, complete the following task instead.',
//                   'On a piece of paper, write down the following checklist. You will then stick this to your wall and use it to remind you on the checks to carry out before you make a major purchase.',
//                   'CHECKLIST',
//                   'Check various physical or online stores and note down the different costs of the item you need to buy. Identify the retailer offering the best deal and buy from there.',
//                   'Search various cashback websites such as Top Cash Back or Quidco, to see if you can make some money when you buy your item online.',
//                   'Consider whether you can wait to buy the item off season when it may be cheaper.',
//                   'Review the need for insurance to protect the item from loss, damage, or theft.',
//                   'Seek help if necessary, to identify the cheapest way of borrowing money to pay for the item.'
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'Which of the following could an insurance policy cover protect your purchase from?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Theft.',
//                   '1': 'Loss.',
//                   '2': 'Damage.',
//                   '3': 'All of the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following will help you save money when making a major purchase?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Comparing the sale price from various retailers.',
//                   '1': 'Taking advantage of cashback opportunities.',
//                   '2': 'Buying off season.',
//                   '3': 'All of the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Your washing machine has broken down for the 8th time this year, following 7 past repairs! The cost of a new washing machine meeting all your requirements at the store A is £300, it can be delivered to you tomorrow. You have seen that you can get the same washing machine, if you order online from Store B, for £210 instead. Unfortunately, Store B would deliver the washing machine one week later than Store A. Both stores offer the same warranty and are both reputable. What should you do? Choose below:',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'Buy the cheaper washing machine from Store B for £210, the laundrette cost in the extra week will cost £20.',
//                   '1': 'Buy from Store A for £300 now.',
//                   '2': 'Buy a used washing machine instead for £210.',
//                   '3': 'Pay for another repair costing £150 now.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You need to buy a new bed. Glossy Beds Direct sell the ideal bed for you for £250 and includes insurance. Master Beds store also offer the same bed for £200 but does not include insurance in the price. You have £200 cash available to spend and will need to borrow money if you select a more expensive bed. What should you do? Choose below:',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'Buy from Master Beds store now for £200 with cash.',
//                   '1':
//                       'Buy from Glossy Beds Direct now for £250 using the cash you have and borrow the rest.',
//                   '2':
//                       'Sell some unwanted items around the home to make up the £50 and buy from Glossy Beds Direct for £250.',
//                   '3': 'Do not buy a new bed.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'The mobile phone you have is two years old and is in good condition except for the screen damage which is a hazard. You currently pay £30 per month for the phone but are out of contract. The screen damage will cost £60 to repair. A new version of the phone has just been released and costs £800 to buy outright or £50 per month for a 2 years contract. What should you do?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0':
//                       'Take out the new contract for £50 per month and get a new phone.',
//                   '1':
//                       'Pay for the screen repair for £60 and continue with the contract you have for £30.',
//                   '2':
//                       'Pay for the screen repair for £60 and identify a new sim only sim deal for less than £30.',
//                   '3': 'Do nothing, so you do not have to pay money.'
//                 },
//               )
//             ],
//           )
//         ],
//         title: 'Money & The Mind',
//         summary:
//             'Learn all about your money mind and how it influences the financial decisions you make.',
//         youtubeUrl: null,
//       ),
//       //Money Management
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Introduction',
//             youtubeUrl: 'https://youtu.be/2iKxFrUXbBs',
//             summary: 'An introduction to money management.',
//             activities: [],
//             questions: [],
//           ),
//           //Creating A Budget
//           LessonModel(
//             title: 'Creating A Budget',
//             youtubeUrl: null,
//             summary:
//                 'Learn about using a personal budget to improve your finances.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'Take some time over the next few days to create a personal budget. As you have learned during the lesson, there are several steps you should complete. The first activity is to take time to review your spend for the last one month.',
//                   'Start by setting aside an afternoon to pull out all your bank statements and credit card statements. Create categories that suit your lifestyle, these may include household spend, personal goods, food, debt, savings and so on. You are to calculate how much you spend in each of these categories during a month by totalling up the spend for each category.',
//                   'Next, total all your income. This should include employed income, benefits and so on.',
//                   'Finally, enter this information into the Budget Planner built into this app.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'After imputing the information into the Budget Planner, you should be able to see your Net Income. Your Net Income shows how much money you have left at the end of the month. This will either be positive or negative.',
//                   'Whether your Net Income is positive or negative, it is a great idea to work on reducing your expenses. Make a start on identifying the variable expenses you can commit to reducing. Once you have identified how much you will reduce the spend to, update the figures on the Budget Planner. This should increase your Net Income figure!',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'The next step is to work to increase your income. If you have ideas, get started on implementing them! But if you do not, do not worry, you will learn about income increasing ideas in later lessons.',
//                   'You should have created your personal goals. Set aside some time to update your budget with this goal. For example, if your goal is to clear a £300 debt in the next 12 months. You could update your budget by adding a debt repayment of £25 per month.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'To maintain healthy budget and finances, how often should you review your personal budget?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'When you get a pay rise.',
//                   '1': 'When you need to make a large purchase.',
//                   '2': 'Every month.',
//                   '3': 'Every week.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following is a consequence of maintaining a personal budget?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Improved credit.',
//                   '1': 'More savings and less debt.',
//                   '2': 'Reduced financial stress.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'What is the first thing you could do if you find that you have a negative Net Income (the amount of money left after subtracting your total expenses from your total income)?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'There is nothing you can do.',
//                   '1': 'Wait until you get a pay rise to make changes.',
//                   '2': 'Reduce your expenses and try to earn more money.',
//                   '3': 'Borrow so you can pay your bills.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'If you find that you have a negative Net Income, what are your lenders and landlord likely to do?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'Cancel their agreement with you.',
//                   '1': 'Penalise you for not generating enough income.',
//                   '2': 'Try to help you fix your situation.',
//                   '3': 'None of the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'What should you do if you find that you are spending more than you budgeted?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0':
//                       'Review your spending habits and identify what categories you are overspending in.',
//                   '1':
//                       'Check whether this spend level is likely to continue and adjust your budget to accommodate the revised spend amount, if so.',
//                   '2': 'Take time to create systems to avoid bad money habits.',
//                   '3': 'All the above.',
//                 },
//               )
//             ],
//           ),
//           //Reducing Your Expenses
//           LessonModel(
//             title: 'Reducing Your Expenses',
//             youtubeUrl: null,
//             summary: 'Explorer the various methods of reducing your expenses.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'Last lesson you created a personal budget and reviewed your expenses for the month. Here, you will take things one step further. Consider some of your variable expenses and the easy ways you could spend less each month.',
//                   '1. Groceries – try shopping at a cheaper supermarket. Keep a record of how much you save after committing to this change for one month.',
//                   '2. Take away food – commit to reducing your spend here by half. This will mean cooking more meals. To make this easier, plan for the week, so you know what you will do on the day.',
//                   '3. Online miscellaneous shopping – Look at your spend at online stores and commit to reducing this by half for the month.',
//                   '4. Monthly subscriptions – it is easy to forget these after you’ve signed up. Take time to review the subscription you have and cancel those that you don’t need. Alternatively see if you could share a subscription with a friend and split the cost.',
//                   '5. Check for discount codes online, before you spend.',
//                   'Keep a record of the saving you make in one month.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Today take time to look through your fixed expenses and identify where you could reduce them. Consider the following type of spend:',
//                   '1. Mobile and broadband – review your contract terms, data, and minutes. Research online to find a better deal. Note down the potential saving you could make.',
//                   '2. Electricity and gas – Use a price comparison website to check if you can get a cheaper deal elsewhere. You will need to enter your current tariff name and details if you have it and postcode.',
//                   '3. Insurance – As your insurances near renewal, always check for a better deal elsewhere. As this time nears, check price comparison websites for cheaper cover.',
//                   'Reach out to a friend or family member to help you with this if necessary.'
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Take time to review the changes you have made to your expenses. Calculate how much money you have been able to cut spend by and update your Budget Planner within this app with the changes.',
//                   'You should end up with a higher Net Income amount. If you are showing a positive balance, you can use this extra money to start paying towards building a saving pot.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'Which of the following are fixed expenses?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'Car insurance premium payment.',
//                   '1': 'Grocery shopping.',
//                   '2': 'Clothes shopping.',
//                   '3': 'Salary.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following best describes a variable expense?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'An expense that varies and that you are in full control of.',
//                   '1': 'An expense that stays the same every month.',
//                   '2': 'A rent payment.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'How often should you review your expenses?',
//                 correctAnswerIndex: 1,
//                 answers: {
//                   '0': 'Every three months.',
//                   '1': 'Every week.',
//                   '2': 'Every month.',
//                   '3': 'When you change your salary.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following is false statement about fixed expenses?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'You can only change to a better deal when your contract is over.',
//                   '1':
//                       'Fixed expenses are usually more expensive than variable expenses.',
//                   '2':
//                       'You can make larger savings by regularly reviewing your fixed expenses.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Sue has just been quoted £355 for her car insurance renewal, but would like to pay less, what can she do?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Request a reduction in her premium as she has been a loyal customer.',
//                   '1':
//                       'Use a price comparison website to see if another provider will offer a better rate.',
//                   '2':
//                       'Check for discount vouchers or money off deal in association with her bank cards.',
//                   '3': 'All the above.',
//                 },
//               )
//             ],
//           ),
//           //Money Management Hacks
//           LessonModel(
//             title: 'Money Management Hacks',
//             youtubeUrl: null,
//             summary: 'Better manage your money with the tips in this lesson.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'Set yourself a target to save a specified amount of money by a set date in the future. Check your budget to make sure this is realistic. Divide the amount you plan to save by the number of months or weeks to the target date. For example, save £100 in the next 5 months. This would mean saving £20 per month for the next 5 months or £5 per week. Commit to saving this amount without fail and put the money into a separate bank account or pot if possible.',
//                   'Record this task in your goal log within this app. Set a day in the week/month to update the goal activity with your progress.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'By now you should have a clear budget in place that shows how much money you spend and earn every month. Look through the bills you pay, are there any that you could automate? Identify bills that you pay regularly, that you could set up on a Direct Debit. Doing so will ensure you do not miss a payment and help improve your credit profile.'
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'Which of the following will help improve your money management?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Setting up automated payments such as Direct Debits and standing orders to pay bills.',
//                   '1': 'Planning for a large purchase in advance.',
//                   '2':
//                       'Separating your money into various pots such as bills, fun and entertainment and savings.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You are out shopping and see something you want to buy that was not budgeted for. What could you do to help decide whether you should make the purchase?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'Buy it now.',
//                   '1':
//                       'Buy it now, then go home and check whether it fits in your buy. If it does not, return it later.',
//                   '2':
//                       'Do not buy it. Instead, wait 7 days to see if you still want it and check that you can afford it within your budget. If yes, buy it.',
//                   '3': 'Do not buy it.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'When is the best time to pay into your savings account?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'As soon as you are paid.',
//                   '1': 'Once you have paid your expenses.',
//                   '2': 'When you receive a bonus from work.',
//                   '3': 'When you make additional income.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'How much money do you need to start saving?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'At least £50 per month.',
//                   '1': 'At least £10 per month.',
//                   '2': 'It does not matter, start with what you have.',
//                   '3':
//                       'Only save when you have something to save for and a plan.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following will help you spend only what was planned when you go shopping:',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Writing a shopping list before you leave for the shop.',
//                   '1':
//                       'Being clear about your budget and what you have allocated to spend on your visit.',
//                   '2':
//                       'Separating your money into various pot so you only spend from a specific pot of money.',
//                   '3': 'All the above.',
//                 },
//               )
//             ],
//           )
//         ],
//         title: 'Money Management',
//         summary:
//             'Start improving your finances by learning how to better manage your money.',
//         youtubeUrl: null,
//       ),
//       //Saving & Increasing Income
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Introduction',
//             youtubeUrl: null,
//             summary:
//                 'An introduction to the saving and additional income module.',
//             activities: [],
//             questions: [],
//           ),
//           //Building An Emergency Fund
//           LessonModel(
//             //TODO:!!!
//             title: 'Building An Emergency Fund',
//             youtubeUrl: null,
//             summary:
//                 'Let’s get started with leaning about building an emergency fund.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'This activity will help you identify how much to save towards for your emergency fund.',
//                   'Step 1. Take some time to identify your essential expenses, this includes items in your budget planner, such as rent, groceries, medication, utilities.',
//                   'Step 2. Add up the amount you spend on each of those items in one month, this will give you a monthly total.',
//                   'Step 3. The target is to save for three months essential expenses cover. Multiply the total from Step 2 by 3 to calculate how much this is.',
//                   'If this figure is high, do not feel overwhelmed. Focus on saving the first months’ worth. Start putting side £10, then £20 and build progressively.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Once you have completed the first activity, you will need to create a system to help you reach your target. Breakdown the target into smaller chucks that fit into your budget planner. For example, identify how much money you have left over after paying the essential expenses, and assign an amount to building your emergency fund. This may be saving £10 per week. Set up the standing order with your bank to transfer this amount every week.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'Which of the follow expenses are to be included when calculating how much you need to save for your emergency fund?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'Luxury items, such as a designer watch.',
//                   '1':
//                       'Holiday expenses, such as travel insurance and holiday money.',
//                   '2':
//                       'Essential items that cover your basic needs, such as rent, utilities and water.',
//                   '3': 'Entertainment subscriptions.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'How many months’ worth of essential expenses should you have in savings as a minimum?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': '6 months.',
//                   '1': '3 months.',
//                   '2': 'As much as you can save.',
//                   '3': 'There is no set amount.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which account feature is not ideal when choosing an account to save your emergency fund money?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'An account where you must give notice to access the funds.',
//                   '1': 'An account with an extremely high interest rate.',
//                   '2':
//                       'An account that does not charge you a penalty for withdrawing funds.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'If you currently have debts, which of the following is the best strategy to take when building an emergency fund?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Focus on saving for the emergency fund before increasing payment to clear debts.',
//                   '1':
//                       'Focus on clearing your debt and building an emergency fund at the same time.',
//                   '2':
//                       'Focus on clearing your debt before saving for the emergency fund.',
//                   '3': 'Any of the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You have saved up an emergency fund of £500, in which of the following situations is it suitable to use?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'When you receive a parking fine of £195.',
//                   '1':
//                       'To pay for a training course teaching you how to make money by selling online.',
//                   '2':
//                       'To buy a new £450 phone, so you can end your mobile contract and be on a sim only deal instead.',
//                   '3':
//                       'To pay for the £75 airport parking for your summer vacation, as you have used all the holiday funds.',
//                 },
//               )
//             ],
//           ),
//           //Money Saving Tips
//           LessonModel(
//             title: 'Money Saving Tips',
//             youtubeUrl: null,
//             summary: 'Use this lesson to learn some money saving tips.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'If you do not have a dedicated savings account, identify an account that suits your needs and open it ready for you to start saving.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Let’s get saving! Grab a sheet of paper and give it a heading ‘Money Saving Challenge’. You are to save £10 every week. Create a Money Saving Challenge record. At the start of each month, list the date for all the Mondays in the month.',
//                   'Every week put £10 into a dedicated savings account and update your record with the amount entered and total up how much you have saved so far.',
//                   'Add the new goal to save £520, by savings £10 per week for a year, to the Goals feature within the App. Start the challenge with a friend or family member for better results!',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'How much money do you need to start saving?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': '£1.',
//                   '1': '£100.',
//                   '2': '£20.',
//                   '3': 'There is no limit.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following will help you be better at saving?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Creating a saving plan.',
//                   '1': 'Opening a dedicated saving account.',
//                   '2': 'Committing to a savings challenge with a friend.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You are out shopping and see an item you really want for £70. This item is not part of your budget and is on sale for the next 7 days only. Which of the following should you do?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Buy it now before the price increases.',
//                   '1': 'Do not buy it as it is outside of your budget.',
//                   '2':
//                       'Do not buy it now. Instead, wait a few days to see if you still want it and check that you can afford it within your budget. If yes, buy it',
//                   '3': 'Borrow the money so you can buy it.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'What should you have sorted before you automate your savings?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'A budget to confirm how much you can save every month.',
//                   '1':
//                       'Adjusted your spend to ensure you are spending less than you earn.',
//                   '2': 'A separate bank account to deposit your savings into.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'When reviewing your savings balance, which of the following should you check?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'The interest rate offered on your savings account to ensure you are getting the best rate possible.',
//                   '1':
//                       'Whether you are on track with the savings target you set.',
//                   '2':
//                       'If it is possible to increase how much you are adding to your regular savings.',
//                   '3': 'All the above.'
//                 },
//               )
//             ],
//           ),
//           //Generating Additional Income
//           LessonModel(
//             title: 'Additional Income Ideas',
//             youtubeUrl: null,
//             summary:
//                 'Learn about the many ways of generating an additional income.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'In this activity you will identify a potential additional income idea.',
//                   'Step 1. Set aside some time to note down your skills, these may be skill you use in your current work or skills you use as hobbies. Alternatively, take time to think about a solution you could provide.',
//                   'Step 2. Consider ways in which your skills could be turned into a service and sold.',
//                   'Step 3. Think about your local community, either physically or online. Are there people that would benefit from your skills or solutions.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'In this activity you will take the steps to generate an additional income.',
//                   'If you were able to identify a service you could offer or a solution you could provide, consider how much you could charge for this. Spend some time searching magazines, newspapers, and online for other people offering a similar service or solution and compare your offering to theirs.',
//                   'Identify where you could advertise your offering. Check Facebook, Gumtree, Craigslist, the local shop window, or library.',
//                   'Finally, advertise your business! This may mean creating a post online, printing a leaflet or simply sharing the news about what you offer within your friend and community circle.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'Which of the following are passive income ideas?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Hiring out your car parking space.',
//                   '1': 'Hiring out your car.',
//                   '2': 'Earning interest income on savings.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following is a good example of an active income idea?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Being employed by a company.',
//                   '1': 'Creating and selling homemade Christmas hampers.',
//                   '2': 'Completing online surveys.',
//                   '3': 'All the above.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following is not correct when considering an additional money-making idea?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'The idea must be profitable. The cost to create the product or service, must be less than the price received from the customer.',
//                   '1':
//                       'You must earn more than you would working as an employee.',
//                   '2':
//                       'The additional money you earn through your additional income, may need to be reported to HMRC and may affect your benefit claims.',
//                   '3':
//                       'You can earn an income both as an employee and outside of your employment.'
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Earning an additional income will solve your money problems. Is this true?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0':
//                       'This is true. Additional income will help you clear any debts you have.',
//                   '1':
//                       'This is true. If you earn more than you earn through employment, you will solve your money problems.',
//                   '2':
//                       'This is not true. Earning more money alone will not solve your money problems. You will need to have a personal Budget in place, a financial plan, goals and be working on creating good habits that you stick to.',
//                   '3':
//                       'This is not true. Earning additional money usually means more problems, and more tax to pay.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'What do you need to sell unwanted items online?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0':
//                       'A camera or phone camera to take a picture of the item.',
//                   '1': 'An account on the platform to sell the item on.',
//                   '2':
//                       'An agree price to list the items for sale at and a means of collection or delivery.',
//                   '3': 'All the above.',
//                 },
//               )
//             ],
//           )
//         ],
//         title: 'Saving & Increasing Income',
//         summary: 'Learn the basics to saving and increasing income.',
//         youtubeUrl: null,
//       ),
//       //Everyday Numeracy
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Estimating',
//             youtubeUrl: null,
//             summary: 'Learn how to estimate and calculate sums quickly!',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'Estimate the length of the main room in your home in meters. Consider that 1 meter is the approximate length an large adult step, the height of a 6-year child or the length of a prayer rug.',
//                 ],
//               ),
//               ActivityModel(
//                 parts: [
//                   'Practice using estimation when you next visit the grocery store. As you walk around the store and add items to your basket, try to keep a rough record of cost of the items. At the end of your shop have a guess as to the total bill amount.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'Estimate the length of a GBP ten-pound note.',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': '10 cm',
//                   '1': '25 cm',
//                   '2': '5 cm',
//                   '3': '130 cm',
//                 },
//               ),
//               QuestionModel(
//                 question: 'Estimate the answer to 1063 multiplied by 27.6',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': '27,000',
//                   '1': '2,700',
//                   '2': '29,338.8',
//                   '3': '27',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You are calculating the 20% tip you want to pay for the meal. Which of the following is a good estimate for a meal costing £26.72?',
//                 correctAnswerIndex: 1,
//                 answers: {
//                   '0': '26 pence',
//                   '1': '£6',
//                   '2': '£2.67',
//                   '3': '£52',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'What is the estimated cost for 18 apples costing 17p each?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': '£4.00',
//                   '1': '£3.06',
//                   '2': '£18',
//                   '3': '£17',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'You had £730 in your bank at the start of the week. Estimate how much you have left in your bank after spending £13.50 on a new shirt, £22.13 on groceries and receiving a bonus of £144.',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': '£1,500',
//                   '1': '838.37',
//                   '2': '£730',
//                   '3': '£850',
//                 },
//               )
//             ],
//           ),
//         ],
//         title: 'Everyday Numeracy',
//         summary: 'Learn some quick techniques to help you with your everyday.',
//         youtubeUrl: null,
//       ),
//       //Debt & Borrowing
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Introduction',
//             youtubeUrl: null,
//             summary: 'An introduction to debt and borrowing.',
//             activities: [],
//             questions: [],
//           ),
//           //Knowing Your Rights & Responsibilities
//           LessonModel(
//             title: 'Knowing Your Rights & Responsibilities',
//             youtubeUrl: null,
//             summary:
//                 'A starter guide to your rights and responsibilities as a borrower',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'If you are currently a borrower, take time to check you are meeting your obligations. These include:',
//                   'Paying you debts on time',
//                   'Paying your debts in full',
//                   'Notifying your lender if you struggle to keep up with repayments.',
//                   'Making sure your lender holds the correct information about you.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question: 'What of the following are you responsible for?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0': 'Making repayments on time and in full.',
//                   '1':
//                       'Ensuring the information held by your lender is correct.',
//                   '2':
//                       'Updating your lender to changes in your finances if you become unable to pay your debts.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'If you have a change in financial circumstances, such that you realise you are struggling to repay your debts, which of the following could you do?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0':
//                       'Take time to become clear on your financial position, understand how much income and expenses you have.',
//                   '1':
//                       'Contact your lender immediately to update them on your change of circumstances.',
//                   '2': 'Seek free debt advice of you need support.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following debts could lead to imprisonment if not paid?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Criminal fines',
//                   '1': 'Council tax (England only)',
//                   '2': 'Child maintenance arrears owed to the CSA.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'Which of the follow can lead to imprisonment?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'Mortgage or rent arrears.',
//                   '1': 'Utility arrears.',
//                   '2': 'Hire purchase debts.',
//                   '3': 'None of the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'If you are seeking debt advice and need some breathing space, what could you do?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'Ask the creditor to give you breathing space and stop calling and writing you for a while.',
//                   '1': 'Nothing, you need to keep up debt repayments.',
//                   '2':
//                       'Stop making debt payments and only deal with the creditor when they contact you.',
//                   '3': 'None of the above.',
//                 },
//               )
//             ],
//           ),
//           //Different Types of Debt
//           LessonModel(
//             title: 'Different Types of Debt',
//             youtubeUrl: null,
//             summary: 'Learn about some of the various debt options available.',
//             activities: [
//               ActivityModel(
//                 parts: [
//                   'This activity applies if you are currently a borrower and repaying a debt.',
//                   'Take time to look through the terms and conditions of your debt to ensure you understand and are aware of the following:',
//                   'The current level of debt you have outstanding.',
//                   'The current interest rate you are paying on your debt.',
//                   'The repayment dates of your debt.',
//                   'The contact details of your lender.',
//                 ],
//               ),
//             ],
//             questions: [
//               QuestionModel(
//                 question:
//                     'Which of the following is the closest description of a secured debt?',
//                 correctAnswerIndex: 0,
//                 answers: {
//                   '0':
//                       'A debt taken out as a loan against collateral or an asset that you own.',
//                   '1': 'A debt that does not require an asset as collateral.',
//                   '2':
//                       'A debt where the lender cannot repossess your assets to recoup your funds.',
//                   '3': 'An unsecured credit card.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'What information will the lender need to know before issuing you with an unsecured debt?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'That your credit profile meets their criteria.',
//                   '1': 'That you can repay the debt.',
//                   '2':
//                       'That you have a manageable level of debt, if you are already borrowing.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question:
//                     'Which of the following type of debt requires you to maintain repayments of a pre agreed amount?',
//                 correctAnswerIndex: 2,
//                 answers: {
//                   '0': 'Secured debt such as a mortgage.',
//                   '1': 'Unsecured debt such as a credit card.',
//                   '2': 'Instalment debt such as a personal loan.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'What is a credit file?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'A record holding all your past borrowing history.',
//                   '1':
//                       'A file used by lenders to check your credit worthiness.',
//                   '2': 'A file recording how much borrowing you have.',
//                   '3': 'All the above.',
//                 },
//               ),
//               QuestionModel(
//                 question: 'What type of debt is a mortgage?',
//                 correctAnswerIndex: 3,
//                 answers: {
//                   '0': 'An instalment debt.',
//                   '1': 'A secured debt.',
//                   '2': 'An instalment and secured debt.',
//                   '3': 'A revolving debt.',
//                 },
//               )
//             ],
//           ),
//         ],
//         title: 'Debt & Borrowing',
//         summary: 'Learn the basics about debt and borrowing.',
//         youtubeUrl: null,
//       ),
//       //Dealing With Financial Difficulty & Getting Help
//       ModuleModel(
//         lessons: [
//           //Introduction
//           LessonModel(
//             title: 'Introduction',
//             youtubeUrl: null,
//             summary: 'An introduction to dealing with financial difficulty.',
//             activities: [],
//             questions: [],
//           ),
//         ],
//         title: 'Dealing With Financial Difficulty & Getting Help',
//         summary:
//             'Learn about alternative debt problem solutions and how to deal with difficult situations.',
//         youtubeUrl: null,
//       ),
//     ],
//   ),
// ];
