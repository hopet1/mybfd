import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ActivityModel {
  String id;
  String name;
  List<String> parts;
  int position;

  ActivityModel({
    @required this.id,
    @required this.name,
    @required this.parts,
    @required this.position,
  });

  factory ActivityModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return ActivityModel(
      id: data['id'],
      name: data['name'],
      parts: List.from(data['parts']),
      position: data['position'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'position': position,
      'parts': parts,
    };
  }
}
