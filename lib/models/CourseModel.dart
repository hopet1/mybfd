import 'package:MyBFD/models/ModuleModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CourseModel {
  String id;
  int position;
  String summary;
  String title;

  CourseModel({
    @required this.id,
    @required this.position,
    @required this.summary,
    @required this.title,
  });

  factory CourseModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return CourseModel(
      id: data['id'],
      position: data['position'],
      summary: data['summary'],
      title: data['title'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position': position,
      'summary': summary,
      'title': title,
    };
  }
}
