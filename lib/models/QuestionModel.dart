import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class QuestionModel {
  String id;
  String question;
  int correctAnswerIndex;
  Map<String, dynamic> answers;
  int position;

  int selectedAnswerIndex; //For FE only.

  QuestionModel({
    @required this.id,
    @required this.question,
    @required this.correctAnswerIndex,
    @required this.answers,
    @required this.position,
  });

  factory QuestionModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return QuestionModel(
      id: data['id'],
      question: data['question'],
      correctAnswerIndex: data['correctAnswerIndex'],
      answers: data['answers'],
      position: data['position'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'question': question,
      'correctAnswerIndex': correctAnswerIndex,
      'answers': answers,
      'position': position,
    };
  }
}
