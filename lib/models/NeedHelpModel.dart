class NeedHelpModel {
  String name;
  String username;
  String message;

  NeedHelpModel({
    this.name,
    this.username,
    this.message,
  });

  NeedHelpModel.fromMap(dynamic json) {
    name = json["name"];
    username = json["email"];
    message = json["message"];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["name"] = name;
    map["email"] = username;
    map["message"] = message;

    return map;
  }
}
