import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ModuleScoreModel {
  String id;
  String courseID;
  String moduleID;
  bool passed;

  ModuleScoreModel({
    @required this.id,
    @required this.courseID,
    @required this.moduleID,
    @required this.passed,
  });

  factory ModuleScoreModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return ModuleScoreModel(
      id: data['id'],
      courseID: data['courseID'],
      moduleID: data['moduleID'],
      passed: data['passed'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'courseID': courseID,
      'moduleID': moduleID,
      'passed': passed,
    };
  }
}
