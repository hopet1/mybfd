import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class GoalModel {
  DateTime modified;
  DateTime created;
  String id;
  String goal;
  String reason;
  bool complete;

  GoalModel({
    @required this.modified,
    @required this.created,
    @required this.id,
    @required this.goal,
    @required this.reason,
    @required this.complete,
  });

  factory GoalModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return GoalModel(
      created: data['created'].toDate(),
      modified: data['modified'].toDate(),
      id: data['id'],
      goal: data['goal'],
      reason: data['reason'],
      complete: data['complete'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'created': created,
      'modified': modified,
      'id': id,
      'goal': goal,
      'reason': reason,
      'complete': complete,
    };
  }
}
