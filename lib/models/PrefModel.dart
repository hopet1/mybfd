import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/PrefService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

class PrefModel {
  String uid;
  String prefId;
  Map<String, dynamic> prefs;

  PrefModel({
    @required this.uid,
    this.prefs,
    this.prefId,
  });

  factory PrefModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return PrefModel(
      uid: data['uid'],
      prefId: data['prefId'],
      prefs: data['prefs'],
    );
  }

  @override
  String toString() {
    String text = "uid: $uid,\nprefs: ${prefs.toString()},\nprefId: $prefId";
    return text;
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'prefs': prefs,
      'prefId': prefId,
    };
  }

  Future<void> addData(Map<String, dynamic> data) async {
    prefs.addAll(data);
    await locator<PrefService>().updatePrefs(this);
  }

  dynamic getData(String key) {
    if (prefs.containsKey(key)) {
      return prefs[key];
    } else {
      String text = "No such key";
      return text;
    }
  }
}
