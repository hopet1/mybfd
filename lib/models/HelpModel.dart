import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HelpModel {
  int level;
  String id;
  bool hasChildren;
  String title;
  String content;
  String parent;

  HelpModel({
    @required this.level,
    @required this.id,
    @required this.hasChildren,
    @required this.title,
    @required this.content,
    @required this.parent,
  });

  factory HelpModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return HelpModel(
      level: data['level'],
      id: data['id'],
      hasChildren: data['hasChildren'],
      title: data['title'],
      content: data['content'],
      parent: data['parentID'],
    );
  }
}
