import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NetWorthItemModel {
  double amount;
  String name;

  NetWorthItemModel({
    @required this.amount,
    @required this.name,
  });

  factory NetWorthItemModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return NetWorthItemModel(
      amount: data['amount'],
      name: data['name'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'amount': amount,
      'name': name,
    };
  }
}
