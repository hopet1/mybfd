import 'package:flutter/material.dart';

class OnboardingQuestionModel {
  String question;
  List<String> answers;
  String selectedAnswer;
  bool isSingleSelect;

  List<bool> checkValues;

  OnboardingQuestionModel({
    @required this.question,
    @required this.answers,
    @required this.selectedAnswer,
    @required this.isSingleSelect,
    this.checkValues,
  });
}
