import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class LessonScoreModel {
  String id;
  String courseID;
  String moduleID;
  String lessonID;
  int correct;
  bool passed;

  LessonScoreModel({
    @required this.id,
    @required this.courseID,
    @required this.moduleID,
    @required this.lessonID,
    @required this.correct,
    @required this.passed,
  });

  factory LessonScoreModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return LessonScoreModel(
      id: data['id'],
      courseID: data['courseID'],
      moduleID: data['moduleID'],
      lessonID: data['lessonID'],
      correct: data['correct'],
      passed: data['passed'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'courseID': courseID,
      'moduleID': moduleID,
      'lessonID': lessonID,
      'correct': correct,
      'passed': passed,
    };
  }
}
