import 'package:MyBFD/models/NetWorthItemModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NetWorthHolderModel {
  int position;
  NetWorthItemModel netWorthItem;

  NetWorthHolderModel({
    @required this.position,
    @required this.netWorthItem,
  });

  factory NetWorthHolderModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return NetWorthHolderModel(
      position: data['position'],
      netWorthItem: NetWorthItemModel(
        name: data['netWorthItem']['name'],
        amount: data['netWorthItem']['amount'],
      ),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'position': position,
      'netWorthItem': netWorthItem.toMap(),
    };
  }
}
