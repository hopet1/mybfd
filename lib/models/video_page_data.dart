class VideoPageData {
  String videoId, title;
  VideoPageData({this.title, this.videoId});
}
