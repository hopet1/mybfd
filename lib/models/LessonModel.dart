import 'package:MyBFD/models/QuestionModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'ActivityModel.dart';

class LessonModel {
  String id;
  int position;
  String summary;
  String title;
  String youtubeUrl;

  LessonModel({
    @required this.id,
    @required this.position,
    @required this.summary,
    @required this.title,
    @required this.youtubeUrl,
  });

  factory LessonModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return LessonModel(
      id: data['id'],
      position: data['position'],
      summary: data['summary'],
      title: data['title'],
      youtubeUrl: data['youtubeUrl'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position': position,
      'summary': summary,
      'title': title,
      'youtubeUrl': youtubeUrl,
    };
  }
}
