import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserModel {
  String firstName;
  String lastName;
  String imgUrl;
  String email;
  DateTime modified;
  DateTime created;
  String uid;
  String username;
  String fcmToken;
  String phoneNumber;
  String streetAddress;
  String city;
  String postCode;

  UserModel({
    @required this.firstName,
    @required this.lastName,
    @required this.imgUrl,
    @required this.email,
    @required this.modified,
    @required this.created,
    @required this.uid,
    @required this.username,
    @required this.fcmToken,
    @required this.phoneNumber,
    @required this.streetAddress,
    @required this.city,
    @required this.postCode,
  });

  factory UserModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return UserModel(
      firstName: data['firstName'],
      lastName: data['lastName'],
      imgUrl: data['imgUrl'],
      email: data['email'],
      created: data['created'].toDate(),
      modified: data['modified'].toDate(),
      uid: data['uid'],
      username: data['username'],
      fcmToken: data['fcmToken'],
      phoneNumber: data['phoneNumber'],
      streetAddress: data['streetAddress'],
      city: data['city'],
      postCode: data['postCode'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'firstName': firstName,
      'lastName': lastName,
      'imgUrl': imgUrl,
      'email': email,
      'created': created,
      'modified': modified,
      'uid': uid,
      'username': username,
      'fcmToken': fcmToken,
      'phoneNumber': phoneNumber,
      'streetAddress': streetAddress,
      'city': city,
      'postCode': postCode,
    };
  }
}
