import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class RewardModel {
  String id;
  bool claimed;
  String imgURL;
  String content;
  String reason;
  String code;
  String moduleId;
  String currId;

  RewardModel({
    @required this.moduleId,
    @required this.id,
    @required this.claimed,
    @required this.imgURL,
    @required this.content,
    @required this.reason,
    @required this.code,
    this.currId,
  });

  factory RewardModel.fromDoc({@required DocumentSnapshot ds}) {
    final Map<String, dynamic> data = ds.data();
    return RewardModel(
      id: data['id'],
      moduleId: data['moduleId'],
      claimed: data['claimed'],
      imgURL: data['imgURL'],
      content: data['content'],
      reason: data['reason'],
      code: data['code'],
      currId: data['currId'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'moduleId': moduleId,
      'claimed': claimed,
      'imgURL': imgURL,
      'content': content,
      'reason': reason,
      'code': code,
      'currId': currId,
    };
  }
}
