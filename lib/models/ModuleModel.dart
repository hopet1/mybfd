import 'package:MyBFD/models/LessonModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ModuleModel {
  String id;
  int position;
  String summary;
  String title;
  String youtubeUrl;

  ModuleModel({
    @required this.id,
    @required this.position,
    @required this.summary,
    @required this.title,
    @required this.youtubeUrl,
  });

  factory ModuleModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return ModuleModel(
      id: data['id'],
      position: data['position'],
      summary: data['summary'],
      title: data['title'],
      youtubeUrl: data['youtubeUrl'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position': position,
      'summary': summary,
      'title': title,
      'youtubeUrl': youtubeUrl,
    };
  }
}
