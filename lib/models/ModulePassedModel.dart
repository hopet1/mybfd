import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ModulePassedModel {
  String id;
  String courseId;
  String moduleId;
  bool passed;

  ModulePassedModel({
    @required this.id,
    @required this.courseId,
    @required this.moduleId,
    @required this.passed,
  });

  factory ModulePassedModel.fromDoc({@required DocumentSnapshot doc}) {
    final Map<String, dynamic> data = doc.data();
    return ModulePassedModel(
      id: data['id'],
      courseId: data['courseID'],
      moduleId: data['moduleID'],
      passed: data['passed'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'courseID': courseId,
      'moduleID': moduleId,
      'passed': passed,
    };
  }
}
