import 'package:flutter/material.dart';

import '../constants.dart';

final String fontFamily = 'Montserrat';

ThemeData themeData = ThemeData(
  primarySwatch: Colors.blue,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  textTheme: TextTheme(
    headline1: TextStyle(
      fontWeight: FontWeight.w200,
      fontSize: 30,
      fontFamily: 'Montserrat',
    ),
    headline2: TextStyle(
      fontWeight: FontWeight.w800,
      fontSize: 18,
      color: Colors.black,
      fontFamily: 'Montserrat',
    ),
    headline3: TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 16,
      fontFamily: 'Montserrat',
    ),
    headline4: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: colorOrange,
      fontFamily: 'Montserrat',
    ),
    headline5: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      fontFamily: 'Montserrat',
    ),
    headline6: TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      fontFamily: 'Montserrat',
    ),
  ),
  dividerColor: Colors.transparent,
  // indicatorColor: colorNavy,
  // iconTheme: IconThemeData(color: colorNavy),
  // appBarTheme: AppBarTheme(color: Colors.red.shade900),
  // scaffoldBackgroundColor: Colors.white,
  // fontFamily: fontFamily,
  // bottomNavigationBarTheme: BottomNavigationBarThemeData(
  //   backgroundColor: Colors.white,
  // ),
  // canvasColor: Colors.white,
  // dividerColor: const Color(0xFF253341),
);
