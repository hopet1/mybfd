import 'package:MyBFD/models/LessonModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class ILessonService {
  Future<void> createLesson(
      {@required String courseID,
      @required String moduleID,
      @required LessonModel lesson});

  Future<List<LessonModel>> retrieveLessons({
    @required String courseID,
    @required String moduleID,
  });

  Future<void> updateLesson(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required Map<String, dynamic> data});

  Future<void> deleteLesson(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID});
}

class LessonService extends ILessonService {
  final CollectionReference _courseColRef =
      FirebaseFirestore.instance.collection('Courses');

  @override
  Future<void> createLesson(
      {@required String courseID,
      @required String moduleID,
      @required LessonModel lesson}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference lessonDocRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc();

      lesson.id = lessonDocRef.id;

      batch.set(lessonDocRef, lesson.toMap());

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteLesson(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID}) async {
    try {
      await _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<LessonModel>> retrieveLessons({
    @required String courseID,
    @required String moduleID,
  }) async {
    try {
      final CollectionReference lessonsColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons');

      Query query = lessonsColRef;

      // if (limit != null) {
      //   query = query.limit(limit);
      // }

      // if (orderBy != null) {
      //   query = query.orderBy(orderBy);
      // }

      return (await query.get())
          .docs
          .map(
            (doc) => LessonModel.fromDoc(
              doc: doc,
            ),
          )
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateLesson(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required Map<String, dynamic> data}) async {
    try {
      final CollectionReference lessonsColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons');
      await lessonsColRef.doc(lessonID).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
