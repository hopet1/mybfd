import 'package:MyBFD/models/CourseModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class ICourseService {
  Future<void> createCourse({@required CourseModel course});

  Future<List<CourseModel>> retrieveCourses();

  Future<void> updateCourse(
      {@required String courseID, @required Map<String, dynamic> data});

  Future<void> deleteCourse({@required String courseID});
}

class CourseService extends ICourseService {
  final CollectionReference _coursesColRef =
      FirebaseFirestore.instance.collection('Courses');

  @override
  Future<void> createCourse({@required CourseModel course}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference courseDocRef = _coursesColRef.doc();

      course.id = courseDocRef.id;

      batch.set(courseDocRef, course.toMap());

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteCourse({@required String courseID}) async {
    try {
      final DocumentReference courseDocRef = _coursesColRef.doc(courseID);

//TODO: Delete all questions
//TODO: Delete all activities
//TODO: Delete all lessons

      //Delete all modules in course.
      final CollectionReference modulesColRef =
          courseDocRef.collection('modules');

      List<QueryDocumentSnapshot> modulesQueryDocSnaps =
          (await modulesColRef.get()).docs;

      for (int i = 0; i < modulesQueryDocSnaps.length; i++) {
        await modulesQueryDocSnaps[i].reference.delete();
      }

      //Delete course.
      await courseDocRef.delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<CourseModel>> retrieveCourses() async {
    try {
      Query query = _coursesColRef;

      // if (limit != null) {
      //   query = query.limit(limit);
      // }

      // if (orderBy != null) {
      //   query = query.orderBy(orderBy);
      // }

      return (await query.get())
          .docs
          .map(
            (doc) => CourseModel.fromDoc(
              doc: doc,
            ),
          )
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateCourse(
      {@required String courseID, @required Map<String, dynamic> data}) async {
    try {
      await _coursesColRef.doc(courseID).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

// @override
// Future<void> createUser({@required UserModel user}) async {
//   try {
//     final WriteBatch batch = FirebaseFirestore.instance.batch();

//     DocumentReference userDocRef = _usersColRef.doc(user.uid);

//     batch.set(userDocRef, user.toMap());

//     batch.update(_tableCountsDocRef, {
//       'users': FieldValue.increment(1),
//     });

//     await batch.commit();

//     return;
//   } catch (e) {
//     throw Exception(
//       e.toString(),
//     );
//   }
// }

// @override
// Future<UserModel> retrieveUser({@required String uid}) async {
//   try {
//     DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
//     return UserModel.fromDoc(ds: documentSnapshot);
//   } catch (e) {
//     throw Exception(e.toString());
//   }
// }

// @override
// Future<void> updateUser(
//     {@required String uid, @required Map<String, dynamic> data}) async {
//   try {
//     DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
//     DocumentReference documentReference = documentSnapshot.reference;
//     await documentReference.update(data);
//     return;
//   } catch (e) {
//     throw Exception(
//       e.toString(),
//     );
//   }
// }

// @override
// Future<List<UserModel>> retrieveUsers({int limit, String orderBy}) async {
//   try {
//     Query query = _usersColRef;

//     if (limit != null) {
//       query = query.limit(limit);
//     }

//     if (orderBy != null) {
//       query = query.orderBy(orderBy);
//     }

//     return (await query.get())
//         .docs
//         .map((doc) => UserModel.fromDoc(ds: doc))
//         .toList();
//   } catch (e) {
//     throw Exception(e.toString());
//   }
// }
}
