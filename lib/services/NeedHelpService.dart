import 'package:MyBFD/models/NeedHelpModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class INeedHelpAbs {
  Future<void> sendQuery(NeedHelpModel query);
}

class NeedHelpService implements INeedHelpAbs {
  NeedHelpService();

  final FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<void> sendQuery(NeedHelpModel query) async {
    try {
      String regards = "\n\nThanks and Regards\n${query.name}";
      String userId = "\nUser ID/Email: ${query.username}";
      String body = "${query.message}$regards" + userId;
      String subject = "Need Help";
      String to = "support@mybfd.co.uk";
      String makeEmail = "mailto:$to?subject=$subject&body=$body";

      launch(makeEmail);
    } catch (e) {
      print(e.toString());
    }
  }
}
