import 'package:cloud_firestore/cloud_firestore.dart';

abstract class IAdminService {
  Future<List<String>> getAdmins();
  Future<bool> isAdmin(String uid);
}

class AdminService extends IAdminService {
  final CollectionReference _adminRef =
      FirebaseFirestore.instance.collection('Admins');

  @override
  Future<List<String>> getAdmins() async {
    try {
      List<String> items =
          (await _adminRef.get()).docs.map((doc) => toMap(doc)).toList();
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<bool> isAdmin(String uid) async {
    final List<String> admins = await getAdmins();
    return admins.contains(uid);
  }

  String toMap(DocumentSnapshot doc) {
    final Map<String, dynamic> data = doc.data();
    return data['uid'];
  }
}
