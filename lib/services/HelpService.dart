import 'package:MyBFD/models/HelpModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class IHelpService {
  Future<List<HelpModel>> listHelp();

  Future<List<HelpModel>> getHelpChildren(
    String id,
    int level,
    String parentID,
  );
}

class HelpService extends IHelpService {
  final CollectionReference _helpColRef =
      FirebaseFirestore.instance.collection('FAQs');

  @override
  Future<List<HelpModel>> getHelpChildren(
      String id, int level, String parentID) async {
    CollectionReference _childHelpRef;

    if (level == 1) {
      _childHelpRef = _helpColRef.doc(id).collection("children");
    } else if (level == 2) {
      _childHelpRef = _helpColRef
          .doc(parentID)
          .collection("children")
          .doc(id)
          .collection("children");
    }

    try {
      List<HelpModel> items = (await _childHelpRef.get())
          .docs
          .map((doc) => HelpModel.fromDoc(ds: doc))
          .toList();
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<HelpModel>> listHelp() async {
    try {
      List<HelpModel> items = (await _helpColRef.get())
          .docs
          .map((doc) => HelpModel.fromDoc(ds: doc))
          .toList();
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
