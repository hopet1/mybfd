import 'package:flutter/material.dart';

abstract class IWidgetService {
  //Create a unique key for each listview one time only, to prevent unneccessary scrolling during edits.
  Key generateKey({
    @required Map<String, dynamic> map,
    @required String name,
    @required Key key,
  });
}

class WidgetService extends IWidgetService {
  @override
  Key generateKey({
    @required Map<String, dynamic> map,
    @required String name,
    @required Key key,
  }) {
    if (map.containsKey(name)) {
      return map[name];
    } else {
      map[name] = key;
      return key;
    }
  }
}
