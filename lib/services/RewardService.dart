import 'package:MyBFD/models/RewardModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'AuthService.dart';

abstract class IRewardService {
  Future<List<RewardModel>> getRewards();

  Future<List<RewardModel>> getIncompleteRewards();

  Future<List<RewardModel>> getDefaultRewards();

  Future<void> updateReward({
    @required String id,
    @required Map<String, dynamic> data,
  });
}

class RewardService extends IRewardService {
  final CollectionReference _userColRef =
      FirebaseFirestore.instance.collection('Users');
  final CollectionReference _rewardsColRef =
      FirebaseFirestore.instance.collection('Rewards');
  CollectionReference _userRewardsRef;

  @override
  Future<List<RewardModel>> getRewards() async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    String uid = user.uid;
    _userRewardsRef = _userColRef.doc(uid).collection('rewards');
    try {
      List<RewardModel> items = (await _userRewardsRef.get())
          .docs
          .map((doc) => RewardModel.fromDoc(ds: doc))
          .toList();
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<RewardModel>> getIncompleteRewards() async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    String uid = user.uid;
    _userRewardsRef = _userColRef.doc(uid).collection('rewards');
    try {
      List<RewardModel> items = (await _userRewardsRef.get())
          .docs
          .map((doc) => RewardModel.fromDoc(ds: doc))
          .toList();
      items.forEach((element) {
        if (element.claimed) {
          items.remove(element);
        }
      });
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> updateReward({
    @required String id,
    @required Map<String, dynamic> data,
  }) async {
    try {
      await _userRewardsRef.doc(id).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<RewardModel>> getDefaultRewards() async {
    try {
      List<RewardModel> items = (await _rewardsColRef.get())
          .docs
          .map((doc) => RewardModel.fromDoc(ds: doc))
          .toList();
      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
