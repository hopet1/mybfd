import 'package:MyBFD/models/GoalModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IGoalService {
  Future<void> createGoal({
    @required String uid,
    @required GoalModel goal,
  });

  Future<GoalModel> retrieveGoal({
    @required String uid,
    @required String goalID,
  });

  Future<List<GoalModel>> listGoals({
    @required String uid,
  });

  Future<Stream<QuerySnapshot>> streamGoals({
    @required String uid,
  });

  Future<void> updateGoal({
    @required String uid,
    @required String goalID,
    @required Map<String, dynamic> data,
  });

  Future<void> deleteGoal({
    @required String uid,
    @required String goalID,
  });
}

class GoalService extends IGoalService {
  final CollectionReference _usersColRef =
      FirebaseFirestore.instance.collection('Users');
  static const String GOALS_COLLECTION = 'goals';

  @override
  Future<void> createGoal({
    @required String uid,
    @required GoalModel goal,
  }) async {
    try {
      DocumentReference goalDocRef =
          _usersColRef.doc(uid).collection(GOALS_COLLECTION).doc();
      goal.id = goalDocRef.id;

      await goalDocRef.set(goal.toMap());

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<GoalModel> retrieveGoal({
    @required String uid,
    @required String goalID,
  }) async {
    try {
      DocumentSnapshot goalDocSnapshot = (await _usersColRef
          .doc(uid)
          .collection(GOALS_COLLECTION)
          .doc(goalID)
          .get());

      final GoalModel goal = GoalModel.fromDoc(ds: goalDocSnapshot);

      return goal;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<GoalModel>> listGoals({@required String uid}) async {
    try {
      List<GoalModel> goals =
          (await _usersColRef.doc(uid).collection(GOALS_COLLECTION).get())
              .docs
              .map((doc) => GoalModel.fromDoc(ds: doc))
              .toList();

      return goals;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> updateGoal({
    @required String uid,
    @required String goalID,
    @required Map<String, dynamic> data,
  }) async {
    try {
      await _usersColRef
          .doc(uid)
          .collection(GOALS_COLLECTION)
          .doc(goalID)
          .update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteGoal(
      {@required String uid, @required String goalID}) async {
    try {
      await _usersColRef
          .doc(uid)
          .collection(GOALS_COLLECTION)
          .doc(goalID)
          .delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<Stream<QuerySnapshot>> streamGoals({@required String uid}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);

      final CollectionReference goalsColRef = userDocRef.collection('goals');

      return goalsColRef.snapshots();
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
