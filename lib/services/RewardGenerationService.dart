import 'package:MyBFD/models/ModulePassedModel.dart';
import 'package:MyBFD/models/RewardModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'AuthService.dart';

abstract class IRewardGenerationService {
  void generateCode();
}

class RewardGenerationService extends IRewardGenerationService {
  final CollectionReference _rewardsColRef =
      FirebaseFirestore.instance.collection('Rewards');
  final CollectionReference _userColRef =
      FirebaseFirestore.instance.collection('Users');
  CollectionReference _userRewardsRef, _userCompletedRef;
  String _uid;
  List<RewardModel> _defRewards;
  List<ModulePassedModel> _modulesPassed;

  RewardGenerationService();

  Future<void> init() async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    _uid = user.uid;
    _userRewardsRef = _userColRef.doc(_uid).collection('rewards');
    _userCompletedRef = _userColRef.doc(_uid).collection('modules');
    _defRewards = (await _rewardsColRef.get())
        .docs
        .map((doc) => RewardModel.fromDoc(ds: doc))
        .toList();
    _modulesPassed = (await _userCompletedRef.get())
        .docs
        .map((doc) => ModulePassedModel.fromDoc(doc: doc))
        .toList();
  }

  void generateRewards() {
    List<String> moduleIds = [];
    _modulesPassed.forEach((element) {
      moduleIds.add(element.moduleId);
    });
    _defRewards.forEach((element) {
      if (moduleIds.contains(element.moduleId)) {
        element.code = element.code + _uid.substring(0, 4).toUpperCase();
        addReward(element.id, element.toMap());
      }
    });
  }

  void updateReward(
      {@required String id, @required Map<String, dynamic> data}) {
    _userRewardsRef.doc(id).update(data);
  }

  void addReward(String id, Map<String, dynamic> data) async {
    List<RewardModel> items = (await _userRewardsRef.get())
        .docs
        .map((doc) => RewardModel.fromDoc(ds: doc))
        .toList();
    List<String> ids = [];
    items.forEach((element) {
      ids.add(element.id);
    });
    if (!ids.contains(id)) {
      DocumentReference ref = await _userRewardsRef.add(data);
      data.addAll({"currId": ref.id});
      updateReward(id: ref.id, data: data);
    }
  }

  @override
  void generateCode() async {
    await init();
    generateRewards();
  }
}
