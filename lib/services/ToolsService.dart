import 'package:MyBFD/models/NetWorthHolderModel.dart';
import 'package:MyBFD/models/NetWorthItemModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IToolsService {
  Future<void> saveNetWorthItems({
    @required String collection,
    @required String uid,
    @required List<NetWorthItemModel> items,
  });

  Future<List<NetWorthItemModel>> listNetWorthItems({
    @required String collection,
    @required String uid,
  });

  Future<void> saveNetWorthHolders({
    @required String collection,
    @required String uid,
    @required List<NetWorthItemModel> items,
  });

  Future<List<NetWorthItemModel>> listNetWorthHolders({
    @required String collection,
    @required String uid,
  });
}

class ToolsService extends IToolsService {
  final CollectionReference _usersColRef =
      FirebaseFirestore.instance.collection('Users');

  @override
  Future<List<NetWorthItemModel>> listNetWorthItems(
      {@required String collection, @required String uid}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);

      final CollectionReference itemsColRef = userDocRef.collection(collection);

      List<NetWorthItemModel> items = (await itemsColRef.get())
          .docs
          .map(
            (doc) => NetWorthItemModel.fromDoc(ds: doc),
          )
          .toList();

      return items;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> saveNetWorthItems(
      {@required String collection,
      @required String uid,
      @required List<NetWorthItemModel> items}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      final DocumentReference userDocRef = _usersColRef.doc(uid);

      final CollectionReference itemsColRef = userDocRef.collection(collection);

      final List<DocumentSnapshot> itemsDocSnap =
          (await itemsColRef.get()).docs;

      //Delete all previous assets.
      for (int i = 0; i < itemsDocSnap.length; i++) {
        final DocumentSnapshot itemDocSnap = itemsDocSnap[i];

        itemDocSnap.reference.delete();
      }

      //Save new assets.
      for (int i = 0; i < items.length; i++) {
        final DocumentReference itemDocRef = itemsColRef.doc();

        final NetWorthItemModel item = items[i];

        batch.set(
          itemDocRef,
          item.toMap(),
        );
      }

      batch.commit();
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<NetWorthItemModel>> listNetWorthHolders({
    @required String collection,
    @required String uid,
  }) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);

      final CollectionReference itemsColRef = userDocRef.collection(collection);

      List<NetWorthHolderModel> netWorthHolders = (await itemsColRef.get())
          .docs
          .map(
            (doc) => NetWorthHolderModel.fromDoc(ds: doc),
          )
          .toList();

      netWorthHolders.sort((a, b) => a.position.compareTo(b.position));

      List<NetWorthItemModel> netWorthItems = netWorthHolders
          .map((netWorthHolder) => netWorthHolder.netWorthItem)
          .toList();

      return netWorthItems;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> saveNetWorthHolders({
    @required String collection,
    @required String uid,
    @required List<NetWorthItemModel> items,
  }) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      final DocumentReference userDocRef = _usersColRef.doc(uid);

      final CollectionReference netWorthHolderColRef =
          userDocRef.collection(collection);

      final List<DocumentSnapshot> netWorthHoldersDocSnap =
          (await netWorthHolderColRef.get()).docs;

      //Delete all previous items.
      for (int i = 0; i < netWorthHoldersDocSnap.length; i++) {
        final DocumentSnapshot netWorthHolderDocSnap =
            netWorthHoldersDocSnap[i];

        netWorthHolderDocSnap.reference.delete();
      }

      //Save new items.
      for (int i = 0; i < items.length; i++) {
        final DocumentReference netWorthHolderDocRef =
            netWorthHolderColRef.doc();

        NetWorthHolderModel netWorthHolder = NetWorthHolderModel(
          position: i,
          netWorthItem: items[i],
        );

        batch.set(
          netWorthHolderDocRef,
          netWorthHolder.toMap(),
        );
      }

      batch.commit();
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
