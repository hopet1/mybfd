import 'package:MyBFD/models/LessonScoreModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/models/ModuleScoreModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/UserService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IScoreService {
  Future<List<LessonScoreModel>> getPassedLessonScoresForModule({
    @required String uid,
    @required String moduleID,
  });

  Future<Map<UserModel, List<ModuleModel>>> getPassedModulesForUser(
      {@required String uid});

  Future<void> updateLessonScore({
    @required String uid,
    @required LessonScoreModel lessonScore,
  });

  Future<LessonScoreModel> getLessonScore({
    @required String uid,
    @required String lessonID,
  });

  Future<void> updateModuleScore({
    @required String uid,
    @required ModuleScoreModel moduleScore,
  });

  Future<ModuleScoreModel> getModuleScore({
    @required String uid,
    @required String moduleID,
  });
}

class ScoreService extends IScoreService {
  final CollectionReference _usersColRef =
      FirebaseFirestore.instance.collection('Users');

  @override
  Future<void> updateLessonScore({
    @required String uid,
    @required LessonScoreModel lessonScore,
  }) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);

      DocumentReference lessonDocRef;
      if (lessonScore.id == null) {
        lessonDocRef = userDocRef.collection('lessons').doc();
        lessonScore.id = lessonDocRef.id;
        lessonDocRef.set(lessonScore.toMap());
      } else {
        lessonDocRef = userDocRef.collection('lessons').doc(lessonScore.id);
        lessonDocRef.update(lessonScore.toMap());
      }

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<LessonScoreModel> getLessonScore({
    @required String uid,
    @required String lessonID,
  }) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);
      final QuerySnapshot lessonQuerySnaps = (await userDocRef
          .collection('lessons')
          .where('lessonID', isEqualTo: '$lessonID')
          .get());

      final List<QueryDocumentSnapshot> lessonQueryDocSnaps =
          lessonQuerySnaps.docs;

      if (lessonQueryDocSnaps.isEmpty) return null;

      final DocumentSnapshot lessonDoc =
          await lessonQueryDocSnaps.first.reference.get();

      return LessonScoreModel.fromDoc(doc: lessonDoc);
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<ModuleScoreModel> getModuleScore(
      {@required String uid, @required String moduleID}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);
      final QuerySnapshot moduleQuerySnaps = (await userDocRef
          .collection('modules')
          .where('moduleID', isEqualTo: '$moduleID')
          .get());

      final List<QueryDocumentSnapshot> moduleQueryDocSnaps =
          moduleQuerySnaps.docs;

      if (moduleQueryDocSnaps.isEmpty) return null;

      final DocumentSnapshot moduleDoc =
          await moduleQueryDocSnaps.first.reference.get();

      return ModuleScoreModel.fromDoc(doc: moduleDoc);
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> updateModuleScore(
      {@required String uid, @required ModuleScoreModel moduleScore}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);

      DocumentReference moduleDocRef;
      if (moduleScore.id == null) {
        moduleDocRef = userDocRef.collection('modules').doc();
        moduleScore.id = moduleDocRef.id;
        moduleDocRef.set(moduleScore.toMap());
      } else {
        moduleDocRef = userDocRef.collection('modules').doc(moduleScore.id);
        moduleDocRef.update(moduleScore.toMap());
      }

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<LessonScoreModel>> getPassedLessonScoresForModule(
      {@required String uid, @required String moduleID}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);
      final QuerySnapshot lessonQuerySnaps = (await userDocRef
          .collection('lessons')
          .where('moduleID', isEqualTo: '$moduleID')
          .where('passed', isEqualTo: true)
          .get());

      final List<QueryDocumentSnapshot> lessonQueryDocSnaps =
          lessonQuerySnaps.docs;

      if (lessonQueryDocSnaps.isEmpty) return [];

      List<LessonScoreModel> lessonScores =
          lessonQueryDocSnaps.map((lessonQueryDocSnap) {
        return LessonScoreModel.fromDoc(doc: lessonQueryDocSnap);
      }).toList();

      return lessonScores;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<Map<UserModel, List<ModuleModel>>> getPassedModulesForUser(
      {@required String uid}) async {
    try {
      final DocumentReference userDocRef = _usersColRef.doc(uid);
      final QuerySnapshot moduleQuerySnaps = (await userDocRef
          .collection('modules')
          .where('passed', isEqualTo: true)
          .get());

      final List<QueryDocumentSnapshot> moduleQueryDocSnaps =
          moduleQuerySnaps.docs;

      UserModel user = await locator<UserService>().retrieveUser(uid: uid);

      if (moduleQueryDocSnaps.isEmpty) return {user: []};

      List<ModuleModel> passedModules =
          moduleQuerySnaps.docs.map((moduleQuerySnap) {
        return ModuleModel.fromDoc(doc: moduleQuerySnap);
      }).toList();

      return {user: passedModules};
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
