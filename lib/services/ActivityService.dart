import 'package:MyBFD/models/ActivityModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IActivityService {
  Future<void> createActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required ActivityModel activity});

  Future<List<ActivityModel>> retrieveActivities({
    @required String courseID,
    @required String moduleID,
    @required String lessonID,
  });

  Future<void> updateActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String activityID,
      @required Map<String, dynamic> data});

  Future<void> deleteActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String activityID});
}

class ActivityService extends IActivityService {
  final CollectionReference _courseColRef =
      FirebaseFirestore.instance.collection('Courses');

  @override
  Future<void> createActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required ActivityModel activity}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference activityDocRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('activities')
          .doc();

      activity.id = activityDocRef.id;

      batch.set(activityDocRef, activity.toMap());

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String activityID}) async {
    try {
      await _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('activities')
          .doc(activityID)
          .delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<ActivityModel>> retrieveActivities({
    @required String courseID,
    @required String moduleID,
    @required String lessonID,
  }) async {
    try {
      final CollectionReference activitiesColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('activities');

      Query query = activitiesColRef;

      // if (limit != null) {
      //   query = query.limit(limit);
      // }

      // if (orderBy != null) {
      //   query = query.orderBy(orderBy);
      // }

      return (await query.get())
          .docs
          .map(
            (doc) => ActivityModel.fromDoc(
              doc: doc,
            ),
          )
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateActivity(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String activityID,
      @required Map<String, dynamic> data}) async {
    try {
      final CollectionReference activitiesColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('activities');
      await activitiesColRef.doc(activityID).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
