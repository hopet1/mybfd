import 'package:MyBFD/models/ModuleModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IModuleService {
  Future<void> createModule(
      {@required String courseID, @required ModuleModel module});

  Future<List<ModuleModel>> retrieveModules({
    @required String courseID,
  });

  Future<void> updateModule(
      {@required String courseID,
      @required String moduleID,
      @required Map<String, dynamic> data});

  Future<void> deleteModule(
      {@required String courseID, @required String moduleID});
}

class ModuleService extends IModuleService {
  final CollectionReference _courseColRef =
      FirebaseFirestore.instance.collection('Courses');

  @override
  Future<void> createModule(
      {@required String courseID, @required ModuleModel module}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference moduleDocRef =
          _courseColRef.doc(courseID).collection('modules').doc();

      module.id = moduleDocRef.id;

      batch.set(moduleDocRef, module.toMap());

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteModule(
      {@required String courseID, @required String moduleID}) async {
    try {
      await _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<ModuleModel>> retrieveModules({
    @required String courseID,
  }) async {
    try {
      final CollectionReference modulesColRef =
          _courseColRef.doc(courseID).collection('modules');

      Query query = modulesColRef;

      // if (limit != null) {
      //   query = query.limit(limit);
      // }

      // if (orderBy != null) {
      //   query = query.orderBy(orderBy);
      // }

      return (await query.get())
          .docs
          .map(
            (doc) => ModuleModel.fromDoc(
              doc: doc,
            ),
          )
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateModule(
      {@required String courseID,
      @required String moduleID,
      @required Map<String, dynamic> data}) async {
    try {
      final CollectionReference modulesColRef =
          _courseColRef.doc(courseID).collection('modules');
      await modulesColRef.doc(moduleID).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

// @override
// Future<void> createUser({@required UserModel user}) async {
//   try {
//     final WriteBatch batch = FirebaseFirestore.instance.batch();

//     DocumentReference userDocRef = _usersColRef.doc(user.uid);

//     batch.set(userDocRef, user.toMap());

//     batch.update(_tableCountsDocRef, {
//       'users': FieldValue.increment(1),
//     });

//     await batch.commit();

//     return;
//   } catch (e) {
//     throw Exception(
//       e.toString(),
//     );
//   }
// }

// @override
// Future<UserModel> retrieveUser({@required String uid}) async {
//   try {
//     DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
//     return UserModel.fromDoc(ds: documentSnapshot);
//   } catch (e) {
//     throw Exception(e.toString());
//   }
// }

// @override
// Future<void> updateUser(
//     {@required String uid, @required Map<String, dynamic> data}) async {
//   try {
//     DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
//     DocumentReference documentReference = documentSnapshot.reference;
//     await documentReference.update(data);
//     return;
//   } catch (e) {
//     throw Exception(
//       e.toString(),
//     );
//   }
// }

// @override
// Future<List<UserModel>> retrieveUsers({int limit, String orderBy}) async {
//   try {
//     Query query = _usersColRef;

//     if (limit != null) {
//       query = query.limit(limit);
//     }

//     if (orderBy != null) {
//       query = query.orderBy(orderBy);
//     }

//     return (await query.get())
//         .docs
//         .map((doc) => UserModel.fromDoc(ds: doc))
//         .toList();
//   } catch (e) {
//     throw Exception(e.toString());
//   }
// }
}
