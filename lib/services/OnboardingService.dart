import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IOnboardingService {
  Future<void> saveAnswers(
      {@required String uid,
      @required List<Map<String, String>> onboardingQuestions});
}

class OnboardingService extends IOnboardingService {
  final CollectionReference _usersColRef =
      FirebaseFirestore.instance.collection('Users');

  @override
  Future<void> saveAnswers(
      {@required String uid,
      @required List<Map<String, String>> onboardingQuestions}) async {
    try {
      DocumentReference userDocRef = _usersColRef.doc(uid);

      await userDocRef.update({'onboardingQuestions': onboardingQuestions});

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
