import 'package:MyBFD/models/UserModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IUserService {
  Future<void> createUser({@required UserModel user});

  Future<UserModel> retrieveUser({@required String uid});

  Future<List<UserModel>> retrieveUsers({int limit, String orderBy});

  Future<void> updateUser(
      {@required String uid, @required Map<String, dynamic> data});
}

class UserService extends IUserService {
  final CollectionReference _usersColRef =
      FirebaseFirestore.instance.collection('Users');
  final DocumentReference _tableCountsDocRef =
      FirebaseFirestore.instance.collection('Data').doc('tableCounts');

  @override
  Future<void> createUser({@required UserModel user}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference userDocRef = _usersColRef.doc(user.uid);

      batch.set(userDocRef, user.toMap());

      batch.update(_tableCountsDocRef, {
        'users': FieldValue.increment(1),
      });

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<UserModel> retrieveUser({@required String uid}) async {
    try {
      DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
      return UserModel.fromDoc(ds: documentSnapshot);
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateUser(
      {@required String uid, @required Map<String, dynamic> data}) async {
    try {
      DocumentSnapshot documentSnapshot = await _usersColRef.doc(uid).get();
      DocumentReference documentReference = documentSnapshot.reference;
      await documentReference.update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<UserModel>> retrieveUsers({int limit, String orderBy}) async {
    try {
      Query query = _usersColRef;

      if (limit != null) {
        query = query.limit(limit);
      }

      if (orderBy != null) {
        query = query.orderBy(orderBy);
      }

      return (await query.get())
          .docs
          .map((doc) => UserModel.fromDoc(ds: doc))
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
