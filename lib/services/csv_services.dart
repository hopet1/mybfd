import 'dart:convert';
import 'dart:typed_data';

import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/UserService.dart';
import 'package:csv/csv.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:universal_io/io.dart';

abstract class ICSV {
  Future<List<List<dynamic>>> pickCSVAndExtractData();
  Future<Stream<List<int>>> pickFile();
  Future<void> createUsersFromCSV(UserModel userModel, String password);
}

class CSVServices extends ICSV {
  @override
  Future<List<List<dynamic>>> pickCSVAndExtractData() async {
    List<List<dynamic>> data = [];

    Stream<List<int>> myFile = await pickFile();

    if (myFile == null) {
      return null;
    }

    var input = myFile;

    final fields = await input
        .transform(utf8.decoder)
        .transform(CsvToListConverter())
        .toList();
    if (fields.isNotEmpty) {
      data = fields;
    }

    // return data if it's not null
    if (fields.every((element) => element.length != 5)) {
      print("========== Wrong csv format =======");
      return null;
    } else {
      return data;
    }
  }

  @override
  Future<Stream<List<int>>> pickFile() async {
    FilePickerResult result = await FilePicker.platform
        .pickFiles(allowedExtensions: ['csv'], type: FileType.custom);
    Stream<List<int>> myFile;
    if (result != null) {
      if (Platform.isIOS || Platform.isAndroid) {
        PlatformFile file = result.files.first;
        if (file == null) {
          return null;
        }
        myFile = File(file.path).openRead();
      } else {
        Uint8List fileBytes = result.files.first.bytes;
        if (fileBytes == null) {
          return null;
        }
        myFile = Stream.value(fileBytes);
      }
    }

    return myFile;
  }

  @override
  Future<void> createUsersFromCSV(UserModel userModel, String password) async {
    print("===registering:${userModel.email}===");
    final UserCredential userCredential = await locator<AuthService>()
        .createUserWithEmailAndPassword(
            email: userModel.email, password: password);
    final User user = userCredential.user;
    userModel.uid = user.uid;

    await locator<UserService>().createUser(user: userModel);
  }
}
