import 'package:MyBFD/models/PrefModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class IPrefService {
  Future<PrefModel> getPrefs();
  Future<void> updatePrefs(PrefModel model);
}

class PrefService extends IPrefService {
  final CollectionReference _userColRef =
      FirebaseFirestore.instance.collection('Users');
  CollectionReference _prefColRef;

  @override
  Future<PrefModel> getPrefs() async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    String uid = user.uid;
    _prefColRef = _userColRef.doc(uid).collection('preferences');
    try {
      List<PrefModel> items = (await _prefColRef.get())
          .docs
          .map((doc) => PrefModel.fromDoc(ds: doc))
          .toList();
      return items.first;
    } catch (e) {
      bool created = await createPrefs();
      if (created) {
        return await getPrefs();
      }
    }
    return null;
  }

  Future<bool> createPrefs() async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    String uid = user.uid;
    _prefColRef = _userColRef.doc(uid).collection('preferences');
    List<dynamic> items = (await _prefColRef.get()).docs.toList();
    if (items.isEmpty) {
      PrefModel newModel = new PrefModel(uid: uid, prefs: {});
      DocumentReference doc = await _prefColRef.add(newModel.toMap());
      newModel.prefId = doc.id;
      updatePrefs(newModel);
      return true;
    } else {
      return false;
    }
  }

  //Working on model based right now,
  @override
  Future<void> updatePrefs(PrefModel model) async {
    UserModel user = await locator<AuthService>().getCurrentUser();
    String uid = user.uid;
    _prefColRef = _userColRef.doc(uid).collection('preferences');
    DocumentReference doc = _prefColRef.doc(model.prefId);
    doc.update(model.toMap()).then((value) => print("Prefs updated"));
  }
}
