import 'package:MyBFD/models/QuestionModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class IQuestionService {
  Future<void> createQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required QuestionModel question});

  Future<List<QuestionModel>> retrieveQuestions({
    @required String courseID,
    @required String moduleID,
    @required String lessonID,
  });

  Future<void> updateQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String questionID,
      @required Map<String, dynamic> data});

  Future<void> deleteQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String questionID});
}

class QuestionService extends IQuestionService {
  final CollectionReference _courseColRef =
      FirebaseFirestore.instance.collection('Courses');

  @override
  Future<void> createQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required QuestionModel question}) async {
    try {
      final WriteBatch batch = FirebaseFirestore.instance.batch();

      DocumentReference questionDocRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('questions')
          .doc();

      question.id = questionDocRef.id;

      batch.set(questionDocRef, question.toMap());

      await batch.commit();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<void> deleteQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String questionID}) async {
    try {
      await _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('questions')
          .doc(questionID)
          .delete();

      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }

  @override
  Future<List<QuestionModel>> retrieveQuestions({
    @required String courseID,
    @required String moduleID,
    @required String lessonID,
  }) async {
    try {
      final CollectionReference questionsColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('questions');

      Query query = questionsColRef;

      // if (limit != null) {
      //   query = query.limit(limit);
      // }

      // if (orderBy != null) {
      //   query = query.orderBy(orderBy);
      // }

      return (await query.get())
          .docs
          .map(
            (doc) => QuestionModel.fromDoc(
              doc: doc,
            ),
          )
          .toList();
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> updateQuestion(
      {@required String courseID,
      @required String moduleID,
      @required String lessonID,
      @required String questionID,
      @required Map<String, dynamic> data}) async {
    try {
      final CollectionReference questionsColRef = _courseColRef
          .doc(courseID)
          .collection('modules')
          .doc(moduleID)
          .collection('lessons')
          .doc(lessonID)
          .collection('questions');
      await questionsColRef.doc(questionID).update(data);
      return;
    } catch (e) {
      throw Exception(
        e.toString(),
      );
    }
  }
}
