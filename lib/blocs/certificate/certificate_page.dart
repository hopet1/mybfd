part of 'certificate_bloc.dart';

class CertificatePage extends StatefulWidget {
  @override
  _CertificatePageState createState() => _CertificatePageState();
}

class _CertificatePageState extends State<CertificatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Topbar(backArrowTap: () {
            Navigator.of(context).pop();
          }),
          BlocConsumer<CertificateBloc, CertificateState>(
            builder: (context, state) {
              if (state is LoadingState) {
                return new CircularProgressIndicator(
                  backgroundColor: Colors.white,
                  valueColor: new AlwaysStoppedAnimation<Color>(colorOrange),
                );
              }
              if (state is CertificateInitial) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "Certificate of Completion",
                      style: TextStyle(
                        color: colorOrange,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                    SizedBox(height: 28),
                    Container(
                      //color: Colors.grey.shade200,
                      child: Padding(
                        padding: EdgeInsets.all(42),
                        child: state.widget,
                      ),
                    ),
                    (!kIsWeb)
                        ? Container(
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, -3),
                                blurRadius: 6.0,
                              ),
                            ]),
                            child: Padding(
                              padding: EdgeInsets.only(top: 36),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      ActionButton(
                                        function: CertificateWidget.share,
                                        icon: Icons.share_outlined,
                                        action: "Share",
                                      ),
                                      ActionButton(
                                        function: CertificateWidget.view,
                                        icon: Icons.remove_red_eye_outlined,
                                        action: "View",
                                      ),
                                      // ActionButton(
                                      //   function: CertificateWidget.getMail,
                                      //   icon: Icons.mail_outline,
                                      //   action: "Get Mail",
                                      // ),
                                    ],
                                  ),
                                  SizedBox(height: 72),
                                  Text(
                                    "Verified by",
                                    style: TextStyle(
                                        color: colorOrange, fontSize: 16),
                                  ),
                                  Image.asset(ASSET_ICON_FULL, height: 64),
                                ],
                              ),
                            ),
                          )
                        : Container(
                            child: Column(
                              children: [
                                Text(
                                  "Verified by",
                                  style: TextStyle(
                                      color: colorOrange, fontSize: 16),
                                ),
                                Image.asset(ASSET_ICON_FULL, height: 64),
                              ],
                            ),
                          ),
                  ],
                );
              }
            },
            listener: (context, state) {},
          ),
        ],
      ),
    );
  }
}

class ActionButton extends StatelessWidget {
  final IconData icon;
  final String action;
  final VoidCallback function;

  const ActionButton({Key key, this.icon, this.action, this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: function,
          child: Container(
            height: 72,
            width: 72,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(38),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(4, 4),
                    blurRadius: 4.0,
                  ),
                ]),
            child: Icon(
              this.icon,
              color: colorOrange,
              size: 42,
            ),
          ),
        ),
        SizedBox(height: 18),
        Text(
          this.action,
          style: TextStyle(
            color: colorOrange,
            fontSize: 16,
          ),
        )
      ],
    );
  }
}
