part of 'certificate_bloc.dart';

abstract class CertificateState extends Equatable {
  const CertificateState();

  @override
  List<Object> get props => [];
}

class CertificateInitial extends CertificateState {
  final Widget widget;

  CertificateInitial(this.widget);
}

class LoadingState extends CertificateState {}
