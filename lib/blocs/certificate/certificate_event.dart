part of 'certificate_bloc.dart';

abstract class CertificateEvent extends Equatable {
  const CertificateEvent();

  @override
  List<Object> get props => [];
}

class InitialEvent extends CertificateEvent {
  final String name;
  final String module;

  InitialEvent(this.name, this.module);
}

class LoadingEvent extends CertificateEvent {}
