import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/widgets/CertificateWidget.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'certificate_event.dart';
part 'certificate_state.dart';
part 'certificate_page.dart';

class CertificateBloc extends Bloc<CertificateEvent, CertificateState> {
  CertificateBloc() : super(CertificateInitial(null));

  @override
  Stream<CertificateState> mapEventToState(
    CertificateEvent event,
  ) async* {
    //Initial event
    if (event is InitialEvent) {
      yield LoadingState();
      //Working logic
      final certificate = CertificateWidget(
        name: event.name,
        module: event.module,
      );
      yield CertificateInitial(certificate);
    }
  }
}
