part of 'networth_bloc.dart';

class NetworthPage extends StatefulWidget {
  @override
  State createState() => _NetworthPageState();
}

class _NetworthPageState extends State<NetworthPage>
    implements NetworthBlocDelegate {
  Map<String, dynamic> _uniqueScrollViewKeys = Map<String, dynamic>();

  @override
  void initState() {
    context.read<NetworthBloc>().setDelegate(delegate: this);
    super.initState();
  }

  bool _allNetWorthItemModelsHaveBeenSelected(
      {@required List<NetWorthItemModel> items,
      @required String notSelectedName}) {
    bool allNetWorthItemModelsHaveBeenSelected = true;

    for (int i = 0; i < items.length; i++) {
      NetWorthItemModel item = items[i];
      if (item.name == notSelectedName) {
        allNetWorthItemModelsHaveBeenSelected = false;
        break;
      }
    }

    return allNetWorthItemModelsHaveBeenSelected;
  }

  @override
  Widget build(BuildContext context) {
    final double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<NetworthBloc, NetworthState>(
        builder: (context, state) {
          if (state is NetworthInitial) {
            return Spinner();
          }

          if (state is AssetsState) {
            final List<NetWorthItemModel> assets = state.assets;
            final double assetsTotal = state.assetsTotal;

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'ASSETS',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(backArrowTap: () {
                  Navigator.of(context).pop();
                }),
                Column(
                  children: [
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: Image.asset(
                            ASSET_BACK_DARK,
                            height: Responsive.isMobile(context) ? 30 : 50,
                            color: Colors.white,
                          ),
                        ),
                        Column(
                          children: [
                            Text(
                              'Assets',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? 24
                                          : 30),
                            ),
                            SizedBox(height: 6.0),
                            Text(
                              'Step 1 of 3',
                              style: TextStyle(
                                fontSize:
                                    Responsive.isMobile(context) ? 18 : 24,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          child: Image.asset(
                            ASSET_FORWARD_DARK,
                            height: Responsive.isMobile(context) ? 30 : 50,
                          ),
                          onTap: () {
                            if (_allNetWorthItemModelsHaveBeenSelected(
                                items: assets,
                                notSelectedName: ASSETS_LIST[0])) {
                              context.read<NetworthBloc>().add(
                                    GoToLiabilitiesEvent(),
                                  );
                            } else {
                              locator<ModalService>().showInSnackBar(
                                  context: context,
                                  message:
                                      'Error, all assets must have an item name.');
                            }
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 24.0),
                    Container(
                      width: _screenWidth * 0.9,
                      child: Column(
                        children: [
                          Container(
                            width: Responsive.isMobile(context)
                                ? _screenWidth * 0.9
                                : _screenWidth * 0.6,
                            color: colorOrange,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Item Worth',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  Text(
                                    'Item Name',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: Responsive.isMobile(context)
                                ? _screenWidth * 0.9
                                : _screenWidth * 0.6,
                            color: Colors.grey.shade300,
                            child: Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: Column(
                                children: [
                                  Column(
                                    children: assets
                                        .map(
                                          (asset) => NetWorthItemWidget(
                                            netWorthItem: asset,
                                            netWorthItemOptions: ASSETS_LIST,
                                            updateSelectedNetWorthItemEvent:
                                                () {
                                              context.read<NetworthBloc>().add(
                                                    UpdateAssetEvent(
                                                        asset: asset),
                                                  );
                                            },
                                          ),
                                        )
                                        .toList(),
                                  ),
                                  SizedBox(height: 24.0),
                                  Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        RawMaterialButton(
                                          onPressed: () {
                                            if (_allNetWorthItemModelsHaveBeenSelected(
                                                items: assets,
                                                notSelectedName:
                                                    ASSETS_LIST[0])) {
                                              context.read<NetworthBloc>().add(
                                                    AddAssetEvent(
                                                      asset: NetWorthItemModel(
                                                        amount: 0.00,
                                                        name: ASSETS_LIST[0],
                                                      ),
                                                    ),
                                                  );
                                            } else {
                                              locator<ModalService>()
                                                  .showInSnackBar(
                                                      context: context,
                                                      message:
                                                          'Error, all assets must have an item name.');
                                            }
                                          },
                                          elevation: 2.0,
                                          fillColor: Colors.white,
                                          child: Icon(
                                            Icons.add,
                                            size: 18.0,
                                          ),
                                          shape: CircleBorder(),
                                        ),
                                        //CHECK: Why is there space here?
                                        Text(
                                          'Add item',
                                          style: TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w200,
                                            color: colorDark,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 36.0),
                    Container(
                      width: Responsive.isMobile(context)
                          ? _screenWidth * 0.9
                          : _screenWidth * 0.6,
                      color: Colors.grey.shade300,
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 24.0),
                          child: Column(
                            children: [
                              Text(
                                'Assets Total',
                                style: TextStyle(
                                    color: colorDark,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(height: 12.0),
                              Text(
                                Money.from(
                                  assetsTotal,
                                  Currency.create(
                                    'GBP',
                                    2,
                                    symbol: '£',
                                    // pattern: 'S0,000.00',
                                  ),
                                ).toString(),
                                style: TextStyle(
                                    color: colorDark,
                                    fontSize: 36.0,
                                    fontWeight: FontWeight.w200),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 48.0),
                    RoundedButton(
                      text: 'Liabilities',
                      foreground: Color(colorOrange.value),
                      background: Colors.white,
                      onTap: () {
                        context
                            .read<NetworthBloc>()
                            .add(GoToLiabilitiesEvent());
                      },
                    ),
                  ],
                ),
                SizedBox(height: 200),
              ],
            );
          }

          if (state is LiabilitiesState) {
            final List<NetWorthItemModel> liabilities = state.liabilities;
            final double liabilitiesTotal = state.liabilitiesTotal;

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'LIABILITIES',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(backArrowTap: () {
                  Navigator.of(context).pop();
                }),
                Column(
                  children: [
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: Image.asset(
                            ASSET_BACK_DARK,
                            height: Responsive.isMobile(context) ? 36 : 56,
                          ),
                          onTap: () {
                            context.read<NetworthBloc>().add(
                                  GoToAssetsEvent(),
                                );
                          },
                        ),
                        Column(
                          children: [
                            Text(
                              'Liabilities',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? 24
                                          : 30),
                            ),
                            SizedBox(height: 6.0),
                            Text(
                              'Step 2 of 3',
                              style: TextStyle(
                                fontSize:
                                    Responsive.isMobile(context) ? 18 : 24,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          child: Image.asset(
                            ASSET_FORWARD_DARK,
                            height: Responsive.isMobile(context) ? 36 : 56,
                          ),
                          onTap: () {
                            context.read<NetworthBloc>().add(
                                  GoToSummaryEvent(),
                                );
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 24.0),
                    Container(
                      width: _screenWidth * 0.8,
                      child: Column(
                        children: [
                          Container(
                            width: Responsive.isMobile(context)
                                ? _screenWidth * 0.8
                                : _screenWidth * 0.6,
                            color: colorOrange,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Amount Owed',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? 18
                                          : 24,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  Text(
                                    'Item Name',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? 18
                                          : 24,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: Responsive.isMobile(context)
                                ? _screenWidth * 0.8
                                : _screenWidth * 0.6,
                            color: Colors.grey.shade300,
                            child: Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: Column(
                                children: [
                                  Column(
                                    children: liabilities
                                        .map(
                                          (liability) => NetWorthItemWidget(
                                            netWorthItem: liability,
                                            netWorthItemOptions:
                                                LIABILITIES_LIST,
                                            updateSelectedNetWorthItemEvent:
                                                () {
                                              context.read<NetworthBloc>().add(
                                                    UpdateLiabilityEvent(
                                                        liability: liability),
                                                  );
                                            },
                                          ),
                                        )
                                        .toList(),
                                  ),
                                  SizedBox(height: 24.0),
                                  Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        RawMaterialButton(
                                          onPressed: () {
                                            context.read<NetworthBloc>().add(
                                                  AddLiabilityEvent(
                                                    liability:
                                                        NetWorthItemModel(
                                                      amount: 0.00,
                                                      name: LIABILITIES_LIST[0],
                                                    ),
                                                  ),
                                                );
                                          },
                                          elevation: 2.0,
                                          fillColor: Colors.white,
                                          child: Icon(
                                            Icons.add,
                                            size: Responsive.isMobile(context)
                                                ? 18
                                                : 24,
                                          ),
                                          shape: CircleBorder(),
                                        ),
                                        //CHECK: Why is there space here?
                                        Text(
                                          'Add item',
                                          style: TextStyle(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? 18
                                                    : 24,
                                            fontWeight: FontWeight.w200,
                                            color: colorDark,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 36.0),
                    Container(
                      width: Responsive.isMobile(context)
                          ? _screenWidth * 0.8
                          : _screenWidth * 0.6,
                      color: Colors.grey.shade300,
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 24.0),
                          child: Column(
                            children: [
                              Text(
                                'Liabilities Total',
                                style: TextStyle(
                                    color: colorDark,
                                    fontSize:
                                        Responsive.isMobile(context) ? 18 : 24,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(height: 12.0),
                              Text(
                                Money.from(
                                  liabilitiesTotal,
                                  Currency.create(
                                    'GBP',
                                    2,
                                    symbol: '£',
                                    // pattern: 'S0,000.00',
                                  ),
                                ).toString(),
                                style: TextStyle(
                                    color: colorDark,
                                    fontSize:
                                        Responsive.isMobile(context) ? 36 : 46,
                                    fontWeight: FontWeight.w200),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 48.0),
                    RoundedButton(
                      text: 'Summary',
                      foreground: Color(colorOrange.value),
                      background: Colors.white,
                      onTap: () {
                        context.read<NetworthBloc>().add(GoToSummaryEvent());
                      },
                    ),
                  ],
                ),
                SizedBox(height: 200),
              ],
            );
          }

          if (state is SummaryState) {
            final String assets = Money.from(
              state.assetTotal,
              Currency.create(
                'GBP',
                2,
                symbol: '£',
                // pattern: 'S0,000.00',
              ),
            ).toString();

            final String liabilities = Money.from(
              state.liabilityTotal,
              Currency.create(
                'GBP',
                2,
                symbol: '£',
                // pattern: 'S0,000.00',
              ),
            ).toString();

            final String total = Money.from(
              state.result,
              Currency.create(
                'GBP',
                2,
                symbol: '£',
                // pattern: 'S0,000.00',
              ),
            ).toString();

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'SUMMARY',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(backArrowTap: () {
                  Navigator.of(context).pop();
                }),
                Column(
                  children: [
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          child: Image.asset(
                            ASSET_BACK_DARK,
                            height: Responsive.isMobile(context) ? 36 : 56,
                          ),
                          onTap: () {
                            context.read<NetworthBloc>().add(
                                  GoToLiabilitiesEvent(),
                                );
                          },
                        ),
                        Column(
                          children: [
                            Text(
                              'Summary',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? 24
                                          : 30),
                            ),
                            SizedBox(height: 6.0),
                            Text(
                              'Step 3 of 3',
                              style: TextStyle(
                                fontSize:
                                    Responsive.isMobile(context) ? 18 : 24,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          child: Image.asset(
                            ASSET_FORWARD_DARK,
                            height: Responsive.isMobile(context) ? 36 : 56,
                            color: Colors.white,
                          ),
                          onTap: () {},
                        )
                      ],
                    ),
                    SizedBox(height: 24.0),
                    Container(
                      width: Responsive.isMobile(context)
                          ? _screenWidth * 0.8
                          : _screenWidth * 0.6,
                      child: Column(
                        children: [
                          Container(
                            width: _screenWidth * 0.8,
                            color: colorOrange,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(
                                'Summary Total',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize:
                                      Responsive.isMobile(context) ? 20 : 26,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: Responsive.isMobile(context)
                                ? _screenWidth * 0.8
                                : _screenWidth * 0.6,
                            color: Colors.grey.shade300,
                            child: Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Total Assets',
                                        style: TextStyle(
                                          fontSize: Responsive.isMobile(context)
                                              ? 16
                                              : 22,
                                        ),
                                      ),
                                      Text(
                                        assets,
                                        style: TextStyle(
                                          fontSize: Responsive.isMobile(context)
                                              ? 16
                                              : 22,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 12.0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Total Liabilities',
                                        style: TextStyle(
                                          fontSize: Responsive.isMobile(context)
                                              ? 16
                                              : 22,
                                        ),
                                      ),
                                      Text(
                                        liabilities,
                                        style: TextStyle(
                                          fontSize: Responsive.isMobile(context)
                                              ? 16
                                              : 22,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            color: colorOrange,
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 12.0),
                              child: (Center(
                                child: Column(
                                  children: [
                                    Text(
                                      'Your Net Worth is:',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: Responsive.isMobile(context)
                                            ? 16
                                            : 22,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text(
                                      total,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: Responsive.isMobile(context)
                                            ? 24
                                            : 30,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )
                                  ],
                                ),
                              )),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 48.0),
                    RoundedButton(
                      text: 'Reset Calculator',
                      foreground: Color(colorOrange.value),
                      background: Colors.white,
                      onTap: () {
                        context
                            .read<NetworthBloc>()
                            .add(ResetCalculatorEvent());
                      },
                    ),
                    SizedBox(height: 12.0),
                    RoundedButton(
                      text: 'Save Data',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () async {
                        final bool confirm =
                            await locator<ModalService>().showConfirmation(
                          context: context,
                          title: 'Save Data',
                          message: 'Are you sure?',
                        );

                        if (!confirm) return;

                        context.read<NetworthBloc>().add(
                              SaveCalculatorEvent(),
                            );
                      },
                    ),
                  ],
                ),
                SizedBox(height: 200),
              ],
            );
          }

          if (state is ErrorState) {
            final String errorMessage =
                state.error.message ?? 'Could not fetch data at this time.';

            return ListView(
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(backArrowTap: () {
                  Navigator.of(context).pop();
                }),
                Text(
                  'Error',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: Responsive.isMobile(context) ? 24 : 30),
                ),
                Expanded(
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        '$errorMessage',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              ],
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }

  @override
  void showMessage({@required String message}) {
    locator<ModalService>().showAlert(
      context: context,
      title: 'Success',
      message: 'Data saved.',
    );
  }
}
