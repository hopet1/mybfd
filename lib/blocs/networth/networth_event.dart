part of 'networth_bloc.dart';

abstract class NetworthEvent extends Equatable {
  const NetworthEvent();

  @override
  List<Object> get props => [];
}

class LoadPageEvent extends NetworthEvent {}

class GoToAssetsEvent extends NetworthEvent {}

class AddAssetEvent extends NetworthEvent {
  final NetWorthItemModel asset;

  const AddAssetEvent({
    @required this.asset,
  });

  @override
  List<Object> get props => [
        asset,
      ];
}

class UpdateAssetEvent extends NetworthEvent {
  final NetWorthItemModel asset;

  const UpdateAssetEvent({
    @required this.asset,
  });

  @override
  List<Object> get props => [
        asset,
      ];
}

class GoToLiabilitiesEvent extends NetworthEvent {}

class AddLiabilityEvent extends NetworthEvent {
  final NetWorthItemModel liability;

  const AddLiabilityEvent({
    @required this.liability,
  });

  @override
  List<Object> get props => [
        liability,
      ];
}

class UpdateLiabilityEvent extends NetworthEvent {
  final NetWorthItemModel liability;

  const UpdateLiabilityEvent({
    @required this.liability,
  });

  @override
  List<Object> get props => [
        liability,
      ];
}

class GoToSummaryEvent extends NetworthEvent {}

class ResetCalculatorEvent extends NetworthEvent {}

class SaveCalculatorEvent extends NetworthEvent {}
