part of 'networth_bloc.dart';

abstract class NetworthState extends Equatable {
  const NetworthState();

  @override
  List<Object> get props => [];
}

class NetworthInitial extends NetworthState {}

class AssetsState extends NetworthState {
  final List<NetWorthItemModel> assets;
  final double assetsTotal;

  const AssetsState({
    @required this.assets,
    @required this.assetsTotal,
  });

  @override
  List<Object> get props => [
        assets,
        assetsTotal,
      ];
}

class LiabilitiesState extends NetworthState {
  final List<NetWorthItemModel> liabilities;
  final double liabilitiesTotal;

  const LiabilitiesState({
    @required this.liabilities,
    @required this.liabilitiesTotal,
  });

  @override
  List<Object> get props => [
        liabilities,
        liabilitiesTotal,
      ];
}

class SummaryState extends NetworthState {
  final double result;
  final double assetTotal;
  final double liabilityTotal;

  const SummaryState({
    @required this.result,
    @required this.assetTotal,
    @required this.liabilityTotal,
  });

  @override
  List<Object> get props => [
        result,
        assetTotal,
        liabilityTotal,
      ];
}

class ErrorState extends NetworthState {
  final dynamic error;

  ErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
