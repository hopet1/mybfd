import 'dart:async';

import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/NetWorthItemModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ToolsService.dart';
import 'package:MyBFD/services/WidgetService.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/NetWorthItemWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:money2/money2.dart';
import 'package:uuid/uuid.dart';

import '../../constants.dart';

part 'networth_event.dart';
part 'networth_page.dart';
part 'networth_state.dart';

abstract class NetworthBlocDelegate {
  void showMessage({@required String message});
}

class NetworthBloc extends Bloc<NetworthEvent, NetworthState> {
  NetworthBloc() : super(NetworthInitial());

  NetworthBlocDelegate _networthBlocDelegate;

  void setDelegate({@required NetworthBlocDelegate delegate}) {
    this._networthBlocDelegate = delegate;
  }

  List<NetWorthItemModel> _assets = [
    NetWorthItemModel(
      amount: 0,
      name: ASSETS_LIST[0],
    )
  ];

  List<NetWorthItemModel> _liabilities = [
    NetWorthItemModel(
      amount: 0,
      name: LIABILITIES_LIST[0],
    )
  ];

  double _assetsTotal = 0;
  double _liabilitiesTotal = 0;
  double _total = 0;

  UserModel _currentUser;

  static const String ASSETS_NAME = 'assets';
  static const String LIABILITIES_NAME = 'liabilities';

  double _calculateTotal() {
    _total = 0;
    _total = _assetsTotal - _liabilitiesTotal;
  }

  @override
  Stream<NetworthState> mapEventToState(
    NetworthEvent event,
  ) async* {
    yield NetworthInitial();

    if (event is LoadPageEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();

        List<NetWorthItemModel> assets =
            await locator<ToolsService>().listNetWorthItems(
          collection: ASSETS_NAME,
          uid: _currentUser.uid,
        );

        _assets = assets.isEmpty
            ? [
                NetWorthItemModel(
                  amount: 0,
                  name: ASSETS_LIST[0],
                )
              ]
            : assets;

        List<NetWorthItemModel> liabilities =
            await locator<ToolsService>().listNetWorthItems(
          collection: LIABILITIES_NAME,
          uid: _currentUser.uid,
        );

        _liabilities = liabilities.isEmpty
            ? [
                NetWorthItemModel(
                  amount: 0,
                  name: LIABILITIES_LIST[0],
                )
              ]
            : liabilities;
        add(GoToAssetsEvent());
      } catch (error) {
        yield ErrorState(error: error);
      }
    }

    if (event is GoToAssetsEvent) {
      yield AssetsState(assets: _assets, assetsTotal: _assetsTotal);
    }

    if (event is AddAssetEvent) {
      final NetWorthItemModel asset = event.asset;

      _assets.add(asset);

      yield AssetsState(assets: _assets, assetsTotal: _assetsTotal);
    }

    if (event is UpdateAssetEvent) {
      final NetWorthItemModel asset = event.asset;

      _assets[_assets.indexOf(asset)] = asset;

      _assetsTotal = 0;

      _assets.forEach(
        (asset) {
          _assetsTotal += asset.amount;
        },
      );

      yield AssetsState(assets: _assets, assetsTotal: _assetsTotal);
    }

    if (event is GoToLiabilitiesEvent) {
      yield LiabilitiesState(
          liabilities: _liabilities, liabilitiesTotal: _liabilitiesTotal);
    }

    if (event is AddLiabilityEvent) {
      final NetWorthItemModel liability = event.liability;

      _liabilities.add(liability);

      yield LiabilitiesState(
          liabilities: _liabilities, liabilitiesTotal: _liabilitiesTotal);
    }

    if (event is UpdateLiabilityEvent) {
      final NetWorthItemModel liability = event.liability;

      _liabilities[_liabilities.indexOf(liability)] = liability;

      _liabilitiesTotal = 0;

      _liabilities.forEach(
        (liability) {
          _liabilitiesTotal += liability.amount;
        },
      );

      yield LiabilitiesState(
          liabilities: _liabilities, liabilitiesTotal: _liabilitiesTotal);
    }

    if (event is GoToSummaryEvent) {
      _assetsTotal = 0;

      _assets.forEach(
        (asset) {
          _assetsTotal += asset.amount;
        },
      );

      _liabilitiesTotal = 0;

      _liabilities.forEach(
        (liability) {
          _liabilitiesTotal += liability.amount;
        },
      );

      _calculateTotal();

      yield SummaryState(
        result: _total,
        assetTotal: _assetsTotal,
        liabilityTotal: _liabilitiesTotal,
      );
    }

    if (event is ResetCalculatorEvent) {
      _assets = [
        NetWorthItemModel(
          amount: 0,
          name: ASSETS_LIST[0],
        )
      ];

      _liabilities = [
        NetWorthItemModel(
          amount: 0,
          name: LIABILITIES_LIST[0],
        )
      ];

      add(
        GoToAssetsEvent(),
      );
    }

    if (event is SaveCalculatorEvent) {
      await locator<ToolsService>().saveNetWorthItems(
        collection: ASSETS_NAME,
        uid: _currentUser.uid,
        items: _assets,
      );

      await locator<ToolsService>().saveNetWorthItems(
        collection: LIABILITIES_NAME,
        uid: _currentUser.uid,
        items: _liabilities,
      );

      _networthBlocDelegate.showMessage(message: 'Data saved.');

      _calculateTotal();

      yield SummaryState(
        result: _total,
        assetTotal: _assetsTotal,
        liabilityTotal: _liabilitiesTotal,
      );
    }
  }
}
