// import 'package:MyBFD/blocs/needhelp/needhelp_bloc.dart';
// import 'package:MyBFD/constants.dart';
// import 'package:MyBFD/extensions/BezierClipper.dart';
// import 'package:MyBFD/services/ModalService.dart';
// import 'package:MyBFD/services/ValidationService.dart';
// import 'package:MyBFD/widgets/RoundedButton.dart';
// import 'package:MyBFD/widgets/Spinner.dart';
// import 'package:bloc/bloc.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// import '../../constants.dart';
// import '../../service_locator.dart';
//
// part 'forgotpassword_event.dart';
// part 'forgotpassword_page.dart';
// part 'forgotpassword_state.dart';
//
// abstract class ForgotPasswordBlocDelegate {
//   void showMessage({@required String message});
// }
//
// class ForgotPasswordBloc
//     extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
//   ForgotPasswordBloc()
//       : super(
//           LoadedState(),
//         );
//
//   ForgotPasswordBlocDelegate _forgotPasswordBlocDelegate;
//
//   void setDelegate({@required ForgotPasswordBlocDelegate delegate}) {
//     this._forgotPasswordBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<ForgotPasswordState> mapEventToState(
//       ForgotPasswordEvent event) async* {
//     if (event is ForgotPassword) {
//       final String input = event.input;
//
//       try {
//         yield LoadingState();
//
//         //TODO: Forgot Password
//
//         yield EmailSent();
//         print('Input string: $input');
//       } catch (error) {
//         _forgotPasswordBlocDelegate.showMessage(
//             message: 'Error: ${error.toString()}');
//         yield ErrorState();
//       }
//     }
//
//     if (event is NoEmailEvent) {
//       yield NoEmail();
//     }
//
//     if (event is ResendEmail) {
//       //TODO: Implement resend email
//     }
//   }
// }
