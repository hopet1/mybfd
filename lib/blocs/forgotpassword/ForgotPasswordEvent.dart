import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ForgotPasswordEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ForgotPassword extends ForgotPasswordEvent {
  final String input;

  ForgotPassword({@required this.input});

  List<Object> get props => [input];
}

class NoEmailEvent extends ForgotPasswordEvent {}

class ResendEmail extends ForgotPasswordEvent {}

class TryAgain extends ForgotPasswordEvent {}
