import 'package:MyBFD/blocs/forgotpassword/ForgotPasswordBloc.dart';
import 'package:MyBFD/blocs/forgotpassword/ForgotPasswordEvent.dart';
import 'package:MyBFD/blocs/forgotpassword/ForgotPasswordState.dart';
import 'package:MyBFD/blocs/needhelp/needhelp_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import '../../service_locator.dart';
// import 'Bloc.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  State createState() => ForgotPasswordPageState();
}

class ForgotPasswordPageState extends State<ForgotPasswordPage>
    implements ForgotPasswordBlocDelegate {
  final TextEditingController _inputController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final double _titleFontSize = 60;

  @override
  void initState() {
    context.read<ForgotPasswordBloc>().setDelegate(delegate: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorOrange,
      body: Form(
        key: _formKey,
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: [
            ClipPath(
              clipper: BezierClipper(),
              child: Container(
                color: Colors.white,
                width: double.infinity,
                height: 400,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        _inputController.text = 'trey.a.hope@gmail.com';
                      },
                      child: Image.asset(
                        ASSET_ICON_TRANSPARENT,
                        height: 100,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'my',
                            style: TextStyle(
                              fontSize: _titleFontSize,
                              fontWeight: FontWeight.bold,
                              color: colorDark,
                            ),
                          ),
                          TextSpan(
                            text: 'bfd',
                            style: TextStyle(
                              fontSize: _titleFontSize,
                              fontWeight: FontWeight.bold,
                              color: colorOrange,
                            ),
                          ),
                          TextSpan(
                            text: '\nFinancial Education App'.toUpperCase(),
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2.0,
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            BlocConsumer<ForgotPasswordBloc, ForgotPasswordState>(
                builder: (context, state) {
                  if (state is LoadingState) {
                    return Spinner(
                      color: Colors.white70,
                    );
                  }

                  if (state is LoadedState) {
                    return Expanded(
                      child: Column(
                        // padding: const EdgeInsets.all(20),
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "Lost Password".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: Responsive.isMobile(context)
                                          ? 28
                                          : 36.0),
                                ),
                                SizedBox(
                                  height: 40.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    '$FORGOT_PASSWORD_MESSAGE',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: Responsive.isMobile(context)
                                            ? 12
                                            : 16),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    validator:
                                        locator<ValidationService>().isEmpty,
                                    controller: _inputController,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          // width: 0.0 produces a thin "hairline" border
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(90.0),
                                          ),
                                          borderSide: BorderSide.none,

                                          //borderSide: const BorderSide(),
                                        ),
                                        hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "WorkSansLight"),
                                        filled: true,
                                        fillColor: Colors.white24,
                                        hintText: 'User ID / Email Address'),
                                  ),
                                ),
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Reset Password',
                            foreground: colorOrange,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              context.read<ForgotPasswordBloc>().add(
                                    ForgotPassword(
                                      input: _inputController.text,
                                    ),
                                  );
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                // locator<ModalService>().showAlert(
                                //   context: context,
                                //   title: 'To Do',
                                //   message: 'Go back to sign in',
                                // );
                                Phoenix.rebirth(context);
                              },
                              child: Text(
                                'Back to sign in',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        NeedhelpBloc(),
                                    child: NeedhelpPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: Text(
                                'Need Help? Contact Us.',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    );
                  }

                  if (state is EmailSent) {
                    return Expanded(
                      child: Column(
                        // padding: const EdgeInsets.all(20),
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "email sent".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: Responsive.isMobile(context)
                                          ? 28
                                          : 36.0),
                                ),
                                SizedBox(
                                  height: 40.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    '$EMAIL_SENT_MESSAGE',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: Responsive.isMobile(context)
                                            ? 12
                                            : 16),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Back To Sign In',
                            foreground: colorOrange,
                            onTap: () async {
                              Phoenix.rebirth(context);
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                context
                                    .read<ForgotPasswordBloc>()
                                    .add(NoEmailEvent());
                              },
                              child: Text(
                                'Didn\'t receive an email?',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        NeedhelpBloc(),
                                    child: NeedhelpPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: Text(
                                'Need Help? Contact Us.',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    );
                  }

                  if (state is NoEmail) {
                    return Expanded(
                      child: Column(
                        // padding: const EdgeInsets.all(20),
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "email sent".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 36.0),
                                ),
                                SizedBox(
                                  height: 40.0,
                                ),
                                Text(
                                  '$EMAIL_SENT_MESSAGE',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Resend Email',
                            foreground: colorOrange,
                            onTap: () async {
                              context
                                  .read<ForgotPasswordBloc>()
                                  .add(ResendEmail());
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                // locator<ModalService>().showAlert(
                                //   context: context,
                                //   title: 'To Do',
                                //   message: 'Go back to sign in',
                                // );
                                Phoenix.rebirth(context);
                              },
                              child: Text(
                                'Back To Sign In',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        NeedhelpBloc(),
                                    child: NeedhelpPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: Text(
                                'Need Help? Contact Us.',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }
                  if (state is ErrorState) {
                    final String errorMessage =
                        'Failed to send password reset link to your provided email';

                    return Center(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: Text(
                              '$errorMessage',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 21,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          RoundedButton(
                            background: colorOrange,
                            foreground: Colors.white,
                            onTap: () {
                              context
                                  .read<ForgotPasswordBloc>()
                                  .add(TryAgain());
                            },
                            text: 'Try Again?',
                          ),
                        ],
                      ),
                    );
                  }
                  return Container();
                },
                listener: (context, index) {})
          ],
        ),
      ),
    );
  }

  @override
  void showMessage({String message}) {
    locator<ModalService>().showInSnackBar(context: context, message: message);
    // scaffoldKey: _scaffoldKey;
  }
}

class BezierClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height * 0.85); //vertical line
    path.cubicTo(size.width / 3, size.height, 2 * size.width / 3,
        size.height * 0.7, size.width, size.height * 0.85); //cubic curve
    path.lineTo(size.width, 0); //vertical line
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
