import 'package:MyBFD/blocs/forgotpassword/Bloc.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

abstract class ForgotPasswordBlocDelegate {
  void showMessage({@required String message});
}

class ForgotPasswordBloc
    extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  ForgotPasswordBloc()
      : super(
          LoadedState(),
        );

  ForgotPasswordBlocDelegate _forgotPasswordBlocDelegate;

  void setDelegate({@required ForgotPasswordBlocDelegate delegate}) {
    this._forgotPasswordBlocDelegate = delegate;
  }

  @override
  Stream<ForgotPasswordState> mapEventToState(
      ForgotPasswordEvent event) async* {
    if (event is ForgotPassword) {
      final String input = event.input;
      print("input: $input");
      try {
        yield LoadingState();

        await locator<AuthService>().resetPassword(email: input);

        yield EmailSent();
        print('Input string: $input');
      } catch (error) {
        _forgotPasswordBlocDelegate.showMessage(
            message: 'Error: ${error.toString()}');
        yield ErrorState();
      }
    }
    if (event is TryAgain) {
      yield LoadedState();
    }
    if (event is NoEmailEvent) {
      yield NoEmail();
    }

    if (event is ResendEmail) {
      yield LoadedState(); // TODO: Maybe send reset link directly
    }
  }
}
