part of 'account_bloc.dart';

abstract class AccountState extends Equatable {
  const AccountState();

  @override
  List<Object> get props => [];
}

class AccountInitialState extends AccountState {}

class AccountLoadingState extends AccountState {}

class AccountLoadedState extends AccountState {
  final UserModel currentUser;
  final bool isAdmin;

  const AccountLoadedState({
    @required this.currentUser,
    @required this.isAdmin,
  });

  @override
  List<Object> get props => [
        currentUser,
      ];
}

class ChangeAccountState extends AccountState {
  final UserModel currentUser;

  const ChangeAccountState({
    @required this.currentUser,
  });

  @override
  List<Object> get props => [
        currentUser,
      ];
}

class ChangeAccountSuccessState extends AccountState {}

class ChangePasswordState extends AccountState {
  final UserModel currentUser;

  const ChangePasswordState({
    @required this.currentUser,
  });

  @override
  List<Object> get props => [
        currentUser,
      ];
}

class ChangePasswordSuccessState extends AccountState {}

class ChangeEmailState extends AccountState {
  final UserModel currentUser;

  const ChangeEmailState({
    @required this.currentUser,
  });

  @override
  List<Object> get props => [
        currentUser,
      ];
}

class ChangeEmailSuccessState extends AccountState {}

class ReadTermsAndConditionsState extends AccountState {
  const ReadTermsAndConditionsState();

  @override
  List<Object> get props => [];
}

class AccountErrorState extends AccountState {
  final dynamic error;

  AccountErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
