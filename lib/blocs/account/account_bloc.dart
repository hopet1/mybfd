import 'dart:async';

import 'package:MyBFD/blocs/admin/admin_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AdminService.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/UserService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'account_event.dart';
part 'account_page.dart';
part 'account_state.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  AccountBloc() : super(AccountInitialState());

  UserModel _currentUser;
  bool _isAdmin = false;

  @override
  Stream<AccountState> mapEventToState(
    AccountEvent event,
  ) async* {
    yield AccountLoadingState();

    if (event is LoadAccountEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();
        _isAdmin = await locator<AdminService>().isAdmin(_currentUser.uid);

        yield AccountLoadedState(currentUser: _currentUser, isAdmin: _isAdmin);
      } catch (error) {
        yield AccountErrorState(error: error);
      }
    }

    if (event is ChangeDetailsEvent) {
      yield ChangeAccountState(currentUser: _currentUser);
    }

    if (event is NavigateToAccountLoadedEvent) {
      yield AccountLoadedState(currentUser: _currentUser, isAdmin: _isAdmin);
    }

    if (event is SubmitChangeDetailsEvent) {
      final String firstName = event.firstName;
      final String lastName = event.lastName;
      final String username = event.username;
      final String phoneNumber = event.phoneNumber;
      final String streetAddress = event.streetAddress;
      final String city = event.city;
      final String postCode = event.postCode;

      try {
        await locator<UserService>().updateUser(
          uid: _currentUser.uid,
          data: {
            'modified': DateTime.now(),
            'firstName': firstName,
            'lastName': lastName,
            'username': username,
            'phoneNumber': phoneNumber,
            'streetAddress': streetAddress,
            'city': city,
            'postCode': postCode,
          },
        );

        yield ChangeAccountSuccessState();
      } catch (error) {
        yield AccountErrorState(error: error);
      }
    }

    if (event is ChangePasswordEvent) {
      yield ChangePasswordState(currentUser: _currentUser);
    }

    if (event is SubmitChangePasswordEvent) {
      final String password = event.password;

      try {
        await locator<AuthService>().updatePassword(
          password: password,
        );

        await locator<UserService>().updateUser(
          uid: _currentUser.uid,
          data: {
            'modified': DateTime.now(),
          },
        );

        yield ChangePasswordSuccessState();
      } catch (error) {
        yield AccountErrorState(error: error);
      }
    }

    if (event is ReadTermsAndConditionsEvent) {
      yield ReadTermsAndConditionsState();
    }

    if (event is ChangeEmailEvent) {
      yield ChangeEmailState(currentUser: _currentUser);
    }

    if (event is SubmitChangeEmailEvent) {
      final String email = event.email;

      try {
        await locator<AuthService>().updateEmail(
          newEmail: email,
        );

        await locator<UserService>().updateUser(
          uid: _currentUser.uid,
          data: {
            'modified': DateTime.now(),
            'email': email,
          },
        );

        yield ChangeEmailSuccessState();
      } catch (error) {
        yield AccountErrorState(error: error);
      }
    }
  }
}
