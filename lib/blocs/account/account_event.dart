part of 'account_bloc.dart';

abstract class AccountEvent extends Equatable {
  const AccountEvent();

  @override
  List<Object> get props => [];
}

class LoadAccountEvent extends AccountEvent {}

class ChangeDetailsEvent extends AccountEvent {}

class SubmitChangeDetailsEvent extends AccountEvent {
  final String firstName;
  final String lastName;
  final String username;
  final String phoneNumber;
  final String streetAddress;
  final String city;
  final String postCode;

  const SubmitChangeDetailsEvent({
    @required this.firstName,
    @required this.lastName,
    @required this.username,
    @required this.phoneNumber,
    @required this.streetAddress,
    @required this.city,
    @required this.postCode,
  });

  @override
  List<Object> get props => [
        firstName,
        lastName,
        username,
        phoneNumber,
        streetAddress,
        city,
        postCode,
      ];
}

class ChangePasswordEvent extends AccountEvent {}

class NavigateToAccountLoadedEvent extends AccountEvent {}

class SubmitChangePasswordEvent extends AccountEvent {
  final String password;

  const SubmitChangePasswordEvent({
    @required this.password,
  });

  @override
  List<Object> get props => [
        password,
      ];
}

class ChangeEmailEvent extends AccountEvent {}

class SubmitChangeEmailEvent extends AccountEvent {
  final String email;

  const SubmitChangeEmailEvent({
    @required this.email,
  });

  @override
  List<Object> get props => [
        email,
      ];
}

class ReadTermsAndConditionsEvent extends AccountEvent {}
