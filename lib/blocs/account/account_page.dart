part of 'account_bloc.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _streetAddressController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _postCodeController = TextEditingController();
  TextEditingController _changePasswordToController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _changeEmailToController = TextEditingController();
  bool _passwordHidden, _confirmPasswordHidden;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _setTextEditingControllers(
      {@required UserModel currentUser, @required bool isEdit}) {
    if (isEdit) {
      _firstNameController.text = currentUser.firstName ?? '(No first name)';
      _lastNameController.text = currentUser.lastName ?? '(No last name)';
      _usernameController.text = currentUser.username;
      _phoneController.text = currentUser.phoneNumber ?? '';
      _streetAddressController.text = currentUser.streetAddress ?? '';
      _cityController.text = currentUser.city ?? '';
      _postCodeController.text = currentUser.postCode ?? '';
    } else {
      _firstNameController.text = currentUser.firstName ?? '(No first name)';
      _lastNameController.text = currentUser.lastName ?? '(No last name)';
      _usernameController.text = currentUser.username;
      _phoneController.text = currentUser.phoneNumber ?? '(No phone number)';
      _streetAddressController.text =
          currentUser.streetAddress ?? '(No street address)';
      _cityController.text = currentUser.city ?? '(No city)';
      _postCodeController.text = currentUser.postCode ?? '(No post code)';
    }
  }

  void _clearPasswordFields() {
    _formKey.currentState.reset();
    _changePasswordToController.clear();
    _confirmPasswordController.clear();
  }

  void _clearEmailFields() {
    _formKey.currentState.reset();
    _emailController.clear();
    _changeEmailToController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AccountBloc, AccountState>(
        builder: (context, state) {
          if (state is AccountLoadingState) {
            _passwordHidden = true;
            _confirmPasswordHidden = true;
            return Spinner();
          }

          if (state is ReadTermsAndConditionsState) {
            return PageStructureWidget(
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Terms & Conditions',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Text('$TERMS_AND_CONDITIONS_P1'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P2'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P3'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P4'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P5'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P6'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P7'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P8'),
                SizedBox(
                  height: 20,
                ),
                Text('$TERMS_AND_CONDITIONS_P9'),
              ],
            );
          }

          if (state is AccountLoadedState) {
            final UserModel currentUser = state.currentUser;

            _setTextEditingControllers(
              currentUser: currentUser,
              isEdit: false,
            );

            return PageStructureWidget(
              children: [
                Text(
                  'Account',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 42),
                Text(
                  'Your First Name',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _firstNameController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Your Last Name',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _lastNameController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Your Username',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _usernameController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Phone Number',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _phoneController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Street Address',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(
                    controller: _streetAddressController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Town/City',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _cityController, editable: false),
                SizedBox(height: 18),
                Text(
                  'Post Code',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w200,
                  ),
                ),
                AccountField(controller: _postCodeController, editable: false),
                SizedBox(height: 42),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          ChangeDetailsEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Change Details',
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          ChangeEmailEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Change Email',
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          ChangePasswordEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Change Password',
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          ReadTermsAndConditionsEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Read T&Cs',
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () {
                    Route route = MaterialPageRoute(
                      builder: (BuildContext context) => BlocProvider(
                        create: (BuildContext context) => AdminBloc()
                          ..add(
                            LoadAdminEvent(),
                          ),
                        child: AdminPage(),
                      ),
                    );
                    Navigator.push(context, route);
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Admin',
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Signout',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    await locator<AuthService>().signOut();

                    Navigator.of(context).popUntil((route) => route.isFirst);
                  },
                  foreground: Colors.white,
                  background: colorOrange,
                  text: 'Signout',
                ),
                SizedBox(height: 20),
              ],
            );
          }

          if (state is ChangeAccountState) {
            final UserModel currentUser = state.currentUser;

            _setTextEditingControllers(
              currentUser: currentUser,
              isEdit: true,
            );

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () async {
                  final bool confirm =
                      await locator<ModalService>().showConfirmation(
                    context: context,
                    title: 'Cancel Changes',
                    message: 'Are you sure?',
                  );

                  if (!confirm) return;
                  context
                      .read<AccountBloc>()
                      .add(NavigateToAccountLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Change Account',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Your First Name',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _firstNameController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Your Last Name',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _lastNameController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Your Username',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _usernameController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Phone Number',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _phoneController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Street Address',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _streetAddressController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Town/City',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(controller: _cityController, editable: true),
                      SizedBox(height: 18),
                      Text(
                        'Post Code',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w200,
                        ),
                      ),
                      AccountField(
                          controller: _postCodeController, editable: true),
                    ],
                  ),
                ),
                SizedBox(height: 48),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 56.0, vertical: 18),
                  child: RoundedButton(
                    onTap: () async {
                      if (!_formKey.currentState.validate()) return;

                      final bool confirm =
                          await locator<ModalService>().showConfirmation(
                        context: context,
                        title: 'Update Account',
                        message: 'Are you sure?',
                      );

                      if (!confirm) return;

                      context.read<AccountBloc>().add(
                            SubmitChangeDetailsEvent(
                              firstName: _firstNameController.text,
                              lastName: _lastNameController.text,
                              username: _usernameController.text,
                              phoneNumber: _phoneController.text,
                              streetAddress: _streetAddressController.text,
                              city: _cityController.text,
                              postCode: _postCodeController.text,
                            ),
                          );
                    },
                    foreground: Colors.white,
                    background: colorOrange,
                    text: 'Save Changes',
                  ),
                ),
              ],
            );
          }

          if (state is ChangeAccountSuccessState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Account Changes',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 28.0,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 48),
                  child: Text(
                    '$ACCOUNT_UPDATE_SUCCESS_MESSAGE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                SizedBox(height: 32),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          LoadAccountEvent(),
                        );
                  },
                  foreground: Colors.white,
                  background: colorOrange,
                  text: 'Back to Account',
                ),
              ],
            );
          }

          if (state is ChangePasswordState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () async {
                  final bool confirm =
                      await locator<ModalService>().showConfirmation(
                    context: context,
                    title: 'Cancel Changes',
                    message: 'Are you sure?',
                  );

                  if (!confirm) return;
                  context
                      .read<AccountBloc>()
                      .add(NavigateToAccountLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Change Password',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          if (!Responsive.isMobile(context))
                            Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
                          Expanded(
                            flex: 3,
                            child: Column(
                              children: [
                                //To Do: Add obscure text stuff that Divyansh added on sign up page.
                                Text(
                                  'Change Password To',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w200,
                                  ),
                                ),
                                SizedBox(height: 6.0),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1,
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: Responsive.isMobile(context)
                                          ? 16.0
                                          : 32,
                                      vertical: 5.0,
                                    ),
                                    child: TextFormField(
                                      autovalidateMode:
                                          AutovalidateMode.onUserInteraction,
                                      validator:
                                          locator<ValidationService>().password,
                                      controller: _changePasswordToController,
                                      obscureText: _passwordHidden,
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            _passwordHidden
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: colorOrange,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              _passwordHidden =
                                                  !_passwordHidden;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 24),
                                Text(
                                  'Confirm Password',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w200,
                                  ),
                                ),
                                SizedBox(height: 6.0),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1,
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0,
                                      vertical: 5.0,
                                    ),
                                    child: TextFormField(
                                      autovalidateMode:
                                          AutovalidateMode.onUserInteraction,
                                      validator:
                                          locator<ValidationService>().password,
                                      controller: _confirmPasswordController,
                                      obscureText: _confirmPasswordHidden,
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            _confirmPasswordHidden
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: colorOrange,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              _confirmPasswordHidden =
                                                  !_confirmPasswordHidden;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                RoundedButton(
                                  onTap: () async {
                                    if (!_formKey.currentState.validate())
                                      return;

                                    if (_changePasswordToController.text !=
                                        _confirmPasswordController.text) {
                                      locator<ModalService>().showAlert(
                                        context: context,
                                        title: 'Error',
                                        message: 'Passwords do not match.',
                                      );
                                      return;
                                    }

                                    final bool confirm =
                                        await locator<ModalService>()
                                            .showConfirmation(
                                      context: context,
                                      title: 'Update Password',
                                      message: 'Are you sure?',
                                    );

                                    if (!confirm) return;

                                    context.read<AccountBloc>().add(
                                          SubmitChangePasswordEvent(
                                              password:
                                                  _confirmPasswordController
                                                      .text),
                                        );

                                    _clearPasswordFields();
                                  },
                                  foreground: Colors.white,
                                  background: colorOrange,
                                  text: 'Change Password',
                                ),
                              ],
                            ),
                          ),
                          if (!Responsive.isMobile(context))
                            Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
                        ],
                      ),
                    ))
              ],
            );
          }

          if (state is ChangePasswordSuccessState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AccountBloc>()
                      .add(NavigateToAccountLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Password Changed',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 28.0,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 48),
                  child: Text(
                    '$PASSWORD_UPDATE_SUCCESS_MESSAGE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            );
          }

          if (state is ChangeEmailState) {
            final UserModel currentUser = state.currentUser;

            _emailController.text = currentUser.email;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () async {
                  final bool confirm =
                      await locator<ModalService>().showConfirmation(
                    context: context,
                    title: 'Cancel Changes',
                    message: 'Are you sure?',
                  );

                  if (!confirm) return;
                  context
                      .read<AccountBloc>()
                      .add(NavigateToAccountLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Change Email',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        if (!Responsive.isMobile(context))
                          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
                        Expanded(
                          flex: 3,
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal:
                                    Responsive.isMobile(context) ? 18 : 36),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Current Email'),
                                TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: locator<ValidationService>().email,
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.done,
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Text('Change Email To'),
                                TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: locator<ValidationService>().email,
                                  controller: _changeEmailToController,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.done,
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                              ],
                            ),
                          ),
                        ),
                        if (!Responsive.isMobile(context))
                          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    if (_emailController.text ==
                        _changeEmailToController.text) {
                      locator<ModalService>().showAlert(
                        context: context,
                        title: 'Error',
                        message: 'New email must be different.',
                      );
                      return;
                    }

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Update Email',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AccountBloc>().add(
                          SubmitChangeEmailEvent(
                            email: _changeEmailToController.text,
                          ),
                        );
                    _clearEmailFields();
                  },
                  foreground: Colors.white,
                  background: colorOrange,
                  text: 'Change Email',
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            );
          }

          if (state is ChangeEmailSuccessState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Email Changed',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    '$EMAIL_UPDATE_SUCCESS_MESSAGE',
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 20),
                RoundedButton(
                  onTap: () {
                    context.read<AccountBloc>().add(
                          LoadAccountEvent(),
                        );
                  },
                  foreground: Colors.white,
                  background: colorOrange,
                  text: 'Back To Account',
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            );
          }

          if (state is AccountErrorState) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Error',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Expanded(
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        '$errorMessage',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
              ],
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}

class AccountField extends StatelessWidget {
  const AccountField({
    Key key,
    @required TextEditingController controller,
    @required bool editable,
  })  : _controller = controller,
        _editable = editable,
        super(key: key);

  final TextEditingController _controller;
  final bool _editable;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          if (!Responsive.isMobile(context))
            Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                color: (_editable) ? Colors.white : Colors.grey.shade300,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                border: (_editable)
                    ? Border.all(
                        color: Colors.black,
                        width: 1,
                      )
                    : Border.all(width: 1, color: Colors.grey.shade300),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: Responsive.isMobile(context) ? 18.0 : 36,
                  vertical: 5.0,
                ),
                child: (_editable)
                    ? TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: locator<ValidationService>().isEmpty,
                        controller: _controller,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                      )
                    : TextField(
                        readOnly: true,
                        controller: _controller,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
              ),
            ),
          ),
          if (!Responsive.isMobile(context))
            Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
        ],
      ),
    );
  }
}
