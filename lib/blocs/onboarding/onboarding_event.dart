part of 'onboarding_bloc.dart';

abstract class OnboardingEvent extends Equatable {
  const OnboardingEvent();

  @override
  List<Object> get props => [];
}

class LoadOnboardingEvent extends OnboardingEvent {}

class DisplayQuestionEvent extends OnboardingEvent {}

class NextQuestionEvent extends OnboardingEvent {
  final int selectedIndex;

  const NextQuestionEvent({
    @required this.selectedIndex,
  });

  @override
  List<Object> get props => [
        selectedIndex,
      ];
}
