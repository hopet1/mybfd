import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/OnboardingQuestionModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/pages/onboarding/OBTCSPage.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/OnboardingService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../service_locator.dart';

part 'onboarding_event.dart';
part 'onboarding_page.dart';
part 'onboarding_state.dart';

class OnboardingBloc extends Bloc<OnboardingEvent, OnboardingState> {
  OnboardingBloc() : super(OnboardingInitialState());

  int _currentIndex = 0;
  UserModel _currentUser;

  @override
  Stream<OnboardingState> mapEventToState(
    OnboardingEvent event,
  ) async* {
    yield OnboardingLoadingState();

    if (event is LoadOnboardingEvent) {
      _currentUser = await locator<AuthService>().getCurrentUser();
      add(DisplayQuestionEvent());
    }

    if (event is DisplayQuestionEvent) {
      yield OnboardingLoadedState(
        question: ONBOARDING_QUESTIONS[_currentIndex],
        index: _currentIndex,
      );
    }

    if (event is NextQuestionEvent) {
      if (ONBOARDING_QUESTIONS[_currentIndex].isSingleSelect) {
        ONBOARDING_QUESTIONS[_currentIndex].selectedAnswer =
            ONBOARDING_QUESTIONS[_currentIndex].answers[event.selectedIndex];
      } else {
        for (int i = 0;
            i < ONBOARDING_QUESTIONS[_currentIndex].checkValues.length;
            i++) {
          if (ONBOARDING_QUESTIONS[_currentIndex].checkValues[i]) {
            ONBOARDING_QUESTIONS[_currentIndex].selectedAnswer +=
                '${ONBOARDING_QUESTIONS[_currentIndex].answers[i]}, ';
          }
        }
      }

      _currentIndex++;

      if (_currentIndex == ONBOARDING_QUESTIONS.length) {
        List<Map<String, String>> onboardingQuestions = [];

        ONBOARDING_QUESTIONS.forEach((onboardingQuestion) {
          onboardingQuestions.add(
              {onboardingQuestion.question: onboardingQuestion.selectedAnswer});
        });

        await locator<OnboardingService>().saveAnswers(
            uid: _currentUser.uid, onboardingQuestions: onboardingQuestions);

        yield OnboardingCompleteState();
      } else {
        add(DisplayQuestionEvent());
      }
    }
  }
}
