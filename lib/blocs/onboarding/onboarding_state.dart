part of 'onboarding_bloc.dart';

abstract class OnboardingState extends Equatable {
  const OnboardingState();

  @override
  List<Object> get props => [];
}

class OnboardingInitialState extends OnboardingState {}

class OnboardingLoadingState extends OnboardingState {}

class OnboardingLoadedState extends OnboardingState {
  final OnboardingQuestionModel question;
  final int index;

  const OnboardingLoadedState({
    @required this.question,
    @required this.index,
  });

  @override
  List<Object> get props => [
        question,
        index,
      ];
}

class OnboardingCompleteState extends OnboardingState {}
