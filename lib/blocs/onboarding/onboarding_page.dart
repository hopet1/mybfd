part of 'onboarding_bloc.dart';

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  @override
  void initState() {
    super.initState();
  }

  int _radio = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<OnboardingBloc, OnboardingState>(
        builder: (context, state) {
          if (state is OnboardingLoadingState) {
            return Spinner();
          }

          if (state is OnboardingLoadedState) {
            final OnboardingQuestionModel question = state.question;
            final int index = state.index;

            return SingleChildScrollView(
              child: Container(
                width: double.infinity,
                height: scrHeight(context),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 90,
                    ),
                    Center(
                      child: Image.asset(
                        ASSET_GOALS_ORANGE,
                        height: Responsive.isMobile(context) ? 50 : 100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                        'Questions - ${index + 1}/${ONBOARDING_QUESTIONS.length}',
                        style: Theme.of(context).textTheme.headline1.copyWith(
                            fontSize:
                                Responsive.isMobile(context) ? 16.0 : 18)),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'You\'ll need to answer these before you get started.',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline3.copyWith(
                            fontSize: Responsive.isMobile(context) ? 16.0 : 18),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        '${question.question}',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline2.copyWith(
                            fontSize: Responsive.isMobile(context) ? 18.0 : 20),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (question.isSingleSelect) ...[
                      for (int i = 0; i < question.answers.length; i++) ...[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Radio(
                              value: i,
                              groupValue: _radio,
                              activeColor: colorOrange,
                              onChanged: (value) {
                                setState(() {
                                  _radio = value;
                                });
                              },
                            ),
                            Text(
                              '${question.answers[i]}',
                              style: TextStyle(
                                fontSize:
                                    Responsive.isMobile(context) ? 16.0 : 18,
                              ),
                            )
                          ],
                        ),
                      ],
                    ],
                    if (!question.isSingleSelect) ...[
                      for (int i = 0; i < question.answers.length; i++) ...[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  Responsive.isMobile(context) ? 28 : 50),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(question.answers[i],
                                      style: TextStyle(
                                        fontSize: Responsive.isMobile(context)
                                            ? 16.0
                                            : 18,
                                      ))),
                              Checkbox(
                                value: question.checkValues[i],
                                onChanged: (value) {
                                  setState(() {
                                    question.checkValues[i] = value;
                                  });
                                },
                              )
                            ],
                          ),
                        )
                      ],
                    ],
                    Spacer(),
                    RoundedButton(
                      background: colorOrange,
                      foreground: Colors.white,
                      onTap: () {
                        if (question.isSingleSelect && _radio < 0) return;
                        context.read<OnboardingBloc>().add(
                              NextQuestionEvent(selectedIndex: _radio),
                            );
                        _radio = -1;
                      },
                      text: 'Next',
                    ),
                    ClipPath(
                      clipper: WaveClipperTwo(flip: false, reverse: true),
                      child: Container(
                        height: 100,
                        color: colorOrange,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }

          if (state is OnboardingCompleteState) {
            return SingleChildScrollView(
              child: Container(
                width: double.infinity,
                height: scrHeight(context),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 90,
                    ),
                    Center(
                      child: Image.asset(
                        ASSET_GOALS_ORANGE,
                        height: Responsive.isMobile(context) ? 50 : 100,
                      ),
                    ),
                    SizedBox(
                      height: 28,
                    ),
                    Text('Questions Complete',
                        style: Theme.of(context).textTheme.headline1.copyWith(
                            fontSize:
                                Responsive.isMobile(context) ? 16.0 : 18)),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'Thank you for your patience.',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline3.copyWith(
                            fontSize: Responsive.isMobile(context) ? 14.0 : 16),
                      ),
                    ),
                    Spacer(),
                    RoundedButton(
                      background: colorOrange,
                      foreground: Colors.white,
                      onTap: () {
                        Route route = MaterialPageRoute(
                            builder: (BuildContext context) => OBTCSPage());
                        Navigator.push(context, route);
                      },
                      text: 'Next',
                    ),
                    Spacer(),
                    ClipPath(
                      clipper: WaveClipperTwo(flip: false, reverse: true),
                      child: Container(
                        height: 100,
                        color: colorOrange,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
