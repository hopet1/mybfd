part of 'lesson_bloc.dart';

abstract class LessonState extends Equatable {
  const LessonState();

  @override
  List<Object> get props => [];
}

class LessonInitial extends LessonState {}

class LessonLoading extends LessonState {}

class YouTubeVideoState extends LessonState {
  final String moduleName;
  final String lessonName;
  final String youtubeUrl;
  final bool hasActivities;

  const YouTubeVideoState({
    @required this.moduleName,
    @required this.lessonName,
    @required this.youtubeUrl,
    @required this.hasActivities,
  });

  @override
  List<Object> get props => [
        moduleName,
        lessonName,
        youtubeUrl,
        hasActivities,
      ];
}

class ActivityState extends LessonState {
  final List<ActivityModel> activites;
  final int index;

  const ActivityState({
    @required this.activites,
    @required this.index,
  });

  @override
  List<Object> get props => [
        activites,
        index,
      ];
}

class LessonAttempted extends LessonState {
  final int correctQuestionsCount;
  final int totalQuestionsCount;

  const LessonAttempted({
    @required this.correctQuestionsCount,
    @required this.totalQuestionsCount,
  });

  @override
  List<Object> get props => [
        correctQuestionsCount,
        totalQuestionsCount,
      ];
}

class LessonPassed extends LessonState {
  final int correctQuestionsCount;
  final int totalQuestionsCount;
  final bool moduleFinished;

  const LessonPassed({
    @required this.correctQuestionsCount,
    @required this.totalQuestionsCount,
    this.moduleFinished,
  });

  @override
  List<Object> get props => [
        correctQuestionsCount,
        totalQuestionsCount,
        moduleFinished,
      ];
}

class LessonActive extends LessonState {
  final String question;
  final int questionIndex;
  final List<String> answers;
  final int totalQuestionsCount;
  final int selectedAnswerIndex;

  const LessonActive({
    @required this.question,
    @required this.questionIndex,
    @required this.answers,
    @required this.totalQuestionsCount,
    @required this.selectedAnswerIndex,
  });

  @override
  List<Object> get props => [
        question,
        questionIndex,
        answers,
        totalQuestionsCount,
        selectedAnswerIndex,
      ];
}

class QuestionsError extends LessonState {
  final dynamic error;

  QuestionsError({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
