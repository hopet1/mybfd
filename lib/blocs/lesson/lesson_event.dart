part of 'lesson_bloc.dart';

abstract class LessonEvent extends Equatable {
  const LessonEvent();

  @override
  List<Object> get props => [];
}

class LoadLessonEvent extends LessonEvent {}

class CheckLessonModuleStatus extends LessonEvent {}

class GoToActivityEvent extends LessonEvent {
  final int activityIndex;

  const GoToActivityEvent({
    @required this.activityIndex,
  });

  @override
  List<Object> get props => [
        activityIndex,
      ];
}

class RetakeTestEvent extends LessonEvent {}

class RestartLessonEvent extends LessonEvent {}

class SelectAnswerEvent extends LessonEvent {
  final int selectedAnswerIndex;

  const SelectAnswerEvent({
    @required this.selectedAnswerIndex,
  });

  @override
  List<Object> get props => [
        selectedAnswerIndex,
      ];
}

class NextQuestionEvent extends LessonEvent {
  const NextQuestionEvent();

  @override
  List<Object> get props => [];
}

class PreviousQuestionEvent extends LessonEvent {}

class BuildQuestionEvent extends LessonEvent {}

class GetResultsEvent extends LessonEvent {}
