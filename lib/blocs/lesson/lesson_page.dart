part of 'lesson_bloc.dart';

class LessonPage extends StatefulWidget {
  @override
  State createState() => _LessonPageState();
}

class _LessonPageState extends State<LessonPage> implements LessonBlocDelegate {
  String selectedAnswer;

  @override
  void initState() {
    context.read<LessonBloc>().setDelegate(delegate: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Navbar(),
        body: BlocConsumer<LessonBloc, LessonState>(
          builder: (context, state) {
            if (state is LessonLoading) {
              return Spinner();
            }

            if (state is YouTubeVideoState) {
              final String initialVideoId = getIdFromUrl('${state.youtubeUrl}');
              final bool hasActivities = state.hasActivities;

              return PageStructureWidget(
                children: [
                  Text(
                    '${state.moduleName}: ${state.lessonName}',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 18.0),
                    child: GestureDetector(
                      onTap: () {
                        VideoPageData video = VideoPageData(
                            videoId: initialVideoId,
                            title: state.moduleName + ": " + state.lessonName);

                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => YoutubeApp(
                            videoId: video.videoId,
                          ),
                        );

                        Navigator.push(context, route);
                      },
                      child: AbsorbPointer(
                        absorbing: true,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.06,
                          ),
                          child: YoutubePlayerIFrame(
                            controller: YoutubePlayerController(
                              initialVideoId: initialVideoId,
                              params: YoutubePlayerParams(
                                autoPlay: false,
                                showControls: true,
                                showFullscreenButton: true,
                                desktopMode: true,
                                privacyEnhanced: false,
                                useHybridComposition: true,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  RoundedButton(
                    text: 'Watch Video on YouTube',
                    background: Colors.white,
                    foreground: Colors.orange,
                    onTap: () {
                      launch('https://www.youtube.com/watch?v=$initialVideoId');
                    },
                  ),
                  if (hasActivities) ...[
                    RoundedButton(
                      text: 'Start Activity',
                      background: colorOrange,
                      foreground: Colors.white,
                      onTap: () {
                        context.read<LessonBloc>().add(
                              GoToActivityEvent(
                                activityIndex: 0,
                              ),
                            );
                      },
                    ),
                  ]
                ],
              );
            }

            if (state is ActivityState) {
              return PageStructureWidget(
                children: [
                  Text(
                    'Activity ${state.index + 1}',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  for (int i = 0;
                      i < state.activites[state.index].parts.length;
                      i++) ...[
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        '${state.activites[state.index].parts[i]}',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ),
                  ],
                  SizedBox(
                    height: 60,
                  ),
                  state.activites.length != state.index + 1
                      ? RoundedButton(
                          text: 'Next Activity',
                          background: colorOrange,
                          foreground: Colors.white,
                          onTap: () {
                            context.read<LessonBloc>().add(
                                  GoToActivityEvent(
                                    activityIndex: state.index + 1,
                                  ),
                                );
                          },
                        )
                      : RoundedButton(
                          text: 'Start Questions',
                          background: colorOrange,
                          foreground: Colors.white,
                          onTap: () {
                            context.read<LessonBloc>().add(
                                  CheckLessonModuleStatus(),
                                );
                          },
                        ),
                ],
              );
            }

            if (state is LessonAttempted) {
              final int correctQuestionsCount = state.correctQuestionsCount;
              final int totalQuestionsCount = state.totalQuestionsCount;

              return PageStructureWidget(
                children: [
                  Text(
                    'Your Score',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text(
                    '$correctQuestionsCount / $totalQuestionsCount correct',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Text(
                    'Please take the test again or start the course from beginning to make sure you know the answers.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        onTap: () {
                          context.read<LessonBloc>().add(
                                RetakeTestEvent(),
                              );
                        },
                        text: 'Retake The Test',
                        background: colorOrange,
                        foreground: Colors.white,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        onTap: () {
                          context.read<LessonBloc>().add(
                                RestartLessonEvent(),
                              );
                        },
                        text: 'Start Course Again',
                        background: colorOrange,
                        foreground: Colors.white,
                      ),
                    ],
                  ),
                ],
              );
            }

            if (state is LessonPassed) {
              final int correctQuestionsCount = state.correctQuestionsCount;
              final int totalQuestionsCount = state.totalQuestionsCount;
              final bool modulePassed = state.moduleFinished;

              return PageStructureWidget(
                children: [
                  Text(
                    'Your Score',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text(
                    '$correctQuestionsCount / $totalQuestionsCount correct',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 28.0),
                    child: Text(
                      'You have passed this lesson.',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  SizedBox(height: 48),
                  Center(
                    child: Visibility(
                      visible: (modulePassed == null) ? false : true,
                      child: RoundedButton(
                        onTap: () {
                          Route route = MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  RewardEarnedPage());
                          Navigator.pop(context);
                          Navigator.push(context, route);
                        },
                        text: 'Generate Reward',
                        background: colorOrange,
                        foreground: Colors.white,
                      ),
                    ),
                  ),
                ],
              );
            }

            if (state is LessonActive) {
              final String question = state.question;
              final int questionIndex = state.questionIndex;
              final int totalQuestionsCount = state.totalQuestionsCount;
              final List<String> answers = state.answers;
              final int selectedAnswerIndex = state.selectedAnswerIndex;

              return PageStructureWidget(
                children: [
                  Text(
                    'Quiz Time',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Let\'s check what we\'ve learned.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'To complete this module, earn a certificate and be eligible for rewards, you must successfully pass the test. You can retake the test multiple times.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Divider(color: Colors.black),
                  ),
                  Text(
                    'Question ${questionIndex + 1}',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    '$question',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 28.0),
                    child: ListView(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: [
                        Column(
                          children: answers
                              .map(
                                (answer) => RadioListTile<String>(
                                    title: Text(answer),
                                    value: answer,
                                    activeColor: colorOrange,
                                    groupValue: answers[selectedAnswerIndex],
                                    onChanged: (String val) {
                                      context.read<LessonBloc>().add(
                                            SelectAnswerEvent(
                                                selectedAnswerIndex:
                                                    answers.indexOf(val)),
                                          );
                                    }),
                              )
                              .toList(),
                        ),
                        (questionIndex + 1) < totalQuestionsCount
                            ? RoundedButton(
                                foreground: Colors.white,
                                background: colorOrange,
                                onTap: () {
                                  context.read<LessonBloc>().add(
                                        NextQuestionEvent(),
                                      );
                                },
                                text: 'Next Question',
                              )
                            : SizedBox.shrink(),
                        SizedBox(height: 20),
                        (questionIndex + 1) > 1
                            ? RoundedButton(
                                background: colorOrange,
                                foreground: Colors.white,
                                onTap: () {
                                  context.read<LessonBloc>().add(
                                        PreviousQuestionEvent(),
                                      );
                                },
                                text: 'Previous Question',
                              )
                            : SizedBox.shrink(),
                        SizedBox(height: 20),
                        (questionIndex + 1) == totalQuestionsCount
                            ? RoundedButton(
                                foreground: Colors.white,
                                background: colorOrange,
                                onTap: () {
                                  context.read<LessonBloc>().add(
                                        GetResultsEvent(),
                                      );
                                },
                                text: 'Get Results',
                              )
                            : SizedBox.shrink(),
                      ],
                    ),
                  ),
                ],
              );
            }

            if (state is QuestionsError) {
              final String errorMessage =
                  state.error.message ?? 'Could not log in at this time.';

              return PageStructureWidget(
                children: [
                  Center(
                    child: Text(
                      '$errorMessage',
                    ),
                  ),
                ],
              );
            }

            return Container();
          },
          listener: (context, state) {},
        ));
  }

  @override
  void showMessage({@required String message}) {
    locator<ModalService>().showAlert(
      context: context,
      title: 'Error',
      message: message,
    );
  }
}
