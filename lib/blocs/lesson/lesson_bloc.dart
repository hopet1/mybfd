import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/ActivityModel.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/LessonScoreModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/models/ModuleScoreModel.dart';
import 'package:MyBFD/models/QuestionModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/models/video_page_data.dart';
import 'package:MyBFD/pages/RewardEarned.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/ActivityService.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/LessonService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/QuestionService.dart';
import 'package:MyBFD/services/ScoreService.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/VideoPlayer/videoPlayer.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_plyr_iframe/youtube_plyr_iframe.dart';

part 'lesson_event.dart';
part 'lesson_page.dart';
part 'lesson_state.dart';

enum LESSON_STATUS {
  Attempted,
  NotAttempted,
  Passed,
}

extension LESSON_STATUS_EXTENSION on LESSON_STATUS {
  String get name {
    switch (this) {
      case LESSON_STATUS.Attempted:
        return 'Attempted';
      case LESSON_STATUS.NotAttempted:
        return 'NotAttempted';
      case LESSON_STATUS.Passed:
        return 'Passed';
      default:
        return null;
    }
  }
}

abstract class LessonBlocDelegate {
  void showMessage({@required String message});
}

class LessonBloc extends Bloc<LessonEvent, LessonState> {
  LessonBloc({
    @required this.course,
    @required this.module,
    @required this.lesson,
  }) : super(LessonInitial());

  final CourseModel course;
  final ModuleModel module;
  final LessonModel lesson;

  List<QuestionModel> _questions;
  List<ActivityModel> _activites;

  LessonScoreModel lessonScore;
  ModuleScoreModel moduleScore;

  int _currentQuestionIndex = 0;

  UserModel _currentUser;

  LessonBlocDelegate _lessonBlocDelegate;

  void setDelegate({@required LessonBlocDelegate delegate}) {
    this._lessonBlocDelegate = delegate;
  }

  @override
  Stream<LessonState> mapEventToState(
    LessonEvent event,
  ) async* {
    if (event is LoadLessonEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();

        _questions = await locator<QuestionService>().retrieveQuestions(
            courseID: course.id, moduleID: module.id, lessonID: lesson.id);
        _activites = await locator<ActivityService>().retrieveActivities(
            courseID: course.id, moduleID: module.id, lessonID: lesson.id);

        if (lesson.youtubeUrl != null) {
          yield YouTubeVideoState(
            lessonName: lesson.title,
            moduleName: module.title,
            youtubeUrl: lesson.youtubeUrl,
            hasActivities: _activites.isNotEmpty,
          );
        } else if (_activites.isNotEmpty) {
          yield ActivityState(activites: _activites, index: 0);
        } else if (_questions.isNotEmpty) {
          add(CheckLessonModuleStatus());
        } else {
          _lessonBlocDelegate.showMessage(
              message: 'There\'s nothing left in this lesson.');
          throw Exception('Nothing to do in this lesson.');
        }
      } catch (error) {
        yield QuestionsError(error: error);
      }
    }

    if (event is GoToActivityEvent) {
      if (_activites.isNotEmpty) {
        yield ActivityState(activites: _activites, index: event.activityIndex);
      } else if (_questions.isNotEmpty) {
        add(
          CheckLessonModuleStatus(),
        );
      } else {
        _lessonBlocDelegate.showMessage(
            message: 'There\'s nothing left in this lesson.');
      }
    }

    if (event is CheckLessonModuleStatus) {
      //Check lesson status.
      lessonScore = await locator<ScoreService>().getLessonScore(
        uid: _currentUser.uid,
        lessonID: lesson.id,
      );

      if (lessonScore == null) {
        lessonScore = LessonScoreModel(
          id: null,
          correct: 0,
          passed: false,
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
        );
      }

      //Check module status.
      moduleScore = await locator<ScoreService>()
          .getModuleScore(uid: _currentUser.uid, moduleID: module.id);

      if (moduleScore == null) {
        moduleScore = ModuleScoreModel(
            id: null, courseID: course.id, moduleID: module.id, passed: false);
      }

      final bool isLessonPassed =
          _isLessonPassed(correctQuestionsCount: lessonScore.correct);

      if (isLessonPassed) {
        yield LessonPassed(
          correctQuestionsCount: lessonScore.correct,
          totalQuestionsCount: _questions.length,
        );
      } else {
        if (lessonScore.correct == 0) {
          add(
            BuildQuestionEvent(),
          );
        } else {
          yield LessonAttempted(
            correctQuestionsCount: lessonScore.correct,
            totalQuestionsCount: _questions.length,
          );
        }
      }
    }

    if (event is SelectAnswerEvent) {
      _questions[_currentQuestionIndex].selectedAnswerIndex =
          event.selectedAnswerIndex;

      add(
        BuildQuestionEvent(),
      );
    }

    if (event is BuildQuestionEvent) {
      final QuestionModel questionModel = _questions[_currentQuestionIndex];
      final String question = questionModel.question;
      final int selectedAnswerIndex = questionModel.selectedAnswerIndex ?? 0;

      List<String> answers = [];

      questionModel.answers.forEach(
        (key, value) {
          answers.add(value);
        },
      );

      yield LessonActive(
        answers: answers,
        question: question,
        questionIndex: _currentQuestionIndex,
        totalQuestionsCount: _questions.length,
        selectedAnswerIndex: selectedAnswerIndex,
      );
    }

    if (event is RetakeTestEvent) {
      _currentQuestionIndex = 0;

      _questions.forEach((question) {
        question.selectedAnswerIndex = null;
      });

      add(
        BuildQuestionEvent(),
      );
    }

    if (event is RestartLessonEvent) {
      add(LoadLessonEvent());
    }

    if (event is NextQuestionEvent) {
      _currentQuestionIndex++;

      add(
        BuildQuestionEvent(),
      );
    }

    if (event is PreviousQuestionEvent) {
      _currentQuestionIndex--;

      add(
        BuildQuestionEvent(),
      );
    }

    if (event is GetResultsEvent) {
      int correctQuestionsCount = 0;

      _questions.forEach((question) {
        if (question.correctAnswerIndex == question.selectedAnswerIndex)
          correctQuestionsCount++;
      });

      await locator<ScoreService>().updateLessonScore(
        uid: _currentUser.uid,
        lessonScore: LessonScoreModel(
          id: lessonScore.id,
          moduleID: lessonScore.moduleID,
          courseID: lessonScore.courseID,
          lessonID: lessonScore.lessonID,
          passed: _isLessonPassed(correctQuestionsCount: correctQuestionsCount),
          correct: correctQuestionsCount,
        ),
      );

      lessonScore = await locator<ScoreService>().getLessonScore(
        uid: _currentUser.uid,
        lessonID: lesson.id,
      );

      List<LessonModel> totalLessonsForModule = await locator<LessonService>()
          .retrieveLessons(courseID: course.id, moduleID: module.id);
      List<LessonScoreModel> passedLessonScoresForModule =
          await locator<ScoreService>().getPassedLessonScoresForModule(
              uid: _currentUser.uid, moduleID: module.id);

      await locator<ScoreService>().updateModuleScore(
        uid: _currentUser.uid,
        moduleScore: ModuleScoreModel(
          passed: totalLessonsForModule.length ==
              passedLessonScoresForModule.length,
          courseID: moduleScore.courseID,
          moduleID: moduleScore.moduleID,
          id: moduleScore.id,
        ),
      );

      moduleScore = await locator<ScoreService>()
          .getModuleScore(uid: _currentUser.uid, moduleID: module.id);

      if (lessonScore.passed) {
        if (moduleScore.passed) {
          yield LessonPassed(
            correctQuestionsCount: correctQuestionsCount,
            totalQuestionsCount: _questions.length,
            moduleFinished: true,
          );
        } else {
          yield LessonPassed(
            correctQuestionsCount: correctQuestionsCount,
            totalQuestionsCount: _questions.length,
          );
        }
      } else {
        yield LessonAttempted(
          correctQuestionsCount: correctQuestionsCount,
          totalQuestionsCount: _questions.length,
        );
      }
    }
  }

  bool _isLessonPassed({
    @required int correctQuestionsCount,
  }) {
    return correctQuestionsCount >
        (LESSON_PASS_RATE_PERCENTAGE * _questions.length);
  }
}
