import 'dart:async';

import 'package:MyBFD/blocs/lessons/lessons_bloc.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/ModuleService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'modules_event.dart';
part 'modules_page.dart';
part 'modules_state.dart';

class ModulesBloc extends Bloc<ModulesEvent, ModulesState> {
  ModulesBloc({@required this.course}) : super(ModulesInitial());
  final CourseModel course;

  @override
  Stream<ModulesState> mapEventToState(
    ModulesEvent event,
  ) async* {
    yield ModulesLoading();

    if (event is LoadModulesEvent) {
      try {
        List<ModuleModel> modules =
            await locator<ModuleService>().retrieveModules(courseID: course.id);
        yield ModulesLoaded(
          course: course,
          modules: modules,
        );
      } catch (error) {
        yield ModulesError(error: error);
      }
    }
  }
}
