part of 'modules_bloc.dart';

abstract class ModulesEvent extends Equatable {
  const ModulesEvent();

  @override
  List<Object> get props => [];
}

class LoadModulesEvent extends ModulesEvent {}

// class LoadModuleEvent extends ModulesEvent {
//   final ModuleModel module;

//   const LoadModuleEvent({@required this.module});

//   @override
//   List<Object> get props => [
//         module,
//       ];
// }

// class GoBackToModulesEvent extends ModulesEvent {}
