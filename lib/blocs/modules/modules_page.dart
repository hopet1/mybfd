part of 'modules_bloc.dart';

class ModulesPage extends StatefulWidget {
  @override
  State createState() => _ModulesPageState();
}

class _ModulesPageState extends State<ModulesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<ModulesBloc, ModulesState>(
        builder: (context, state) {
          if (state is ModulesLoading) {
            return Spinner();
          }

          if (state is ModulesLoaded) {
            final CourseModel course = state.course;
            final List<ModuleModel> modules = state.modules;

            return PageStructureWidget(
              children: [
                Text(
                  'Modules',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  '${course.title}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline4,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: modules.length,
                    itemBuilder: (context, index) {
                      final ModuleModel module = modules[index];
                      return InfoWidget(
                        onTap: () {
                          Route route = MaterialPageRoute(
                            builder: (BuildContext context) => BlocProvider(
                              create: (BuildContext context) =>
                                  LessonsBloc(course: course, module: module)
                                    ..add(LoadLessonsEvent()),
                              child: LessonsPage(),
                            ),
                          );
                          Navigator.push(context, route);
                        },
                        title: '${index + 1}. ' + module.title,
                      );
                    },
                  ),
                ),
              ],
            );
          }

          if (state is ModulesError) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return Center(
              child: Text('$errorMessage'),
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
