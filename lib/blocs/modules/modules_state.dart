part of 'modules_bloc.dart';

abstract class ModulesState extends Equatable {
  const ModulesState();

  @override
  List<Object> get props => [];
}

class ModulesInitial extends ModulesState {}

class ModulesLoading extends ModulesState {}

class ModulesLoaded extends ModulesState {
  final CourseModel course;
  final List<ModuleModel> modules;

  const ModulesLoaded({
    @required this.course,
    @required this.modules,
  });

  @override
  List<Object> get props => [
        course,
        modules,
      ];
}

class ModuleLoading extends ModulesState {}

class ModuleLoaded extends ModulesState {
  final ModuleModel module;
  final List<LessonModel> lessons;

  const ModuleLoaded({
    @required this.module,
    @required this.lessons,
  });

  @override
  List<Object> get props => [
        module,
        lessons,
      ];
}

class ModulesError extends ModulesState {
  final dynamic error;

  ModulesError({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
