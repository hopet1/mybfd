part of 'lessons_bloc.dart';

abstract class LessonsState extends Equatable {
  const LessonsState();

  @override
  List<Object> get props => [];
}

class LessonsInitial extends LessonsState {}

class LessonsLoading extends LessonsState {}

class LessonsLoaded extends LessonsState {
  final CourseModel course;
  final ModuleModel module;
  final List<LessonModel> lessons;
  final bool passed;

  const LessonsLoaded({
    @required this.course,
    @required this.module,
    @required this.lessons,
    @required this.passed,
  });

  @override
  List<Object> get props => [
        course,
        module,
        lessons,
        passed,
      ];
}

class LessonsError extends LessonsState {
  final dynamic error;

  LessonsError({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
