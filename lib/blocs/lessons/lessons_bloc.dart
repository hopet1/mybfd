import 'dart:async';

import 'package:MyBFD/blocs/certificate/certificate_bloc.dart';
import 'package:MyBFD/blocs/lesson/lesson_bloc.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/models/ModuleScoreModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/LessonService.dart';
import 'package:MyBFD/services/ScoreService.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/VideoPlayer/videoPlayer.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_parser/youtube_parser.dart';
import 'package:youtube_plyr_iframe/youtube_plyr_iframe.dart';

import '../../constants.dart';

part 'lessons_event.dart';
part 'lessons_page.dart';
part 'lessons_state.dart';

class LessonsBloc extends Bloc<LessonsEvent, LessonsState> {
  LessonsBloc({@required this.course, @required this.module})
      : super(LessonsInitial());
  final CourseModel course;
  final ModuleModel module;

  UserModel _currentUser;

  @override
  Stream<LessonsState> mapEventToState(
    LessonsEvent event,
  ) async* {
    yield LessonsLoading();

    if (event is LoadLessonsEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();
        final ModuleScoreModel moduleScore =
            await locator<ScoreService>().getModuleScore(
          uid: _currentUser.uid,
          moduleID: module.id,
        );

        List<LessonModel> lessons = await locator<LessonService>()
            .retrieveLessons(courseID: course.id, moduleID: module.id);

        yield LessonsLoaded(
          course: course,
          module: module,
          lessons: lessons,
          passed: moduleScore != null && moduleScore.passed,
        );
      } catch (error) {
        yield LessonsError(error: error);
      }
    }
  }
}
