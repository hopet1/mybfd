part of 'lessons_bloc.dart';

class LessonsPage extends StatefulWidget {
  @override
  State createState() => _LessonsPageState();
}

class _LessonsPageState extends State<LessonsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<LessonsBloc, LessonsState>(
        builder: (context, state) {
          if (state is LessonsLoading) {
            return Spinner();
          }

          if (state is LessonsLoaded) {
            final CourseModel course = state.course;
            final ModuleModel module = state.module;
            final List<LessonModel> lessons = state.lessons;
            final bool passed = state.passed;

            String initialVideoId;

            if (module.youtubeUrl != null)
              initialVideoId = getIdFromUrl('${module.youtubeUrl}');

            return PageStructureWidget(
              children: [
                Text(
                  '${module.title}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 8),
                passed
                    ? Text(
                        'Complete',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headline4,
                      )
                    : SizedBox.shrink(),
                SizedBox(
                  height: 20,
                ),
                Text(
                  '${module.summary}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3,
                ),
                SizedBox(height: 12),
                passed
                    ? RoundedButton(
                        onTap: () {
                          Route route = MaterialPageRoute(
                            builder: (BuildContext context) => BlocProvider(
                              create: (BuildContext context) =>
                                  CertificateBloc()
                                    ..add(
                                      InitialEvent(name, module.title),
                                    ),
                              child: CertificatePage(),
                            ),
                          );
                          Navigator.push(context, route);
                        },
                        background: colorOrange,
                        foreground: Colors.white,
                        text: 'Download Certificate',
                      )
                    : SizedBox.shrink(),
                if (initialVideoId != null) ...[
                  GestureDetector(
                    onTap: () {
                      Route route = MaterialPageRoute(
                        builder: (BuildContext context) => YoutubeApp(
                          videoId: initialVideoId,
                        ),
                      );

                      Navigator.push(context, route);
                    },
                    child: AbsorbPointer(
                      absorbing: true,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width * 0.1,
                            vertical: MediaQuery.of(context).size.height * 0.1),
                        child: YoutubePlayerIFrame(
                          controller: YoutubePlayerController(
                            initialVideoId: initialVideoId,
                            params: YoutubePlayerParams(
                              autoPlay: false,
                              showControls: true,
                              showFullscreenButton: true,
                              desktopMode: true,
                              privacyEnhanced: false,
                              useHybridComposition: true,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  RoundedButton(
                    text: 'Watch Video on YouTube',
                    background: Colors.white,
                    foreground: Colors.orange,
                    onTap: () {
                      launch('https://www.youtube.com/watch?v=$initialVideoId');
                    },
                  ),
                ],
                //TODO Video player app
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: lessons.length,
                    itemBuilder: (context, index) {
                      final LessonModel lesson = lessons[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: GestureDetector(
                          onTap: () {
                            Route route = MaterialPageRoute(
                              builder: (BuildContext context) => BlocProvider(
                                create: (BuildContext context) => LessonBloc(
                                  course: course,
                                  module: module,
                                  lesson: lesson,
                                )..add(LoadLessonEvent()),
                                child: LessonPage(),
                              ),
                            );
                            Navigator.push(context, route);
                          },
                          child: Container(
                            height: 170.0,
                            width: screenWidth * 0.8,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.2),
                                  offset: Offset(2, 2),
                                  blurRadius: 4.0,
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 24.0,
                                    vertical: 18.0,
                                  ),
                                  child: Text(
                                    "Lesson ${index + 1}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: colorOrange,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 24.0,
                                    vertical: 10.0,
                                  ),
                                  child: Text(
                                    "${lesson.title}",
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 24.0,
                                  ),
                                  child: Text(
                                    "${lesson.summary}",
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          }

          if (state is LessonsError) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return Center(
              child: Text('$errorMessage'),
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
