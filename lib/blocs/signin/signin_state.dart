part of 'package:MyBFD/blocs/signin/signin_bloc.dart';

abstract class SigninState extends Equatable {
  const SigninState();

  @override
  List<Object> get props => [];
}

class SigninLoading extends SigninState {}

class SigninInitial extends SigninState {}

class SigninSuccess extends SigninState {}

class SigninFailure extends SigninState {
  final dynamic error;

  SigninFailure({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
