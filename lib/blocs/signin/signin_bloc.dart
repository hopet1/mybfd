import 'dart:async';

import 'package:MyBFD/blocs/forgotpassword/Bloc.dart';
import 'package:MyBFD/blocs/needhelp/needhelp_bloc.dart';
import 'package:MyBFD/blocs/signup/signup_bloc.dart' as SIGN_UP_BP;
import 'package:MyBFD/constants.dart';
// import 'package:MyBFD/extensions/BezierClipper.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import '../../service_locator.dart';

part 'package:MyBFD/blocs/signin/signin_event.dart';
part 'package:MyBFD/blocs/signin/signin_page.dart';
part 'package:MyBFD/blocs/signin/signin_state.dart';

class SigninBloc extends Bloc<SigninEvent, SigninState> {
  SigninBloc() : super(SigninInitial());

  @override
  Stream<SigninState> mapEventToState(
    SigninEvent event,
  ) async* {
    yield SigninLoading();

    if (event is Signin) {
      final String email = event.email;
      final String password = event.password;
      try {
        UserCredential userCredential =
            await locator<AuthService>().signInWithEmailAndPassword(
          email: email,
          password: password,
        );

        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('uid', userCredential.user.uid);

        yield SigninSuccess();
      } catch (error) {
        yield SigninFailure(error: error);
      }
    }

    if (event is TryAgain) {
      yield SigninInitial();
    }
  }
}
