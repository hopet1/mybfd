part of 'package:MyBFD/blocs/signin/signin_bloc.dart';

class SigninPage extends StatefulWidget {
  @override
  State createState() => SigninPageState();
}

class SigninPageState extends State<SigninPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final double _titleFontSize = 60;
  bool _passwordHidden;

  @override
  void initState() {
    super.initState();
    _passwordHidden = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorOrange,
      body: Form(
        key: _formKey,
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              ClipPath(
                clipper: BezierClipper(),
                child: Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 400,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          _emailController.text = 'trey.a.hope@gmail.com';
                          _passwordController.text = 'Peachy33';
                        },
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: 100,
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'my',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorDark,
                              ),
                            ),
                            TextSpan(
                              text: 'bfd',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorOrange,
                              ),
                            ),
                            TextSpan(
                              text: '\nFinancial Education App'.toUpperCase(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 2.0,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              BlocConsumer<SigninBloc, SigninState>(
                builder: (context, state) {
                  if (state is SigninLoading) {
                    return Spinner();
                  }

                  if (state is SigninFailure) {
                    final String errorMessage =
                        state.error.message ?? 'Could not log in at this time.';

                    return Center(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: Text(
                              '$errorMessage',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 21,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          RoundedButton(
                            background: colorOrange,
                            foreground: Colors.white,
                            onTap: () {
                              context.read<SigninBloc>().add(
                                    TryAgain(),
                                  );
                            },
                            text: 'Try Again?',
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is SigninInitial) {
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: _emailController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Email'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().password,
                                  obscureText: _passwordHidden,
                                  controller: _passwordController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: Colors.white,
                                      ),
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          _passwordHidden
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _passwordHidden = !_passwordHidden;
                                          });
                                        },
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Password'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Sign In',
                            foreground: colorOrange,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              context.read<SigninBloc>().add(
                                    Signin(
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                    ),
                                  );
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        SIGN_UP_BP.SignupBloc(),
                                    child: SIGN_UP_BP.SignupPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? 14
                                          : 20),
                                  children: [
                                    TextSpan(
                                      text: 'New to My BFD?',
                                    ),
                                    TextSpan(
                                      text: ' Signup',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                // locator<ModalService>().showAlert(
                                //   context: context,
                                //   title: 'To Do',
                                //   message: 'Open Lost Password Page.',
                                // );
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        ForgotPasswordBloc(),
                                    child: ForgotPasswordPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: Text(
                                'Lost Password?',
                                style: TextStyle(
                                    letterSpacing: 1.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize:
                                        Responsive.isMobile(context) ? 14 : 20),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Route route = MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BlocProvider(
                                    create: (BuildContext context) =>
                                        NeedhelpBloc(),
                                    child: NeedhelpPage(),
                                  ),
                                );
                                Navigator.push(context, route);
                              },
                              child: Text(
                                'Need Help? Contact Us.',
                                style: TextStyle(
                                    letterSpacing: 1.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize:
                                        Responsive.isMobile(context) ? 14 : 20),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    );
                  }

                  return Container();
                },
                listener: (context, state) {
                  if (state == SigninSuccess()) {
                    Phoenix.rebirth(context);
                  }
                  if (state is SigninFailure) {
                    print('failure');
                    //todo: Report this sign in failure somewhere perhaps?
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
