part of 'package:MyBFD/blocs/signin/signin_bloc.dart';

abstract class SigninEvent extends Equatable {
  const SigninEvent();

  @override
  List<Object> get props => [];
}

class Signin extends SigninEvent {
  final String email;
  final String password;

  Signin({
    @required this.email,
    @required this.password,
  });

  List<Object> get props => [
        email,
        password,
      ];
}

class TryAgain extends SigninEvent {}
