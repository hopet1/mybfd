import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/ActivityModel.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/ActivityService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

part 'admin_activities_eventt.dart';
part 'admin_activities_page.dart';
part 'admin_activities_state.dart';

class AdminActivitiesBloc
    extends Bloc<AdminActivitiesEvent, AdminActivitiesState> {
  AdminActivitiesBloc({
    @required this.course,
    @required this.module,
    @required this.lesson,
  }) : super(ActivitiesInitialState());
  final CourseModel course;
  final ModuleModel module;
  final LessonModel lesson;

  List<ActivityModel> _activities = [];

  @override
  Stream<AdminActivitiesState> mapEventToState(
    AdminActivitiesEvent event,
  ) async* {
    yield ActivitiesLoadingState();

    if (event is LoadActivitiesEvent) {
      try {
        _activities = await locator<ActivityService>().retrieveActivities(
            courseID: course.id, moduleID: module.id, lessonID: lesson.id);
        add(GoToActivitiesLoadedEvent());
      } catch (error) {
        yield ActivityErrorState(error: error);
      }
    }

    if (event is GoToActivitiesLoadedEvent) {
      _activities.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield ActivitiesLoadedState(
          course: course,
          module: module,
          lesson: lesson,
          activities: _activities);
    }

    if (event is GoToEditActivityEvent) {
      yield EditActivityState(activity: event.activity);
    }

    if (event is SubmitEditActivityEvent) {
      try {
        final ActivityModel activity = event.activity;

        await locator<ActivityService>().updateActivity(
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
          activityID: activity.id,
          data: activity.toMap(),
        );

        add(LoadActivitiesEvent()); //Note, only doing this here becase editing a module is messy.
      } catch (error) {
        yield ActivityErrorState(error: error);
      }
    }

    if (event is GoToSortActivitiesEvent) {
      _activities.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield SortActivitiesState(activities: _activities);
    }

    if (event is SubmitSortActivitiesEvent) {
      for (int i = 0; i < _activities.length; i++) {
        ActivityModel activity = _activities[i];

        await locator<ActivityService>().updateActivity(
            courseID: course.id,
            moduleID: module.id,
            lessonID: lesson.id,
            activityID: activity.id,
            data: {
              'position': activity.position,
            });
      }

      add(LoadActivitiesEvent());
    }

    if (event is ShallowSubmitSortActivitiesEvent) {
      final int oldIndex = event.oldIndex;
      final int newIndex = event.newIndex;

      ActivityModel module = _activities.removeAt(oldIndex);
      _activities.insert(newIndex, module);

      _updateActivityPositionValues();

      add(GoToSortActivitiesEvent());
    }

    if (event is GoToAddActivityEvent) {
      yield AddActivityState();
    }

    if (event is SubmitAddActivityEvent) {
      try {
        final ActivityModel activity = event.activity;

        activity.position = _activities.length;

        await locator<ActivityService>().createActivity(
            courseID: course.id,
            moduleID: module.id,
            lessonID: lesson.id,
            activity: activity);

        _activities.add((activity));

        add(GoToActivitiesLoadedEvent());
      } catch (error) {
        yield ActivityErrorState(error: error);
      }
    }

    if (event is DeleteActivityEvent) {
      try {
        final String activityID = event.activityID;

        await locator<ActivityService>().deleteActivity(
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
          activityID: activityID,
        );

        _activities.removeWhere((activity) => activity.id == activityID);

        _updateActivityPositionValues();

        add(SubmitSortActivitiesEvent());
      } catch (error) {
        yield ActivityErrorState(error: error);
      }
    }
  }

  void _updateActivityPositionValues() {
    for (int i = 0; i < _activities.length; i++) {
      ActivityModel c = _activities[i];

      c.position = i;
    }
  }
}
