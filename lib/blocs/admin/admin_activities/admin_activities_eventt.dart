part of 'admin_activities_bloc.dart';

//NOTE: FOR SOME ODD REASON, I HAVE TO NAME THIS FILE 'eventt' OTHERWISE THE IMPORT WON'T WORK.
abstract class AdminActivitiesEvent extends Equatable {
  const AdminActivitiesEvent();

  @override
  List<Object> get props => [];
}

class LoadActivitiesEvent extends AdminActivitiesEvent {}

class GoToActivitiesLoadedEvent extends AdminActivitiesEvent {}

class GoToSortActivitiesEvent extends AdminActivitiesEvent {}

class SubmitSortActivitiesEvent extends AdminActivitiesEvent {
  const SubmitSortActivitiesEvent();

  @override
  List<Object> get props => [];
}

class ShallowSubmitSortActivitiesEvent extends AdminActivitiesEvent {
  final int oldIndex;
  final int newIndex;

  const ShallowSubmitSortActivitiesEvent({
    @required this.oldIndex,
    @required this.newIndex,
  });

  @override
  List<Object> get props => [
        oldIndex,
        newIndex,
      ];
}

class GoToEditActivityEvent extends AdminActivitiesEvent {
  final ActivityModel activity;

  const GoToEditActivityEvent({
    @required this.activity,
  });

  @override
  List<Object> get props => [
        activity,
      ];
}

class SubmitEditActivityEvent extends AdminActivitiesEvent {
  final ActivityModel activity;

  const SubmitEditActivityEvent({
    @required this.activity,
  });

  @override
  List<Object> get props => [
        activity,
      ];
}

class GoToAddActivityEvent extends AdminActivitiesEvent {}

class SubmitAddActivityEvent extends AdminActivitiesEvent {
  final ActivityModel activity;

  const SubmitAddActivityEvent({
    @required this.activity,
  });

  @override
  List<Object> get props => [
        activity,
      ];
}

class DeleteActivityEvent extends AdminActivitiesEvent {
  final String activityID;

  const DeleteActivityEvent({
    @required this.activityID,
  });

  @override
  List<Object> get props => [
        activityID,
      ];
}
