part of 'admin_activities_bloc.dart';

abstract class AdminActivitiesState extends Equatable {
  const AdminActivitiesState();

  @override
  List<Object> get props => [];
}

class ActivitiesInitialState extends AdminActivitiesState {}

class ActivitiesLoadingState extends AdminActivitiesState {}

class ActivitiesLoadedState extends AdminActivitiesState {
  final CourseModel course;
  final ModuleModel module;
  final LessonModel lesson;

  final List<ActivityModel> activities;

  const ActivitiesLoadedState({
    @required this.course,
    @required this.module,
    @required this.lesson,
    @required this.activities,
  });

  @override
  List<Object> get props => [
        course,
        module,
        lesson,
        activities,
      ];
}

class SortActivitiesState extends AdminActivitiesState {
  final List<ActivityModel> activities;

  const SortActivitiesState({
    @required this.activities,
  });

  @override
  List<Object> get props => [
        activities,
      ];
}

class EditActivityState extends AdminActivitiesState {
  final ActivityModel activity;

  const EditActivityState({
    @required this.activity,
  });

  @override
  List<Object> get props => [
        activity,
      ];
}

class AddActivityState extends AdminActivitiesState {
  const AddActivityState();

  @override
  List<Object> get props => [];
}

class ActivityErrorState extends AdminActivitiesState {
  final dynamic error;

  ActivityErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
