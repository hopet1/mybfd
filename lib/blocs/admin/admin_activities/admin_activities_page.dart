part of 'admin_activities_bloc.dart';

class AdminActivitiesPage extends StatefulWidget {
  @override
  _AdminActivitiesPageState createState() => _AdminActivitiesPageState();
}

class _AdminActivitiesPageState extends State<AdminActivitiesPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _name = '';
  List<String> _steps = [''];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminActivitiesBloc, AdminActivitiesState>(
        builder: (context, state) {
          if (state is ActivitiesLoadingState) {
            return Spinner();
          }

          if (state is ActivitiesLoadedState) {
            final CourseModel course = state.course;
            final ModuleModel module = state.module;
            final LessonModel lesson = state.lesson;

            final List<ActivityModel> activities = state.activities;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'Activities',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: activities.length,
                  itemBuilder: (context, index) {
                    final ActivityModel activity = activities[index];

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            final bool confirm =
                                await locator<ModalService>().showConfirmation(
                              context: context,
                              title: 'Delete Activity: ${activity.name}',
                              message: 'Are you sure?',
                            );

                            if (!confirm) return;

                            context.read<AdminActivitiesBloc>().add(
                                  DeleteActivityEvent(
                                    activityID: activity.id,
                                  ),
                                );
                          },
                        ),
                        InfoWidget(
                          onTap: () {},
                          title: '${activity.position + 1}. ${activity.name}',
                        ),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            context.read<AdminActivitiesBloc>().add(
                                  GoToEditActivityEvent(
                                    activity: activity,
                                  ),
                                );
                          },
                        ),
                      ],
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add Activity',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () {
                        context.read<AdminActivitiesBloc>().add(
                              GoToAddActivityEvent(),
                            );
                      },
                    ),
                    RoundedButton(
                      text: 'Sort Activities',
                      foreground: colorOrange,
                      background: Colors.white,
                      onTap: () {
                        context.read<AdminActivitiesBloc>().add(
                              GoToSortActivitiesEvent(),
                            );
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is SortActivitiesState) {
            final List<ActivityModel> activities = state.activities;

            var itemRows = activities.map((activity) {
              return ReorderableTableRow(
                //a key must be specified for each row
                key: ObjectKey(activity),
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    // activities.map((activity) => Text('hello')).toList()
                    <Widget>[
                  Text(
                    '${activity.position + 1}',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text('${activity.parts.toString()}'),
                ],
              );
            }).toList();

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminActivitiesBloc>()
                      .add(GoToActivitiesLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Sort Activities',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReorderableTable(
                  header: ReorderableTableRow(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Position', textScaleFactor: 1.5),
                      Text('Title', textScaleFactor: 1.5),
                    ],
                  ),
                  children: itemRows,
                  onReorder: (int oldIndex, int newIndex) {
                    context.read<AdminActivitiesBloc>().add(
                          ShallowSubmitSortActivitiesEvent(
                            oldIndex: oldIndex,
                            newIndex: newIndex,
                          ),
                        );
                  },
                  onNoReorder: (int index) {
                    //this callback is optional
                    debugPrint(
                        '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminActivitiesBloc>().add(
                          SubmitSortActivitiesEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is EditActivityState) {
            final ActivityModel activity = state.activity;

            List<String> steps = _steps.map((part) {
              return part;
            }).toList();

            _steps = steps;

            List<TextEditingController> stepControllers = _steps.map((part) {
              return TextEditingController(text: part);
            }).toList();

            TextEditingController nameController =
                TextEditingController(text: _name);

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminActivitiesBloc>()
                      .add(GoToActivitiesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Edit Activity',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Expanded(
                          child: TextFormField(
                            onChanged: (value) {
                              _name = value;
                            },
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: locator<ValidationService>().isEmpty,
                            controller: nameController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              errorStyle: TextStyle(
                                color: Colors.black,
                              ),
                              prefixIcon: Icon(
                                Icons.arrow_forward,
                                color: Colors.black,
                              ),
                              hintText: 'Name',
                            ),
                          ),
                        ),
                      ),
                      for (int i = 0; i < _steps.length; i++) ...[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  onChanged: (value) {
                                    _steps[i] = value;
                                  },
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: stepControllers[i],
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.black,
                                    ),
                                    hintText: 'Step ${i + 1}',
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  setState(() {
                                    _steps.removeAt(i);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                      FloatingActionButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              _steps.add('');
                            });
                          })
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminActivitiesBloc>().add(
                          SubmitEditActivityEvent(
                            activity: ActivityModel(
                              name: activity.name,
                              id: activity.id,
                              parts: stepControllers
                                  .map((stepController) => stepController.text)
                                  .toList(),
                              position: activity.position,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is AddActivityState) {
            List<TextEditingController> stepControllers = _steps.map((part) {
              return TextEditingController(text: part);
            }).toList();
            TextEditingController nameController =
                TextEditingController(text: _name);
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminActivitiesBloc>()
                      .add(GoToActivitiesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Add Activity',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Expanded(
                          child: TextFormField(
                            onChanged: (value) {
                              _name = value;
                            },
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: locator<ValidationService>().isEmpty,
                            controller: nameController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              errorStyle: TextStyle(
                                color: Colors.black,
                              ),
                              prefixIcon: Icon(
                                Icons.arrow_forward,
                                color: Colors.black,
                              ),
                              hintText: 'Name',
                            ),
                          ),
                        ),
                      ),
                      for (int i = 0; i < _steps.length; i++) ...[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  onChanged: (value) {
                                    _steps[i] = value;
                                  },
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: stepControllers[i],
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.black,
                                    ),
                                    hintText: 'Step ${i + 1}',
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  setState(() {
                                    _steps.removeAt(i);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                      FloatingActionButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              _steps.add('');
                            });
                          })
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminActivitiesBloc>().add(
                          SubmitAddActivityEvent(
                            activity: ActivityModel(
                              name: nameController.text,
                              id: null,
                              parts: stepControllers
                                  .map((controller) => controller.text)
                                  .toList(),
                              position: null,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is ActivityErrorState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminActivitiesBloc>()
                      .add(GoToActivitiesLoadedEvent());
                },
              ),
              children: [
                Center(
                  child: Text(
                    'Error: ${state.error.toString()}',
                  ),
                )
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is AddActivityState) {
            _steps = [''];
          }
          if (state is EditActivityState) {
            _steps = state.activity.parts;
          }
        },
      ),
    );
  }
}
