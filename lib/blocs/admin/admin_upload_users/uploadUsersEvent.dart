part of 'uploadUsersBloc.dart';

abstract class UploadUsersEvent extends Equatable {
  const UploadUsersEvent();

  @override
  List<Object> get props => [];
}

class LoadedEvent extends UploadUsersEvent {}

class TryAgain extends UploadUsersEvent {}

class PickCSVEvent extends UploadUsersEvent {}

class UploadToFirebase extends UploadUsersEvent {
  final List<List<dynamic>> csvData;
  const UploadToFirebase({@required this.csvData});

  @override
  List<Object> get props => [
        csvData,
      ];
}
