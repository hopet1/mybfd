part of 'uploadUsersBloc.dart';

abstract class UploadUsersState extends Equatable {
  const UploadUsersState();

  @override
  List<Object> get props => [];
}

class UploadUsersInitialState extends UploadUsersState {}

class UploadUsersLoadingState extends UploadUsersState {}

class UploadUsersSuccessState extends UploadUsersState {
  const UploadUsersSuccessState();

  @override
  List<Object> get props => [];
}

class UploadUsersErrorState extends UploadUsersState {
  final dynamic error;

  UploadUsersErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
