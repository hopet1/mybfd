import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/services/csv_services.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../service_locator.dart';

part 'uploadUsersEvent.dart';
part 'uploadUsersPage.dart';
part 'uploadUsersState.dart';

class UploadUsersBloc extends Bloc<UploadUsersEvent, UploadUsersState> {
  UploadUsersBloc({UploadUsersState initialState})
      : super(UploadUsersInitialState());

  @override
  Stream<UploadUsersState> mapEventToState(UploadUsersEvent event) async* {
    if (event is PickCSVEvent) {
      yield UploadUsersLoadingState();
    }
    if (event is UploadToFirebase) {
      yield UploadUsersLoadingState();

      try {
        var csvData = event.csvData;
        for (int i = 0; i < csvData.length; i++) {
          List<dynamic> currentRow = csvData[i];
          //csv data indexing
          // 0 = first name,
          // 1 = last name,
          // 2 = username,
          // 3 = email,
          // 4 = password
          UserModel userModel = UserModel(
            imgUrl: DUMMY_PROFILE_PHOTO_URL,
            email: currentRow[3],
            modified: DateTime.now(),
            created: DateTime.now(),
            uid: '', //will be assigned in createUsersFromCSV()
            username: currentRow[2],
            fcmToken: null,
            city: null,
            phoneNumber: null,
            postCode: null,
            streetAddress: null,
            firstName: currentRow[0],
            lastName: currentRow[1],
          );
          await locator<CSVServices>()
              .createUsersFromCSV(userModel, currentRow[4]);
        }
        yield UploadUsersSuccessState();
      } catch (e) {
        print(e);
        yield UploadUsersErrorState(error: e.toString());
      }
    }
    if (event is TryAgain) {
      yield UploadUsersInitialState();
    }
  }
}
