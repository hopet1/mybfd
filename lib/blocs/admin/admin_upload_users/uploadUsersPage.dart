part of 'uploadUsersBloc.dart';

class UploadCsvPage extends StatefulWidget {
  const UploadCsvPage({Key key}) : super(key: key);

  @override
  _UploadCsvPageState createState() => _UploadCsvPageState();
}

class _UploadCsvPageState extends State<UploadCsvPage> {
  List<List<dynamic>> csvData = [];
  List<dynamic> currentRow = [];

  // void iterate() async {
  //
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<UploadUsersBloc, UploadUsersState>(
        builder: (BuildContext context, state) {
          if (state is UploadUsersLoadingState) {
            return Spinner();
          }
          if (state is UploadUsersInitialState) {
            return PageStructureWidget(
              children: [
                Text(
                  "Upload users",
                  style: TextStyle(color: colorOrange, fontSize: 24),
                ),
                SizedBox(height: 42),
                Text("Total entries: ${csvData.length}"),
                SizedBox(height: 16),
                currentRow.isEmpty
                    ? Container()
                    : Text(
                        "first Column: ${currentRow[2]} : second Column: ${currentRow[3]}"),
                SizedBox(height: 16),
                csvData.isEmpty
                    ? Container()
                    : CsvTable(
                        csvData: csvData,
                      ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    button("pick csv", onTap: () async {
                      var data =
                          await locator<CSVServices>().pickCSVAndExtractData();
                      if (data == null) {
                        context.read<UploadUsersBloc>().add(TryAgain());
                      } else {
                        setState(() {
                          csvData = data;
                        });
                      }
                    }),
                    csvData.isEmpty
                        ? Container()
                        : Row(
                            children: [
                              button("Upload", onTap: () async {
                                context
                                    .read<UploadUsersBloc>()
                                    .add(UploadToFirebase(csvData: csvData));
                              }),
                              button("clear", onTap: () async {
                                setState(() {
                                  csvData.clear();
                                });
                              }),
                            ],
                          ),
                  ],
                )
              ],
            );
          }

          if (state is UploadUsersSuccessState) {
            return PageStructureWidget(
              children: [
                Text(
                  "Upload users",
                  style: TextStyle(color: colorOrange, fontSize: 24),
                ),
                SizedBox(height: 64),
                Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Text(
                          'Users successfully uploaded!',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 21,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                      RoundedButton(
                        background: colorOrange,
                        foreground: Colors.white,
                        onTap: () {
                          Navigator.pop(context);
                        },
                        text: 'Go Back?',
                      ),
                    ],
                  ),
                )
              ],
            );
          }

          if (state is UploadUsersErrorState) {
            final String errorMessage =
                state.error ?? "Operation failed! Did you pick the csv file?";
            final String message = errorMessage.contains("email-already-in-use")
                ? "Some or any of the provided emails is already registered with MyBFD, Please upload a new csv file or try again."
                : errorMessage;
            return PageStructureWidget(children: [
              Text(
                "Upload Users",
                style: TextStyle(color: colorOrange, fontSize: 24),
              ),
              SizedBox(height: 42),
              Text("Something went wrong!"),
              SizedBox(height: 16),
              Center(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Text(
                        '$message',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 21,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                    RoundedButton(
                      background: colorOrange,
                      foreground: Colors.white,
                      onTap: () {
                        context.read<UploadUsersBloc>().add(TryAgain());
                      },
                      text: 'Try Again?',
                    ),
                  ],
                ),
              )
            ]);
          }
          return Container();
        },
        listener: (BuildContext context, state) {},
      ),
    );
  }

  Widget button(title, {Function onTap}) {
    return MaterialButton(
      onPressed: onTap,
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "$title",
            style: TextStyle(color: Colors.white),
          ),
        ),
        color: colorOrange,
      ),
    );
  }
}

class CsvTable extends StatelessWidget {
  List<String> headerNames = [
    "#",
    "First Name",
    "Last Name",
    "Username",
    "Email",
    "Password"
  ];
  final List<List<dynamic>> csvData;
  CsvTable({this.csvData});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12),
      child: Column(
        children: [
          tableHeadPackage(),
          csvContent(),
        ],
      ),
    );
  }

  Widget tableHeadPackage() {
    return Table(
      columnWidths: {
        0: FlexColumnWidth(1),
        1: FlexColumnWidth(2),
        2: FlexColumnWidth(2),
        3: FlexColumnWidth(2),
        4: FlexColumnWidth(4),
        5: FlexColumnWidth(2),
      },
      children: [
        TableRow(
            decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(5), topLeft: Radius.circular(5))),
            children: List.generate(
              headerNames.length,
              (index) => Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: Center(
                  child: Text(
                    headerNames[index],
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 10),
                  ),
                ),
              ),
            )),
      ],
    );
  }

  Widget csvContent() {
    return Table(
      columnWidths: {
        0: FlexColumnWidth(1),
        1: FlexColumnWidth(2),
        2: FlexColumnWidth(2),
        3: FlexColumnWidth(2),
        4: FlexColumnWidth(4),
        5: FlexColumnWidth(2),
      },
      children: List.generate(
          csvData.length,
          (i) => TableRow(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(5),
                      bottomLeft: Radius.circular(5))),
              children: List.generate(
                //columns
                csvData[i].length + 1,
                (index) {
                  List<dynamic> d = csvData[i == csvData[i].length ? i - 1 : i];
                  print(index);
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: Center(
                      child: Text(
                        index == 0 ? "${i + 1}" : d[index - 1],
                        style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.normal,
                            fontSize: 10),
                      ),
                    ),
                  );
                },
              ))),
    );
  }
}
