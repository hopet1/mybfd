part of 'admin_bloc.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _newCourseTitleController =
      TextEditingController();

  final TextEditingController _newCourseSummaryController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminBloc, AdminState>(
        builder: (context, state) {
          if (state is AdminLoadingState) {
            return Spinner();
          }

          if (state is AdminLoadedState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Admin',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () {
                    Route route = MaterialPageRoute(
                      builder: (BuildContext context) => BlocProvider(
                        create: (BuildContext context) => AdminCoursesBloc()
                          ..add(
                            LoadCoursesEvent(),
                          ),
                        child: AdminCoursesPage(),
                      ),
                    );
                    Navigator.push(context, route);
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Edit Courses',
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () {
                    Route route = MaterialPageRoute(
                      builder: (BuildContext context) => BlocProvider(
                        create: (BuildContext context) => UploadUsersBloc(
                            initialState: UploadUsersInitialState()),
                        child: UploadCsvPage(),
                      ),
                    );

                    Navigator.push(context, route);
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Upload Users',
                ),
              ],
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
