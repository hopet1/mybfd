part of 'admin_modules_bloc.dart';

class AdminModulesPage extends StatefulWidget {
  @override
  _AdminModulesPageState createState() => _AdminModulesPageState();
}

class _AdminModulesPageState extends State<AdminModulesPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();

  final TextEditingController _summaryController = TextEditingController();

  final TextEditingController _youtubeUrlController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminModulesBloc, AdminModulesState>(
        builder: (context, state) {
          if (state is ModulesLoadingState) {
            return Spinner();
          }

          if (state is ModulesLoadedState) {
            final CourseModel course = state.course;
            final List<ModuleModel> modules = state.modules;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'Modules',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: modules.length,
                  itemBuilder: (context, index) {
                    final ModuleModel module = modules[index];

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            final bool confirm =
                                await locator<ModalService>().showConfirmation(
                              context: context,
                              title: 'Delete Module: ${module.title}',
                              message:
                                  'This will delete all attached lessons, activities, and questions. Are you sure?',
                            );

                            if (!confirm) return;

                            context.read<AdminModulesBloc>().add(
                                  DeleteModuleEvent(
                                    moduleID: module.id,
                                  ),
                                );
                          },
                        ),
                        InfoWidget(
                          onTap: () {
                            Route route = MaterialPageRoute(
                              builder: (BuildContext context) => BlocProvider(
                                create: (BuildContext context) =>
                                    AdminLessonsBloc(
                                        course: course, module: module)
                                      ..add(
                                        LoadLessonsEvent(),
                                      ),
                                child: AdminLessonsPage(),
                              ),
                            );
                            Navigator.push(context, route);
                          },
                          title: '${module.position + 1}. ' + module.title,
                        ),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            context.read<AdminModulesBloc>().add(
                                  GoToEditModuleEvent(
                                    module: module,
                                  ),
                                );
                          },
                        ),
                      ],
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add Module',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () {
                        context.read<AdminModulesBloc>().add(
                              GoToAddModuleEvent(),
                            );
                      },
                    ),
                    RoundedButton(
                      text: 'Sort Modules',
                      foreground: colorOrange,
                      background: Colors.white,
                      onTap: () {
                        context.read<AdminModulesBloc>().add(
                              GoToSortModulesEvent(),
                            );
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is SortModulesState) {
            final List<ModuleModel> modules = state.modules;

            var itemRows = modules.map((module) {
              return ReorderableTableRow(
                //a key must be specified for each row
                key: ObjectKey(module),
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    // modules.map((module) => Text('hello')).toList()
                    <Widget>[
                  Text(
                    '${module.position + 1}',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text('${module.title}'),
                ],
              );
            }).toList();

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminModulesBloc>()
                      .add(GoToModulesLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Sort Modules',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReorderableTable(
                  header: ReorderableTableRow(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Position', textScaleFactor: 1.5),
                      Text('Title', textScaleFactor: 1.5),
                    ],
                  ),
                  children: itemRows,
                  onReorder: (int oldIndex, int newIndex) {
                    context.read<AdminModulesBloc>().add(
                          ShallowSubmitSortModulesEvent(
                            oldIndex: oldIndex,
                            newIndex: newIndex,
                          ),
                        );
                  },
                  onNoReorder: (int index) {
                    //this callback is optional
                    debugPrint(
                        '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminModulesBloc>().add(
                          SubmitSortModulesEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is EditModuleState) {
            final ModuleModel module = state.module;

            _titleController.text = module.title;
            _summaryController.text = module.summary;
            _youtubeUrlController.text =
                module.youtubeUrl == null ? '' : module.youtubeUrl;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminModulesBloc>()
                      .add(GoToModulesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Edit Module',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _titleController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            hintText: 'Title',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _summaryController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.text_fields,
                              color: Colors.black,
                            ),
                            hintText: 'Summary',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _youtubeUrlController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.youtube_searched_for_rounded,
                              color: Colors.black,
                            ),
                            hintText: 'YouTube URL',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminModulesBloc>().add(
                          SubmitEditModuleEvent(
                            module: ModuleModel(
                              id: module.id,
                              summary: _summaryController.text,
                              title: _titleController.text,
                              youtubeUrl: _youtubeUrlController.text.isEmpty
                                  ? null
                                  : _youtubeUrlController.text,
                              position: module.position,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is AddModuleState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminModulesBloc>()
                      .add(GoToModulesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Add Module',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _titleController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            hintText: 'Title',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _summaryController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.text_fields,
                              color: Colors.black,
                            ),
                            hintText: 'Summary',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _youtubeUrlController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.youtube_searched_for_rounded,
                              color: Colors.black,
                            ),
                            hintText: 'YouTube URL',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminModulesBloc>().add(
                          SubmitAddModuleEvent(
                            module: ModuleModel(
                              id: null,
                              summary: _summaryController.text,
                              title: _titleController.text,
                              youtubeUrl: _youtubeUrlController.text.isEmpty
                                  ? null
                                  : _youtubeUrlController.text,
                              position: null,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is ModuleErrorState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminModulesBloc>()
                      .add(GoToModulesLoadedEvent());
                },
              ),
              children: [
                Center(
                  child: Text(
                    'Error: ${state.error.toString()}',
                  ),
                )
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is AddModuleState) {
            _clearTextControllers();
          }
        },
      ),
    );
  }

  void _clearTextControllers() {
    _titleController.clear();
    _summaryController.clear();
    _youtubeUrlController.clear();
  }
}
