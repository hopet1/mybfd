import 'dart:async';

import 'package:MyBFD/blocs/admin/admin_lessons/admin_lessons_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ModuleService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

part 'admin_modules_event.dart';
part 'admin_modules_page.dart';
part 'admin_modules_state.dart';

class AdminModulesBloc extends Bloc<AdminModulesEvent, AdminModulesState> {
  AdminModulesBloc({@required this.course}) : super(ModulesInitialState());
  final CourseModel course;
  List<ModuleModel> _modules = [];

  @override
  Stream<AdminModulesState> mapEventToState(
    AdminModulesEvent event,
  ) async* {
    yield ModulesLoadingState();

    if (event is LoadModulesEvent) {
      try {
        _modules =
            await locator<ModuleService>().retrieveModules(courseID: course.id);
        add(GoToModulesLoadedEvent());
      } catch (error) {
        yield ModuleErrorState(error: error);
      }
    }

    if (event is GoToModulesLoadedEvent) {
      _modules.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield ModulesLoadedState(course: course, modules: _modules);
    }

    if (event is GoToEditModuleEvent) {
      yield EditModuleState(module: event.module);
    }

    if (event is SubmitEditModuleEvent) {
      try {
        final ModuleModel module = event.module;

        await locator<ModuleService>().updateModule(
          courseID: course.id,
          moduleID: module.id,
          data: {
            'title': module.title,
            'summary': module.summary,
            'youtubeUrl': module.youtubeUrl,
          },
        );

        add(LoadModulesEvent()); //Note, only doing this here becase editing a module is messy.
      } catch (error) {
        yield ModuleErrorState(error: error);
      }
    }

    if (event is GoToSortModulesEvent) {
      _modules.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield SortModulesState(modules: _modules);
    }

    if (event is SubmitSortModulesEvent) {
      for (int i = 0; i < _modules.length; i++) {
        ModuleModel c = _modules[i];

        await locator<ModuleService>()
            .updateModule(courseID: course.id, moduleID: c.id, data: {
          'position': c.position,
        });
      }

      add(LoadModulesEvent());
    }

    if (event is ShallowSubmitSortModulesEvent) {
      final int oldIndex = event.oldIndex;
      final int newIndex = event.newIndex;

      ModuleModel module = _modules.removeAt(oldIndex);
      _modules.insert(newIndex, module);

      _updateModulePositionValues();

      add(GoToSortModulesEvent());
    }

    if (event is GoToAddModuleEvent) {
      yield AddModuleState();
    }

    if (event is SubmitAddModuleEvent) {
      try {
        final ModuleModel module = event.module;

        module.position = _modules.length;

        await locator<ModuleService>()
            .createModule(courseID: course.id, module: module);

        _modules.add((module));

        add(GoToModulesLoadedEvent());
      } catch (error) {
        yield ModuleErrorState(error: error);
      }
    }

    if (event is DeleteModuleEvent) {
      try {
        final String moduleID = event.moduleID;

        await locator<ModuleService>()
            .deleteModule(courseID: course.id, moduleID: moduleID);

        _modules.removeWhere((module) => module.id == moduleID);

        _updateModulePositionValues();

        add(SubmitSortModulesEvent());
      } catch (error) {
        yield ModuleErrorState(error: error);
      }
    }
  }

  void _updateModulePositionValues() {
    for (int i = 0; i < _modules.length; i++) {
      ModuleModel c = _modules[i];

      c.position = i;
    }
  }
}
