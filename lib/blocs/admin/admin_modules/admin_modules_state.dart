part of 'admin_modules_bloc.dart';

abstract class AdminModulesState extends Equatable {
  const AdminModulesState();

  @override
  List<Object> get props => [];
}

class ModulesInitialState extends AdminModulesState {}

class ModulesLoadingState extends AdminModulesState {}

class ModulesLoadedState extends AdminModulesState {
  final CourseModel course;
  final List<ModuleModel> modules;

  const ModulesLoadedState({
    @required this.course,
    @required this.modules,
  });

  @override
  List<Object> get props => [
        course,
        modules,
      ];
}

class SortModulesState extends AdminModulesState {
  final List<ModuleModel> modules;

  const SortModulesState({
    @required this.modules,
  });

  @override
  List<Object> get props => [
        modules,
      ];
}

class EditModuleState extends AdminModulesState {
  final ModuleModel module;

  const EditModuleState({
    @required this.module,
  });

  @override
  List<Object> get props => [
        module,
      ];
}

class AddModuleState extends AdminModulesState {
  const AddModuleState();

  @override
  List<Object> get props => [];
}

class ModuleErrorState extends AdminModulesState {
  final dynamic error;

  ModuleErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
