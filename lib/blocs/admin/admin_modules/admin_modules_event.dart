part of 'admin_modules_bloc.dart';

abstract class AdminModulesEvent extends Equatable {
  const AdminModulesEvent();

  @override
  List<Object> get props => [];
}

class LoadModulesEvent extends AdminModulesEvent {}

class GoToModulesLoadedEvent extends AdminModulesEvent {}

class GoToSortModulesEvent extends AdminModulesEvent {}

class SubmitSortModulesEvent extends AdminModulesEvent {
  const SubmitSortModulesEvent();

  @override
  List<Object> get props => [];
}

class ShallowSubmitSortModulesEvent extends AdminModulesEvent {
  final int oldIndex;
  final int newIndex;

  const ShallowSubmitSortModulesEvent({
    @required this.oldIndex,
    @required this.newIndex,
  });

  @override
  List<Object> get props => [
        oldIndex,
        newIndex,
      ];
}

class GoToEditModuleEvent extends AdminModulesEvent {
  final ModuleModel module;

  const GoToEditModuleEvent({
    @required this.module,
  });

  @override
  List<Object> get props => [
        module,
      ];
}

class SubmitEditModuleEvent extends AdminModulesEvent {
  final ModuleModel module;

  const SubmitEditModuleEvent({
    @required this.module,
  });

  @override
  List<Object> get props => [
        module,
      ];
}

class GoToAddModuleEvent extends AdminModulesEvent {}

class SubmitAddModuleEvent extends AdminModulesEvent {
  final ModuleModel module;

  const SubmitAddModuleEvent({
    @required this.module,
  });

  @override
  List<Object> get props => [
        module,
      ];
}

class DeleteModuleEvent extends AdminModulesEvent {
  final String moduleID;

  const DeleteModuleEvent({
    @required this.moduleID,
  });

  @override
  List<Object> get props => [
        moduleID,
      ];
}
