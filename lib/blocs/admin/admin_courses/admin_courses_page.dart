part of 'admin_courses_bloc.dart';

class AdminCoursesPage extends StatefulWidget {
  @override
  _AdminCoursesPageState createState() => _AdminCoursesPageState();
}

class _AdminCoursesPageState extends State<AdminCoursesPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _newCoursesTitleController =
      TextEditingController();

  final TextEditingController _newCoursesSummaryController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminCoursesBloc, AdminCoursesState>(
        builder: (context, state) {
          if (state is CoursesLoadingState) {
            return Spinner();
          }

          if (state is CoursesLoadedState) {
            final List<CourseModel> courses = state.courses;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'Courses',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: Responsive.isMobile(context) ? 24 : 26),
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: courses.length,
                  itemBuilder: (context, index) {
                    final CourseModel course = courses[index];

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (Responsive.isDesktop(context))
                          SizedBox(width: screenWidth * 0.2),
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            final bool confirm =
                                await locator<ModalService>().showConfirmation(
                              context: context,
                              title: 'Delete Course: ${course.title}',
                              message:
                                  'This will delete all attached modules, lessons, activities, and questions. Are you sure?',
                            );

                            if (!confirm) return;

                            context.read<AdminCoursesBloc>().add(
                                  DeleteCourseEvent(
                                    courseID: course.id,
                                  ),
                                );
                          },
                        ),
                        InfoWidget(
                          onTap: () {
                            Route route = MaterialPageRoute(
                              builder: (BuildContext context) => BlocProvider(
                                create: (BuildContext context) =>
                                    AdminModulesBloc(course: course)
                                      ..add(
                                        LoadModulesEvent(),
                                      ),
                                child: AdminModulesPage(),
                              ),
                            );
                            Navigator.push(context, route);
                          },
                          title: '${course.position + 1}. ' + course.title,
                        ),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            context.read<AdminCoursesBloc>().add(
                                  GoToEditCourseEvent(
                                    course: course,
                                  ),
                                );
                          },
                        ),
                        if (Responsive.isDesktop(context))
                          SizedBox(width: screenWidth * 0.2),
                      ],
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add Course',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () {
                        context.read<AdminCoursesBloc>().add(
                              GoToAddCourseEvent(),
                            );
                      },
                    ),
                    RoundedButton(
                      text: 'Sort Courses',
                      foreground: colorOrange,
                      background: Colors.white,
                      onTap: () {
                        context.read<AdminCoursesBloc>().add(
                              GoToSortCoursesEvent(),
                            );
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is SortCoursesState) {
            final List<CourseModel> courses = state.courses;

            var itemRows = courses.map((course) {
              return ReorderableTableRow(
                //a key must be specified for each row
                key: ObjectKey(course),
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    // courses.map((course) => Text('hello')).toList()
                    <Widget>[
                  Text(
                    '${course.position + 1}',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text('${course.title}'),
                ],
              );
            }).toList();

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminCoursesBloc>()
                      .add(GoToCoursesLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Sort Courses',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReorderableTable(
                  header: ReorderableTableRow(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Position', textScaleFactor: 1.5),
                      Text('Title', textScaleFactor: 1.5),
                    ],
                  ),
                  children: itemRows,
                  onReorder: (int oldIndex, int newIndex) {
                    context.read<AdminCoursesBloc>().add(
                          ShallowSubmitSortCoursesEvent(
                            oldIndex: oldIndex,
                            newIndex: newIndex,
                          ),
                        );
                  },
                  onNoReorder: (int index) {
                    //this callback is optional
                    debugPrint(
                        '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminCoursesBloc>().add(
                          SubmitSortCoursesEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is EditCourseState) {
            final CourseModel course = state.course;

            _newCoursesTitleController.text = course.title;
            _newCoursesSummaryController.text = course.summary;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminCoursesBloc>()
                      .add(GoToCoursesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Edit Course',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _newCoursesTitleController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            // border: OutlineInputBorder(
                            //   // width: 0.0 produces a thin "hairline" border
                            //   borderRadius: BorderRadius.all(
                            //     Radius.circular(90.0),
                            //   ),
                            //   borderSide: BorderSide.none,

                            //   //borderSide: const BorderSide(),
                            // ),
                            // hintStyle: TextStyle(
                            //     color: Colors.white, fontFamily: "WorkSansLight"),
                            // filled: true,
                            // fillColor: Colors.white24,
                            hintText: 'Title',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _newCoursesSummaryController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.text_fields,
                              color: Colors.black,
                            ),
                            // border: OutlineInputBorder(
                            //   // width: 0.0 produces a thin "hairline" border
                            //   borderRadius: BorderRadius.all(
                            //     Radius.circular(90.0),
                            //   ),
                            //   borderSide: BorderSide.none,

                            //   //borderSide: const BorderSide(),
                            // ),
                            // hintStyle: TextStyle(
                            //     color: Colors.white, fontFamily: "WorkSansLight"),
                            // filled: true,
                            // fillColor: Colors.white24,
                            hintText: 'Summary',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminCoursesBloc>().add(
                          SubmitEditCourseEvent(
                            courseID: course.id,
                            title: _newCoursesTitleController.text,
                            summary: _newCoursesSummaryController.text,
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is AddCourseState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminCoursesBloc>()
                      .add(GoToCoursesLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Add Course',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            if (!Responsive.isMobile(context))
                              Spacer(
                                  flex: Responsive.isDesktop(context) ? 2 : 1),
                            Expanded(
                              flex: 3,
                              child: TextFormField(
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                validator: locator<ValidationService>().isEmpty,
                                controller: _newCoursesTitleController,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                    color: Colors.black,
                                  ),
                                  prefixIcon: Icon(
                                    Icons.person,
                                    color: Colors.black,
                                  ),
                                  // border: OutlineInputBorder(
                                  //   // width: 0.0 produces a thin "hairline" border
                                  //   borderRadius: BorderRadius.all(
                                  //     Radius.circular(90.0),
                                  //   ),
                                  //   borderSide: BorderSide.none,

                                  //   //borderSide: const BorderSide(),
                                  // ),
                                  // hintStyle: TextStyle(
                                  //     color: Colors.white, fontFamily: "WorkSansLight"),
                                  // filled: true,
                                  // fillColor: Colors.white24,
                                  hintText: 'Title',
                                ),
                              ),
                            ),
                            if (!Responsive.isMobile(context))
                              Spacer(
                                  flex: Responsive.isDesktop(context) ? 2 : 1),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            if (!Responsive.isMobile(context))
                              Spacer(
                                  flex: Responsive.isDesktop(context) ? 2 : 1),
                            Expanded(
                              flex: 3,
                              child: TextFormField(
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                validator: locator<ValidationService>().isEmpty,
                                controller: _newCoursesSummaryController,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                    color: Colors.black,
                                  ),
                                  prefixIcon: Icon(
                                    Icons.text_fields,
                                    color: Colors.black,
                                  ),
                                  // border: OutlineInputBorder(
                                  //   // width: 0.0 produces a thin "hairline" border
                                  //   borderRadius: BorderRadius.all(
                                  //     Radius.circular(90.0),
                                  //   ),
                                  //   borderSide: BorderSide.none,

                                  //   //borderSide: const BorderSide(),
                                  // ),
                                  // hintStyle: TextStyle(
                                  //     color: Colors.white, fontFamily: "WorkSansLight"),
                                  // filled: true,
                                  // fillColor: Colors.white24,
                                  hintText: 'Summary',
                                ),
                              ),
                            ),
                            if (!Responsive.isMobile(context))
                              Spacer(
                                  flex: Responsive.isDesktop(context) ? 2 : 1),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminCoursesBloc>().add(
                          SubmitAddCourseEvent(
                            course: CourseModel(
                              id: null,
                              summary: _newCoursesSummaryController.text,
                              title: _newCoursesTitleController.text,
                              position: null,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
                SizedBox(height: 20),
              ],
            );
          }

          if (state is CourseErrorState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminCoursesBloc>()
                      .add(GoToCoursesLoadedEvent());
                },
              ),
              children: [
                Center(
                  child: Text(
                    'Error: ${state.error.toString()}',
                  ),
                )
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is AddCourseState) {
            _clearTextControllers();
          }
        },
      ),
    );
  }

  void _clearTextControllers() {
    _newCoursesTitleController.clear();
    _newCoursesSummaryController.clear();
  }
}
