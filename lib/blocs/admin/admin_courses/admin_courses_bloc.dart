import 'dart:async';

import 'package:MyBFD/blocs/admin/admin_modules/admin_modules_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/CourseService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

part 'admin_courses_event.dart';
part 'admin_courses_page.dart';
part 'admin_courses_state.dart';

class AdminCoursesBloc extends Bloc<AdminCoursesEvent, AdminCoursesState> {
  AdminCoursesBloc() : super(CoursesInitialState());

  List<CourseModel> _courses = [];

  @override
  Stream<AdminCoursesState> mapEventToState(
    AdminCoursesEvent event,
  ) async* {
    yield CoursesLoadingState();

    if (event is LoadCoursesEvent) {
      try {
        _courses = await locator<CourseService>().retrieveCourses();
        add(GoToCoursesLoadedEvent());
      } catch (error) {
        yield CourseErrorState(error: error);
      }
    }

    if (event is GoToCoursesLoadedEvent) {
      _courses.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield CoursesLoadedState(courses: _courses);
    }

    if (event is GoToEditCourseEvent) {
      yield EditCourseState(course: event.course);
    }

    if (event is SubmitEditCourseEvent) {
      try {
        final String courseID = event.courseID;
        final String title = event.title;
        final String summary = event.summary;

        await locator<CourseService>().updateCourse(
          courseID: courseID,
          data: {
            'title': title,
            'summary': summary,
          },
        );

        add(LoadCoursesEvent()); //Note, only doing this here becase editing a course is messy.
      } catch (error) {
        yield CourseErrorState(error: error);
      }
    }

    if (event is GoToSortCoursesEvent) {
      _courses.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield SortCoursesState(courses: _courses);
    }

    if (event is SubmitSortCoursesEvent) {
      for (int i = 0; i < _courses.length; i++) {
        CourseModel c = _courses[i];

        await locator<CourseService>()
            .updateCourse(courseID: c.id, data: {'position': c.position});
      }

      add(LoadCoursesEvent());
    }

    if (event is ShallowSubmitSortCoursesEvent) {
      final int oldIndex = event.oldIndex;
      final int newIndex = event.newIndex;

      CourseModel course = _courses.removeAt(oldIndex);
      _courses.insert(newIndex, course);

      _updateCoursePositionValues();

      add(GoToSortCoursesEvent());
    }

    if (event is GoToAddCourseEvent) {
      yield AddCourseState();
    }

    if (event is SubmitAddCourseEvent) {
      try {
        final CourseModel course = event.course;

        course.position = _courses.length;

        await locator<CourseService>().createCourse(course: course);

        _courses.add((course));

        add(GoToCoursesLoadedEvent());
      } catch (error) {
        yield CourseErrorState(error: error);
      }
    }

    if (event is DeleteCourseEvent) {
      try {
        final String courseID = event.courseID;

        await locator<CourseService>().deleteCourse(courseID: courseID);

        _courses.removeWhere((course) => course.id == courseID);

        _updateCoursePositionValues();

        add(SubmitSortCoursesEvent());
      } catch (error) {
        yield CourseErrorState(error: error);
      }
    }
  }

  void _updateCoursePositionValues() {
    for (int i = 0; i < _courses.length; i++) {
      CourseModel c = _courses[i];

      c.position = i;
    }
  }
}
