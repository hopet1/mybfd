part of 'admin_courses_bloc.dart';

abstract class AdminCoursesState extends Equatable {
  const AdminCoursesState();

  @override
  List<Object> get props => [];
}

class CoursesInitialState extends AdminCoursesState {}

class CoursesLoadingState extends AdminCoursesState {}

class CoursesLoadedState extends AdminCoursesState {
  final List<CourseModel> courses;

  const CoursesLoadedState({
    @required this.courses,
  });

  @override
  List<Object> get props => [
        courses,
      ];
}

class SortCoursesState extends AdminCoursesState {
  final List<CourseModel> courses;

  const SortCoursesState({
    @required this.courses,
  });

  @override
  List<Object> get props => [
        courses,
      ];
}

class EditCourseState extends AdminCoursesState {
  final CourseModel course;

  const EditCourseState({
    @required this.course,
  });

  @override
  List<Object> get props => [
        course,
      ];
}

class AddCourseState extends AdminCoursesState {
  const AddCourseState();

  @override
  List<Object> get props => [];
}

class CourseErrorState extends AdminCoursesState {
  final dynamic error;

  CourseErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
