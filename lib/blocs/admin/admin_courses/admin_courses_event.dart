part of 'admin_courses_bloc.dart';

abstract class AdminCoursesEvent extends Equatable {
  const AdminCoursesEvent();

  @override
  List<Object> get props => [];
}

class LoadCoursesEvent extends AdminCoursesEvent {}

class GoToCoursesLoadedEvent extends AdminCoursesEvent {}

class GoToSortCoursesEvent extends AdminCoursesEvent {}

class SubmitSortCoursesEvent extends AdminCoursesEvent {
  const SubmitSortCoursesEvent();

  @override
  List<Object> get props => [];
}

class ShallowSubmitSortCoursesEvent extends AdminCoursesEvent {
  final int oldIndex;
  final int newIndex;

  const ShallowSubmitSortCoursesEvent({
    @required this.oldIndex,
    @required this.newIndex,
  });

  @override
  List<Object> get props => [
        oldIndex,
        newIndex,
      ];
}

class GoToEditCourseEvent extends AdminCoursesEvent {
  final CourseModel course;

  const GoToEditCourseEvent({
    @required this.course,
  });

  @override
  List<Object> get props => [
        course,
      ];
}

class SubmitEditCourseEvent extends AdminCoursesEvent {
  final String courseID;
  final String title;
  final String summary;

  const SubmitEditCourseEvent({
    @required this.courseID,
    @required this.title,
    @required this.summary,
  });

  @override
  List<Object> get props => [
        courseID,
        title,
        summary,
      ];
}

class GoToAddCourseEvent extends AdminCoursesEvent {}

class SubmitAddCourseEvent extends AdminCoursesEvent {
  final CourseModel course;

  const SubmitAddCourseEvent({
    @required this.course,
  });

  @override
  List<Object> get props => [
        course,
      ];
}

class DeleteCourseEvent extends AdminCoursesEvent {
  final String courseID;

  const DeleteCourseEvent({
    @required this.courseID,
  });

  @override
  List<Object> get props => [
        courseID,
      ];
}
