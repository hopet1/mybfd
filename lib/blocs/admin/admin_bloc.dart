import 'dart:async';

import 'package:MyBFD/blocs/admin/admin_courses/admin_courses_bloc.dart';
import 'package:MyBFD/blocs/admin/admin_upload_users/uploadUsersBloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'admin_event.dart';
part 'admin_page.dart';
part 'admin_state.dart';

class AdminBloc extends Bloc<AdminEvent, AdminState> {
  AdminBloc() : super(AdminInitialState());

  @override
  Stream<AdminState> mapEventToState(
    AdminEvent event,
  ) async* {
    yield AdminLoadingState();

    if (event is LoadAdminEvent) {
      try {
        yield AdminLoadedState();
      } catch (error) {
        yield AdminErrorState(error: error);
      }
    }
  }
}
