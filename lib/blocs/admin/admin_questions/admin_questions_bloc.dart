import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/models/QuestionModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/QuestionService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

part 'admin_questions_event.dart';
part 'admin_questions_page.dart';
part 'admin_questions_state.dart';

class AdminQuestionsBloc
    extends Bloc<AdminQuestionsEvent, AdminQuestionsState> {
  AdminQuestionsBloc({
    @required this.course,
    @required this.module,
    @required this.lesson,
  }) : super(QuestionsInitialState());
  final CourseModel course;
  final ModuleModel module;
  final LessonModel lesson;

  List<QuestionModel> _questions = [];

  @override
  Stream<AdminQuestionsState> mapEventToState(
    AdminQuestionsEvent event,
  ) async* {
    yield QuestionsLoadingState();

    if (event is LoadQuestionsEvent) {
      try {
        _questions = await locator<QuestionService>().retrieveQuestions(
            courseID: course.id, moduleID: module.id, lessonID: lesson.id);
        add(GoToQuestionsLoadedEvent());
      } catch (error) {
        yield QuestionErrorState(error: error);
      }
    }

    if (event is GoToQuestionsLoadedEvent) {
      _questions.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield QuestionsLoadedState(
          course: course,
          module: module,
          lesson: lesson,
          questions: _questions);
    }

    if (event is GoToEditQuestionEvent) {
      yield EditQuestionState(question: event.question);
    }

    if (event is SubmitEditQuestionEvent) {
      try {
        final QuestionModel question = event.question;

        await locator<QuestionService>().updateQuestion(
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
          questionID: question.id,
          data: question.toMap(),
        );

        add(LoadQuestionsEvent()); //Note, only doing this here becase editing a module is messy.
      } catch (error) {
        yield QuestionErrorState(error: error);
      }
    }

    if (event is GoToSortQuestionsEvent) {
      _questions.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield SortQuestionsState(questions: _questions);
    }

    if (event is SubmitSortQuestionsEvent) {
      for (int i = 0; i < _questions.length; i++) {
        QuestionModel question = _questions[i];

        await locator<QuestionService>().updateQuestion(
            courseID: course.id,
            moduleID: module.id,
            lessonID: lesson.id,
            questionID: question.id,
            data: {
              'position': question.position,
            });
      }

      add(LoadQuestionsEvent());
    }

    if (event is ShallowSubmitSortQuestionsEvent) {
      final int oldIndex = event.oldIndex;
      final int newIndex = event.newIndex;

      QuestionModel module = _questions.removeAt(oldIndex);
      _questions.insert(newIndex, module);

      _updateQuestionPositionValues();

      add(GoToSortQuestionsEvent());
    }

    if (event is GoToAddQuestionEvent) {
      yield AddQuestionState();
    }

    if (event is SubmitAddQuestionEvent) {
      try {
        final QuestionModel question = event.question;

        question.position = _questions.length;

        await locator<QuestionService>().createQuestion(
            courseID: course.id,
            moduleID: module.id,
            lessonID: lesson.id,
            question: question);

        _questions.add((question));

        add(GoToQuestionsLoadedEvent());
      } catch (error) {
        yield QuestionErrorState(error: error);
      }
    }

    if (event is DeleteQuestionEvent) {
      try {
        final String questionID = event.questionID;

        await locator<QuestionService>().deleteQuestion(
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
          questionID: questionID,
        );

        _questions.removeWhere((question) => question.id == questionID);

        _updateQuestionPositionValues();

        add(SubmitSortQuestionsEvent());
      } catch (error) {
        yield QuestionErrorState(error: error);
      }
    }
  }

  void _updateQuestionPositionValues() {
    for (int i = 0; i < _questions.length; i++) {
      QuestionModel c = _questions[i];

      c.position = i;
    }
  }
}
