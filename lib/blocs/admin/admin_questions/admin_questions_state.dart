part of 'admin_questions_bloc.dart';

abstract class AdminQuestionsState extends Equatable {
  const AdminQuestionsState();

  @override
  List<Object> get props => [];
}

class QuestionsInitialState extends AdminQuestionsState {}

class QuestionsLoadingState extends AdminQuestionsState {}

class QuestionsLoadedState extends AdminQuestionsState {
  final CourseModel course;
  final ModuleModel module;
  final LessonModel lesson;

  final List<QuestionModel> questions;

  const QuestionsLoadedState({
    @required this.course,
    @required this.module,
    @required this.lesson,
    @required this.questions,
  });

  @override
  List<Object> get props => [
        course,
        module,
        lesson,
        questions,
      ];
}

class SortQuestionsState extends AdminQuestionsState {
  final List<QuestionModel> questions;

  const SortQuestionsState({
    @required this.questions,
  });

  @override
  List<Object> get props => [
        questions,
      ];
}

class EditQuestionState extends AdminQuestionsState {
  final QuestionModel question;

  const EditQuestionState({
    @required this.question,
  });

  @override
  List<Object> get props => [
        question,
      ];
}

class AddQuestionState extends AdminQuestionsState {
  const AddQuestionState();

  @override
  List<Object> get props => [];
}

class QuestionErrorState extends AdminQuestionsState {
  final dynamic error;

  QuestionErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
