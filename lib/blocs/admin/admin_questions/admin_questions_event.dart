part of 'admin_questions_bloc.dart';

//NOTE: FOR SOME ODD REASON, I HAVE TO NAME THIS FILE 'eventt' OTHERWISE THE IMPORT WON'T WORK.
abstract class AdminQuestionsEvent extends Equatable {
  const AdminQuestionsEvent();

  @override
  List<Object> get props => [];
}

class LoadQuestionsEvent extends AdminQuestionsEvent {}

class GoToQuestionsLoadedEvent extends AdminQuestionsEvent {}

class GoToSortQuestionsEvent extends AdminQuestionsEvent {}

class SubmitSortQuestionsEvent extends AdminQuestionsEvent {
  const SubmitSortQuestionsEvent();

  @override
  List<Object> get props => [];
}

class ShallowSubmitSortQuestionsEvent extends AdminQuestionsEvent {
  final int oldIndex;
  final int newIndex;

  const ShallowSubmitSortQuestionsEvent({
    @required this.oldIndex,
    @required this.newIndex,
  });

  @override
  List<Object> get props => [
        oldIndex,
        newIndex,
      ];
}

class GoToEditQuestionEvent extends AdminQuestionsEvent {
  final QuestionModel question;

  const GoToEditQuestionEvent({
    @required this.question,
  });

  @override
  List<Object> get props => [
        question,
      ];
}

class SubmitEditQuestionEvent extends AdminQuestionsEvent {
  final QuestionModel question;

  const SubmitEditQuestionEvent({
    @required this.question,
  });

  @override
  List<Object> get props => [
        question,
      ];
}

class GoToAddQuestionEvent extends AdminQuestionsEvent {}

class SubmitAddQuestionEvent extends AdminQuestionsEvent {
  final QuestionModel question;

  const SubmitAddQuestionEvent({
    @required this.question,
  });

  @override
  List<Object> get props => [
        question,
      ];
}

class DeleteQuestionEvent extends AdminQuestionsEvent {
  final String questionID;

  const DeleteQuestionEvent({
    @required this.questionID,
  });

  @override
  List<Object> get props => [
        questionID,
      ];
}
