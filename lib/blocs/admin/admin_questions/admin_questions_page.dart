part of 'admin_questions_bloc.dart';

class AdminQuestionsPage extends StatefulWidget {
  @override
  _AdminQuestionsPageState createState() => _AdminQuestionsPageState();
}

class _AdminQuestionsPageState extends State<AdminQuestionsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _question = '';
  List<String> _answers = [''];
  int correctAnswerIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminQuestionsBloc, AdminQuestionsState>(
        builder: (context, state) {
          if (state is QuestionsLoadingState) {
            return Spinner();
          }

          if (state is QuestionsLoadedState) {
            final List<QuestionModel> questions = state.questions;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'Questions',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: questions.length,
                  itemBuilder: (context, index) {
                    final QuestionModel question = questions[index];

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            final bool confirm =
                                await locator<ModalService>().showConfirmation(
                              context: context,
                              title: 'Delete Question: ${question.question}',
                              message: 'Are you sure?',
                            );

                            if (!confirm) return;

                            context.read<AdminQuestionsBloc>().add(
                                  DeleteQuestionEvent(
                                    questionID: question.id,
                                  ),
                                );
                          },
                        ),
                        InfoWidget(
                          onTap: () {},
                          title:
                              '${question.position + 1}. ${question.question}',
                        ),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            context.read<AdminQuestionsBloc>().add(
                                  GoToEditQuestionEvent(
                                    question: question,
                                  ),
                                );
                          },
                        ),
                      ],
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add Question',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () {
                        context.read<AdminQuestionsBloc>().add(
                              GoToAddQuestionEvent(),
                            );
                      },
                    ),
                    RoundedButton(
                      text: 'Sort Qs',
                      foreground: colorOrange,
                      background: Colors.white,
                      onTap: () {
                        context.read<AdminQuestionsBloc>().add(
                              GoToSortQuestionsEvent(),
                            );
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is SortQuestionsState) {
            final List<QuestionModel> questions = state.questions;

            var itemRows = questions.map((question) {
              return ReorderableTableRow(
                //a key must be specified for each row
                key: ObjectKey(question),
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    // questions.map((question) => Text('hello')).toList()
                    <Widget>[
                  Text(
                    '${question.position + 1}',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text('${question.question}'),
                ],
              );
            }).toList();

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminQuestionsBloc>()
                      .add(GoToQuestionsLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Sort Questions',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReorderableTable(
                  header: ReorderableTableRow(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Position', textScaleFactor: 1.5),
                      Text('Title', textScaleFactor: 1.5),
                    ],
                  ),
                  children: itemRows,
                  onReorder: (int oldIndex, int newIndex) {
                    context.read<AdminQuestionsBloc>().add(
                          ShallowSubmitSortQuestionsEvent(
                            oldIndex: oldIndex,
                            newIndex: newIndex,
                          ),
                        );
                  },
                  onNoReorder: (int index) {
                    //this callback is optional
                    debugPrint(
                        '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminQuestionsBloc>().add(
                          SubmitSortQuestionsEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is EditQuestionState) {
            final QuestionModel question = state.question;

            List<TextEditingController> answerControllers =
                _answers.map((part) {
              return TextEditingController(text: part);
            }).toList();
            TextEditingController questionController =
                TextEditingController(text: _question);

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminQuestionsBloc>()
                      .add(GoToQuestionsLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Edit Question',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Expanded(
                          child: TextFormField(
                            onChanged: (value) {
                              _question = value;
                            },
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: locator<ValidationService>().isEmpty,
                            controller: questionController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              errorStyle: TextStyle(
                                color: Colors.black,
                              ),
                              prefixIcon: Icon(
                                Icons.arrow_forward,
                                color: Colors.black,
                              ),
                              hintText: 'Question',
                            ),
                          ),
                        ),
                      ),
                      for (int i = 0; i < answerControllers.length; i++) ...[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  onChanged: (value) {
                                    _answers[i] = value;
                                  },
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: answerControllers[i],
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.black,
                                    ),
                                    hintText: 'Answer ${i + 1}',
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  setState(() {
                                    _answers.removeAt(i);
                                  });
                                },
                              ),
                              IconButton(
                                icon: correctAnswerIndex == i
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      )
                                    : Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                      ),
                                onPressed: () {
                                  setState(() {
                                    correctAnswerIndex = i;
                                  });
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                      FloatingActionButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              _answers.add('');
                            });
                          })
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    Map<String, dynamic> answerData = {};
                    for (int i = 0; i < answerControllers.length; i++) {
                      answerData['$i'] = answerControllers[i].text;
                    }

                    context.read<AdminQuestionsBloc>().add(
                          SubmitEditQuestionEvent(
                            question: QuestionModel(
                              question: _question,
                              answers: answerData,
                              id: question.id,
                              correctAnswerIndex: correctAnswerIndex,
                              position: question.position,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is AddQuestionState) {
            List<TextEditingController> answerControllers =
                _answers.map((part) {
              return TextEditingController(text: part);
            }).toList();
            TextEditingController questionController =
                TextEditingController(text: _question);
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminQuestionsBloc>()
                      .add(GoToQuestionsLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Add Question',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Expanded(
                          child: TextFormField(
                            onChanged: (value) {
                              _question = value;
                            },
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: locator<ValidationService>().isEmpty,
                            controller: questionController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              errorStyle: TextStyle(
                                color: Colors.black,
                              ),
                              prefixIcon: Icon(
                                Icons.arrow_forward,
                                color: Colors.black,
                              ),
                              hintText: 'Question',
                            ),
                          ),
                        ),
                      ),
                      for (int i = 0; i < answerControllers.length; i++) ...[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  onChanged: (value) {
                                    _answers[i] = value;
                                  },
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: answerControllers[i],
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    prefixIcon: Icon(
                                      Icons.arrow_forward,
                                      color: Colors.black,
                                    ),
                                    hintText: 'Answer ${i + 1}',
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  setState(() {
                                    _answers.removeAt(i);
                                  });
                                },
                              ),
                              IconButton(
                                icon: correctAnswerIndex == i
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      )
                                    : Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                      ),
                                onPressed: () {
                                  setState(() {
                                    correctAnswerIndex = i;
                                  });
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                      FloatingActionButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              _answers.add('');
                            });
                          })
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    Map<String, dynamic> answerData = {};
                    for (int i = 0; i < answerControllers.length; i++) {
                      answerData['$i'] = answerControllers[i].text;
                    }

                    context.read<AdminQuestionsBloc>().add(
                          SubmitAddQuestionEvent(
                            question: QuestionModel(
                              question: _question,
                              answers: answerData,
                              id: null,
                              correctAnswerIndex: correctAnswerIndex,
                              position: null,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is QuestionErrorState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminQuestionsBloc>()
                      .add(GoToQuestionsLoadedEvent());
                },
              ),
              children: [
                Center(
                  child: Text(
                    'Error: ${state.error.toString()}',
                  ),
                )
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is AddQuestionState) {
            _question = '';
            _answers = [''];
            correctAnswerIndex = 0;
          }
          if (state is EditQuestionState) {
            _question = state.question.question;
            _answers = [];
            correctAnswerIndex = state.question.correctAnswerIndex;
            List<String> answerIndexes = state.question.answers.keys.toList();
            answerIndexes.sort((a, b) => a.compareTo(b));
            for (int i = 0; i < answerIndexes.length; i++) {
              _answers.add(state.question.answers['$i']);
            }
          }
        },
      ),
    );
  }
}
