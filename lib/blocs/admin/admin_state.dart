part of 'admin_bloc.dart';

abstract class AdminState extends Equatable {
  const AdminState();

  @override
  List<Object> get props => [];
}

class AdminInitialState extends AdminState {}

class AdminLoadingState extends AdminState {}

class AdminLoadedState extends AdminState {
  const AdminLoadedState();

  @override
  List<Object> get props => [];
}

class AdminErrorState extends AdminState {
  final dynamic error;

  AdminErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
