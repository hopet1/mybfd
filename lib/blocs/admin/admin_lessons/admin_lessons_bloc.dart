import 'dart:async';

import 'package:MyBFD/blocs/admin/admin_activities/admin_activities_bloc.dart';
import 'package:MyBFD/blocs/admin/admin_questions/admin_questions_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/LessonService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

part 'admin_lessons_event.dart';
part 'admin_lessons_page.dart';
part 'admin_lessons_state.dart';

class AdminLessonsBloc extends Bloc<AdminLessonsEvent, AdminLessonsState> {
  AdminLessonsBloc({
    @required this.course,
    @required this.module,
  }) : super(LessonsInitialState());

  final CourseModel course;
  final ModuleModel module;
  List<LessonModel> _lessons = [];

  @override
  Stream<AdminLessonsState> mapEventToState(
    AdminLessonsEvent event,
  ) async* {
    yield LessonsLoadingState();

    if (event is LoadLessonsEvent) {
      try {
        _lessons = await locator<LessonService>()
            .retrieveLessons(courseID: course.id, moduleID: module.id);
        add(GoToLessonsLoadedEvent());
      } catch (error) {
        yield LessonErrorState(error: error);
      }
    }

    if (event is GoToLessonsLoadedEvent) {
      _lessons.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield LessonsLoadedState(
        course: course,
        module: module,
        lessons: _lessons,
      );
    }

    if (event is GoToEditLessonEvent) {
      yield EditLessonState(lesson: event.lesson);
    }

    if (event is SubmitEditLessonEvent) {
      try {
        final LessonModel lesson = event.lesson;

        await locator<LessonService>().updateLesson(
          courseID: course.id,
          moduleID: module.id,
          lessonID: lesson.id,
          data: {
            'title': lesson.title,
            'summary': lesson.summary,
            'youtubeUrl': lesson.youtubeUrl,
          },
        );

        add(LoadLessonsEvent()); //Note, only doing this here becase editing a lesson is messy.
      } catch (error) {
        yield LessonErrorState(error: error);
      }
    }

    if (event is GoToSortLessonsEvent) {
      _lessons.sort(
        (a, b) => a.position.compareTo(b.position),
      );
      yield SortLessonsState(lessons: _lessons);
    }

    if (event is SubmitSortLessonsEvent) {
      for (int i = 0; i < _lessons.length; i++) {
        LessonModel c = _lessons[i];

        await locator<LessonService>().updateLesson(
            courseID: course.id,
            moduleID: module.id,
            lessonID: c.id,
            data: {
              'position': c.position,
            });
      }

      add(LoadLessonsEvent());
    }

    if (event is ShallowSubmitSortLessonsEvent) {
      final int oldIndex = event.oldIndex;
      final int newIndex = event.newIndex;

      LessonModel lesson = _lessons.removeAt(oldIndex);
      _lessons.insert(newIndex, lesson);

      _updateLessonPositionValues();

      add(GoToSortLessonsEvent());
    }

    if (event is GoToAddLessonEvent) {
      yield AddLessonState();
    }

    if (event is SubmitAddLessonEvent) {
      try {
        final LessonModel lesson = event.lesson;

        lesson.position = _lessons.length;

        await locator<LessonService>().createLesson(
            courseID: course.id, moduleID: module.id, lesson: lesson);

        _lessons.add((lesson));

        add(GoToLessonsLoadedEvent());
      } catch (error) {
        yield LessonErrorState(error: error);
      }
    }

    if (event is DeleteLessonEvent) {
      try {
        final String lessonID = event.lessonID;

        await locator<LessonService>().deleteLesson(
            courseID: course.id, moduleID: module.id, lessonID: lessonID);

        _lessons.removeWhere((lesson) => lesson.id == lessonID);

        _updateLessonPositionValues();

        add(SubmitSortLessonsEvent());
      } catch (error) {
        yield LessonErrorState(error: error);
      }
    }
  }

  void _updateLessonPositionValues() {
    for (int i = 0; i < _lessons.length; i++) {
      LessonModel c = _lessons[i];

      c.position = i;
    }
  }
}
