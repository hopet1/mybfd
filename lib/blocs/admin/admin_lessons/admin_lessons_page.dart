part of 'admin_lessons_bloc.dart';

class AdminLessonsPage extends StatefulWidget {
  @override
  _AdminLessonsPageState createState() => _AdminLessonsPageState();
}

class _AdminLessonsPageState extends State<AdminLessonsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _titleController = TextEditingController();

  final TextEditingController _summaryController = TextEditingController();

  final TextEditingController _youtubeUrlController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<AdminLessonsBloc, AdminLessonsState>(
        builder: (context, state) {
          if (state is LessonsLoadingState) {
            return Spinner();
          }

          if (state is LessonsLoadedState) {
            final CourseModel course = state.course;
            final ModuleModel module = state.module;

            final List<LessonModel> lessons = state.lessons;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'Lessons',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: lessons.length,
                  itemBuilder: (context, index) {
                    final LessonModel lesson = lessons[index];

                    return Container(
                      height: 75,
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () async {
                              final bool confirm = await locator<ModalService>()
                                  .showConfirmation(
                                context: context,
                                title: 'Delete Lesson: ${lesson.title}',
                                message:
                                    'This will delete all attached lessons, lessons, activities, and questions. Are you sure?',
                              );

                              if (!confirm) return;

                              context.read<AdminLessonsBloc>().add(
                                    DeleteLessonEvent(
                                      lessonID: lesson.id,
                                    ),
                                  );
                            },
                          ),
                          InfoWidget(
                            onTap: () {},
                            title: '${lesson.position + 1}. ' + lesson.title,
                          ),
                          IconButton(
                            icon: Icon(Icons.question_answer_outlined),
                            onPressed: () async {
                              Route route = MaterialPageRoute(
                                builder: (BuildContext context) => BlocProvider(
                                  create: (BuildContext context) =>
                                      AdminQuestionsBloc(
                                          course: course,
                                          module: module,
                                          lesson: lesson)
                                        ..add(
                                          LoadQuestionsEvent(),
                                        ),
                                  child: AdminQuestionsPage(),
                                ),
                              );
                              Navigator.push(context, route);
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.local_activity_rounded),
                            onPressed: () async {
                              Route route = MaterialPageRoute(
                                builder: (BuildContext context) => BlocProvider(
                                  create: (BuildContext context) =>
                                      AdminActivitiesBloc(
                                          course: course,
                                          module: module,
                                          lesson: lesson)
                                        ..add(
                                          LoadActivitiesEvent(),
                                        ),
                                  child: AdminActivitiesPage(),
                                ),
                              );
                              Navigator.push(context, route);
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () async {
                              context.read<AdminLessonsBloc>().add(
                                    GoToEditLessonEvent(
                                      lesson: lesson,
                                    ),
                                  );
                            },
                          ),
                        ],
                      ),
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add Lesson',
                      foreground: Colors.white,
                      background: colorOrange,
                      onTap: () {
                        context.read<AdminLessonsBloc>().add(
                              GoToAddLessonEvent(),
                            );
                      },
                    ),
                    RoundedButton(
                      text: 'Sort Lessons',
                      foreground: colorOrange,
                      background: Colors.white,
                      onTap: () {
                        context.read<AdminLessonsBloc>().add(
                              GoToSortLessonsEvent(),
                            );
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is SortLessonsState) {
            final List<LessonModel> lessons = state.lessons;

            var itemRows = lessons.map((lesson) {
              return ReorderableTableRow(
                //a key must be specified for each row
                key: ObjectKey(lesson),
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:
                    // lessons.map((lesson) => Text('hello')).toList()
                    <Widget>[
                  Text(
                    '${lesson.position + 1}',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text('${lesson.title}'),
                ],
              );
            }).toList();

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminLessonsBloc>()
                      .add(GoToLessonsLoadedEvent());
                },
              ),
              children: [
                Text(
                  'Sort Lessons',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReorderableTable(
                  header: ReorderableTableRow(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Position', textScaleFactor: 1.5),
                      Text('Title', textScaleFactor: 1.5),
                    ],
                  ),
                  children: itemRows,
                  onReorder: (int oldIndex, int newIndex) {
                    context.read<AdminLessonsBloc>().add(
                          ShallowSubmitSortLessonsEvent(
                            oldIndex: oldIndex,
                            newIndex: newIndex,
                          ),
                        );
                  },
                  onNoReorder: (int index) {
                    //this callback is optional
                    debugPrint(
                        '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminLessonsBloc>().add(
                          SubmitSortLessonsEvent(),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is EditLessonState) {
            final LessonModel lesson = state.lesson;

            _titleController.text = lesson.title;
            _summaryController.text = lesson.summary;
            _youtubeUrlController.text =
                lesson.youtubeUrl == null ? '' : lesson.youtubeUrl;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminLessonsBloc>()
                      .add(GoToLessonsLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Edit Lesson',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _titleController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            hintText: 'Title',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _summaryController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.text_fields,
                              color: Colors.black,
                            ),
                            hintText: 'Summary',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _youtubeUrlController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.youtube_searched_for_rounded,
                              color: Colors.black,
                            ),
                            hintText: 'YouTube URL',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminLessonsBloc>().add(
                          SubmitEditLessonEvent(
                            lesson: LessonModel(
                              id: lesson.id,
                              summary: _summaryController.text,
                              title: _titleController.text,
                              youtubeUrl: _youtubeUrlController.text.isEmpty
                                  ? null
                                  : _youtubeUrlController.text,
                              position: lesson.position,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is AddLessonState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminLessonsBloc>()
                      .add(GoToLessonsLoadedEvent());
                },
              ),
              children: [
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Add Lesson',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 60,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _titleController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.black,
                            ),
                            hintText: 'Title',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: locator<ValidationService>().isEmpty,
                          controller: _summaryController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.text_fields,
                              color: Colors.black,
                            ),
                            hintText: 'Summary',
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _youtubeUrlController,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                              color: Colors.black,
                            ),
                            prefixIcon: Icon(
                              Icons.youtube_searched_for_rounded,
                              color: Colors.black,
                            ),
                            hintText: 'YouTube URL',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  onTap: () async {
                    if (!_formKey.currentState.validate()) return;

                    final bool confirm =
                        await locator<ModalService>().showConfirmation(
                      context: context,
                      title: 'Submit',
                      message: 'Are you sure?',
                    );

                    if (!confirm) return;

                    context.read<AdminLessonsBloc>().add(
                          SubmitAddLessonEvent(
                            lesson: LessonModel(
                              id: null,
                              summary: _summaryController.text,
                              title: _titleController.text,
                              youtubeUrl: _youtubeUrlController.text.isEmpty
                                  ? null
                                  : _youtubeUrlController.text,
                              position: null,
                            ),
                          ),
                        );
                  },
                  foreground: colorOrange,
                  background: Colors.white,
                  text: 'Submit',
                ),
              ],
            );
          }

          if (state is LessonErrorState) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context
                      .read<AdminLessonsBloc>()
                      .add(GoToLessonsLoadedEvent());
                },
              ),
              children: [
                Center(
                  child: Text(
                    'Error: ${state.error.toString()}',
                  ),
                )
              ],
            );
          }
          return Container();
        },
        listener: (context, state) {
          if (state is AddLessonState) {
            _clearTextControllers();
          }
        },
      ),
    );
  }

  void _clearTextControllers() {
    _titleController.clear();
    _summaryController.clear();
    _youtubeUrlController.clear();
  }
}
