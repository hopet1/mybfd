part of 'admin_lessons_bloc.dart';

abstract class AdminLessonsEvent extends Equatable {
  const AdminLessonsEvent();

  @override
  List<Object> get props => [];
}

class LoadLessonsEvent extends AdminLessonsEvent {}

class GoToLessonsLoadedEvent extends AdminLessonsEvent {}

class GoToSortLessonsEvent extends AdminLessonsEvent {}

class SubmitSortLessonsEvent extends AdminLessonsEvent {
  const SubmitSortLessonsEvent();

  @override
  List<Object> get props => [];
}

class ShallowSubmitSortLessonsEvent extends AdminLessonsEvent {
  final int oldIndex;
  final int newIndex;

  const ShallowSubmitSortLessonsEvent({
    @required this.oldIndex,
    @required this.newIndex,
  });

  @override
  List<Object> get props => [
        oldIndex,
        newIndex,
      ];
}

class GoToEditLessonEvent extends AdminLessonsEvent {
  final LessonModel lesson;

  const GoToEditLessonEvent({
    @required this.lesson,
  });

  @override
  List<Object> get props => [
        lesson,
      ];
}

class SubmitEditLessonEvent extends AdminLessonsEvent {
  final LessonModel lesson;

  const SubmitEditLessonEvent({
    @required this.lesson,
  });

  @override
  List<Object> get props => [
        lesson,
      ];
}

class GoToAddLessonEvent extends AdminLessonsEvent {}

class SubmitAddLessonEvent extends AdminLessonsEvent {
  final LessonModel lesson;

  const SubmitAddLessonEvent({
    @required this.lesson,
  });

  @override
  List<Object> get props => [
        lesson,
      ];
}

class DeleteLessonEvent extends AdminLessonsEvent {
  final String lessonID;

  const DeleteLessonEvent({
    @required this.lessonID,
  });

  @override
  List<Object> get props => [
        lessonID,
      ];
}
