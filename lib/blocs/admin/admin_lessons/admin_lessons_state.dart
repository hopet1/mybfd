part of 'admin_lessons_bloc.dart';

abstract class AdminLessonsState extends Equatable {
  const AdminLessonsState();

  @override
  List<Object> get props => [];
}

class LessonsInitialState extends AdminLessonsState {}

class LessonsLoadingState extends AdminLessonsState {}

class LessonsLoadedState extends AdminLessonsState {
  final CourseModel course;
  final ModuleModel module;

  final List<LessonModel> lessons;

  const LessonsLoadedState({
    @required this.course,
    @required this.module,
    @required this.lessons,
  });

  @override
  List<Object> get props => [
        course,
        module,
        lessons,
      ];
}

class SortLessonsState extends AdminLessonsState {
  final List<LessonModel> lessons;

  const SortLessonsState({
    @required this.lessons,
  });

  @override
  List<Object> get props => [
        lessons,
      ];
}

class EditLessonState extends AdminLessonsState {
  final LessonModel lesson;

  const EditLessonState({
    @required this.lesson,
  });

  @override
  List<Object> get props => [
        lesson,
      ];
}

class AddLessonState extends AdminLessonsState {
  const AddLessonState();

  @override
  List<Object> get props => [];
}

class LessonErrorState extends AdminLessonsState {
  final dynamic error;

  LessonErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
