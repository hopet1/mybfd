part of 'goals_bloc.dart';

class GoalsPage extends StatefulWidget {
  @override
  State createState() => GoalsPageState();
}

class GoalsPageState extends State<GoalsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _goalController = TextEditingController();
  final TextEditingController _reasonController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<GoalsBloc, GoalsState>(
        builder: (context, state) {
          if (state is GoalsLoading) {
            return Spinner();
          }

          if (state is AddGoal) {
            return PageStructureWidget(
              children: [
                Text(
                  'My Goals',
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(
                  height: 32,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: scrWidth(context) * 0.025),
                            child: Text(
                              'Add A Goal',
                              style: Theme.of(context).textTheme.headline3,
                              textAlign: TextAlign.left,
                            ),
                          ),
                          SizedBox(height: 6.0),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: scrWidth(context) * 0.05),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              border: Border.all(
                                color: Colors.grey,
                                width: 1,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                                vertical: 5.0,
                              ),
                              child: TextFormField(
                                cursorColor:
                                    Theme.of(context).textTheme.headline4.color,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                controller: _goalController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.done,
                                validator: locator<ValidationService>().isEmpty,
                                style: TextStyle(color: Colors.black),
                                maxLines: 5,
                                maxLength: 200,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  counterStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 18.0),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: scrWidth(context) * 0.025),
                            child: Text(
                              'Reason For Goal',
                              style: Theme.of(context).textTheme.headline3,
                              textAlign: TextAlign.left,
                            ),
                          ),
                          SizedBox(height: 6.0),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: scrWidth(context) * 0.05),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              border: Border.all(
                                color: Colors.grey,
                                width: 1,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                                vertical: 5.0,
                              ),
                              child: TextFormField(
                                cursorColor:
                                    Theme.of(context).textTheme.headline4.color,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                controller: _reasonController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.done,
                                validator: locator<ValidationService>().isEmpty,
                                style: TextStyle(color: Colors.black),
                                maxLines: 5,
                                maxLength: 200,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  counterStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 32.0),
                    Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          RoundedButton(
                            text: 'Save Goal',
                            background: Colors.white,
                            foreground: colorOrange,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              final bool confirm = await locator<ModalService>()
                                  .showConfirmation(
                                context: context,
                                title: 'Submit',
                                message: 'Are you sure?',
                              );

                              if (!confirm) return;

                              context.read<GoalsBloc>().add(
                                    SubmitAddGoalEvent(
                                      goal: _goalController.text,
                                      reason: _reasonController.text,
                                    ),
                                  );

                              _formKey.currentState.reset();
                              _goalController.clear();
                              _reasonController.clear();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            );
          }

          if (state is EditGoal) {
            final GoalModel goal = state.goal;

            _goalController.text = goal.goal;
            _reasonController.text = goal.reason;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context.read<GoalsBloc>().add(
                        LoadPageEvent(),
                      );
                },
              ),
              children: [
                Text(
                  'My Goal',
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 48.0),
                Column(
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Your Goal (click in the box to edit)',
                            style: Theme.of(context).textTheme.headline3,
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(height: 6.0),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              border: Border.all(
                                color: Colors.grey,
                                width: 1,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                                vertical: 5.0,
                              ),
                              child: TextFormField(
                                cursorColor:
                                    Theme.of(context).textTheme.headline4.color,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                controller: _goalController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.done,
                                validator: locator<ValidationService>().isEmpty,
                                style: TextStyle(color: Colors.black),
                                maxLines: 5,
                                maxLength: 200,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  counterStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 18.0),
                          Text(
                            'Reason For Goal (click in the box to edit)',
                            style: Theme.of(context).textTheme.headline3,
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(height: 6.0),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              border: Border.all(
                                color: Colors.black,
                                width: 1,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12.0,
                                vertical: 5.0,
                              ),
                              child: TextFormField(
                                cursorColor:
                                    Theme.of(context).textTheme.headline4.color,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                controller: _reasonController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.done,
                                validator: locator<ValidationService>().isEmpty,
                                style: TextStyle(color: Colors.black),
                                maxLines: 5,
                                maxLength: 200,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  counterStyle: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .headline6
                                          .color),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 24.0),
                    Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          RoundedButton(
                            text: 'Save Changes',
                            foreground: Color(colorOrange.value),
                            background: Colors.white,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              final bool confirm = await locator<ModalService>()
                                  .showConfirmation(
                                context: context,
                                title: 'Submit',
                                message: 'Are you sure?',
                              );

                              if (!confirm) return;

                              context.read<GoalsBloc>().add(
                                    SubmitEditGoalEvent(
                                      goalID: goal.id,
                                      goal: _goalController.text,
                                      reason: _reasonController.text,
                                    ),
                                  );

                              _formKey.currentState.reset();
                              _goalController.clear();
                              _reasonController.clear();
                            },
                          ),
                          SizedBox(height: 12),
                          RoundedButton(
                              text: 'Delete Goal',
                              foreground: Colors.white,
                              background: colorOrange,
                              onTap: () async {
                                final bool confirm =
                                    await locator<ModalService>()
                                        .showConfirmation(
                                  context: context,
                                  title: 'Submit',
                                  message: 'Are you sure?',
                                );

                                if (!confirm) return;

                                context.read<GoalsBloc>().add(
                                      DeleteGoalEvent(
                                        goalID: goal.id,
                                      ),
                                    );
                              }),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            );
          }

          if (state is GoalComplete) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  context.read<GoalsBloc>().add(LoadPageEvent());
                },
              ),
              children: [
                Text(
                  'Congratulations!',
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 64),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'You\'ve marked a goal as done.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'This is fantastic news, well done. You\'ve made a great achievement and deserve a reward. Check your rewards section for a new achievement.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
                SizedBox(height: 48),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RoundedButton(
                      text: 'Add A New Goal',
                      background: Colors.white,
                      foreground: colorOrange,
                      onTap: () {
                        context.read<GoalsBloc>().add(AddGoalEvent());
                      },
                    ),
                  ],
                )
              ],
            );
          }

          if (state is GoalsEmpty) {
            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'My Goals',
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 32),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 48),
                  child: Text(
                    'You currently have no goals setup. \nUse the button below to start setting up your financial goals for you to work towards.',
                    style: Theme.of(context).textTheme.headline3,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 32),
                RoundedButton(
                  text: 'Add A Goal',
                  foreground: Color(colorOrange.value),
                  background: Colors.white,
                  onTap: () {
                    context.read<GoalsBloc>().add(AddGoalEvent());
                  },
                ),
              ],
            );
          }

          if (state is GoalsLoaded) {
            final List<GoalModel> goals = state.goals;

            return PageStructureWidget(
              topbar: Topbar(
                backArrowTap: () {
                  Navigator.of(context).pop();
                },
              ),
              children: [
                Text(
                  'My Goals',
                  style: Theme.of(context).textTheme.headline1,
                ),
                SizedBox(height: 8.0),
                ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: goals.length,
                  itemBuilder: (context, index) {
                    final GoalModel goal = goals[index];
                    return Column(
                      children: [
                        GoalWidget(
                          goal: goal,
                          markAsDone: () async {
                            final bool confirm =
                                await locator<ModalService>().showConfirmation(
                              context: context,
                              title: 'Mark Goal As Done',
                              message: 'Are you sure?',
                            );

                            if (!confirm) return;

                            context.read<GoalsBloc>().add(
                                  MarkAsDoneEvent(goalID: goal.id),
                                );
                          },
                          edit: () {
                            context.read<GoalsBloc>().add(
                                  EditGoalEvent(
                                    goal: goal,
                                  ),
                                );
                          },
                        ),
                        SizedBox(height: 18.0),
                      ],
                    );
                  },
                ),
                RoundedButton(
                  text: 'Add A Goal',
                  foreground: Color(colorOrange.value),
                  background: Colors.white,
                  onTap: () {
                    context.read<GoalsBloc>().add(AddGoalEvent());
                  },
                ),
              ],
            );
          }

          if (state is GoalsError) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return Center(
              child: Text('$errorMessage'),
            );
          }

          return Container();
        },
        listener: (context, state) {
          if (state is GoalsLoading) {
            if (_formKey.currentState != null) _formKey.currentState.reset();
            _goalController.clear();
            _reasonController.clear();
          }
        },
      ),
    );
  }
}
