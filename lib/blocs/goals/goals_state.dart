part of 'goals_bloc.dart';

abstract class GoalsState extends Equatable {
  const GoalsState();

  @override
  List<Object> get props => [];
}

class GoalsInitial extends GoalsState {}

class GoalsLoading extends GoalsState {}

class GoalsLoaded extends GoalsState {
  final List<GoalModel> goals;

  const GoalsLoaded({
    @required this.goals,
  });

  @override
  List<Object> get props => [
        goals,
      ];
}

class AddGoal extends GoalsState {}

class EditGoal extends GoalsState {
  final GoalModel goal;

  const EditGoal({
    @required this.goal,
  });

  @override
  List<Object> get props => [
        goal,
      ];
}

class GoalsEmpty extends GoalsState {}

class GoalComplete extends GoalsState {}

class GoalsError extends GoalsState {
  final dynamic error;

  GoalsError({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
