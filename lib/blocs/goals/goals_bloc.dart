import 'dart:async';

import 'package:MyBFD/models/GoalModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/GoalService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/GoalWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants.dart';
import '../../service_locator.dart';

part 'goals_event.dart';
part 'goals_page.dart';
part 'goals_state.dart';

class GoalsBloc extends Bloc<GoalsEvent, GoalsState> {
  GoalsBloc() : super(GoalsInitial());

  UserModel _currentUser;

  List<GoalModel> _goals = [];

  @override
  Stream<GoalsState> mapEventToState(
    GoalsEvent event,
  ) async* {
    yield GoalsLoading();
    if (event is LoadPageEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();

        _goals = await locator<GoalService>().listGoals(uid: _currentUser.uid);

        if (_goals.isEmpty) {
          yield GoalsEmpty();
        } else {
          _goals.sort(
            (a, b) => b.modified.compareTo(a.modified),
          );
          yield GoalsLoaded(goals: _goals);
        }
      } catch (error) {
        yield GoalsError(error: error);
      }
    }

    if (event is AddGoalEvent) {
      yield AddGoal();
    }

    if (event is EditGoalEvent) {
      final GoalModel goal = event.goal;
      yield EditGoal(goal: goal);
    }

    if (event is SubmitEditGoalEvent) {
      final String goalID = event.goalID;
      final String goal = event.goal;
      final String reason = event.reason;

      final DateTime now = DateTime.now();

      await locator<GoalService>().updateGoal(
        uid: _currentUser.uid,
        goalID: goalID,
        data: {
          'goal': goal,
          'reason': reason,
          'modified': now,
        },
      );

      add(LoadPageEvent());
    }

    if (event is SubmitAddGoalEvent) {
      final String goal = event.goal;
      final String reason = event.reason;

      final DateTime now = DateTime.now();

      await locator<GoalService>().createGoal(
          uid: _currentUser.uid,
          goal: GoalModel(
            id: null,
            complete: false,
            modified: now,
            created: now,
            reason: reason,
            goal: goal,
          ));

      add(LoadPageEvent());
    }

    if (event is MarkAsDoneEvent) {
      final String goalID = event.goalID;

      final DateTime now = DateTime.now();

      await locator<GoalService>().updateGoal(
        uid: _currentUser.uid,
        goalID: goalID,
        data: {
          'complete': true,
          'modified': now,
        },
      );

      yield GoalComplete();
    }

    if (event is DeleteGoalEvent) {
      final String goalID = event.goalID;
      await locator<GoalService>().deleteGoal(
        uid: _currentUser.uid,
        goalID: goalID,
      );

      add(LoadPageEvent());
    }
  }
}
