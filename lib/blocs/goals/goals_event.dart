part of 'goals_bloc.dart';

abstract class GoalsEvent extends Equatable {
  const GoalsEvent();

  @override
  List<Object> get props => [];
}

class LoadPageEvent extends GoalsEvent {}

class AddGoalEvent extends GoalsEvent {}

class EditGoalEvent extends GoalsEvent {
  final GoalModel goal;

  const EditGoalEvent({
    @required this.goal,
  });

  @override
  List<Object> get props => [
        goal,
      ];
}

class DeleteGoalEvent extends GoalsEvent {
  final String goalID;

  const DeleteGoalEvent({
    @required this.goalID,
  });

  @override
  List<Object> get props => [
        goalID,
      ];
}

class MarkAsDoneEvent extends GoalsEvent {
  final String goalID;

  const MarkAsDoneEvent({
    @required this.goalID,
  });

  @override
  List<Object> get props => [
        goalID,
      ];
}

class SubmitAddGoalEvent extends GoalsEvent {
  final String goal;
  final String reason;

  const SubmitAddGoalEvent({
    @required this.goal,
    @required this.reason,
  });

  @override
  List<Object> get props => [
        goal,
        reason,
      ];
}

class SubmitEditGoalEvent extends GoalsEvent {
  final String goalID;
  final String goal;
  final String reason;

  const SubmitEditGoalEvent({
    @required this.goalID,
    @required this.goal,
    @required this.reason,
  });

  @override
  List<Object> get props => [
        goal,
        reason,
      ];
}
