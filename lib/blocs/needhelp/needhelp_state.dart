part of 'needhelp_bloc.dart';

abstract class NeedhelpState extends Equatable {
  const NeedhelpState();

  @override
  List<Object> get props => [];
}

class NeedhelpInitial extends NeedhelpState {}

class NeedhelpSent extends NeedhelpState {}
