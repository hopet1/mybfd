part of "needhelp_bloc.dart";

class NeedhelpPage extends StatefulWidget {
  @override
  State createState() => _NeedhelpPageState();
}

class _NeedhelpPageState extends State<NeedhelpPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _messageController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final double _titleFontSize = 60;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorOrange,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Form(
          key: _formKey,
          child: ListView(
            children: [
              ClipPath(
                clipper: BezierClipper(),
                child: Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 400,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          _usernameController.text = 'dg1199';
                          _nameController.text = 'Divyansh Gandhi';
                          _messageController.text = 'Test message entered';
                        },
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: 100,
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'my',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorDark,
                              ),
                            ),
                            TextSpan(
                              text: 'bfd',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorOrange,
                              ),
                            ),
                            TextSpan(
                              text: '\nFinancial Education App'.toUpperCase(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 2.0,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              BlocConsumer<NeedhelpBloc, NeedhelpState>(
                builder: (context, state) {
                  if (state is NeedhelpInitial) {
                    return Expanded(
                      child: Column(
                        // padding: const EdgeInsets.all(20),
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "need help?".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: Responsive.isMobile(context)
                                          ? 28
                                          : 36.0),
                                ),
                                SizedBox(
                                  height: 40.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('$FORGOT_PASSWORD_MESSAGE',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: Responsive.isMobile(context)
                                              ? 12
                                              : 16)),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    validator:
                                        locator<ValidationService>().isEmpty,
                                    controller: _nameController,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          // width: 0.0 produces a thin "hairline" border
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(90.0),
                                          ),
                                          borderSide: BorderSide.none,

                                          //borderSide: const BorderSide(),
                                        ),
                                        hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "WorkSansLight"),
                                        filled: true,
                                        fillColor: Colors.white24,
                                        hintText: 'Name'),
                                  ),
                                ),
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    validator:
                                        locator<ValidationService>().isEmpty,
                                    controller: _usernameController,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          // width: 0.0 produces a thin "hairline" border
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(90.0),
                                          ),
                                          borderSide: BorderSide.none,

                                          //borderSide: const BorderSide(),
                                        ),
                                        hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "WorkSansLight"),
                                        filled: true,
                                        fillColor: Colors.white24,
                                        hintText: 'User ID / Email Address'),
                                  ),
                                ),
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    validator:
                                        locator<ValidationService>().isEmpty,
                                    controller: _messageController,
                                    minLines: 4,
                                    maxLines: 6,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          // width: 0.0 produces a thin "hairline" border
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(24.0),
                                          ),
                                          borderSide: BorderSide.none,

                                          //borderSide: const BorderSide(),
                                        ),
                                        hintStyle: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "WorkSansLight"),
                                        filled: true,
                                        fillColor: Colors.white24,
                                        hintText: 'Message'),
                                  ),
                                ),
                                if (!Responsive.isMobile(context))
                                  Spacer(
                                      flex: Responsive.isDesktop(context)
                                          ? 2
                                          : 1),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Send',
                            foreground: colorOrange,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              context.read<NeedhelpBloc>().add(
                                    NeedHelp(
                                      name: _nameController.text,
                                      username: _usernameController.text,
                                      message: _messageController.text,
                                    ),
                                  );
                            },
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                // locator<ModalService>().showAlert(
                                //   context: context,
                                //   title: 'To Do',
                                //   message: 'Go back to sign in',
                                // );
                                Phoenix.rebirth(context);
                              },
                              child: Text(
                                'Back to sign in',
                                style: TextStyle(
                                  letterSpacing: 1.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    );
                  }

                  if (state is NeedhelpSent) {
                    return Expanded(
                      child: Column(
                        // padding: const EdgeInsets.all(20),
                        children: [
                          Center(
                            child: Column(
                              children: [
                                Text(
                                  "email sent".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 36.0),
                                ),
                                SizedBox(
                                  height: 40.0,
                                ),
                                Text(
                                  '$HELP_SENT_MESSAGE',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Back To Sign In',
                            foreground: colorOrange,
                            onTap: () async {
                              Phoenix.rebirth(context);
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    );
                  }

                  return Container();
                },
                listener: (context, state) {
                  if (state is NeedhelpInitial) {
                    print('Initial');
                  }
                  if (state is NeedhelpSent) {
                    print('Sent');
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
