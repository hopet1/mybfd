import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/BezierClipper.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/NeedHelpModel.dart';
import 'package:MyBFD/services/NeedHelpService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import '../../constants.dart';
import '../../service_locator.dart';

part 'needhelp_event.dart';
part 'needhelp_page.dart';
part 'needhelp_state.dart';

class NeedhelpBloc extends Bloc<NeedhelpEvent, NeedhelpState> {
  NeedhelpBloc() : super(NeedhelpInitial());

  @override
  Stream<NeedhelpState> mapEventToState(NeedhelpEvent event) async* {
    if (event is NeedHelp) {
      final String name = event.name;
      final String username = event.username;
      final String message = event.message;

      NeedHelpModel help = NeedHelpModel(
        name: name,
        username: username,
        message: message,
      );
      await locator<NeedHelpService>().sendQuery(help);
      await Future.delayed(Duration(seconds: 1), () {});

      yield NeedhelpSent();
    }
  }
}
