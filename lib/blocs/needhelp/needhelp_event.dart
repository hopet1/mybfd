part of 'needhelp_bloc.dart';

abstract class NeedhelpEvent extends Equatable {
  const NeedhelpEvent();

  @override
  List<Object> get props => [];
}

class NeedHelp extends NeedhelpEvent {
  final String name;
  final String username;
  final String message;

  NeedHelp({
    @required this.name,
    @required this.username,
    @required this.message,
  });

  List<Object> get props => [
        name,
        username,
        message,
      ];
}
