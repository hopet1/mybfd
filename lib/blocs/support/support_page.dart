part of 'support_bloc.dart';

class SupportPage extends StatefulWidget {
  @override
  State createState() => _SupportPageState();
}

class _SupportPageState extends State<SupportPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _messageController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SupportBloc, SupportState>(
        builder: (context, state) {
          if (state is SupportInitial) {
            return Column(
              children: [
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Image.asset(
                          ASSET_BACK_ARROW,
                          height: 30,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Center(
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: 70,
                        ),
                      ),
                      Image.asset(
                        ASSET_BACK_ARROW,
                        height: 30,
                        color: Colors.transparent,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 48.0),
                Text(
                  "Ask for Help",
                  style: TextStyle(
                    fontSize: 24,
                    color: colorDark,
                  ),
                ),
                SizedBox(height: 48.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 48.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Your Name",
                        style: TextStyle(
                          color: colorDark.withOpacity(0.5),
                          fontSize: 18.0,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: _nameController,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                              borderSide: BorderSide(
                                color: colorDark,
                                width: 0.0,
                              ),
                            ),
                            hintStyle: TextStyle(
                                color: colorDark, fontFamily: "WorkSansLight"),
                            filled: true,
                            fillColor: Colors.white24,
                            hintText: 'Name'),
                      ),
                      SizedBox(height: 24.0),
                      Text(
                        "Your Question",
                        style: TextStyle(
                          color: colorDark.withOpacity(0.5),
                          fontSize: 18.0,
                        ),
                      ),
                      SizedBox(height: 12.0),
                      TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: _messageController,
                        minLines: 6,
                        maxLines: 6,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                              borderSide: BorderSide(
                                color: colorDark,
                                width: 0.0,
                              ),
                            ),
                            hintStyle: TextStyle(
                                color: colorDark, fontFamily: "WorkSansLight"),
                            filled: true,
                            fillColor: Colors.white24,
                            hintText: 'Type your message..'),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 48.0),
                Center(
                  child: GestureDetector(
                    onTap: () async {
                      context.read<SupportBloc>().add(
                            Support(
                              name: _nameController.text,
                              message: _messageController.text,
                            ),
                          );
                    },
                    child: Container(
                      height: 48.0,
                      width: screenWidth * 0.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 4.0,
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          "Send Message",
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 18,
                            color: colorOrange,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }

          if (state is SupportSent) {
            return Column(
              children: [
                SizedBox(
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Image.asset(
                          ASSET_BACK_ARROW,
                          height: 30,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Center(
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: 70,
                        ),
                      ),
                      Image.asset(
                        ASSET_BACK_ARROW,
                        height: 30,
                        color: Colors.transparent,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 48.0),
                Text(
                  "Thank you",
                  style: TextStyle(
                    fontSize: 24,
                    color: colorDark,
                  ),
                ),
                SizedBox(height: 72.0),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 56.0),
                  child: Text(
                    '$HELP_SENT_MESSAGE',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18.0,
                      color: colorDark,
                    ),
                  ),
                ),
                SizedBox(height: 72.0),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: 48.0,
                      width: screenWidth * 0.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 4.0,
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          "Back To FAQs",
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 18,
                            color: colorOrange,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }

          return Container();
        },
        listener: (context, state) {
          // TODO: implement listener
        },
      ),
    );
  }
}
