import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'support_event.dart';
part 'support_page.dart';
part 'support_state.dart';

class SupportBloc extends Bloc<SupportEvent, SupportState> {
  SupportBloc() : super(SupportInitial());

  @override
  Stream<SupportState> mapEventToState(
    SupportEvent event,
  ) async* {
    if (event is Support) {
      final String name = event.name;
      final String message = event.message;

      //TODO: Connect backend to send support messages

      yield SupportSent();
    }
  }
}
