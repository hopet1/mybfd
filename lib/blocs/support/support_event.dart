part of 'support_bloc.dart';

abstract class SupportEvent extends Equatable {
  const SupportEvent();

  @override
  List<Object> get props => [];
}

class Support extends SupportEvent {
  String name;
  String message;

  Support({
    @required this.name,
    @required this.message,
  });

  List<Object> get props => [
        name,
        message,
      ];
}
