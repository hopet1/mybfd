part of 'rewards_bloc.dart';

abstract class RewardsEvent extends Equatable {
  const RewardsEvent();

  @override
  List<Object> get props => [];
}

class LoadRewardsEvent extends RewardsEvent {}

class ClaimRewardEvent extends RewardsEvent {
  final RewardModel reward;

  ClaimRewardEvent(this.reward);

  @override
  List<Object> get props => [reward];
}
