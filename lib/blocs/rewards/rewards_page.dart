part of 'rewards_bloc.dart';

class RewardsPage extends StatefulWidget {
  @override
  State createState() => _RewardsPageState();
}

class _RewardsPageState extends State<RewardsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 64,
          ),
          Topbar(),
          SizedBox(height: 16.0),
          BlocConsumer<RewardsBloc, RewardsState>(
            builder: (context, state) {
              if (state is RewardsInitial) {
                return Center(
                  child: Spinner(),
                );
              }
              if (state is NoRewardsState) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //SizedBox(height: 42.0),
                    Text(
                      'Rewards',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    SizedBox(height: 42.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 42.0),
                      child: Text(
                        NO_REWARDS_TEXT,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(height: 28),
                    RoundedButton(
                      background: Colors.white,
                      text: 'Go to Courses',
                      foreground: colorOrange,
                      onTap: () {
                        Navigator.pop(context);
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => CoursesBloc()
                              ..add(
                                LoadCoursesEvent(),
                              ),
                            child: CoursesPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                    ),
                  ],
                );
              }

              if (state is RewardsLoadedState) {
                final List<RewardModel> _rewardModels = state.rewardItems;
                List<Widget> _rewardItems = [
                  Center(
                    child: Text(
                      'Rewards',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  SizedBox(height: 42.0),
                ];

                _rewardModels.forEach((element) {
                  _rewardItems.add(RewardItem(
                    text: element.reason,
                    function: () {
                      print("Item tapped");
                      context
                          .read<RewardsBloc>()
                          .add(ClaimRewardEvent(element));
                    },
                    claimed: element.claimed,
                  ));
                });

                return Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: _rewardItems,
                  ),
                );
              }

              if (state is RewardChosenState) {
                RewardModel currReward = state.reward;
                return Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Column(
                        children: [
                          Text(
                            'Claim Reward',
                            style: Theme.of(context).textTheme.headline1,
                          ),
                          SizedBox(height: 36.0),
                          Text(
                            state.reward.reason,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(height: 24.0),
                          RewardWidget(
                            imgURL: currReward.imgURL,
                            content: currReward.content,
                            code: currReward.code,
                          ),
                          SizedBox(height: 18),
                          RoundedButton(
                            background: Colors.white,
                            text: 'I\'ve claimed this',
                            foreground: colorOrange,
                            onTap: () {
                              currReward.claimed = true;
                              locator<RewardService>().updateReward(
                                  id: currReward.currId,
                                  data: currReward.toMap());
                              context
                                  .read<RewardsBloc>()
                                  .add(LoadRewardsEvent());
                            },
                          ),
                          SizedBox(height: 24),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            child: Text(
                              CLAIM_REWARDS_TEXT,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              }

              return Container();
            },
            listener: (context, state) {},
          ),
        ],
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}

class RewardItem extends StatelessWidget {
  final String text;
  final Function function;
  final bool claimed;

  const RewardItem({
    Key key,
    this.text,
    this.function,
    this.claimed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Column(
        children: [
          Container(
            width: screenWidth * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: Offset(2, 2),
                  blurRadius: 4.0,
                ),
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 18.0,
                vertical: 24.0,
              ),
              child: Row(
                children: [
                  ImageIcon(
                    AssetImage(ASSET_REWARDS_WHITE),
                    color: colorOrange,
                    size: 42,
                  ),
                  SizedBox(width: 20.0),
                  Flexible(
                    child: Text(
                      text,
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: screenWidth * 0.8,
            decoration: BoxDecoration(
              color: (!claimed) ? colorOrange : Colors.grey.shade400,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: Offset(2, 2),
                  blurRadius: 4.0,
                ),
              ],
            ),
            child: (claimed)
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "Reward Claimed",
                        style: TextStyle(
                          color: colorDark,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ))
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "Claim Reward",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
          ),
          SizedBox(height: 18),
        ],
      ),
    );
  }
}
