part of 'rewards_bloc.dart';

abstract class RewardsState extends Equatable {
  const RewardsState();

  @override
  List<Object> get props => [];
}

class RewardsInitial extends RewardsState {}

class NoRewardsState extends RewardsState {}

class RewardsLoadedState extends RewardsState {
  final List<RewardModel> rewardItems;

  const RewardsLoadedState({
    @required this.rewardItems,
  });

  @override
  List<Object> get props => [
        rewardItems,
      ];
}

class RewardChosenState extends RewardsState {
  final RewardModel reward;

  const RewardChosenState({
    @required this.reward,
  });

  @override
  List<Object> get props => [
        reward,
      ];
}
