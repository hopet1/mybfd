import 'dart:async';

import 'package:MyBFD/blocs/courses/courses_bloc.dart';
import 'package:MyBFD/models/RewardModel.dart';
import 'package:MyBFD/services/RewardService.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/RewardWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants.dart';
import '../../service_locator.dart';

part 'rewards_event.dart';
part 'rewards_page.dart';
part 'rewards_state.dart';

class RewardsBloc extends Bloc<RewardsEvent, RewardsState> {
  RewardsBloc() : super(RewardsInitial());

  //TODO: Implement User based Rewards
  bool hasRewards = true;

  @override
  Stream<RewardsState> mapEventToState(
    RewardsEvent event,
  ) async* {
    //First load event
    if (event is LoadRewardsEvent) {
      // locator<RewardGenerationService>().generateCode();
      List<RewardModel> _rewardItems;
      _rewardItems = await locator<RewardService>().getRewards();
      hasRewards = _rewardItems.length != 0;
      if (hasRewards) {
        yield RewardsLoadedState(rewardItems: _rewardItems);
      } else {
        yield NoRewardsState();
      }
    }

    //Claim reward event
    if (event is ClaimRewardEvent) {
      yield RewardChosenState(reward: event.reward);
    }
  }
}
