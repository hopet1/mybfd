part of 'courses_bloc.dart';

abstract class CoursesState extends Equatable {
  const CoursesState();

  @override
  List<Object> get props => [];
}

class CoursesInitial extends CoursesState {}

class CoursesLoading extends CoursesState {}

class CoursesLoaded extends CoursesState {
  final List<CourseModel> courses;

  const CoursesLoaded({
    @required this.courses,
  });

  @override
  List<Object> get props => [
        courses,
      ];
}

class CourseLoading extends CoursesState {}

class CourseLoaded extends CoursesState {
  final CourseModel course;
  final List<ModuleModel> modules;

  const CourseLoaded({
    @required this.course,
    @required this.modules,
  });

  @override
  List<Object> get props => [
        course,
        modules,
      ];
}

class ModuleLoaded extends CoursesState {
  final CourseModel course;
  final ModuleModel module;
  final List<LessonModel> lessons;

  const ModuleLoaded({
    @required this.course,
    @required this.module,
    @required this.lessons,
  });

  @override
  List<Object> get props => [
        course,
        module,
        lessons,
      ];
}

class CoursesError extends CoursesState {
  final dynamic error;

  CoursesError({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
