part of 'courses_bloc.dart';

class CoursesPage extends StatefulWidget {
  @override
  State createState() => _CoursesPageState();
}

class _CoursesPageState extends State<CoursesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<CoursesBloc, CoursesState>(
        builder: (context, state) {
          if (state is CoursesLoading || state is CourseLoading) {
            return Spinner();
          }

          if (state is CoursesLoaded) {
            final List<CourseModel> courses = state.courses;

            return PageStructureWidget(
              children: [
                Text(
                  'Courses',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: courses?.length,
                    itemBuilder: (context, index) {
                      final CourseModel course = courses[index];

                      return InfoWidget(
                        onTap: () {
                          Route route = MaterialPageRoute(
                            builder: (BuildContext context) => BlocProvider(
                              create: (BuildContext context) =>
                                  ModulesBloc(course: course)
                                    ..add(
                                      LoadModulesEvent(),
                                    ),
                              child: ModulesPage(),
                            ),
                          );
                          Navigator.push(context, route);
                        },
                        title: '${index + 1}. ' + course.title,
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            );
          }

          if (state is CoursesError) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return Center(
              child: Text('$errorMessage'),
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
