import 'dart:async';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/blocs/modules/modules_bloc.dart';
import 'package:MyBFD/models/CourseModel.dart';
import 'package:MyBFD/models/LessonModel.dart';
import 'package:MyBFD/models/ModuleModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/CourseService.dart';
import 'package:MyBFD/widgets/InfoWidget.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/PageStructureWidget.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'courses_event.dart';
part 'courses_state.dart';
part 'courses_page.dart';

class CoursesBloc extends Bloc<CoursesEvent, CoursesState> {
  CoursesBloc() : super(CoursesInitial());

  @override
  Stream<CoursesState> mapEventToState(
    CoursesEvent event,
  ) async* {
    yield CoursesLoading();

    if (event is LoadCoursesEvent) {
      try {
        List<CourseModel> courses =
            await locator<CourseService>().retrieveCourses();

        yield CoursesLoaded(courses: courses);
      } catch (error) {
        yield CoursesError(error: error);
      }
    }
  }
}
