import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/BezierClipper.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/UserService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

import '../../constants.dart';
import '../../service_locator.dart';

part 'package:MyBFD/blocs/signup/signup_event.dart';

part 'package:MyBFD/blocs/signup/signup_page.dart';

part 'package:MyBFD/blocs/signup/signup_state.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  SignupBloc() : super(SignupInitial());

  @override
  Stream<SignupState> mapEventToState(
    SignupEvent event,
  ) async* {
    if (event is Signup) {
      final String email = event.email;
      final String password = event.password;
      final String username = event.username;
      final String firstName = event.firstName;
      final String lastName = event.lastName;

      try {
        yield SignupLoading();

        final UserCredential userCredential = await locator<AuthService>()
            .createUserWithEmailAndPassword(email: email, password: password);

        final User user = userCredential.user;

        UserModel userModel = UserModel(
          imgUrl: DUMMY_PROFILE_PHOTO_URL,
          email: email,
          modified: DateTime.now(),
          created: DateTime.now(),
          uid: user.uid,
          username: username,
          fcmToken: null,
          city: null,
          phoneNumber: null,
          postCode: null,
          streetAddress: null,
          firstName: firstName,
          lastName: lastName,
        );

        await locator<UserService>().createUser(user: userModel);
        yield SignupSuccess();
      } catch (error) {
        yield SignupFailure(error: error);
      }
    }

    if (event is TryAgain) {
      yield SignupInitial();
    }
  }
}
