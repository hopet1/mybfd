part of 'package:MyBFD/blocs/signup/signup_bloc.dart';

abstract class SignupEvent extends Equatable {
  const SignupEvent();

  @override
  List<Object> get props => [];
}

class Signup extends SignupEvent {
  final String email;
  final String password;
  final String username;
  final String firstName;
  final String lastName;

  Signup({
    @required this.email,
    @required this.password,
    @required this.username,
    @required this.firstName,
    @required this.lastName,
  });

  List<Object> get props => [
        email,
        password,
        username,
      ];
}

class TryAgain extends SignupEvent {}
