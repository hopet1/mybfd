part of 'package:MyBFD/blocs/signup/signup_bloc.dart';

abstract class SignupState extends Equatable {
  const SignupState();

  @override
  List<Object> get props => [];
}

class SignupLoading extends SignupState {}

class SignupInitial extends SignupState {}

class SignupSuccess extends SignupState {}

class SignupFailure extends SignupState {
  final dynamic error;

  SignupFailure({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
