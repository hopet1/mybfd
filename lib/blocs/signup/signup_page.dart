part of 'package:MyBFD/blocs/signup/signup_bloc.dart';

class SignupPage extends StatefulWidget {
  @override
  State createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage> {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final double _titleFontSize = 60;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorOrange,
      body: Form(
        key: _formKey,
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              ClipPath(
                clipper: BezierClipper(),
                child: Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 400,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          _usernameController.text = 'Travisty92';
                          _emailController.text = 'trey.a.hope@gmail.com';
                          _passwordController.text = 'Peachy33';
                        },
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: 100,
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'my',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorDark,
                              ),
                            ),
                            TextSpan(
                              text: 'bfd',
                              style: TextStyle(
                                fontSize: _titleFontSize,
                                fontWeight: FontWeight.bold,
                                color: colorOrange,
                              ),
                            ),
                            TextSpan(
                              text: '\nFinancial Education App'.toUpperCase(),
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 2.0,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              BlocConsumer<SignupBloc, SignupState>(
                builder: (context, state) {
                  if (state is SignupLoading) {
                    return Spinner();
                  }

                  if (state is SignupFailure) {
                    final String errorMessage =
                        state.error.message ?? 'Could not log in at this time.';

                    return Center(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: Text(
                              '$errorMessage',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 21,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          RoundedButton(
                            background: colorOrange,
                            foreground: Colors.white,
                            onTap: () {
                              context.read<SignupBloc>().add(
                                    TryAgain(),
                                  );
                            },
                            text: 'Try Again?',
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is SignupInitial) {
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: _firstNameController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,
                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'First name'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: _lastNameController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Last name'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().isEmpty,
                                  controller: _usernameController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.person,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Username'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator: locator<ValidationService>().email,
                                  controller: _emailController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.email,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Email'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  validator:
                                      locator<ValidationService>().password,
                                  obscureText: true,
                                  controller: _passwordController,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(
                                        color: Colors.white,
                                      ),
                                      prefixIcon: Icon(
                                        Icons.lock,
                                        color: Colors.white,
                                      ),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(90.0),
                                        ),
                                        borderSide: BorderSide.none,

                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "WorkSansLight"),
                                      filled: true,
                                      fillColor: Colors.white24,
                                      hintText: 'Password'),
                                ),
                              ),
                              if (!Responsive.isMobile(context))
                                Spacer(
                                    flex:
                                        Responsive.isDesktop(context) ? 2 : 1),
                            ],
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          RoundedButton(
                            background: Colors.white,
                            text: 'Sign Up',
                            foreground: colorOrange,
                            onTap: () async {
                              if (!_formKey.currentState.validate()) return;

                              final bool confirm = await locator<ModalService>()
                                  .showConfirmation(
                                context: context,
                                title: 'Submit',
                                message: 'Are you sure?',
                              );

                              if (!confirm) return;

                              context.read<SignupBloc>().add(
                                    Signup(
                                      firstName: _firstNameController.text,
                                      lastName: _lastNameController.text,
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                      username: _usernameController.text,
                                    ),
                                  );
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? 14
                                          : 20),
                                  children: [
                                    TextSpan(
                                      text: 'Already have an account?',
                                    ),
                                    TextSpan(
                                      text: ' Login',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    );
                  }

                  return Container();
                },
                listener: (context, state) {
                  if (state == SignupSuccess()) {
                    Phoenix.rebirth(context);
                  }
                  if (state is SignupFailure) {
                    print('failure');
                    //todo: Report this sign in failure somewhere perhaps?
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
