part of 'budget_planner_bloc.dart';

class BudgetPlannerPage extends StatefulWidget {
  @override
  State createState() => _BudgetPlannerPageState();
}

class _BudgetPlannerPageState extends State<BudgetPlannerPage>
    implements BudgetPlannerBlocDelegate {
  Map<String, dynamic> _uniqueScrollViewKeys = Map<String, dynamic>();

  @override
  void initState() {
    context.read<BudgetPlannerBloc>().setDelegate(delegate: this);

    super.initState();
  }

  Row titleNavWidget({
    @required String title,
    @required int step,
    @required bool showPrev,
    @required bool showNext,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          onPressed: () {
            if (showPrev) {
              context.read<BudgetPlannerBloc>().add(PreviousStepEvent());
            }
          },
          iconSize: Responsive.isMobile(context) ? 40 : 50,
          icon: Icon(
            Icons.chevron_left,
            color: showPrev ? Colors.grey : Colors.transparent,
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Text(
                '$title',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
              ),
              SizedBox(height: 6.0),
              Text(
                'Step $step of 8',
                style: TextStyle(
                  fontSize: Responsive.isMobile(context) ? 16 : 22,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
        ),
        IconButton(
          onPressed: () {
            if (showNext) {
              context.read<BudgetPlannerBloc>().add(
                    NextStepEvent(),
                  );
            }
          },
          iconSize: Responsive.isMobile(context) ? 40 : 50,
          icon: Icon(
            Icons.chevron_right,
            color: showNext ? Colors.grey : Colors.transparent,
          ),
        )
      ],
    );
  }

  Widget netWorthItemWidgetBuilder({
    @required NetWorthItemModel netWorthItemModel,
    @required String title,
    @required bool lastItem,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$title',
          style: TextStyle(
            fontSize: Responsive.isMobile(context) ? 18 : 24,
            fontWeight: FontWeight.w200,
            color: colorDark,
          ),
        ),
        SizedBox(height: 12.0),
        NetWorthItemWidget(
          netWorthItem: netWorthItemModel,
          updateSelectedNetWorthItemEvent: () {
            context.read<BudgetPlannerBloc>().add(
                  UpdateNetWorthItemEvent(),
                );
          },
          netWorthItemOptions: BUDGET_FREQUENCY_LIST,
        ),
        lastItem ? SizedBox.shrink() : SizedBox(height: 24.0),
      ],
    );
  }

  @override
  void showMessage({@required String message}) {
    locator<ModalService>().showAlert(
      context: context,
      title: 'Success',
      message: 'Data saved.',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Navbar(),
      body: BlocConsumer<BudgetPlannerBloc, BudgetPlannerState>(
        builder: (context, state) {
          if (state is LoadingState) {
            return Spinner();
          }

          if (state is IncomeState) {
            final List<NetWorthItemModel> incomeItems = state.incomeItems;
            final double incomeTotal = state.incomeTotal;

            final NetWorthItemModel payAfterTaxItem = incomeItems[0];
            final NetWorthItemModel incomeFromSelfEmploymentItem =
                incomeItems[1];
            final NetWorthItemModel childCareItem = incomeItems[2];
            final NetWorthItemModel jobseekersAllowance = incomeItems[3];
            final NetWorthItemModel incomeSupport = incomeItems[4];
            final NetWorthItemModel workingTaxCreditItem = incomeItems[5];
            final NetWorthItemModel childTaxCreditItem = incomeItems[6];
            final NetWorthItemModel childBenefitItem = incomeItems[7];
            final NetWorthItemModel employmentAndSupportAllowanceItem =
                incomeItems[8];
            final NetWorthItemModel universalCreditItem = incomeItems[9];
            final NetWorthItemModel disabilityLivingAllowanceItem =
                incomeItems[10];
            final NetWorthItemModel attendanceAllowanceItem = incomeItems[11];
            final NetWorthItemModel carersAllowanceItem = incomeItems[12];
            final NetWorthItemModel housingBenefitItem = incomeItems[13];
            final NetWorthItemModel maternityPaternityAllowanceItem =
                incomeItems[14];
            final NetWorthItemModel statePensionItem = incomeItems[15];
            final NetWorthItemModel employerPensionItem = incomeItems[16];
            final NetWorthItemModel privatePensionOrAnnuityItem =
                incomeItems[17];
            final NetWorthItemModel incomeFromSavingsAndInvestmentsItem =
                incomeItems[18];
            final NetWorthItemModel childMaintenanceItem = incomeItems[19];
            final NetWorthItemModel studentLoansAndGrantsItem = incomeItems[20];
            final NetWorthItemModel additionalItemsItem = incomeItems[21];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'INCOME',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Income',
                        step: 1,
                        showPrev: false,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'Earnings',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Pay (after tax)',
                            netWorthItemModel: payAfterTaxItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Income from self-employment',
                            netWorthItemModel: incomeFromSelfEmploymentItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Benefits and Tax Credits',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Childcare',
                            netWorthItemModel: childCareItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Jobseeker\s Allowance',
                            netWorthItemModel: jobseekersAllowance,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Income Support',
                            netWorthItemModel: incomeSupport,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Working Tax Credit',
                            netWorthItemModel: workingTaxCreditItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Child Tax Credit',
                            netWorthItemModel: childTaxCreditItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Child Benefit',
                            netWorthItemModel: childBenefitItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title:
                                'Employment and Support Allowance (or Incapacity Benefit)',
                            netWorthItemModel:
                                employmentAndSupportAllowanceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Universal Credit',
                            netWorthItemModel: universalCreditItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title:
                                'Disability Living Allowance (or Personal Independence Payment)',
                            netWorthItemModel: disabilityLivingAllowanceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Attendance Allowance',
                            netWorthItemModel: attendanceAllowanceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Carer\'s Allowance',
                            netWorthItemModel: carersAllowanceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Housing Benefit',
                            netWorthItemModel: housingBenefitItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Maternity/Paternity Allowance',
                            netWorthItemModel: maternityPaternityAllowanceItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Pension',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'State Pension',
                            netWorthItemModel: statePensionItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Employer Pension',
                            netWorthItemModel: employerPensionItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Private Pension or Annuity',
                            netWorthItemModel: privatePensionOrAnnuityItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Other Income',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Income From Savings & Investments',
                            netWorthItemModel:
                                incomeFromSavingsAndInvestmentsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Child Maintenance',
                            netWorthItemModel: childMaintenanceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Student Loans & Grants',
                            netWorthItemModel: studentLoansAndGrantsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Income Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    incomeTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Household Bills',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          //TODO: Next page should scroll to the top.
                          context.read<BudgetPlannerBloc>().add(
                                NextStepEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is HouseholdState) {
            final List<NetWorthItemModel> householdItems = state.householdItems;
            final double householdTotal = state.householdTotal;

            final NetWorthItemModel mortgageItem = householdItems[0];
            final NetWorthItemModel rentItem = householdItems[1];
            final NetWorthItemModel mortgageLifeInsuranceItem =
                householdItems[2];
            final NetWorthItemModel mortgagePaymentProtectionInsuranceItem =
                householdItems[3];
            final NetWorthItemModel groundRentItem = householdItems[4];
            final NetWorthItemModel serviceChargeItem = householdItems[5];
            final NetWorthItemModel councilTaxItem = householdItems[6];
            final NetWorthItemModel gasItem = householdItems[7];
            final NetWorthItemModel electricityItem = householdItems[8];
            final NetWorthItemModel waterItem = householdItems[9];
            final NetWorthItemModel homePhoneItem = householdItems[10];
            final NetWorthItemModel mobilePhoneItem = householdItems[11];
            final NetWorthItemModel internetBroadbandTvItem =
                householdItems[12];
            final NetWorthItemModel tvLicenseItem = householdItems[13];
            final NetWorthItemModel buildingInsuranceItem = householdItems[14];
            final NetWorthItemModel contentsInsuranceItem = householdItems[15];
            final NetWorthItemModel additionalItemsItem = householdItems[16];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'HOUSE',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Household',
                        step: 2,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'General',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Mortgage',
                            netWorthItemModel: mortgageItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Rent',
                            netWorthItemModel: rentItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Mortgage Life Insurance',
                            netWorthItemModel: mortgageLifeInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Mortgage Payment Protection Insurance',
                            netWorthItemModel:
                                mortgagePaymentProtectionInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Ground Rent',
                            netWorthItemModel: groundRentItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Service Charge',
                            netWorthItemModel: serviceChargeItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Utilites',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Council Tax',
                            netWorthItemModel: councilTaxItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Gas',
                            netWorthItemModel: gasItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Electricity',
                            netWorthItemModel: electricityItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Water',
                            netWorthItemModel: waterItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Home Phone',
                            netWorthItemModel: homePhoneItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Mobile Phone',
                            netWorthItemModel: mobilePhoneItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Internet/Broadband/TV',
                            netWorthItemModel: internetBroadbandTvItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'TV Licensce',
                            netWorthItemModel: tvLicenseItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Insurance',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Building Insurance',
                            netWorthItemModel: buildingInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Contents Insurance',
                            netWorthItemModel: contentsInsuranceItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Household Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    householdTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Living Costs',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is LivingCostsState) {
            final List<NetWorthItemModel> livingCostsItems =
                state.livingCostsItems;
            final double livingCostsTotal = state.livingCostsTotal;

            final NetWorthItemModel groceryShoppingItem = livingCostsItems[0];
            final NetWorthItemModel takeawayFoodItem = livingCostsItems[1];
            final NetWorthItemModel alcoholItem = livingCostsItems[2];
            final NetWorthItemModel cigarettesTobaccoItem = livingCostsItems[3];
            final NetWorthItemModel hairdressingItem = livingCostsItems[4];
            final NetWorthItemModel beautyTreatmentsItem = livingCostsItems[5];
            final NetWorthItemModel toiletriesItem = livingCostsItems[6];
            final NetWorthItemModel eyeCareItem = livingCostsItems[7];
            final NetWorthItemModel dentalCareItem = livingCostsItems[8];
            final NetWorthItemModel prescriptionsMedicinesItem =
                livingCostsItems[9];
            final NetWorthItemModel clothesItem = livingCostsItems[10];
            final NetWorthItemModel shoesItem = livingCostsItems[11];
            final NetWorthItemModel laundryAndDryCleaningItem =
                livingCostsItems[12];
            final NetWorthItemModel foodCoffeeItem = livingCostsItems[13];
            final NetWorthItemModel professionalFeesItem = livingCostsItems[14];
            final NetWorthItemModel workClothesItem = livingCostsItems[15];
            final NetWorthItemModel additionalItemsItem = livingCostsItems[16];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'LIVING',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Living Costs',
                        step: 3,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'Food & Drink',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Grocery Shopping',
                            netWorthItemModel: groceryShoppingItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Takeaway Food',
                            netWorthItemModel: takeawayFoodItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Alcohol',
                            netWorthItemModel: alcoholItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Cigarettes & Tobacco',
                            netWorthItemModel: cigarettesTobaccoItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Health & Beauty',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Hairdressing',
                            netWorthItemModel: hairdressingItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Beauty Treatments',
                            netWorthItemModel: beautyTreatmentsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Toiletries',
                            netWorthItemModel: toiletriesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Eye Care',
                            netWorthItemModel: eyeCareItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Dental Care',
                            netWorthItemModel: dentalCareItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Prescriptions & Medicines',
                            netWorthItemModel: prescriptionsMedicinesItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Clothes & Shoes',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Clothes',
                            netWorthItemModel: clothesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Shoes',
                            netWorthItemModel: shoesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Laundry and Dry Cleaning',
                            netWorthItemModel: laundryAndDryCleaningItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Work',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Food and Coffee',
                            netWorthItemModel: foodCoffeeItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Professional Fees',
                            netWorthItemModel: professionalFeesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Work Clothes',
                            netWorthItemModel: workClothesItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Living Costs Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    livingCostsTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Finance & Insurance',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is FinanceInsuranceState) {
            final List<NetWorthItemModel> financeInsuranceItems =
                state.financeInsuranceItems;
            final double financeInsuranceTotal = state.financeInsuranceTotal;

            final NetWorthItemModel overdraftItem = financeInsuranceItems[0];
            final NetWorthItemModel bankfeesItem = financeInsuranceItems[1];
            final NetWorthItemModel regularSavingItem =
                financeInsuranceItems[2];
            final NetWorthItemModel isaSSavingItem = financeInsuranceItems[3];
            final NetWorthItemModel buyingSharesItem = financeInsuranceItems[4];
            final NetWorthItemModel privatePensionContributionsItem =
                financeInsuranceItems[5];
            final NetWorthItemModel loanRepaymentsItem =
                financeInsuranceItems[6];
            final NetWorthItemModel studentLoanRepaymentsItem =
                financeInsuranceItems[7];
            final NetWorthItemModel creditCardRepaymentsItem =
                financeInsuranceItems[8];
            final NetWorthItemModel hirePurchaseCatalogueRepaymentsItem =
                financeInsuranceItems[9];
            final NetWorthItemModel lifeInsuranceItem =
                financeInsuranceItems[10];
            final NetWorthItemModel incomeProtectionInsurance =
                financeInsuranceItems[11];
            final NetWorthItemModel criticalIllnessInsuranceItem =
                financeInsuranceItems[12];
            final NetWorthItemModel paymentProtectionInsuranceItem =
                financeInsuranceItems[13];
            final NetWorthItemModel healthInsuranceItem =
                financeInsuranceItems[14];
            final NetWorthItemModel dentalInsuranceItem =
                financeInsuranceItems[15];
            final NetWorthItemModel additionalItemsItem =
                financeInsuranceItems[16];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'DENTAL',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Finance & Insurance',
                        step: 4,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'Banking',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Overdraft Charges and Interest',
                            netWorthItemModel: overdraftItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Bank Fees',
                            netWorthItemModel: bankfeesItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Saving & Investments',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Regular Saving',
                            netWorthItemModel: regularSavingItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'ISAs Saving',
                            netWorthItemModel: isaSSavingItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Buying Shares',
                            netWorthItemModel: buyingSharesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Private Pension Contributions',
                            netWorthItemModel: privatePensionContributionsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Loans & Credit',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Loan Repayments',
                            netWorthItemModel: loanRepaymentsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Student Loan Repayments',
                            netWorthItemModel: studentLoanRepaymentsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Credit Card Repayments',
                            netWorthItemModel: creditCardRepaymentsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Hire Purchase & Catalogue Repayments',
                            netWorthItemModel:
                                hirePurchaseCatalogueRepaymentsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Insurance',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Life Insurance',
                            netWorthItemModel: lifeInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Income Protections Insurance',
                            netWorthItemModel: incomeProtectionInsurance,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Critical Illness Insurance',
                            netWorthItemModel: criticalIllnessInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Payment Protection Insurance',
                            netWorthItemModel: paymentProtectionInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Health Insurance Item',
                            netWorthItemModel: healthInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Dental Insurance Item',
                            netWorthItemModel: dentalInsuranceItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Finance & Insurance Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    financeInsuranceTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Children',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is ChildrenState) {
            final List<NetWorthItemModel> childrenItems = state.childrenItems;
            final double childrenTotal = state.childrenTotal;

            final NetWorthItemModel childcareItem = childrenItems[0];
            final NetWorthItemModel childrenClothesShoesItem = childrenItems[1];
            final NetWorthItemModel schoolUniformItem = childrenItems[2];
            final NetWorthItemModel nappiesBabyItemsItem = childrenItems[3];
            final NetWorthItemModel activitiesClubsItem = childrenItems[4];
            final NetWorthItemModel toysTreatsItem = childrenItems[5];
            final NetWorthItemModel pocketMoneyItem = childrenItems[6];
            final NetWorthItemModel maintenanceChildSupportItem =
                childrenItems[7];
            final NetWorthItemModel schoolFeesItem = childrenItems[8];
            final NetWorthItemModel schoolTripsItem = childrenItems[9];
            final NetWorthItemModel schoolDinnersItem = childrenItems[10];
            final NetWorthItemModel afterSchoolClubsItem = childrenItems[11];
            final NetWorthItemModel additionalItemsItem = childrenItems[12];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'CHILDREN',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Children',
                        step: 5,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'General',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Childcare',
                            netWorthItemModel: childcareItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Children\'s Clothes & Shoes',
                            netWorthItemModel: childrenClothesShoesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'School Uniform',
                            netWorthItemModel: schoolUniformItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Nappies and Baby Items',
                            netWorthItemModel: nappiesBabyItemsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Activities & Clubs',
                            netWorthItemModel: activitiesClubsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Toys & Treats',
                            netWorthItemModel: toysTreatsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Pocket Money',
                            netWorthItemModel: pocketMoneyItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Maintenance or Child Support',
                            netWorthItemModel: maintenanceChildSupportItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'School',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'School Fees',
                            netWorthItemModel: schoolFeesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'School Trips',
                            netWorthItemModel: schoolTripsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'School Dinners',
                            netWorthItemModel: schoolDinnersItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'After-school Clubs',
                            netWorthItemModel: afterSchoolClubsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Children Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    childrenTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Travel',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is TravelState) {
            final List<NetWorthItemModel> travelItems = state.travelItems;
            final double travelTotal = state.travelTotal;

            final NetWorthItemModel fuelItem = travelItems[0];
            final NetWorthItemModel carInsuranceItem = travelItems[1];
            final NetWorthItemModel breakdownCoverItem = travelItems[2];
            final NetWorthItemModel carTaxItem = travelItems[3];
            final NetWorthItemModel carFinanceLoanRepaymentItem =
                travelItems[4];
            final NetWorthItemModel loanInsuranceItem = travelItems[5];
            final NetWorthItemModel motItem = travelItems[6];
            final NetWorthItemModel maintenanceRepairsItem = travelItems[7];
            final NetWorthItemModel parkingTollsItem = travelItems[8];
            final NetWorthItemModel busTubeTramFaresItem = travelItems[9];
            final NetWorthItemModel trainsItem = travelItems[10];
            final NetWorthItemModel taxisItem = travelItems[11];
            final NetWorthItemModel flightsItem = travelItems[12];
            final NetWorthItemModel additionalItemsItem = travelItems[13];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'TRAVEL',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Travel',
                        step: 6,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'Vehicle',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Fuel',
                            netWorthItemModel: fuelItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Car Insurance',
                            netWorthItemModel: carInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Breakdown Cover',
                            netWorthItemModel: breakdownCoverItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Car Tax',
                            netWorthItemModel: carTaxItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Car Finance or Loan Repayment',
                            netWorthItemModel: carFinanceLoanRepaymentItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Loan Insurance',
                            netWorthItemModel: loanInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'MOT',
                            netWorthItemModel: motItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Maintenance & Repairs',
                            netWorthItemModel: maintenanceRepairsItem,
                            lastItem: true,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Parking & Tolls',
                            netWorthItemModel: parkingTollsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Public Transport',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Bus, Tube & Tram Fares',
                            netWorthItemModel: busTubeTramFaresItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Trains',
                            netWorthItemModel: trainsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Taxis',
                            netWorthItemModel: taxisItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Flights',
                            netWorthItemModel: flightsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Travel Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    travelTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Other',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is OtherState) {
            final List<NetWorthItemModel> otherItems = state.otherItems;
            final double otherTotal = state.otherTotal;

            final NetWorthItemModel cinemaTheatreTripsItem = otherItems[0];
            final NetWorthItemModel daysOutItem = otherItems[1];
            final NetWorthItemModel booksMusicFilmsGamesItem = otherItems[2];
            final NetWorthItemModel hoobiesItem = otherItems[3];
            final NetWorthItemModel eatingOutItem = otherItems[4];
            final NetWorthItemModel goingForDrinksItem = otherItems[5];
            final NetWorthItemModel sportGymItem = otherItems[6];
            final NetWorthItemModel newspapersMagazinesItem = otherItems[7];
            final NetWorthItemModel birthdaysItem = otherItems[8];
            final NetWorthItemModel christmasItem = otherItems[9];
            final NetWorthItemModel otherFestivalsCelebrationsItem =
                otherItems[10];
            final NetWorthItemModel weddingsItem = otherItems[11];
            final NetWorthItemModel travelInsuranceItem = otherItems[12];
            final NetWorthItemModel spendingMoneyItem = otherItems[13];
            final NetWorthItemModel otherHolidayCostsItem = otherItems[14];
            final NetWorthItemModel foodItem = otherItems[15];
            final NetWorthItemModel vetBillsItem = otherItems[16];
            final NetWorthItemModel petInsuranceItem = otherItems[17];
            final NetWorthItemModel additionalItemsItem = otherItems[18];

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'OTHER',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Other',
                        step: 7,
                        showPrev: true,
                        showNext: true,
                      ),
                      SizedBox(height: 36.0),
                      CustomContentBox(
                        title: 'Entertainment',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Cinema & Theatre Trips',
                            netWorthItemModel: cinemaTheatreTripsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Days Out',
                            netWorthItemModel: daysOutItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Books, Music, Films, Games, Etc.',
                            netWorthItemModel: booksMusicFilmsGamesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Hobbies',
                            netWorthItemModel: hoobiesItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Eating Out',
                            netWorthItemModel: eatingOutItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Going Out For Drinks',
                            netWorthItemModel: goingForDrinksItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Sport & Gym',
                            netWorthItemModel: sportGymItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Newspapers & Magazines',
                            netWorthItemModel: newspapersMagazinesItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'General',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Birthdays',
                            netWorthItemModel: birthdaysItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Christmas',
                            netWorthItemModel: christmasItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Other Festivals & Celebrations',
                            netWorthItemModel: otherFestivalsCelebrationsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Weddings',
                            netWorthItemModel: weddingsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Holidays',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Travel Insurance',
                            netWorthItemModel: travelInsuranceItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Spending Money',
                            netWorthItemModel: spendingMoneyItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Other Holiday Costs',
                            netWorthItemModel: otherHolidayCostsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Pets',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Food',
                            netWorthItemModel: foodItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Vet Bills',
                            netWorthItemModel: vetBillsItem,
                            lastItem: false,
                          ),
                          netWorthItemWidgetBuilder(
                            title: 'Pet Insurance',
                            netWorthItemModel: petInsuranceItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 24.0),
                      CustomContentBox(
                        title: 'Additional Items',
                        children: [
                          netWorthItemWidgetBuilder(
                            title: 'Items Total',
                            netWorthItemModel: additionalItemsItem,
                            lastItem: true,
                          ),
                        ],
                      ),
                      SizedBox(height: 48.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 24.0),
                            child: Column(
                              children: [
                                Text(
                                  'Other Total',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                                Text(
                                  Money.from(
                                    otherTotal,
                                    Currency.create(
                                      'GBP',
                                      2,
                                      symbol: '£',
                                      // pattern: 'S0,000.00',
                                    ),
                                  ).toString(),
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 36.0,
                                      fontWeight: FontWeight.w200),
                                ),
                                Text(
                                  'per month',
                                  style: TextStyle(
                                      color: colorDark,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                SizedBox(height: 12.0),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      RoundedButton(
                        text: 'Summary',
                        foreground: Color(colorOrange.value),
                        background: Colors.white,
                        onTap: () {
                          context
                              .read<BudgetPlannerBloc>()
                              .add(NextStepEvent());
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Save Data',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () async {
                          final bool confirm =
                              await locator<ModalService>().showConfirmation(
                            context: context,
                            title: 'Save Data',
                            message: 'Are you sure?',
                          );

                          if (!confirm) return;

                          context.read<BudgetPlannerBloc>().add(
                                SaveCalculatorEvent(),
                              );
                        },
                      ),
                      SizedBox(height: 12),
                      RoundedButton(
                        text: 'Skip To Summary',
                        foreground: Colors.white,
                        background: colorOrange,
                        onTap: () {
                          context.read<BudgetPlannerBloc>().add(
                                GoToStepEvent(step: 7),
                              );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            );
          }

          if (state is SummaryState) {
            final String frequency = state.frequency;
            final double incomeTotal = state.incomeTotal;
            final double householdTotal = state.householdTotal;
            final double livingCostsTotal = state.livingCostsTotal;
            final double financeInsuranceTotal = state.financeInsuranceTotal;
            final double childrenTotal = state.childrenTotal;
            final double travelTotal = state.travelTotal;
            final double otherTotal = state.otherTotal;

            final double outgoingTotal = householdTotal +
                livingCostsTotal +
                financeInsuranceTotal +
                childrenTotal +
                travelTotal +
                otherTotal;

            return ListView(
              key: locator<WidgetService>().generateKey(
                  map: _uniqueScrollViewKeys,
                  name: 'SUMMARY',
                  key: ObjectKey(Uuid().v1())),
              padding: const EdgeInsets.all(20),
              children: [
                Topbar(
                  backArrowTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                  width: Responsive.isMobile(context)
                      ? screenWidth * 0.8
                      : screenWidth * 0.7,
                  child: Column(
                    children: [
                      SizedBox(height: 12.0),
                      titleNavWidget(
                        title: 'Summary',
                        step: 8,
                        showPrev: true,
                        showNext: false,
                      ),
                      SizedBox(height: 36.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        child: DropdownButton(
                          isExpanded: true,
                          hint: Text('Select Item'),
                          value: frequency,
                          underline: SizedBox(),
                          onChanged: (newValue) {
                            context.read<BudgetPlannerBloc>().add(
                                  UpdateSummaryFrequencyEvent(
                                      frequency: newValue),
                                );
                          },
                          items: BUDGET_FREQUENCY_LIST.map(
                            (option) {
                              return DropdownMenuItem(
                                child: Text(option),
                                value: option,
                              );
                            },
                          ).toList(),
                        ),
                      ),
                      SizedBox(height: 36.0),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: colorOrange,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text(
                            'Summary Total',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: Colors.grey.shade300,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Income'),
                                  Text(
                                    Money.from(
                                      incomeTotal,
                                      Currency.create(
                                        'GBP',
                                        2,
                                        symbol: '£',
                                        // pattern: 'S0,000.00',
                                      ),
                                    ).toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Outgoing'),
                                  Text(
                                    Money.from(
                                      outgoingTotal,
                                      Currency.create(
                                        'GBP',
                                        2,
                                        symbol: '£',
                                        // pattern: 'S0,000.00',
                                      ),
                                    ).toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: Responsive.isMobile(context)
                            ? screenWidth * 0.8
                            : screenWidth * 0.7,
                        color: colorOrange,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Net Income',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                Money.from(
                                  incomeTotal - outgoingTotal >= 0
                                      ? 0
                                      : incomeTotal - outgoingTotal,
                                  Currency.create(
                                    'GBP',
                                    2,
                                    symbol: '£',
                                    // pattern: 'S0,000.00',
                                  ),
                                ).toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 36.0),
                      PieChart(
                        dataMap: {
                          'Household Bills - ${Money.from(
                            householdTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': householdTotal,
                          'Living Costs - ${Money.from(
                            livingCostsTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': livingCostsTotal,
                          'Finance & Insurance - ${Money.from(
                            financeInsuranceTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': financeInsuranceTotal,
                          'Children - ${Money.from(
                            childrenTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': childrenTotal,
                          'Travel - ${Money.from(
                            travelTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': travelTotal,
                          'Other - ${Money.from(
                            otherTotal,
                            Currency.create(
                              'GBP',
                              2,
                              symbol: '£',
                              // pattern: 'S0,000.00',
                            ),
                          ).toString()}': otherTotal,
                        },
                        animationDuration: Duration(milliseconds: 800),
                        chartLegendSpacing: 32,
                        chartRadius: MediaQuery.of(context).size.width / 3.2,
                        colorList: [
                          Colors.green,
                          Colors.lightBlue,
                          Colors.red,
                          Colors.orange,
                          Colors.brown,
                          Colors.yellow,
                        ],
                        initialAngleInDegree: 0,
                        chartType: ChartType.ring,
                        ringStrokeWidth: 16,
                        // centerText: 'HYBRID',
                        legendOptions: LegendOptions(
                          showLegendsInRow: false,
                          legendPosition: LegendPosition.bottom,
                          showLegends: true,
                          //legendShape: LegendShape.Circle,
                          legendTextStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        chartValuesOptions: ChartValuesOptions(
                          showChartValueBackground: true,
                          showChartValues: false,
                          showChartValuesInPercentage: false,
                          showChartValuesOutside: true,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Responsive.isMobile(context) ? 50 : 150),
                  child: RoundedButton(
                    text: 'Save Data',
                    foreground: Colors.white,
                    background: colorOrange,
                    onTap: () async {
                      final bool confirm =
                          await locator<ModalService>().showConfirmation(
                        context: context,
                        title: 'Save Data',
                        message: 'Are you sure?',
                      );

                      if (!confirm) return;

                      context.read<BudgetPlannerBloc>().add(
                            SaveCalculatorEvent(),
                          );
                    },
                  ),
                ),
              ],
            );
          }

          if (state is ErrorState) {
            final String errorMessage =
                state.error.message ?? 'Could not log in at this time.';

            return Center(
              child: Text('$errorMessage'),
            );
          }

          return Container();
        },
        listener: (context, state) {},
      ),
    );
  }
}
