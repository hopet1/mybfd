part of 'budget_planner_bloc.dart';

abstract class BudgetPlannerState extends Equatable {
  const BudgetPlannerState();

  @override
  List<Object> get props => [];
}

class InitialState extends BudgetPlannerState {}

class LoadingState extends BudgetPlannerState {}

class IncomeState extends BudgetPlannerState {
  final List<NetWorthItemModel> incomeItems;
  final double incomeTotal;

  IncomeState({
    @required this.incomeItems,
    @required this.incomeTotal,
  });

  @override
  List<Object> get props => [
        incomeItems,
        incomeTotal,
      ];
}

class HouseholdState extends BudgetPlannerState {
  final List<NetWorthItemModel> householdItems;
  final double householdTotal;

  HouseholdState({
    @required this.householdItems,
    @required this.householdTotal,
  });

  @override
  List<Object> get props => [
        householdItems,
        householdTotal,
      ];
}

class LivingCostsState extends BudgetPlannerState {
  final List<NetWorthItemModel> livingCostsItems;
  final double livingCostsTotal;

  LivingCostsState({
    @required this.livingCostsItems,
    @required this.livingCostsTotal,
  });

  @override
  List<Object> get props => [
        livingCostsItems,
        livingCostsTotal,
      ];
}

class FinanceInsuranceState extends BudgetPlannerState {
  final List<NetWorthItemModel> financeInsuranceItems;
  final double financeInsuranceTotal;

  FinanceInsuranceState({
    @required this.financeInsuranceItems,
    @required this.financeInsuranceTotal,
  });

  @override
  List<Object> get props => [
        financeInsuranceItems,
        financeInsuranceTotal,
      ];
}

class ChildrenState extends BudgetPlannerState {
  final List<NetWorthItemModel> childrenItems;
  final double childrenTotal;

  ChildrenState({
    @required this.childrenItems,
    @required this.childrenTotal,
  });

  @override
  List<Object> get props => [
        childrenItems,
        childrenTotal,
      ];
}

class TravelState extends BudgetPlannerState {
  final List<NetWorthItemModel> travelItems;
  final double travelTotal;

  TravelState({
    @required this.travelItems,
    @required this.travelTotal,
  });

  @override
  List<Object> get props => [
        travelItems,
        travelTotal,
      ];
}

class OtherState extends BudgetPlannerState {
  final List<NetWorthItemModel> otherItems;
  final double otherTotal;

  OtherState({
    @required this.otherItems,
    @required this.otherTotal,
  });

  @override
  List<Object> get props => [
        otherItems,
        otherTotal,
      ];
}

class SummaryState extends BudgetPlannerState {
  final String frequency;
  final double incomeTotal;
  final double householdTotal;
  final double livingCostsTotal;
  final double financeInsuranceTotal;
  final double childrenTotal;
  final double travelTotal;
  final double otherTotal;

  SummaryState({
    @required this.frequency,
    @required this.incomeTotal,
    @required this.householdTotal,
    @required this.livingCostsTotal,
    @required this.financeInsuranceTotal,
    @required this.childrenTotal,
    @required this.travelTotal,
    @required this.otherTotal,
  });

  @override
  List<Object> get props => [
        frequency,
        incomeTotal,
        householdTotal,
        livingCostsTotal,
        financeInsuranceTotal,
        childrenTotal,
        travelTotal,
        otherTotal,
      ];
}

class ErrorState extends BudgetPlannerState {
  final dynamic error;

  ErrorState({
    @required this.error,
  });

  @override
  List<Object> get props => [
        error,
      ];
}
