part of 'budget_planner_bloc.dart';

abstract class BudgetPlannerEvent extends Equatable {
  const BudgetPlannerEvent();

  @override
  List<Object> get props => [];
}

class BudgetPlannerLoadPageEvent extends BudgetPlannerEvent {}

class NextStepEvent extends BudgetPlannerEvent {}

class PreviousStepEvent extends BudgetPlannerEvent {}

class GoToStepEvent extends BudgetPlannerEvent {
  final int step;

  const GoToStepEvent({
    @required this.step,
  });

  @override
  List<Object> get props => [
        step,
      ];
}

class UpdateNetWorthItemEvent extends BudgetPlannerEvent {
  const UpdateNetWorthItemEvent();

  @override
  List<Object> get props => [];
}

class UpdateSummaryFrequencyEvent extends BudgetPlannerEvent {
  final String frequency;

  const UpdateSummaryFrequencyEvent({
    @required this.frequency,
  });

  @override
  List<Object> get props => [
        frequency,
      ];
}

class SaveCalculatorEvent extends BudgetPlannerEvent {}
