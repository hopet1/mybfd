import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/NetWorthItemModel.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ToolsService.dart';
import 'package:MyBFD/services/WidgetService.dart';
import 'package:MyBFD/widgets/CustomContentBox.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/NetWorthItemWidget.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:money2/money2.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:uuid/uuid.dart';

part 'budget_planner_event.dart';
part 'budget_planner_page.dart';
part 'budget_planner_state.dart';

abstract class BudgetPlannerBlocDelegate {
  void showMessage({@required String message});
}

class BudgetPlannerBloc extends Bloc<BudgetPlannerEvent, BudgetPlannerState> {
  BudgetPlannerBloc() : super(InitialState());

  BudgetPlannerBlocDelegate _budgetPlannerBlocDelegate;

  void setDelegate({@required BudgetPlannerBlocDelegate delegate}) {
    this._budgetPlannerBlocDelegate = delegate;
  }

  int _currentStep = 0;
  UserModel _currentUser;

  String _summaryFrequency = BUDGET_FREQUENCY_LIST[1];

  double _incomeTotal = 0;

  List<NetWorthItemModel> _incomeItems = [
    //Pay (after tax)
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Income from self-employment
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Childcare
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Jobseeker's Allowance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Income Support
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Working Tax Credit
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Child Tax Credit
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Child Benefit
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Employment and Support Allowance (or Incapacity Benefit)
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Universal Credit
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Disability Living Allowance (or Personal Independence Payment)
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Attendance Allowance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Carer's Allowance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Housing Benefit
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Maternity/Paternity Allowance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //State Pension
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Employer Pension
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Private Pension or Annuity
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Income From Savings & Investments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Child Maintenance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Student Loans& Grants
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additonal Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _householdTotal = 0;

  List<NetWorthItemModel> _householdItems = [
    //Mortage
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Rent
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Mortage Life Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Mortage Payment Protection Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Ground Rent
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Service Charge
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Council Tax
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Gas
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Electricity
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Water
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Home Phone
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Mobile Phone
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Internet/Broadband/TV
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //TV Licence
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Building Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Contents Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additional Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _livingCostsTotal = 0;

  List<NetWorthItemModel> _livingCostsItems = [
    //Grocery Shopping
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Takeaway Food
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Alcohol
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Cigarettes & Tobacco
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Hairdressing
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Beauty Treatments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Toiletries
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Eye Care
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Dental Care
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Prescriptions & Medicines
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Clothes
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Shoes
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Laundry & Dry Cleaning
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Food & Coffee
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Professional Fees
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Work Clothes
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additional Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _financeInsuranceTotal = 0;

  List<NetWorthItemModel> _financeInsuranceItems = [
    //Overdraft Charges and Interest
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Bank Fees
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Regular Saving
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //ISAs Saving
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Buying Shares
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Private Pension Contributions
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Loan Repayments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Student Loan Repayments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Credit Card Repayments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Hire Purchase & Catalogue Repayments
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Life Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Income Protection Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Critical Illness Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Payment Protection Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Health Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Dental Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additional Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _childrenTotal = 0;

  List<NetWorthItemModel> _childrenItems = [
    //Childcare
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Children's Clothes & Shoes
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //School Uniform
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Nappies and Baby Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Activities & Clubs
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Toys & Treats
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Pocket Money
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Maintenance or Child Support
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //School Fees
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //School Trips
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //School Dinnerss
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //After-school Clubs
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additional Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _travelTotal = 0;

  List<NetWorthItemModel> _travelItems = [
    //Fuel
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Car Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Breakdown Cover
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Car Tax
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Car Finance or Loan Repayment
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Loan Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //MOT
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Maintenance & Repairs
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Parking & Tools
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Bus, Tube & Tram Fares
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Trains
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Taxes
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Flights
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additional Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  double _otherTotal = 0;

  List<NetWorthItemModel> _otherItems = [
    //Cinema & Theatre Trips
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Days Out
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Books, Music, Films, Games, etc.
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Hobbies
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Eating Out
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Going Out For Drinks
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Sport & Gym
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Newspapers & Magazines
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Birthdays
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Christmas
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Other Festivals & Celebrations
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Weddings
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Travel Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Spending Money
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Other Holiday Costs
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Food
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Vet Bills
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Pet Insurance
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
    //Additonal Items
    NetWorthItemModel(
      amount: 0,
      name: BUDGET_FREQUENCY_LIST[1],
    ),
  ];

  static const String INCOME_ITEMS_NAME = 'income';
  static const String HOUSEHOLD_ITEMS_NAME = 'household';
  static const String LIVINGCOSTS_ITEMS_NAME = 'livingcosts';
  static const String FINANCEINSURANCE_ITEMS_NAME = 'financeinsurance';
  static const String CHILDREN_ITEMS_NAME = 'children';
  static const String TRAVEL_ITEMS_NAME = 'travel';
  static const String OTHER_ITEMS_NAME = 'other';

  @override
  Stream<BudgetPlannerState> mapEventToState(
    BudgetPlannerEvent event,
  ) async* {
    yield LoadingState();

    if (event is BudgetPlannerLoadPageEvent) {
      try {
        _currentUser = await locator<AuthService>().getCurrentUser();

        //Load income items.
        List<NetWorthItemModel> incomeItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: INCOME_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _incomeItems = incomeItems.isEmpty ? _incomeItems : incomeItems;

        //Load household items.
        List<NetWorthItemModel> householdItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: HOUSEHOLD_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _householdItems =
            householdItems.isEmpty ? _householdItems : householdItems;

        //Load living costs items.
        List<NetWorthItemModel> livingCostsItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: LIVINGCOSTS_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _livingCostsItems =
            livingCostsItems.isEmpty ? _livingCostsItems : livingCostsItems;

        //Load financial insurance items.
        List<NetWorthItemModel> financeInsuranceItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: FINANCEINSURANCE_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _financeInsuranceItems = financeInsuranceItems.isEmpty
            ? _financeInsuranceItems
            : financeInsuranceItems;

        //Load children items.
        List<NetWorthItemModel> childrenItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: CHILDREN_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _childrenItems = childrenItems.isEmpty ? _childrenItems : childrenItems;

        //Load travel items.
        List<NetWorthItemModel> travelItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: TRAVEL_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _travelItems = travelItems.isEmpty ? _travelItems : travelItems;

        //Load other items.
        List<NetWorthItemModel> otherItems =
            await locator<ToolsService>().listNetWorthHolders(
          collection: OTHER_ITEMS_NAME,
          uid: _currentUser.uid,
        );
        _otherItems = otherItems.isEmpty ? _otherItems : otherItems;

        add(
          GoToStepEvent(step: _currentStep),
        );
      } catch (error) {
        yield ErrorState(error: error);
      }
    }

    if (event is NextStepEvent) {
      _currentStep++;
      add(
        GoToStepEvent(step: _currentStep),
      );
    }

    if (event is PreviousStepEvent) {
      _currentStep--;
      add(
        GoToStepEvent(step: _currentStep),
      );
    }

    if (event is GoToStepEvent) {
      final int step = event.step;

      _currentStep = step;

      switch (step) {
        case 0:
          _incomeTotal = _getTotalForBudget(netWorthItems: _incomeItems);

          yield IncomeState(
            incomeItems: _incomeItems,
            incomeTotal: _incomeTotal,
          );

          break;
        case 1:
          _householdTotal = _getTotalForBudget(netWorthItems: _householdItems);

          yield HouseholdState(
            householdItems: _householdItems,
            householdTotal: _householdTotal,
          );
          break;
        case 2:
          _livingCostsTotal =
              _getTotalForBudget(netWorthItems: _livingCostsItems);

          yield LivingCostsState(
            livingCostsItems: _livingCostsItems,
            livingCostsTotal: _livingCostsTotal,
          );
          break;
        case 3:
          _financeInsuranceTotal =
              _getTotalForBudget(netWorthItems: _financeInsuranceItems);

          yield FinanceInsuranceState(
              financeInsuranceItems: _financeInsuranceItems,
              financeInsuranceTotal: _financeInsuranceTotal);
          break;
        case 4:
          _childrenTotal = _getTotalForBudget(netWorthItems: _childrenItems);

          yield ChildrenState(
              childrenItems: _childrenItems, childrenTotal: _childrenTotal);
          break;
        case 5:
          _travelTotal = _getTotalForBudget(netWorthItems: _travelItems);

          yield TravelState(
              travelItems: _travelItems, travelTotal: _travelTotal);
          break;
        case 6:
          _otherTotal = _getTotalForBudget(netWorthItems: _otherItems);

          yield OtherState(otherItems: _otherItems, otherTotal: _otherTotal);
          break;
        case 7:
          switch (_summaryFrequency) {
            case 'per week':
              yield SummaryState(
                frequency: _summaryFrequency,
                incomeTotal: _incomeTotal * 4,
                householdTotal: _householdTotal * 4,
                livingCostsTotal: _livingCostsTotal * 4,
                financeInsuranceTotal: _financeInsuranceTotal * 4,
                childrenTotal: _childrenTotal * 4,
                travelTotal: _travelTotal * 4,
                otherTotal: _otherTotal * 4,
              );
              break;
            case 'per month':
              yield SummaryState(
                frequency: _summaryFrequency,
                incomeTotal: _incomeTotal,
                householdTotal: _householdTotal,
                livingCostsTotal: _livingCostsTotal,
                financeInsuranceTotal: _financeInsuranceTotal,
                childrenTotal: _childrenTotal,
                travelTotal: _travelTotal,
                otherTotal: _otherTotal,
              );
              break;
            case 'per year':
              yield SummaryState(
                frequency: _summaryFrequency,
                incomeTotal: _incomeTotal / 12,
                householdTotal: _householdTotal / 12,
                livingCostsTotal: _livingCostsTotal / 12,
                financeInsuranceTotal: _financeInsuranceTotal / 12,
                childrenTotal: _childrenTotal / 12,
                travelTotal: _travelTotal / 12,
                otherTotal: _otherTotal / 12,
              );
              break;
            default:
              yield SummaryState(
                frequency: 'per month',
                incomeTotal: _incomeTotal,
                householdTotal: _householdTotal,
                livingCostsTotal: _livingCostsTotal,
                financeInsuranceTotal: _financeInsuranceTotal,
                childrenTotal: _childrenTotal,
                travelTotal: _travelTotal,
                otherTotal: _otherTotal,
              );
              break;
          }
          break;
      }
    }

    if (event is UpdateNetWorthItemEvent) {
      add(
        GoToStepEvent(step: _currentStep),
      );
    }

    if (event is UpdateSummaryFrequencyEvent) {
      _summaryFrequency = event.frequency;

      add(
        GoToStepEvent(step: _currentStep),
      );
    }

    if (event is SaveCalculatorEvent) {
      try {
        await locator<ToolsService>().saveNetWorthHolders(
          collection: INCOME_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _incomeItems,
        );

        await locator<ToolsService>().saveNetWorthHolders(
          collection: HOUSEHOLD_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _householdItems,
        );

        await locator<ToolsService>().saveNetWorthHolders(
          collection: LIVINGCOSTS_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _livingCostsItems,
        );

        await locator<ToolsService>().saveNetWorthHolders(
          collection: FINANCEINSURANCE_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _financeInsuranceItems,
        );

        await locator<ToolsService>().saveNetWorthHolders(
          collection: CHILDREN_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _childrenItems,
        );

        await locator<ToolsService>().saveNetWorthHolders(
          collection: TRAVEL_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _travelItems,
        );
        await locator<ToolsService>().saveNetWorthHolders(
          collection: OTHER_ITEMS_NAME,
          uid: _currentUser.uid,
          items: _otherItems,
        );

        _budgetPlannerBlocDelegate.showMessage(message: 'Data saved.');

        add(GoToStepEvent(step: _currentStep));
      } catch (error) {
        yield ErrorState(error: error);
      }
    }
  }

  double _getTotalForBudget({@required List<NetWorthItemModel> netWorthItems}) {
    double total = 0;

    netWorthItems.forEach(
      (netWorthItem) {
        switch (netWorthItem.name) {
          case 'per week':
            total += (netWorthItem.amount * 4);
            break;
          case 'per month':
            total += (netWorthItem.amount * 1);
            break;
          case 'per year':
            total += (netWorthItem.amount / 12);
            break;
          default:
            total += (netWorthItem.amount * 1);
            break;
        }
      },
    );

    return total;
  }
}
