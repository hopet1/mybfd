part of 'helpcenter_bloc.dart';

abstract class HelpcenterState extends Equatable {
  const HelpcenterState();

  @override
  List<Object> get props => [];
}

class HelpCenterLoading extends HelpcenterState {}

class HelpcenterInitial extends HelpcenterState {}

class HelpCenterLoaded extends HelpcenterState {
  final List<HelpModel> helpItems;
  final int level;

  const HelpCenterLoaded({
    @required this.helpItems,
    @required this.level,
  });

  @override
  List<Object> get props => [
        helpItems,
        level,
      ];
}
