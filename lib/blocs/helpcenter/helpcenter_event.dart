part of 'helpcenter_bloc.dart';

abstract class HelpcenterEvent extends Equatable {
  const HelpcenterEvent();

  @override
  List<Object> get props => [];
}

class LoadHelpEvent extends HelpcenterEvent {}

class HelpTopicChooseEvent extends HelpcenterEvent {
  final String id;
  final int level;
  final String parentID;

  HelpTopicChooseEvent({
    @required this.id,
    @required this.level,
    this.parentID,
  });

  @override
  List<Object> get props => [
        id,
        level,
        parentID,
      ];
}
