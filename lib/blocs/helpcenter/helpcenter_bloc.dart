import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/HelpModel.dart';
import 'package:MyBFD/services/HelpService.dart';
import 'package:MyBFD/widgets/HelpWidget.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../service_locator.dart';

part 'helpcenter_event.dart';
part 'helpcenter_page.dart';
part 'helpcenter_state.dart';

class HelpcenterBloc extends Bloc<HelpcenterEvent, HelpcenterState> {
  HelpcenterBloc() : super(HelpcenterInitial());

  @override
  Stream<HelpcenterState> mapEventToState(
    HelpcenterEvent event,
  ) async* {
    yield HelpCenterLoading();

    if (event is LoadHelpEvent) {
      try {
        List<HelpModel> _helpItems;
        _helpItems = await locator<HelpService>().listHelp();
        yield HelpCenterLoaded(helpItems: _helpItems, level: 0);
      } catch (e) {
        throw Exception(
          e.toString(),
        );
      }
    }

    if (event is HelpTopicChooseEvent) {
      yield HelpCenterLoading();
      try {
        List<HelpModel> _helpItems;
        _helpItems = await locator<HelpService>()
            .getHelpChildren(event.id, event.level, event.parentID);
        yield HelpCenterLoaded(helpItems: _helpItems, level: event.level);
      } catch (e) {
        throw Exception(e.toString());
      }
    }
  }
}
