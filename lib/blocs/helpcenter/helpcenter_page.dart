part of 'helpcenter_bloc.dart';

class HelpCenterPage extends StatefulWidget {
  @override
  State createState() => _HelpCenterPageState();
}

class _HelpCenterPageState extends State<HelpCenterPage> {
  int _level;
  String parent;

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: BlocConsumer<HelpcenterBloc, HelpcenterState>(
        builder: (context, state) {
          if (state is HelpCenterLoading) {
            return Column(
              children: [
                SizedBox(
                  height: 60,
                ),
                Topbar(),
                SizedBox(height: 42.0),
                Text('Help Center',
                    style: Theme.of(context).textTheme.headline1),
                SizedBox(height: 64.0),
                Center(child: Spinner()),
              ],
            );
          }

          if (state is HelpCenterLoaded) {
            final List<HelpModel> _helpModels = state.helpItems;
            List<HelpWidget> _helpItems = [];
            _level = state.level;

            _helpModels.forEach((element) {
              parent = element.parent;
              _helpItems.add(
                HelpWidget(
                  title: element.title,
                  content: element.content,
                  hasChildren: element.hasChildren,
                  screenWidth: _screenWidth,
                  tap: (element.hasChildren)
                      ? () => context.read<HelpcenterBloc>().add(
                            HelpTopicChooseEvent(
                              id: element.id,
                              level: _level + 1,
                              parentID: parent,
                            ),
                          )
                      : () {},
                ),
              );
            });

            return Column(
              children: [
                SizedBox(height: 20),
                Topbar(),
                SizedBox(height: 15.0),
                Text(
                  'Help Center',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: Responsive.isMobile(context) ? 20 : 22),
                ),
                SizedBox(height: 16.0),
                Column(children: _helpItems)
              ],
            );
          }

          return Container(
            height: screenHeight,
            width: screenWidth,
            color: Colors.red,
          );
        },
        listener: (context, state) {
          if (state is HelpcenterInitial) {
            print('Initial');
          }
          if (state is HelpCenterLoaded) {
            print('Loaded');
          }
        },
      ),
    );
  }
}
