import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/pages/onboarding/OBRewardsPage.dart';
import 'package:MyBFD/widgets/OnboardingPoint.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../constants.dart';

class OBFinancesPage extends StatefulWidget {
  @override
  State createState() => OBFinancesPageState();
}

class OBFinancesPageState extends State<OBFinancesPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: scrHeight(context),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 90,
              ),
              Center(
                child: Image.asset(
                  ASSET_COURSES_ORANGE,
                  height: Responsive.isMobile(context) ? 50 : 100,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Learn To Manage Your Finances',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? 20
                          : Responsive.isTablet(context)
                              ? 22
                              : 24,
                    ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'You\'ll receive easy to follow lessons on how to save, budget, and manage your finances.',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: Responsive.isMobile(context) ? 14 : 16),
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'You\'ll learn:',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: Responsive.isMobile(context) ? 16 : 18),
                ),
              ),
              Spacer(),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text:
                      'How to save, budget, improve your credit score and much more'),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text:
                      'Take control of your money - save, invest and improve numeracy'),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text:
                      'Reduce debt, manage credit and deal with financial difficulty'),
              Spacer(),
              RoundedButton(
                background: colorOrange,
                foreground: Colors.white,
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => OBRewardsPage(),
                  );
                  Navigator.push(context, route);
                },
                text: 'Next',
              ),
              Spacer(),
              ClipPath(
                clipper: WaveClipperTwo(flip: false, reverse: true),
                child: Container(
                  height: 100,
                  color: colorOrange,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
