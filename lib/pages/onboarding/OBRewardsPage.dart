import 'package:MyBFD/blocs/onboarding/onboarding_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/widgets/OnboardingPoint.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../constants.dart';

class OBRewardsPage extends StatefulWidget {
  @override
  State createState() => OBRewardsPageState();
}

class OBRewardsPageState extends State<OBRewardsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: scrHeight(context),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 90,
              ),
              Center(
                child: Image.asset(
                  ASSET_ICON_BADGE,
                  height: Responsive.isMobile(context) ? 50 : 100,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Earn Rewards And Vouchers',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? 20
                          : Responsive.isTablet(context)
                              ? 22
                              : 24,
                    ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'Be progressing through the courses you will earn rewards, coupons, and vouchers for you success.',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: Responsive.isMobile(context) ? 14 : 16),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'You\'ll receive:',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: Responsive.isMobile(context) ? 16 : 18),
                ),
              ),
              Spacer(),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text: 'In-app rewards for course progression'),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text: 'Coupon codes & vouchers for top brands'),
              OnboardingPoint(
                  assetPath: ASSET_ICON_CHECK,
                  text: 'Certificates for completion of courses'),
              Spacer(),
              RoundedButton(
                background: colorOrange,
                foreground: Colors.white,
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => BlocProvider(
                      create: (BuildContext context) => OnboardingBloc()
                        ..add(
                          LoadOnboardingEvent(),
                        ),
                      child: OnboardingPage(),
                    ),
                  );
                  Navigator.push(context, route);
                },
                text: 'Next',
              ),
              Spacer(),
              ClipPath(
                clipper: WaveClipperTwo(flip: false, reverse: true),
                child: Container(
                  height: 100,
                  color: colorOrange,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
