import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/widgets/OnboardingPoint.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../constants.dart';
import 'OBFinancesPage.dart';

class OBGoalsPage extends StatefulWidget {
  @override
  State createState() => OBGoalsPageState();
}

class OBGoalsPageState extends State<OBGoalsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: scrHeight(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 90,
            ),
            Center(
              child: Image.asset(ASSET_GOALS_ORANGE,
                  height: Responsive.isMobile(context) ? 50 : 100),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Setting Up Your Goals',
              style: Theme.of(context).textTheme.headline2.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? 20
                        : Responsive.isTablet(context)
                            ? 22
                            : 24,
                  ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'Once you\'ve finished these steps, you\'ll be able to set up your financial goals.',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(fontSize: Responsive.isMobile(context) ? 14 : 16),
              ),
            ),
            SizedBox(
              height: 28,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'You\'ll be able to:',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(fontSize: Responsive.isMobile(context) ? 16 : 18),
              ),
            ),
            Spacer(),
            OnboardingPoint(
                assetPath: ASSET_ICON_CHECK,
                text: 'Set your financial goals and tasks'),
            OnboardingPoint(
                assetPath: ASSET_ICON_CHECK, text: 'Track your progress'),
            OnboardingPoint(
                assetPath: ASSET_ICON_CHECK,
                text: 'Achieve your financial plans'),
            Spacer(),
            RoundedButton(
              background: colorOrange,
              foreground: Colors.white,
              onTap: () {
                Route route = MaterialPageRoute(
                    builder: (BuildContext context) => OBFinancesPage());
                Navigator.push(context, route);
              },
              text: 'Next',
            ),
            Spacer(),
            ClipPath(
              clipper: WaveClipperTwo(flip: false, reverse: true),
              child: Container(
                height: 100,
                color: colorOrange,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
