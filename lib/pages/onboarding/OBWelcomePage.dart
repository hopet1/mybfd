import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/pages/onboarding/OBGoalsPage.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/widgets/OnboardingPoint.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../constants.dart';
import '../../service_locator.dart';

class OBWelcomePage extends StatefulWidget {
  @override
  State createState() => OBWelcomePageState();
}

class OBWelcomePageState extends State<OBWelcomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<UserModel>(
      future: locator<AuthService>().getCurrentUser(), // async work
      builder: (BuildContext context, AsyncSnapshot<UserModel> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Spinner();
          default:
            if (snapshot.hasError)
              return Text('Error: ${snapshot.error}');
            else {
              final UserModel user = snapshot.data;
              return SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  height: scrHeight(context),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 60,
                      ),
                      Center(
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: Responsive.isMobile(context)
                              ? 50
                              : Responsive.isTablet(context)
                                  ? 70
                                  : 100,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Text(
                          'Welcome',
                          style: Theme.of(context).textTheme.headline1.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? 24
                                  : Responsive.isTablet(context)
                                      ? 26
                                      : 30),
                        ),
                      ),
                      Center(
                        child: Text(
                          user.username,
                          style: Theme.of(context).textTheme.headline2.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? 20
                                    : Responsive.isTablet(context)
                                        ? 22
                                        : 24,
                              ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            'As this is your first time signing in to this app, we want to let you know what it\'s all about.',
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? 16
                                          : Responsive.isTablet(context)
                                              ? 18
                                              : 22,
                                    ),
                          ),
                        ),
                      ),
                      Spacer(),
                      OnboardingPoint(
                          assetPath: ASSET_GOALS_ORANGE,
                          text: 'Set Up Your Goals'),
                      OnboardingPoint(
                          assetPath: ASSET_COURSES_ORANGE,
                          text: 'Learn To Manage Finances'),
                      OnboardingPoint(
                          assetPath: ASSET_ICON_BADGE,
                          text: 'Earn Rewards & Vouchers'),
                      Spacer(),
                      RoundedButton(
                        background: colorOrange,
                        foreground: Colors.white,
                        onTap: () {
                          Route route = MaterialPageRoute(
                              builder: (BuildContext context) => OBGoalsPage());
                          Navigator.push(context, route);
                        },
                        text: 'Next',
                      ),
                      Spacer(),
                      ClipPath(
                        clipper: WaveClipperTwo(flip: false, reverse: true),
                        child: Container(
                          height: 100,
                          color: colorOrange,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
        }
      },
    ));
  }
}
