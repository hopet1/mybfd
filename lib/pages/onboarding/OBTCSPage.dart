import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/PrefModel.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../../constants.dart';
import '../LandingPage.dart';

class OBTCSPage extends StatefulWidget {
  @override
  State createState() => OBTCSPageState();
}

class OBTCSPageState extends State<OBTCSPage> {
  bool _termsServicesChecked = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: scrHeight(context),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 90,
              ),
              Center(
                child: Image.asset(
                  ASSET_ICON_BADGE,
                  height: Responsive.isMobile(context)
                      ? 50
                      : Responsive.isTablet(context)
                          ? 70
                          : 100,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text('Let\'s Get Started',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1),
              SizedBox(
                height: 20,
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Now you know what this app includes, let\'s get your goals set up and start learning.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? 16
                              : Responsive.isTablet(context)
                                  ? 18
                                  : 20,
                        ),
                  )),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: Responsive.isMobile(context)
                        ? screenWidth * 0.10
                        : screenWidth * 0.20),
                width: screenWidth,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Terms & Conditions',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: Responsive.isMobile(context) ? 18 : 24,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Your access to and use of MyBFD (the app) is subject exclusively to these Terms and Conditions. You will not use the app for any purpose that is unlawful or prohibited by these Terms and Conditions. By using the app you are fully accepting the terms, conditions and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the app.',
                        style: TextStyle(
                            fontSize: Responsive.isMobile(context) ? 14 : 16),
                      ),
                      CheckboxListTile(
                        checkColor: Colors.white,
                        title: Text(
                          'I agree to the T&Cs',
                          style: TextStyle(
                              color: colorOrange,
                              fontSize: Responsive.isMobile(context) ? 14 : 16),
                        ),
                        value: _termsServicesChecked,
                        onChanged: (newValue) {
                          setState(() {
                            _termsServicesChecked = newValue;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Spacer(),
              _termsServicesChecked
                  ? RoundedButton(
                      background: colorOrange,
                      foreground: Colors.white,
                      onTap: () async {
                        final PrefModel prefs = await setPrefs();
                        prefs.addData({"onBoardingComplete": true});
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => LandingPage(),
                        );
                        Navigator.push(context, route);
                      },
                      text: 'Get Started',
                    )
                  : SizedBox.shrink(),
              Spacer(),
              ClipPath(
                clipper: WaveClipperTwo(flip: false, reverse: true),
                child: Container(
                  height: 100,
                  color: colorOrange,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
