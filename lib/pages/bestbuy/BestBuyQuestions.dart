import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/pages/bestbuy/BestBuyResult.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:flutter/material.dart';

class BestBuyQuestions extends StatefulWidget {
  @override
  _BestBuyQuestionsState createState() => _BestBuyQuestionsState();
}

class _BestBuyQuestionsState extends State<BestBuyQuestions> {
  int question = 1;
  //int score = 0;
  int _radio1 = -1;
  int _radio2 = -1;
  int _radio3 = -1;
  List<String> result = ["Incorrect", "Incorrect", "Incorrect"];
  List<int> score = [0, 0, 0];
  double width;
  bool nextVisible = true;
  bool prevVisible = false;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: Responsive.isMobile(context)
              ? MediaQuery.of(context).size.height
              : 700,
          child: ListView(
            children: [
              SizedBox(
                height: 60,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: Image.asset(
                        ASSET_BACK_ARROW,
                        height: Responsive.isMobile(context) ? 30 : 50,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Center(
                      child: Image.asset(
                        ASSET_ICON_TRANSPARENT,
                        height: Responsive.isMobile(context) ? 70 : 100,
                      ),
                    ),
                    Image.asset(
                      ASSET_BACK_ARROW,
                      height: Responsive.isMobile(context) ? 30 : 50,
                      color: Colors.transparent,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 48.0),
              Center(
                child: Text(
                  'Activity',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: Responsive.isMobile(context) ? 18 : 20,
                    color: colorOrange,
                  ),
                ),
              ),
              SizedBox(height: 16.0),
              Center(
                child: Text(
                  'Best Buy Deals',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: Responsive.isMobile(context) ? 24 : 26),
                ),
              ),
              SizedBox(height: 24.0),
              Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Responsive.isMobile(context) ? 60.0 : 80),
                  child: Text(
                    BEST_BUY,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: colorDark,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 42.0),
                  child: getWidget(question, width),
                ),
              ),
              Center(
                child: Visibility(
                  visible: prevVisible,
                  child: RoundedButton(
                    background: Colors.white,
                    text: 'Previous Question',
                    foreground: colorOrange,
                    onTap: () {
                      setState(() {
                        question = question - 1;
                        if (question == 1) {
                          prevVisible = false;
                        }
                        nextVisible = true;
                      });
                    },
                  ),
                ),
              ),
              Center(
                child: Visibility(
                  visible: nextVisible,
                  child: RoundedButton(
                    background: Colors.white,
                    text: 'Next Question',
                    foreground: colorOrange,
                    onTap: () {
                      setState(() {
                        question = question + 1;
                        if (question == 3) {
                          nextVisible = false;
                        }
                        prevVisible = true;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _handleValueChangeOne(int val) {
    setState(() {
      _radio1 = val;
    });

    switch (_radio1) {
      case 0:
        score.removeAt(0);
        score.insert(0, 0);
        result.removeAt(0);
        result.insert(0, "Incorrect");
        break;
      case 1:
        score.removeAt(0);
        score.insert(0, 0);
        result.removeAt(0);
        result.insert(0, "Incorrect");
        break;
      case 2:
        score.removeAt(0);
        score.insert(0, 1);
        result.removeAt(0);
        result.insert(0, "Correct");
        break;
      default:
    }
  }

  void _handleValueChangeTwo(int val) {
    setState(() {
      _radio2 = val;
    });

    switch (_radio2) {
      case 0:
        score.removeAt(1);
        score.insert(1, 0);
        result.removeAt(1);
        result.insert(1, "Incorrect");
        break;
      case 1:
        score.removeAt(1);
        score.insert(1, 1);
        result.removeAt(1);
        result.insert(1, "Correct");
        break;
      default:
    }
  }

  void _handleValueChangeThree(int val) {
    setState(() {
      _radio3 = val;
    });

    switch (_radio3) {
      case 0:
        score.removeAt(2);
        score.insert(2, 0);
        result.removeAt(2);
        result.insert(2, "Incorrect");
        break;
      case 1:
        score.removeAt(2);
        score.insert(2, 1);
        result.removeAt(2);
        result.insert(2, "Correct");
        break;
      default:
    }
  }

  Widget getWidget(int question, double width) {
    if (question == 1) {
      return Container(
        width: width * 0.8,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Question 1',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: Responsive.isMobile(context) ? 18 : 20,
                color: colorDark,
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Which shop offers better value for money?',
              style: TextStyle(
                fontSize: Responsive.isMobile(context) ? 16 : 18,
                color: colorDark,
              ),
            ),
            SizedBox(height: 24.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 0,
                  groupValue: _radio1,
                  activeColor: colorOrange,
                  onChanged: _handleValueChangeOne,
                ),
                Text(
                  'Shop A: Offering 5 for £3.20',
                  style: TextStyle(
                    fontSize: Responsive.isMobile(context) ? 16 : 18,
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 1,
                  groupValue: _radio1,
                  activeColor: colorOrange,
                  onChanged: _handleValueChangeOne,
                ),
                Text(
                  'Shop B: Offering 7 for £4.10',
                  style: TextStyle(
                    fontSize: Responsive.isMobile(context) ? 16 : 18,
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 2,
                  groupValue: _radio1,
                  activeColor: colorOrange,
                  onChanged: _handleValueChangeOne,
                ),
                Text(
                  'Shop C: Offering 3 for £0.95',
                  style: TextStyle(
                    fontSize: Responsive.isMobile(context) ? 16 : 18,
                  ),
                )
              ],
            ),
            SizedBox(height: 48.0),
          ],
        ),
      );
    } else if (question == 2) {
      return Container(
        width: width * 0.8,
        child: Column(
          children: [
            Text(
              'Question 2',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: Responsive.isMobile(context) ? 18 : 20,
                color: colorDark,
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Which shop offers better value for money?',
              style: TextStyle(
                fontSize: Responsive.isMobile(context) ? 16 : 18,
                color: colorDark,
              ),
            ),
            SizedBox(height: 24.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Radio(
                    value: 0,
                    groupValue: _radio2,
                    activeColor: colorOrange,
                    onChanged: _handleValueChangeTwo,
                  ),
                  Flexible(
                    child: Text(
                      'Apples cost £8.50 for a 6kg bag in Tesco.',
                      style: TextStyle(
                        fontSize: Responsive.isMobile(context) ? 16 : 18,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: 1,
                  groupValue: _radio2,
                  activeColor: colorOrange,
                  onChanged: _handleValueChangeTwo,
                ),
                Flexible(
                  child: Text(
                    'The same type of potatoes cost £2.47 for a 2.5kg bag in the local farm shop.',
                    style: TextStyle(
                      fontSize: Responsive.isMobile(context) ? 16 : 18,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 48.0),
          ],
        ),
      );
    } else if (question == 3) {
      return Column(
        children: [
          Text(
            'Question 3',
            style: TextStyle(
              fontWeight: FontWeight.w800,
              fontSize: Responsive.isMobile(context) ? 18 : 20,
              color: colorDark,
            ),
          ),
          SizedBox(height: 16.0),
          Text(
            'The coat you want to buy is on offer at two different shops. Work out the difference in cost between the coats at the two shops.',
            style: TextStyle(
              fontSize: Responsive.isMobile(context) ? 16 : 18,
              color: colorDark,
            ),
          ),
          SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Radio(
                value: 0,
                groupValue: _radio3,
                activeColor: colorOrange,
                onChanged: _handleValueChangeThree,
              ),
              Text(
                'ASOS 15% off usual price of £150',
                style: TextStyle(
                  fontSize: Responsive.isMobile(context) ? 16 : 18,
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Radio(
                value: 1,
                groupValue: _radio3,
                activeColor: colorOrange,
                onChanged: _handleValueChangeThree,
              ),
              Text(
                'Levi £94 plus VAT at 20%',
                style: TextStyle(
                  fontSize: Responsive.isMobile(context) ? 16 : 18,
                ),
              )
            ],
          ),
          SizedBox(height: 48.0),
          RoundedButton(
            background: colorOrange,
            text: 'Get results',
            foreground: Colors.white,
            onTap: () {
              Navigator.pop(context);
              Route route = MaterialPageRoute(
                  builder: (BuildContext context) => BestBuyResult(
                        score: score,
                        result: result,
                      ));
              Navigator.push(context, route);
            },
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}
