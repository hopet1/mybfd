import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:flutter/material.dart';

import 'BestBuyQuestions.dart';

class BestBuyResult extends StatefulWidget {
  final List<int> score;
  final List<String> result;

  const BestBuyResult({Key key, this.score, this.result}) : super(key: key);

  @override
  _BestBuyResultState createState() => _BestBuyResultState();
}

class _BestBuyResultState extends State<BestBuyResult> {
  int _score;
  List<String> _result;

  @override
  Widget build(BuildContext context) {
    _score = widget.score.fold(0, (p, c) => p + c);
    _result = widget.result;

    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: Responsive.isMobile(context)
              ? MediaQuery.of(context).size.height
              : 700,
          child: ListView(
            children: [
              SizedBox(
                height: 60,
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Image.asset(
                          ASSET_BACK_ARROW,
                          height: Responsive.isMobile(context) ? 30 : 50,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Center(
                        child: Image.asset(
                          ASSET_ICON_TRANSPARENT,
                          height: Responsive.isMobile(context) ? 70 : 100,
                        ),
                      ),
                      Image.asset(
                        ASSET_BACK_ARROW,
                        height: Responsive.isMobile(context) ? 30 : 50,
                        color: Colors.transparent,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 42.0),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Your Score',
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: Responsive.isMobile(context) ? 24 : 26,
                    color: colorDark,
                  ),
                ),
              ),
              SizedBox(height: 18.0),
              Align(
                alignment: Alignment.center,
                child: Text(
                  '$_score/3 correct',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: Responsive.isMobile(context) ? 28 : 30,
                    color: colorDark,
                  ),
                ),
              ),
              SizedBox(height: 48.0),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'Well done!',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: Responsive.isMobile(context) ? 18 : 20,
                    color: colorDark,
                  ),
                ),
              ),
              SizedBox(height: 18.0),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28.0),
                  child: Text(
                    'Feel free to take the activity again and see how you do.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: Responsive.isMobile(context) ? 18 : 20,
                      color: colorDark,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 28.0),
              Align(
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: getResult(),
                ),
              ),
              SizedBox(height: 64.0),
              Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                    Route route = MaterialPageRoute(
                        builder: (BuildContext context) => BestBuyQuestions());
                    Navigator.push(context, route);
                  },
                  child: Container(
                    height: 48.0,
                    width: Responsive.isMobile(context)
                        ? screenWidth * 0.6
                        : screenWidth * 0.4,
                    decoration: BoxDecoration(
                      color: colorOrange,
                      borderRadius: BorderRadius.all(
                        Radius.circular(8),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          offset: Offset(2, 2),
                          blurRadius: 4.0,
                        ),
                      ],
                    ),
                    child: Center(
                      child: Text(
                        "Start Activity Again",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: Responsive.isMobile(context) ? 18 : 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 18.0),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> getResult() {
    List<Widget> output = [];
    int index;
    for (index = 1; index < _result.length + 1; index++) {
      output.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Question $index - ",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
              ),
            ),
            Text(
              _result[index - 1],
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      );
      output.add(SizedBox(height: 10.0));
    }
    return output;
  }
}
