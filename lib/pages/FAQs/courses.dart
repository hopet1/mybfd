import 'package:MyBFD/widgets/Navbar.dart';
import 'package:flutter/material.dart';
import 'package:MyBFD/constants.dart';

class CoursesPage extends StatefulWidget {
  @override
  State createState() => _CoursesPageState();
}

class _CoursesPageState extends State<CoursesPage> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 60,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Image.asset(
                    ASSET_BACK_ARROW,
                    height: 30,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Center(
                  child: Image.asset(
                    ASSET_ICON_TRANSPARENT,
                    height: 70,
                  ),
                ),
                Image.asset(
                  ASSET_BACK_ARROW,
                  height: 30,
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
          SizedBox(height: 42.0),
          Text(
            'Courses & Certificates',
            style: TextStyle(
              fontSize: 24,
              color: colorDark,
            ),
          ),
          SizedBox(height: 64.0),
          GestureDetector(
            onTap: () => print("Implement"),
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Q1",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 16,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () => print("Implement"),
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Q2",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 16,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () => print("Financial Planning"),
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Q3",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 16,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () => print("Help With Bills"),
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Q4",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 16,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 64.0),
          GestureDetector(
            onTap: () => print("Support page route"),
            child: Text(
              "Still need help? Contact us",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 16,
                color: colorDark,
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}
