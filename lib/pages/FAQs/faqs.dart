import 'package:MyBFD/blocs/support/support_bloc.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:flutter/material.dart';
import 'package:MyBFD/constants.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'using.dart';
import 'bills.dart';
import 'courses.dart';
import 'finance.dart';

class FAQPage extends StatefulWidget {
  @override
  State createState() => _FAQPageState();
}

class _FAQPageState extends State<FAQPage> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 60,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Image.asset(
                    ASSET_BACK_ARROW,
                    height: 30,
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Center(
                  child: Image.asset(
                    ASSET_ICON_TRANSPARENT,
                    height: 70,
                  ),
                ),
                Image.asset(
                  ASSET_BACK_ARROW,
                  height: 30,
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
          SizedBox(height: 42.0),
          Text(
            'FAQs',
            style: TextStyle(
              fontSize: 24,
              color: colorDark,
            ),
          ),
          SizedBox(height: 64.0),
          GestureDetector(
            onTap: () {
              Route route = MaterialPageRoute(
                  builder: (BuildContext context) => UsingPage());
              Navigator.push(context, route);
            },
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Using This App",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () {
              Route route = MaterialPageRoute(
                  builder: (BuildContext context) => CoursesPage());
              Navigator.push(context, route);
            },
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Courses & Certificates",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () {
              Route route = MaterialPageRoute(
                  builder: (BuildContext context) => FinancePage());
              Navigator.push(context, route);
            },
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Financial Planning",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 18.0),
          GestureDetector(
            onTap: () {
              Route route = MaterialPageRoute(
                  builder: (BuildContext context) => BillsPage());
              Navigator.push(context, route);
            },
            child: Container(
              height: 56.0,
              width: screenWidth * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(2, 2),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24.0,
                  vertical: 18.0,
                ),
                child: Text(
                  "Help With Bills",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: colorOrange,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 64.0),
          GestureDetector(
            onTap: () async {
              Route route = MaterialPageRoute(
                builder: (BuildContext context) => BlocProvider(
                  create: (BuildContext context) => SupportBloc(),
                  child: SupportPage(),
                ),
              );
              Navigator.push(context, route);
            },
            child: Text(
              "Still need help? Contact us",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 16,
                color: colorDark,
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}
