import 'package:MyBFD/blocs/budget_planner/budget_planner_bloc.dart';
import 'package:MyBFD/blocs/networth/networth_bloc.dart';
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/pages/Activities.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:MyBFD/widgets/ToolWidget.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ToolsPage extends StatefulWidget {
  @override
  State createState() => _ToolsPageState();
}

class _ToolsPageState extends State<ToolsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: scrHeight(context),
          child: Column(
            children: [
              Topbar(),
              Text('Tools',
                  style: Theme.of(context).textTheme.headline1
                    ..copyWith(
                        fontSize: Responsive.isMobile(context) ? 16 : 20)),
              SizedBox(height: 64.0),
              ToolWidget(
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => BlocProvider(
                      create: (BuildContext context) => NetworthBloc()
                        ..add(
                          LoadPageEvent(),
                        ),
                      child: NetworthPage(),
                    ),
                  );
                  Navigator.push(context, route);
                },
                title: "Net Worth Calculator",
              ),
              SizedBox(height: 18.0),
              ToolWidget(
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => BlocProvider(
                      create: (BuildContext context) => BudgetPlannerBloc()
                        ..add(
                          BudgetPlannerLoadPageEvent(),
                        ),
                      child: BudgetPlannerPage(),
                    ),
                  );
                  Navigator.push(context, route);
                },
                title: "Budget Planner",
              ),
              SizedBox(height: 18.0),
              ToolWidget(
                onTap: () {
                  Route route = MaterialPageRoute(
                      builder: (BuildContext context) => ActivitiesPage());
                  Navigator.push(context, route);
                },
                title: "Activities",
              ),
              SizedBox(height: 18.0),
              // ToolWidget(
              //   onTap: () async {
              //     PrefModel model = await locator<PrefService>().getPrefs();
              //     Route route = MaterialPageRoute(
              //         builder: (BuildContext context) => TestingPage());
              //     Navigator.push(context, route);
              //   },
              //   title: "Test for Server Prefs",
              // ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}
