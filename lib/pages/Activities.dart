import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/pages/bestbuy/BestBuyQuestions.dart';
import 'package:MyBFD/widgets/Navbar.dart';
import 'package:flutter/material.dart';

class ActivitiesPage extends StatefulWidget {
  @override
  _ActivitiesPageState createState() => _ActivitiesPageState();
}

class _ActivitiesPageState extends State<ActivitiesPage> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: Responsive.isMobile(context)
              ? MediaQuery.of(context).size.height
              : 700,
          child: Column(
            children: [
              SizedBox(
                height: 60,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      child: Image.asset(
                        ASSET_BACK_ARROW,
                        height: Responsive.isMobile(context) ? 30 : 50,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Center(
                      child: Image.asset(
                        ASSET_ICON_TRANSPARENT,
                        height: Responsive.isMobile(context) ? 70 : 100,
                      ),
                    ),
                    Image.asset(
                      ASSET_BACK_ARROW,
                      height: Responsive.isMobile(context) ? 30 : 50,
                      color: Colors.transparent,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 42.0),
              Text('Activities',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: Responsive.isMobile(context) ? 24 : 26)),
              SizedBox(height: 64.0),
              GestureDetector(
                onTap: () {
                  Route route = MaterialPageRoute(
                      builder: (BuildContext context) => BestBuyQuestions());
                  Navigator.push(context, route);
                },
                child: Container(
                  height: 56.0,
                  width: screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        offset: Offset(2, 2),
                        blurRadius: 4.0,
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 18.0,
                    ),
                    child: Text(
                      "Best Buy Activity",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: Responsive.isMobile(context) ? 18 : 20,
                        color: colorOrange,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 18.0),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Navbar(),
    );
  }
}
