import 'package:MyBFD/constants.dart';
import 'package:MyBFD/blocs/account/account_bloc.dart';
import 'package:MyBFD/blocs/courses/courses_bloc.dart';
import 'package:MyBFD/blocs/goals/goals_bloc.dart';
import 'package:MyBFD/blocs/helpcenter/helpcenter_bloc.dart';
import 'package:MyBFD/blocs/rewards/rewards_bloc.dart';
import 'package:MyBFD/pages/ToolsPage.dart';
import 'package:MyBFD/widgets/LandingTile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LandingWeb extends StatefulWidget {
  final String greeting;

  const LandingWeb({Key key, this.greeting}) : super(key: key);

  @override
  _LandingWebState createState() => _LandingWebState();
}

class _LandingWebState extends State<LandingWeb> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 72),
        Center(
          child: Image.asset(
            ASSET_ICON_TRANSPARENT,
            height: 72,
          ),
        ),
        SizedBox(height: 28),
        Text(
          'You are logged in as',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 24,
            color: colorOrange,
          ),
        ),
        SizedBox(height: 6),
        Text(
          widget.greeting,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 32,
          ),
        ),
        SizedBox(height: scrHeight(context) * 0.1),
        Expanded(
          child: ListView(
            children: [
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //Courses Tile
                    LandingTile(
                      onTap: () {
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => CoursesBloc()
                              ..add(
                                LoadCoursesEvent(),
                              ),
                            child: CoursesPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_COURSES_WHITE,
                      title: "Courses",
                    ),
                    SizedBox(width: 28.0),
                    //Goals Tile
                    LandingTile(
                      onTap: () async {
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => GoalsBloc()
                              ..add(
                                LoadPageEvent(),
                              ),
                            child: GoalsPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_GOALS_WHITE,
                      title: "My Goals",
                    ),
                    SizedBox(width: 28.0),
                    //Rewards Tile
                    LandingTile(
                      onTap: () {
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => RewardsBloc()
                              ..add(
                                LoadRewardsEvent(),
                              ),
                            child: RewardsPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_REWARDS_WHITE,
                      title: "Rewards",
                    ),
                  ],
                ),
              ),
              SizedBox(height: 28),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //Tools Tile
                    LandingTile(
                      onTap: () {
                        Route route = MaterialPageRoute(
                            builder: (BuildContext context) => ToolsPage());
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_TOOLS_ORANGE,
                      title: "Tools",
                    ),
                    SizedBox(width: 28.0),
                    LandingTile(
                      onTap: () {
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => HelpcenterBloc()
                              ..add(
                                LoadHelpEvent(),
                              ),
                            child: HelpCenterPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_HELP_ORANGE,
                      title: "Help Center",
                    ),
                    SizedBox(width: 28.0),
                    //Account Tile
                    LandingTile(
                      onTap: () {
                        Route route = MaterialPageRoute(
                          builder: (BuildContext context) => BlocProvider(
                            create: (BuildContext context) => AccountBloc()
                              ..add(
                                LoadAccountEvent(),
                              ),
                            child: AccountPage(),
                          ),
                        );
                        Navigator.push(context, route);
                      },
                      imagePath: ASSET_ACCOUNTS_WHITE,
                      title: "Account",
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 16),
        Padding(
          child: Text('My BFD App version ${packageInfo.version}'),
          padding: EdgeInsets.only(bottom: 42),
        ),
      ],
    );
    //Two division layout for web
    // return Center(
    //   child: Row(
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     children: [
    //       Container(
    //         width: screenWidth * 0.5,
    //         height: screenHeight,
    //         child: Column(
    //           children: [
    //             SizedBox(height: 64),
    //             Center(
    //               child: Image.asset(
    //                 ASSET_ICON_TRANSPARENT,
    //                 height: 50,
    //               ),
    //             ),
    //             SizedBox(height: 28),
    //             Text(
    //               'You are logged in as',
    //               style: TextStyle(
    //                 fontWeight: FontWeight.w800,
    //                 fontSize: 21,
    //                 color: colorOrange,
    //               ),
    //             ),
    //             SizedBox(height: 6),
    //             Text(
    //               widget.greeting,
    //               style: TextStyle(
    //                 fontWeight: FontWeight.w600,
    //                 fontSize: 21,
    //               ),
    //             ),
    //             SizedBox(height: 28),
    //             Padding(
    //               child: Text('My BFD App version ${packageInfo.version}'),
    //               padding: EdgeInsets.only(bottom: 40),
    //             ),
    //           ],
    //         ),
    //       ),
    //       Container(
    //         width: screenWidth * 0.5,
    //         height: screenHeight,
    //         child: Column(
    //           children: [
    //             Center(
    //               child: Row(
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   //Courses Tile
    //                   LandingTile(
    //                     onTap: () {
    //                       Route route = MaterialPageRoute(
    //                         builder: (BuildContext context) => BlocProvider(
    //                           create: (BuildContext context) => CoursesBloc()
    //                             ..add(
    //                               LoadCoursesEvent(),
    //                             ),
    //                           child: CoursesPage(),
    //                         ),
    //                       );
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_COURSES_WHITE,
    //                     title: "Courses",
    //                   ),
    //                   SizedBox(width: 28.0),
    //                   //Goals Tile
    //                   LandingTile(
    //                     onTap: () async {
    //                       Route route = MaterialPageRoute(
    //                         builder: (BuildContext context) => BlocProvider(
    //                           create: (BuildContext context) => GoalsBloc()
    //                             ..add(
    //                               LoadPageEvent(),
    //                             ),
    //                           child: GoalsPage(),
    //                         ),
    //                       );
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_GOALS_WHITE,
    //                     title: "My Goals",
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             SizedBox(height: 28),
    //             Center(
    //               child: Row(
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   //Rewards Tile
    //                   LandingTile(
    //                     onTap: () {
    //                       Route route = MaterialPageRoute(
    //                         builder: (BuildContext context) => BlocProvider(
    //                           create: (BuildContext context) => RewardsBloc()
    //                             ..add(
    //                               LoadRewardsEvent(),
    //                             ),
    //                           child: RewardsPage(),
    //                         ),
    //                       );
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_REWARDS_WHITE,
    //                     title: "Rewards",
    //                   ),
    //                   SizedBox(width: 28.0),
    //                   //Tools Tile
    //                   LandingTile(
    //                     onTap: () {
    //                       Route route = MaterialPageRoute(
    //                           builder: (BuildContext context) => ToolsPage());
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_TOOLS_ORANGE,
    //                     title: "Tools",
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             SizedBox(height: 28),
    //             Center(
    //               child: Row(
    //                 mainAxisAlignment: MainAxisAlignment.center,
    //                 children: [
    //                   //Help Center Tile
    //                   LandingTile(
    //                     onTap: () {
    //                       Route route = MaterialPageRoute(
    //                         builder: (BuildContext context) => BlocProvider(
    //                           create: (BuildContext context) => HelpcenterBloc()
    //                             ..add(
    //                               LoadHelpEvent(),
    //                             ),
    //                           child: HelpCenterPage(),
    //                         ),
    //                       );
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_HELP_ORANGE,
    //                     title: "Help Center",
    //                   ),
    //                   SizedBox(width: 28.0),
    //                   //Account Tile
    //                   LandingTile(
    //                     onTap: () {
    //                       Route route = MaterialPageRoute(
    //                         builder: (BuildContext context) => BlocProvider(
    //                           create: (BuildContext context) => AccountBloc()
    //                             ..add(
    //                               LoadAccountEvent(),
    //                             ),
    //                           child: AccountPage(),
    //                         ),
    //                       );
    //                       Navigator.push(context, route);
    //                     },
    //                     imagePath: ASSET_ACCOUNTS_WHITE,
    //                     title: "Account",
    //                   ),
    //                 ],
    //               ),
    //             ),
    //           ],
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
