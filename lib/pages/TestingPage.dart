// import 'package:MyBFD/constants.dart';
// import 'package:MyBFD/models/UserModel.dart';
// import 'package:MyBFD/service_locator.dart';
// import 'package:MyBFD/services/csv_services.dart';
// import 'package:MyBFD/widgets/PageStructureWidget.dart';
// import 'package:flutter/material.dart';
//
// class TestingPage extends StatefulWidget {
//   const TestingPage({Key key}) : super(key: key);
//
//   @override
//   _TestingPageState createState() => _TestingPageState();
// }
//
// class _TestingPageState extends State<TestingPage> {
//   List<List<dynamic>> csvData = [];
//   List<dynamic> currentRow = [];
//
//   void iterate() async {
//     for (int i = 0; i < csvData.length; i++) {
//       setState(() {
//         currentRow = csvData[i];
//       });
//       UserModel userModel = UserModel(
//         imgUrl: DUMMY_PROFILE_PHOTO_URL,
//         email: currentRow[3],
//         modified: DateTime.now(),
//         created: DateTime.now(),
//         uid: '', //will be assigned later after user creation,
//         username: currentRow[2],
//         fcmToken: null,
//         city: null,
//         phoneNumber: null,
//         postCode: null,
//         streetAddress: null,
//         firstName: currentRow[0],
//         lastName: currentRow[1],
//       );
//       locator<CSVServices>().createUsersFromCSV(userModel, currentRow[4]);
//       // a delayed timer to show the entities in the UI
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: PageStructureWidget(
//         children: [
//           Text(
//             "Upload users",
//             style: TextStyle(color: colorOrange, fontSize: 24),
//           ),
//           SizedBox(height: 42),
//           Text("Total entries: ${csvData.length}"),
//           SizedBox(height: 16),
//           currentRow.isEmpty
//               ? Container()
//               : Text(
//                   "first Column: ${currentRow[2]} : second Column: ${currentRow[3]}"),
//           SizedBox(height: 16),
//           csvData.isEmpty
//               ? Container()
//               : CsvTable(
//                   csvData: csvData,
//                 ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               button("pick csv", onTap: () async {
//                 var data = await locator<CSVServices>().pickCSVAndExtractData();
//                 if (data == null) {
//                   print("No file selected");
//                 } else {
//                   setState(() {
//                     csvData = data;
//                   });
//                 }
//               }),
//               csvData.isEmpty
//                   ? Container()
//                   : Row(
//                       children: [
//                         button("iterate", onTap: () async {
//                           setState(() {
//                             iterate();
//                           });
//                         }),
//                         button("clear", onTap: () async {
//                           setState(() {
//                             csvData.clear();
//                           });
//                         }),
//                       ],
//                     ),
//             ],
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget button(title, {Function onTap}) {
//     return MaterialButton(
//       onPressed: onTap,
//       child: Container(
//         child: Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//             "$title",
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//         color: colorOrange,
//       ),
//     );
//   }
// }
//
// class CsvTable extends StatelessWidget {
//   List<String> headerNames = [
//     "#",
//     "First Name",
//     "Last Name",
//     "Username",
//     "Email",
//     "Password"
//   ];
//   final List<List<dynamic>> csvData;
//   CsvTable({this.csvData});
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12),
//       child: Column(
//         children: [
//           tableHeadPackage(),
//           csvContent(),
//         ],
//       ),
//     );
//   }
//
//   Widget tableHeadPackage() {
//     return Table(
//       columnWidths: {
//         0: FlexColumnWidth(1),
//         1: FlexColumnWidth(2),
//         2: FlexColumnWidth(2),
//         3: FlexColumnWidth(2),
//         4: FlexColumnWidth(4),
//         5: FlexColumnWidth(2),
//       },
//       children: [
//         TableRow(
//             decoration: BoxDecoration(
//                 color: Colors.black54,
//                 borderRadius: BorderRadius.only(
//                     topRight: Radius.circular(5), topLeft: Radius.circular(5))),
//             children: List.generate(
//               headerNames.length,
//               (index) => Padding(
//                 padding: EdgeInsets.symmetric(
//                   vertical: 20,
//                 ),
//                 child: Center(
//                   child: Text(
//                     headerNames[index],
//                     style: TextStyle(
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold,
//                         fontSize: 10),
//                   ),
//                 ),
//               ),
//             )),
//       ],
//     );
//   }
//
//   Widget csvContent() {
//     return Table(
//       columnWidths: {
//         0: FlexColumnWidth(1),
//         1: FlexColumnWidth(2),
//         2: FlexColumnWidth(2),
//         3: FlexColumnWidth(2),
//         4: FlexColumnWidth(4),
//         5: FlexColumnWidth(2),
//       },
//       children: List.generate(
//           csvData.length,
//           (i) => TableRow(
//               decoration: BoxDecoration(
//                   color: Colors.white,
//                   borderRadius: BorderRadius.only(
//                       bottomRight: Radius.circular(5),
//                       bottomLeft: Radius.circular(5))),
//               children: List.generate(
//                 //columns
//                 csvData[i].length + 1,
//                 (index) {
//                   List<dynamic> d = csvData[i == csvData[i].length ? i - 1 : i];
//                   print(index);
//                   return Padding(
//                     padding: EdgeInsets.symmetric(
//                       vertical: 20,
//                     ),
//                     child: Center(
//                       child: Text(
//                         index == 0 ? "${i + 1}" : d[index - 1],
//                         style: TextStyle(
//                             color: Colors.black54,
//                             fontWeight: FontWeight.normal,
//                             fontSize: 10),
//                       ),
//                     ),
//                   );
//                 },
//               ))),
//     );
//   }
// }
