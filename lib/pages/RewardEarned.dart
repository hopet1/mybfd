import 'dart:async';

import 'package:MyBFD/constants.dart';
import 'package:MyBFD/blocs/rewards/rewards_bloc.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/RewardGenerationService.dart';
import 'package:MyBFD/widgets/RoundedButton.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:animated_widgets/animated_widgets.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RewardEarnedPage extends StatefulWidget {
  @override
  _RewardEarnedPageState createState() => _RewardEarnedPageState();
}

class _RewardEarnedPageState extends State<RewardEarnedPage> {
  ConfettiController _controller;
  Timer _timer;
  bool _shake = false;

  void init() {
    _controller = ConfettiController(duration: const Duration(seconds: 2));
    _controller.play();
    _timer = new Timer(Duration(seconds: 3), () {
      setState(() {
        _shake = true;
      });
    });
    _timer = new Timer(Duration(seconds: 10), () {
      setState(() {
        _shake = false;
      });
    });
  }

  @override
  initState() {
    init();
    locator<RewardGenerationService>().generateCode();
    super.initState();
  }

  @override
  dispose() {
    _timer.cancel();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: ConfettiWidget(
              confettiController: _controller,
              blastDirectionality: BlastDirectionality.explosive,
              shouldLoop: false,
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ],
              numberOfParticles: 10,
              gravity: 0.4,
            ),
          ),
          Topbar(
            backArrowTap: () {
              Navigator.pop(context);
            },
          ),
          Text(
            "Congratulations",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: colorOrange,
              fontSize: 32,
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 6),
          Text(
            name,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2,
          ),
          SizedBox(height: 56),
          Text(
            "You just unlocked\na reward",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline1,
          ),
          SizedBox(height: 32),
          ShakeAnimatedWidget(
            enabled: _shake,
            duration: Duration(milliseconds: 1000),
            shakeAngle: Rotation.deg(z: 5),
            curve: Curves.ease,
            child: Container(
              width: 280,
              height: 100,
              decoration: BoxDecoration(
                color: colorOrange,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: Image.asset(
                ASSET_REWARD_LAYOUT,
                height: 100,
              ),
            ),
          ),
          SizedBox(height: 56),
          Text(
            "Go to the Rewards section to claim it",
            style: Theme.of(context).textTheme.headline3,
          ),
          SizedBox(height: 16),
          RoundedButton(
            text: "Go to Rewards",
            background: Colors.white,
            foreground: colorOrange,
            onTap: () {
              Route route = MaterialPageRoute(
                builder: (BuildContext context) => BlocProvider(
                  create: (BuildContext context) => RewardsBloc()
                    ..add(
                      LoadRewardsEvent(),
                    ),
                  child: RewardsPage(),
                ),
              );
              Navigator.pop(context);
              Navigator.push(context, route);
            },
          ),
        ],
      ),
    );
  }
}
