import 'package:MyBFD/extensions/responsive.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/pages/landing/LandingMobile.dart';
import 'package:MyBFD/pages/landing/LandingWeb.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:flutter/material.dart';

import '../constants.dart';
import '../service_locator.dart';

class LandingPage extends StatefulWidget {
  @override
  State createState() => LandingPageState();
}

class LandingPageState extends State<LandingPage> {
  String greeting = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<UserModel>(
          future: locator<AuthService>().getCurrentUser(), // async work
          builder: (BuildContext context, AsyncSnapshot<UserModel> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Spinner();
              default:
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');
                else {
                  final UserModel user = snapshot.data;
                  String text = user.firstName;
                  if (text == null) {
                    greeting = user.username;
                  } else {
                    fName = user.firstName;
                    name = user.firstName + " " + user.lastName;
                    greeting = name;
                  }

                  if (Responsive.isMobile(context)) {
                    return LandingMobile(greeting: greeting);
                  } else {
                    return LandingWeb(greeting: greeting);
                  }
                }
            }
          }),
    );
  }
}
