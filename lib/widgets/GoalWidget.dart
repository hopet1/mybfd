import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/GoalModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GoalWidget extends StatelessWidget {
  GoalWidget({
    Key key,
    @required this.goal,
    @required this.markAsDone,
    @required this.edit,
  }) : super(key: key);

  final GoalModel goal;
  final Function markAsDone;
  final Function edit;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 4.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(15),
            child: GestureDetector(
              onTap: edit,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Created: ${DateFormat('MMMM dd, yyyy').format(goal.created)}',
                        style: Theme.of(context).textTheme.headline3,
                      )
                    ],
                  ),
                  SizedBox(height: 16),
                  Text(
                    goal.goal,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  SizedBox(height: 12),
                ],
              ),
            ),
          ),
          goal.complete
              ? SizedBox.shrink()
              : Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                            color: colorOrange,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(8),
                            ),
                          ),
                          height: 50,
                          child: Center(
                            child: Text(
                              'Mark As Done',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                        ),
                        onTap: markAsDone,
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(8),
                            ),
                          ),
                          height: 50,
                          child: Center(
                            child: Text(
                              'Edit Goal',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ),
                        ),
                        onTap: edit,
                      ),
                    )
                  ],
                ),
        ],
      ),
    );
  }
}
