import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:money2/money2.dart';
import '../models/NetWorthItemModel.dart';

class NetWorthItemWidget extends StatelessWidget {
  NetWorthItemWidget({
    @required this.netWorthItem,
    @required this.updateSelectedNetWorthItemEvent,
    @required this.netWorthItemOptions,
  });

  final NetWorthItemModel netWorthItem;
  final Function updateSelectedNetWorthItemEvent;
  final List<String> netWorthItemOptions;

  final TextEditingController _amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _amountController.text = Money.from(
      netWorthItem.amount,
      Currency.create(
        'GBP',
        2,
        symbol: '£',
        // pattern: 'S0,000.00',
      ),
    ).toString();

    //This moves the cursor to the end of the text field on each rebuild of the widget.
    _amountController.selection = TextSelection.fromPosition(
        TextPosition(offset: _amountController.text.length));

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              color: Colors.white,
              height: 48,
              width: 120,
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  child: TextFormField(
                    controller: _amountController,
                    inputFormatters: [CurrencyTextInputFormatter()],
                    keyboardType: TextInputType.numberWithOptions(
                      signed: true,
                    ),
                    onChanged: (value) {
                      if (value == '') value = '0';
                      value = value.replaceAll(',', '');
                      final double amount = double.parse(value);
                      netWorthItem.amount = amount;
                    },
                    onEditingComplete: () {
                      updateSelectedNetWorthItemEvent();
                      FocusScope.of(context).unfocus();
                    },
                    maxLines: 1,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              height: 48,
              width: 168,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text('Select Item'),
                  value: netWorthItem.name,
                  underline: SizedBox(),
                  onChanged: (newValue) {
                    netWorthItem.name = newValue;
                    updateSelectedNetWorthItemEvent();
                  },
                  items: netWorthItemOptions.map(
                    (option) {
                      return DropdownMenuItem(
                        child: Text(option),
                        value: option,
                      );
                    },
                  ).toList(),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 4.0),
      ],
    );
  }
}
