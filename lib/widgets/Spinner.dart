import 'package:MyBFD/constants.dart';
import 'package:flutter/material.dart';

class Spinner extends StatelessWidget {
  Spinner({Key key, this.text, this.color}) : super(key: key);
  final Color color; // optional color parameter
  final String text; //Text that's displayed under spinner.

  @override
  Widget build(BuildContext context) {
    Color color = this.color ?? colorOrange;
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(color),
      ),
    );
  }
}
