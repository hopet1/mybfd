import 'package:MyBFD/extensions/responsive.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class Topbar extends StatefulWidget {
  Topbar({this.backArrowTap});

  final Function backArrowTap;

  @override
  _TopbarState createState() => _TopbarState();
}

class _TopbarState extends State<Topbar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 64, 20, 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            child: Image.asset(
              ASSET_BACK_ARROW,
              height: Responsive.isMobile(context) ? 30 : 50,
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          Center(
            child: Image.asset(
              ASSET_ICON_TRANSPARENT,
              height: Responsive.isMobile(context) ? 70 : 100,
            ),
          ),
          Image.asset(
            ASSET_BACK_ARROW,
            height: Responsive.isMobile(context) ? 30 : 50,
            color: Colors.transparent,
          ),
        ],
      ),
    );
  }
}
