import 'package:MyBFD/blocs/account/account_bloc.dart';
import 'package:MyBFD/blocs/courses/courses_bloc.dart';
import 'package:MyBFD/blocs/goals/goals_bloc.dart';
import 'package:MyBFD/blocs/rewards/rewards_bloc.dart';
import 'package:MyBFD/models/GoalModel.dart';
import 'package:MyBFD/models/RewardModel.dart';
import 'package:MyBFD/service_locator.dart';
import 'package:MyBFD/services/GoalService.dart';
import 'package:MyBFD/services/RewardService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../constants.dart';

class Navbar extends StatefulWidget {
  @override
  State createState() => NavbarState();
}

class NavbarState extends State<Navbar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future listGoalsFuture =
        locator<GoalService>().listGoals(uid: prefs?.getString('uid'));
    Future listRewardsFuture = locator<RewardService>().getIncompleteRewards();
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 120,
      color: colorOrange,
      child: FutureBuilder(
        future: Future.wait([
          listGoalsFuture,
          listRewardsFuture,
        ]),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return buildButtons(goalCount: 0, rewardCount: 0);
          } else if (!snapshot.hasData) {
            return buildButtons(goalCount: 0, rewardCount: 0);
          } else {
            final List<GoalModel> goals = snapshot.data[0] as List<GoalModel>;
            final List<RewardModel> rewards =
                snapshot.data[1] as List<RewardModel>;
            return buildButtons(
                goalCount: goals.length, rewardCount: rewards.length);
          }
        },
      ),
    );
  }

  _buildCountBadge({@required int count}) {
    return count == 0
        ? SizedBox.shrink()
        : Positioned(
            top: 0,
            right: 0,
            child: CircleAvatar(
              radius: 10,
              backgroundColor: Colors.black,
              child: Center(
                child: Text(
                  '$count',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          );
  }

  buildButtons({@required int goalCount, @required int rewardCount}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Expanded(
                child: GestureDetector(
              onTap: () async {
                Route route = MaterialPageRoute(
                  builder: (BuildContext context) => BlocProvider(
                    create: (BuildContext context) => GoalsBloc()
                      ..add(
                        LoadPageEvent(),
                      ),
                    child: GoalsPage(),
                  ),
                );
                Navigator.pushReplacement(context, route);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Image.asset(
                        ASSET_GOALS_WHITE,
                        height: 40,
                      ),
                      _buildCountBadge(count: goalCount)
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Goals',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            )),
            Expanded(
                child: GestureDetector(
              onTap: () {
                Route route = MaterialPageRoute(
                  builder: (BuildContext context) => BlocProvider(
                    create: (BuildContext context) => CoursesBloc()
                      ..add(
                        LoadCoursesEvent(),
                      ),
                    child: CoursesPage(),
                  ),
                );
                Navigator.pushReplacement(context, route);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    ASSET_COURSES_WHITE,
                    height: 40,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Courses',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            )),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => BlocProvider(
                      create: (BuildContext context) => RewardsBloc()
                        ..add(
                          LoadRewardsEvent(),
                        ),
                      child: RewardsPage(),
                    ),
                  );
                  Navigator.pushReplacement(context, route);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Stack(
                      children: [
                        Image.asset(
                          ASSET_REWARDS_WHITE,
                          height: 40,
                        ),
                        _buildCountBadge(count: rewardCount)
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Rewards',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  Route route = MaterialPageRoute(
                    builder: (BuildContext context) => BlocProvider(
                      create: (BuildContext context) => AccountBloc()
                        ..add(
                          LoadAccountEvent(),
                        ),
                      child: AccountPage(),
                    ),
                  );
                  Navigator.pushReplacement(context, route);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      ASSET_ACCOUNTS_WHITE,
                      height: 40,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Account',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        // Container(height: 10, color: colorOrange,)
      ],
    );
  }
}
