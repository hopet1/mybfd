import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youtube_plyr_iframe/youtube_plyr_iframe.dart';

class YoutubeApp extends StatefulWidget {
  final String videoId;
  YoutubeApp({@required this.videoId});
  @override
  _YoutubeAppState createState() => _YoutubeAppState();
}

class _YoutubeAppState extends State<YoutubeApp> {
  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    WidgetsFlutterBinding.ensureInitialized();
    _controller = YoutubePlayerController(
      initialVideoId: widget.videoId,
      params: YoutubePlayerParams(
          autoPlay: true,
          showControls: true,
          showFullscreenButton: true,
          desktopMode: false,
          privacyEnhanced: false,
          strictRelatedVideos: true),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return YoutubePlayerControllerProvider(
      // Passing controller to widgets below.
      controller: _controller,

      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          leading: BackButton(
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: Colors.black,
        ),
        backgroundColor: Colors.black,
        body: LayoutBuilder(
          builder: (context, constraints) {
            if (kIsWeb && constraints.maxWidth > 800) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Expanded(child: player),
                  // const SizedBox(
                  //   width: 500,
                  //   child: SingleChildScrollView(
                  //     child: Controls(),
                  //   ),
                  // ),
                ],
              );
            }
            return Center(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: player,
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.close();
    super.dispose();
  }
}
