import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class InfoWidget extends StatelessWidget {
  final String title;
  final Function onTap;

  const InfoWidget({
    Key key,
    @required this.title,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 18,
        ),
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(16),
            child: Center(
              child: Text(
                '$title',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: colorOrange,
                  fontFamily: 'Montserrat',
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
