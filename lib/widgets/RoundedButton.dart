import 'package:flutter/material.dart';

import '../constants.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function onTap;
  final Color foreground;
  final Color background;

  const RoundedButton({
    Key key,
    this.text,
    this.onTap,
    this.foreground,
    this.background,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      //minWidth: 144,
      height: 52,
      child: ElevatedButton(
        onPressed: onTap,
        style: ButtonStyle(
          foregroundColor:
              MaterialStateProperty.resolveWith((states) => foreground),
          backgroundColor:
              MaterialStateProperty.resolveWith((states) => background),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 10.0,
            vertical: 8.0,
          ),
          child: Text(
            text,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: background == Colors.white ? colorOrange : Colors.white,
              fontFamily: 'Montserrat',
            ),
          ),
        ),
      ),
    );
  }
}
