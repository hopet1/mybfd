import 'package:MyBFD/constants.dart';
import 'package:flutter/material.dart';

class CustomTotalBox extends StatefulWidget {
  CustomTotalBox({
    @required this.title,
  });

  final String title;

  @override
  State createState() => _CustomTotalBox(
        title: title,
      );
}

class _CustomTotalBox extends State<CustomTotalBox> {
  _CustomTotalBox({@required this.title});

  final String title;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth * 0.8,
      color: Colors.grey.shade300,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24.0),
          child: Column(
            children: [
              Text(
                '$title Total',
                style: TextStyle(
                    color: colorDark,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 12.0),
              Text(
                '\$3000',
                style: TextStyle(
                    color: colorDark,
                    fontSize: 36.0,
                    fontWeight: FontWeight.w200),
              ),
              SizedBox(height: 12.0),
              Text(
                'per month',
                style: TextStyle(
                  color: colorDark,
                  fontSize: 18.0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
