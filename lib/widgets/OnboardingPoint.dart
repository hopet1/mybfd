import 'package:flutter/material.dart';

class OnboardingPoint extends StatelessWidget {
  OnboardingPoint({
    Key key,
    @required this.assetPath,
    @required this.text,
  }) : super(key: key);

  final String assetPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(24, 12, 24, 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            assetPath,
            height: 30,
          ),
          SizedBox(
            width: 20,
          ),
          Flexible(
            child: Text(
              text,
              style: Theme.of(context).textTheme.headline4,
            ),
          )
        ],
      ),
    );
  }
}
