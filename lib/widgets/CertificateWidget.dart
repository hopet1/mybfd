import 'dart:io';
import 'dart:typed_data';

import 'package:file_saver/file_saver.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:native_pdf_view/native_pdf_view.dart' as np;
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:share/share.dart';

import '../constants.dart';

class CertificateWidget extends StatelessWidget {
  final pdf = pw.Document();

  final String name;
  final String module;
  BuildContext context;

  CertificateWidget({Key key, this.name, this.module}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    buildPage();
    context = context;
    return FutureBuilder<Widget>(
        future: buildPreview(),
        builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data;
          } else {
            return new CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor: new AlwaysStoppedAnimation<Color>(colorOrange),
            );
          }
        });
  }

  Future<Widget> buildPreview() async {
    Directory appDir;
    if (kIsWeb) {
      return Container(
        width: double.infinity,
        child: Center(
          child: Text(
            "Certificate will download shortly",
            style: TextStyle(color: colorOrange),
          ),
        ),
      );
    } else {
      appDir = await getApplicationDocumentsDirectory();
      final path = appDir.path + "/certificate.pdf";
      final data = await File(path).readAsBytes();
      final np.PdfDocument document = await np.PdfDocument.openData(data);
      final np.PdfPage page = await document.getPage(1);
      final np.PdfPageImage image = await page.render(
          height: 480, width: 640, backgroundColor: '#FFFFFF');
      final preview = Image.memory(image.bytes);
      return preview;
    }
  }

  static getFunction(int option) {
    if (option == 1) {
      return share;
    }
  }

  static void share() async {
    final appDir = await getApplicationDocumentsDirectory();
    final path = appDir.path + "/certificate.pdf";
    Share.shareFiles([path], text: "Certificate of Completion");
  }

  static void getMail() {
    print("Sent mail");
  }

  static void view() async {
    final appDir = await getApplicationDocumentsDirectory();
    final path = appDir.path + "/certificate.pdf";
    OpenFile.open(path);
    // final pdfController = np.PdfController(
    //   document: np.PdfDocument.openAsset(path),
    // );
    // Get.to(PdfPreviewPage(controller: pdfController));
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (getConte) => PdfPreviewPage(controller: pdfController),
    //   ),
    // );
  }

  // Future<Uint8List> _readFileByte(String filePath) async {
  //   Uri myUri = Uri.parse(filePath);
  //   File audioFile = new File.fromUri(myUri);
  //   Uint8List bytes;
  //   await audioFile.readAsBytes().then((value) {
  //     bytes = Uint8List.fromList(value);
  //     print('reading of bytes is completed');
  //   }).catchError((onError) {
  //     print('Exception Error while reading audio from path:' +
  //         onError.toString());
  //   });
  //   return bytes;
  // }

  buildPage() async {
    PdfPageFormat pdfFormat = PdfPageFormat.a4;
    final PdfColor color = PdfColor.fromHex("#ff6500");

    //Loading fonts
    final mFontData = await rootBundle.load('fonts/Montserrat-Regular.ttf');
    final montserrat = pw.Font.ttf(mFontData);
    final kFontData = await rootBundle.load('fonts/Kollektif-Bold.ttf');
    final kollektif = pw.Font.ttf(kFontData);

    // //Loading images
    final ByteData bgBytes = await rootBundle.load('assets/images/bg.png');
    final bgImage = pw.MemoryImage(bgBytes.buffer.asUint8List());
    final ByteData logoBytes = await rootBundle.load('assets/images/logo.png');
    final logoImage = pw.MemoryImage(logoBytes.buffer.asUint8List());
    final ByteData sigBytes =
        await rootBundle.load('assets/images/signature.png');
    final sigImage = pw.MemoryImage(sigBytes.buffer.asUint8List());

    final String line1 = "Certificate of COMPLETION".toUpperCase();
    final String line2 = "This is awarded to";
    final String line3 = name.toUpperCase();
    final String line4 = "for completion of";
    final String line5 = module.toUpperCase();
    final String line6 = "module";
    final String line7 = "ON BEHALF OF MYBFD";

    pdf.addPage(
      pw.Page(
        build: (context) => pw.Row(
          mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
          children: [
            pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.SizedBox(height: 32),

                //1st line - "Certificate of COMPLETION"
                pw.Text(
                  line1,
                  style: pw.TextStyle(
                    font: kollektif,
                    fontSize: 24,
                    color: color,
                  ),
                ),
                pw.SizedBox(height: 28),

                //2nd line - "This is awarded to"
                pw.Text(
                  line2,
                  style: pw.TextStyle(
                    font: montserrat,
                    fontSize: 12,
                    color: PdfColors.black,
                  ),
                ),
                pw.SizedBox(height: 18),

                //3rd line - var - name
                pw.Text(
                  line3,
                  style: pw.TextStyle(
                    font: kollektif,
                    fontSize: 48,
                    color: color,
                  ),
                ),
                pw.SizedBox(height: 24),

                //4th line - "for completion of"
                pw.Text(
                  line4,
                  style: pw.TextStyle(
                    font: montserrat,
                    fontSize: 12,
                    color: PdfColors.black,
                  ),
                ),
                pw.SizedBox(height: 24),

                //5th line - var - module
                pw.Text(
                  line5,
                  style: pw.TextStyle(
                    font: kollektif,
                    fontSize: 24,
                    color: color,
                  ),
                ),
                pw.SizedBox(height: 20),

                //6th line - "module"
                pw.Text(
                  line6,
                  style: pw.TextStyle(
                    font: montserrat,
                    fontSize: 12,
                    color: PdfColors.black,
                  ),
                ),

                //Signature and divider
                pw.SizedBox(height: 42),
                pw.Container(
                  height: 60,
                  width: 140,
                  //color: PdfColors.red,
                  child: pw.Image(sigImage),
                ),
                pw.Container(height: 2, width: 140, color: color),

                //Regards
                pw.SizedBox(height: 8),
                pw.Text(
                  line7,
                  style: pw.TextStyle(
                    font: kollektif,
                    fontSize: 12,
                    color: color,
                  ),
                ),

                pw.SizedBox(height: 12),
                pw.Container(
                  height: 48,
                  width: 48,
                  child: pw.Image(logoImage),
                ),
              ],
            ),
            pw.SizedBox(width: 96),
            pw.Container(
              width: 240,
              child: pw.Image(bgImage),
            ),
          ],
        ),
        pageTheme: pw.PageTheme(
          theme: pw.ThemeData.withFont(
            base: montserrat,
          ),
          pageFormat: pdfFormat.landscape,
          buildBackground: (context) => pw.FullPage(
            ignoreMargins: true,
            child: pw.Container(color: PdfColors.white),
          ),
          orientation: pw.PageOrientation.landscape,
        ),
      ),
    );

    // ignore: await_only_futures
    if (kIsWeb) {
      await FileSaver.instance.saveFile("certificate", await pdf.save(), "pdf");
    } else {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      String appDocPath = appDocDir.path;
      final path = "$appDocPath/certificate.pdf";
      final file = File(path);
      await file.writeAsBytes(await pdf.save());
    }
  }
}
