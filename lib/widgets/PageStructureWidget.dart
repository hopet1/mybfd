import 'package:MyBFD/constants.dart';
import 'package:MyBFD/widgets/Topbar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

//TODO: Come up with a better name for this widget later.

class PageStructureWidget extends StatelessWidget {
  final Widget topbar;
  final List<Widget> children;

  PageStructureWidget({
    this.topbar,
    @required this.children,
  });

  @override
  Widget build(BuildContext context) {
    if (topbar == null) {
      children.insert(
        0,
        Topbar(backArrowTap: () {
          Navigator.of(context).pop();
        }),
      );
    } else {
      children.insert(
        0,
        topbar,
      );
    }

    return SingleChildScrollView(
      //key: ObjectKey(Uuid().v1()),
      child: Column(children: children),
    );
  }
}
