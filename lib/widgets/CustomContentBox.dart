import 'package:MyBFD/constants.dart';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';

class CustomContentBox extends StatelessWidget {
  CustomContentBox({
    Key key,
    @required this.title,
    @required this.children,
  }) : super(key: key);

  final String title;
  final List<Widget> children;
  final ExpandableController _expandableController = ExpandableController();

  @override
  Widget build(BuildContext context) {
    _expandableController.expanded = true;
    return ExpandablePanel(
      controller: _expandableController,
      //hasIcon: false,
      header: Container(
        width: screenWidth * 0.8,
        color: colorOrange,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            '$title',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
      collapsed: SizedBox.shrink(),
      expanded: Container(
        // width: screenWidth * 0.8,
        color: Colors.grey.shade300,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: children,
          ),
        ),
      ),
    );
  }
}
