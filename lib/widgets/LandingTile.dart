import 'package:MyBFD/constants.dart';
import 'package:flutter/material.dart';

class LandingTile extends StatefulWidget {
  final Function onTap;
  final String imagePath;
  final String title;

  const LandingTile({
    Key key,
    @required this.onTap,
    @required this.imagePath,
    @required this.title,
  }) : super(key: key);

  @override
  _LandingTileState createState() => _LandingTileState();
}

class _LandingTileState extends State<LandingTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        height: 140,
        width: 120,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                offset: Offset(2, 2),
                blurRadius: 4.0,
              ),
            ]),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageIcon(
                AssetImage(widget.imagePath),
                color: colorOrange,
                size: 42,
              ),
              SizedBox(height: 18),
              Text(
                widget.title,
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 18,
                  color: colorOrange,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
