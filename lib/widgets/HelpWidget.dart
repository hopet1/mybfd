import 'package:flutter/material.dart';
import '../constants.dart';

class HelpWidget extends StatelessWidget {
  final String title;
  final bool hasChildren;
  final double screenWidth;
  final String content;
  final Function tap;

  const HelpWidget({
    Key key,
    this.title,
    this.hasChildren,
    this.screenWidth,
    this.content,
    this.tap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: tap,
          child: Container(
            width: screenWidth * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: Offset(2, 2),
                  blurRadius: 4.0,
                ),
              ],
            ),
            child: (hasChildren)
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 20.0,
                    ),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 18,
                        color: colorOrange,
                      ),
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 6.0,
                    ),
                    child: ExpansionTile(
                      tilePadding: EdgeInsets.all(2),
                      childrenPadding: EdgeInsets.all(4),
                      title: Text(
                        title,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: colorOrange,
                        ),
                      ),
                      children: [
                        Text(
                          content,
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: colorDark,
                          ),
                        ),
                        SizedBox(height: 8.0)
                      ],
                    ),
                  ),
          ),
        ),
        SizedBox(height: 18.0)
      ],
    );
  }
}
