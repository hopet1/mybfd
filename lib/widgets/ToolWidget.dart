import 'package:MyBFD/constants.dart';
import 'package:MyBFD/extensions/responsive.dart';
import 'package:flutter/material.dart';

class ToolWidget extends StatelessWidget {
  final Function onTap;
  final String title;
  const ToolWidget({
    Key key,
    this.onTap,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: Responsive.isMobile(context) ? 56.0 : 75,
        width: Responsive.isMobile(context)
            ? screenWidth * 0.8
            : screenWidth * 0.6,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              offset: Offset(2, 2),
              blurRadius: 4.0,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24.0,
            vertical: 18.0,
          ),
          child: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: Responsive.isMobile(context) ? 18 : 24,
              color: colorOrange,
            ),
          ),
        ),
      ),
    );
  }
}
