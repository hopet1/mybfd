import 'package:MyBFD/services/ActivityService.dart';
import 'package:MyBFD/services/AdminService.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/services/CourseService.dart';
import 'package:MyBFD/services/FCMNotificationService.dart';
import 'package:MyBFD/services/GoalService.dart';
import 'package:MyBFD/services/HelpService.dart';
import 'package:MyBFD/services/LessonService.dart';
import 'package:MyBFD/services/ModalService.dart';
import 'package:MyBFD/services/ModuleService.dart';
import 'package:MyBFD/services/NeedHelpService.dart';
import 'package:MyBFD/services/OnboardingService.dart';
import 'package:MyBFD/services/PrefService.dart';
import 'package:MyBFD/services/QuestionService.dart';
import 'package:MyBFD/services/RewardGenerationService.dart';
import 'package:MyBFD/services/RewardService.dart';
import 'package:MyBFD/services/ScoreService.dart';
import 'package:MyBFD/services/StorageService.dart';
import 'package:MyBFD/services/ToolsService.dart';
import 'package:MyBFD/services/UserService.dart';
import 'package:MyBFD/services/ValidationService.dart';
import 'package:MyBFD/services/WidgetService.dart';
import 'package:MyBFD/services/csv_services.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.I;

void setUpLocater() {
  locator.registerLazySingleton(() => ActivityService());
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => CourseService());
  locator.registerLazySingleton(() => FCMNotificationService());
  locator.registerLazySingleton(() => GoalService());
  locator.registerLazySingleton(() => HelpService());
  locator.registerLazySingleton(() => LessonService());
  locator.registerLazySingleton(() => ModalService());
  locator.registerLazySingleton(() => ModuleService());
  locator.registerLazySingleton(() => OnboardingService());
  locator.registerLazySingleton(() => QuestionService());
  locator.registerLazySingleton(() => RewardService());
  locator.registerLazySingleton(() => ScoreService());
  locator.registerLazySingleton(() => StorageService());
  locator.registerLazySingleton(() => ToolsService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => ValidationService());
  locator.registerLazySingleton(() => WidgetService());
  locator.registerLazySingleton(() => RewardGenerationService());
  locator.registerLazySingleton(() => NeedHelpService());
  locator.registerLazySingleton(() => PrefService());
  locator.registerLazySingleton(() => CSVServices());
  locator.registerLazySingleton(() => AdminService());
}
