import 'package:MyBFD/blocs/signin/signin_bloc.dart'
    as SIGN_IN_BP; //sign up block package.
import 'package:MyBFD/constants.dart';
import 'package:MyBFD/models/UserModel.dart';
import 'package:MyBFD/pages/LandingPage.dart';
import 'package:MyBFD/pages/onboarding/OBWelcomePage.dart';
import 'package:MyBFD/services/AdminService.dart';
import 'package:MyBFD/services/AuthService.dart';
import 'package:MyBFD/style/ThemeData.dart';
import 'package:MyBFD/widgets/Spinner.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'service_locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  prefs = await SharedPreferences.getInstance();
  print(prefs.getKeys());

  setUpLocater();

  if (kIsWeb) {
    packageInfo = PackageInfo(
      appName: WebVersionInfo.name,
      version: WebVersionInfo.version,
      buildNumber: WebVersionInfo.build,
      packageName: '',
    );
  } else {
    packageInfo = await PackageInfo.fromPlatform();
  }

  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );

  runApp(
    Phoenix(child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MyBFD',
      theme: themeData,
      home: StreamBuilder(
        stream: locator<AuthService>().onAuthStateChanged(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          final User firebaseUser = snapshot.data;
          screenWidth = MediaQuery.of(context).size.width;
          screenHeight = MediaQuery.of(context).size.height;

          if (firebaseUser == null) {
            return BlocProvider<SIGN_IN_BP.SigninBloc>(
              create: (BuildContext context) => SIGN_IN_BP.SigninBloc(),
              child: SIGN_IN_BP.SigninPage(),
            );
          }

          return FutureBuilder(
              future: setPrefs(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  final bool onboardingComplete =
                      snapshot.data.prefs["onBoardingComplete"];
                  if (onboardingComplete) {
                    return LandingPage();
                  } else {
                    return OBWelcomePage();
                  }
                } else {
                  return Scaffold(
                    body: Spinner(),
                  );
                }
              });
        },
      ),
    );
  }
}
