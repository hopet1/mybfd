// import 'package:MyBFD/blocs/signin/signin_bloc.dart';
// import 'package:MyBFD/services/AuthService.dart';
// import 'file:///Users/treyhope/Desktop/Development/mybfd/test/setup_firebase_auth_mocks.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:get_it/get_it.dart';
// import 'dart:async';
// import 'package:mockito/mockito.dart';
// import 'package:bloc_test/bloc_test.dart';
// import 'package:firebase_auth_platform_interface/src/method_channel/method_channel_firebase_auth.dart';

// class MockAuthService extends Mock implements AuthService {}

// class MockUserCredential extends Mock implements UserCredential {}

// void main() async {
//   setupFirebaseAuthMocks();

//   group('SigninBloc', () {
//     MockAuthService mockAuthService;
//     MockUserCredential mockUserCredential;
//     GetIt locator;
//     SigninBloc signinBloc;

//     setUpAll(() async {
//       await Firebase.initializeApp();

//       mockAuthService = MockAuthService();
//       locator = GetIt.I;
//       locator.registerLazySingleton(() => AuthService());

//       signinBloc = SigninBloc();

//       MethodChannelFirebaseAuth.channel.setMockMethodCallHandler((call) async {
//         switch (call.method) {
//           default:
//             // mockUserCredential = await FirebaseAuth.instance
//             //     .signInWithEmailAndPassword(email: 'test', password: 'test');
//             return <String, dynamic>{'user': null};
//         }
//       });
//     });

//     group('Signin', () {
//       blocTest(
//         'emits [SigninLoading, SigninSuccess]',
//         build: () {
//           when(
//             mockAuthService.signInWithEmailAndPassword(
//               email: 'test',
//               password: 'test',
//             ),
//           ).thenAnswer(
//             (_) => Future<UserCredential>.value(mockUserCredential),
//           );
//           return signinBloc;
//         },
//         act: (bloc) => bloc.add(
//           Signin(email: 'test', password: 'test'),
//         ),
//         expect: [
//           SigninLoading(),
//           SigninSuccess(),
//         ],
//       );

//       //todo: Get this test to work.
//       // blocTest(
//       //   'emits [SigninLoading, SigninFailure]',
//       //   build: () {
//       //     when(
//       //       mockAuthService.signInWithEmailAndPassword(
//       //         email: 'test',
//       //         password: 'test',
//       //       ),
//       //     ).thenThrow('Error');
//       //     return signinBloc;
//       //   },
//       //   act: (bloc) => bloc.add(
//       //     Signin(email: 'test', password: 'test'),
//       //   ),
//       //   expect: [
//       //     SigninLoading(),
//       //     SigninFailure(error: 'Error'),
//       //   ],
//       // );
//     });
//   });
// }
